﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMS.Core.Enums
{
    [Flags]
    public enum GatewayType
    {
        None = 0,
        Stripe = 1,
        Paypal = 2,
        CardConnect = 3

    }
}
