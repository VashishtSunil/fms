﻿using System.ComponentModel;

namespace FMS.Core.Enums
{
    public enum ApiHeader
    {
        [Description("Member")]
        Member = 0,
        [Description("Language")]
        Language = 1,
        [Description("Brand")]
        Brand = 2,
        [Description("Channel")]
        Channel = 3
    }
}
