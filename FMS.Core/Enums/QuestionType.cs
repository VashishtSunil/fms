﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMS.Core.Enums
{
    public enum QuestionType
    {
        [Description("Yes/No")]
        YN = 0,
        [Description("True/False")]
        TF =1 ,
        [Description("PickOne")]
        PickOne = 2,
        [Description("PickMany")]
        PickMany = 3
    }
}
