﻿namespace FMS.Core.Enums
{
    public enum SubscriptionPlanStatus
    {
        NotStarted = 0,
        Active = 1,
        Cancelled = 2
    }
}
