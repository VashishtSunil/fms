﻿using System.ComponentModel;

namespace FMS.Core.Enums
{
    public enum QuestionGroupType
    {
        [Description("Seeded Response")]
        Group0 = 0,
        [Description("Element")]
        Group1 = 1,
        [Description("Option Response")]
        Group2 = 2,
        [Description("Pieline Group")]
        Group3 = 3
    }
}
