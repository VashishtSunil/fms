﻿using System.ComponentModel;

namespace FMS.Core.Enums
{
    public enum StripeAccountType
    {
        [Description("custom")]
        Custom=0,
        [Description("express")]
        Express =1,
        [Description("standard")]
        Standard =2
    }
}
