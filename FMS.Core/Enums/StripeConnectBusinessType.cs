﻿using System.ComponentModel;

namespace FMS.Core.Enums
{
    public enum StripeConnectBusinessType
    {
        [Description("individual")]
        Individual =0,
        [Description("company")]
        Company =1,
        [Description("non_profit")]
        NonProfit =2,
        [Description("government_entity")]
        GovernmentEntity =3
    }
}
