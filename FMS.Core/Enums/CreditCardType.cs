﻿using System.ComponentModel;

namespace FMS.Core.Enums
{
    public enum CredirCardType
    {
        [Description("Visa")]
        Visa = 1,
        [Description("Mastercard")]
        Mastercard = 2,
        [Description("Discover")]
        Discover =3,
        [Description("American Express")]
        AmericanExpress =4
    }
}
