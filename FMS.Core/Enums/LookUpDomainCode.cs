﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMS.Core.Enums
{
    public enum LookUpDomainCode
    {
        [Description("SiteSetting")]
        SiteSetting = 1,
        [Description("QuestionType")]
        QuestionType = 2,
        [Description("Language")]
        Language = 3,
        [Description("FMS")]
        FMS = 4
    }
}
