﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMS.Core.Enums
{
    public enum StripeCustomerType
    {
        [Description("Customer and Employee")]
        Customer = 0,
        [Description("Owner")]
        Owner = 1
    }
}
