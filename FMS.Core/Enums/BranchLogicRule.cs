﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMS.Core.Enums
{
    public enum BranchLogicRule
    {
        [Description("And")]
        And=0,
        [Description("OR")]
        Or=1
    }
}
