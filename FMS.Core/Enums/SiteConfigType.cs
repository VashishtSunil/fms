﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMS.Core.Enums
{
    public enum SiteConfigType
    {
        StripeApiPrivateKey = 0,
        StripeApiPublicKey = 1,
        StripeClientId=2,
        StripeAccountType=3,
        SubscriptionFreeTimePeriod=4
    }
}
