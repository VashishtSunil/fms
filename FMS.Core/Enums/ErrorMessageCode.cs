﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMS.Core.Enums
{

    public enum ErrorMessageCode
    {
        [Description("Internal server error")]
        InternalServerError = 500,

        [Description("Device already exists")]
        DuplicateDevice = 2001,

        [Description("Device does not exist")]
        DeviceNotFound = 2002,

        [Description("Program not Found")]
        ProgramNotFound = 2005,

        [Description("Voucher not Found")]
        VoucherNotFound = 2006,

        [Description("Unexpected error occurred")]
        UnexpectedError = 2007,

        [Description("Incorrect Key")]
        IncorrectKey = 2008,

        [Description("Missing expected values in request payload")]
        InvalidModel = 2009,

        [Description("Missing value for action")]
        MissingAction = 2010,

        [Description("Invalid action")]
        InvalidAction = 2011,

        [Description("Device is required")]
        RequiredDevice = 2012,

        [Description("Please use valid clientId and clientKey")]
        InvalidClientIdAndSecretKey = 2013,

        [Description("This Email is already taken, Please enter new one")]
        DuplicateEmail = 2014,

        [Description("Pre-Tax is required")]
        RequiredPreTax = 2015,

        [Description("Member not Found")]
        MemberNotFound = 2016,

        [Description("Offer not Found")]
        OfferNotFound = 2017,

        [Description("NoContent Found")]
        NoContent = 2018

    }
}
