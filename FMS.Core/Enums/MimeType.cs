﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMS.Core.Enums
{
    public enum MimeType
    {
        None = 0,
        pdf = 1,
        doc = 2,
        excel = 3,
        excelOld = 28,
        zip = 4,
        txt = 6,
        xml = 7,
        ppt = 9,
        pub = 10,
        rtf = 11,
        odt = 12,
        tif = 13,
        eps = 14,
        psd = 15,
        gsheet = 16,
        gdraw = 17,
        jpg = 18,
        png = 19,
        bmp = 20,
        gdoc = 21,
        exe = 22,
        svg = 23,
        gif = 24,
        ai = 25,
        jpeg = 26,
        html = 27,
        pages = 29,
        dotx = 30,
        mht = 31,
        xlsm = 32,
        csv = 33,
        odp = 34,
        odf = 35,
        ods = 36,
        mp3 = 37,
        wmv = 38,
        ps = 39,
        wps = 40,
        xps = 41,
        gform = 42,
        vsd = 43,
        part = 44,
        numbers = 45,
        wmz = 46,
        mp4 = 47,
        docx = 48,
        xlsx = 49
    }

}
