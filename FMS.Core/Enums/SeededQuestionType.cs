﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMS.Core.Enums
{
    public enum SeededQuestionType
    {
        [Description("Gender")]
        Gender=0,
        [Description("FMS")]
        FMS =1,
        [Description("Language")]
        Language =2,
        [Description("Yes/No")]
        YesNo =3,
        [Description("Date/Time")]
        DateTime =4,
        [Description("Text")]
        Text =5,
        [Description("Paragraph")]
        Paragraph =6,
        [Description("Number")]
        Number =7
    }
}
