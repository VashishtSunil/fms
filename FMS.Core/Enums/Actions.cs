﻿using System.ComponentModel;

namespace FMS.Core.Enums
{
    public enum Actions
    {
        [Description("Update")]
        Update = 0,

        [Description("Delete")]
        Delete = 1,

        [Description("Get")]
        Get = 2,

        [Description("Getall")]
        Getall = 3,

        [Description("Authenticate")]
        Authenticate = 4,

        [Description("Register")]
        Register = 5,

        [Description("Reregister")]
        Reregister = 6,

        [Description("GetProgramFMS")]
        GetProgramFMS = 7,

        [Description("Get FMS Lesson")]
        GetFMSLesson = 8,

        [Description("Get FMS Quiz")]
        GetFMSQuiz = 9,

        [Description("Start Quiz")]
        StartQuiz = 10,

        [Description("LogIn")]
        Login = 11,

        [Description("List")]
        List = 12,

        [Description("View")]
        View = 13,

        [Description("Save")]
        Save = 14,

        [Description("LogOut")]
        Logout = 15,

        [Description("Burn")]
        Burn = 16,

        [Description("Pend")]
        Pend = 17,

        [Description("Reset")]
        Reset = 18,

        [Description("AddCustomer")]
        AddCustomer = 19,

        [Description("AddCard")]
        AddCard = 20,

        [Description("Charge")]
        Charge = 21,

        [Description("CheckOut")]
        CheckOut = 22,

        [Description("Refund")]
        Refund = 23
    }
}
