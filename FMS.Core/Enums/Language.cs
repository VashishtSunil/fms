﻿using System.ComponentModel;

namespace FMS.Core.Enums
{
    public enum Language
    {
        [Description("en")]
        English = 1,
        [Description("es")]
        Spanish = 2
    }
}
