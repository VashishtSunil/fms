﻿namespace FMS.Core.Enums
{
    public enum CurrencyType
    {
        INR=0,
        CAD = 1,
        USD = 2,
        GBP = 3
    }
}
