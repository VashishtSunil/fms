﻿using System.ComponentModel;

namespace FMS.Core.Enums
{
    public enum FileUploadingPurpose
    {
        [Description("additional_verification")]
        AdditionalVerification =0,
        [Description("business_icon")]
        BusinessIcon =1,
        [Description("business_logo")]
        BusinessLogo =2,
        [Description("customer_signature")]
        CustomerSignature =3,
        [Description("dispute_evidence")]
        DisputeEvidence =4,
        [Description("document_provider_identity_document")]
        DocumentProviderIdentityDocument =5,
        [Description("finance_report_run")]
        FinanceReportRun =6,
        [Description("identity_document")]
        IdentityDocument =7,
        [Description("incorporation_article")]
        IncorporationArticle =8,
        [Description("incorporation_document")]
        IncorporationDocument =9,
        [Description("payment_provider_transfer")]
        PaymentProviderTransfer =10,
        [Description("pci_document")]
        PciDocument =11,
        [Description("product_feed")]
        ProductFeed =12,
        [Description("sigma_scheduled_query")]
        SigmaScheduledQuery =13,
        [Description("tax_document_user_upload")]
        TaxDocumentUserUpload =14
    }
}
