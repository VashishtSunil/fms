﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMS.Core.Enums
{
    /// <summary>
    /// TypeLandingBallotInterstitialPopUp
    /// </summary>
    public enum PageType
    {
        [Description("Landingo")]
        Landing = 1,
        [Description("Ballot")]
        Ballot = 2 ,
        [Description("Interstitial")]
        Interstitial = 3,
        [Description("PopUp")]
        PopUp = 4
    }
}
