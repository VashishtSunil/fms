﻿using System.ComponentModel;

namespace FMS.Core.Enums
{
    public enum UserRole
    {

        [Description("Super Admin")]//Me + 
        SuperAdmin = 1,
        [Description("Trainer")]
        Trainer = 2,
        [Description("Trainee")]
        Trainee = 3,

        [Description("Admin")]// Gym Owner 
        Admin = 4,

    }
}
