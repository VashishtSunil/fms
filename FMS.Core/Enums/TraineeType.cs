﻿namespace FMS.Core.Enums
{
    public enum TraineeType
    {
        NormalTrainee = 1,
        PersonalTrainee = 2
    }
}
