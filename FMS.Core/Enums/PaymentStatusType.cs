﻿namespace FMS.Core.Enums
{
    public enum PaymentStatusType
    {
        Completed = 0,
        Pending = 1,
        Failed = 2
    }
}
