﻿using System.ComponentModel;

namespace FMS.Core.Enums
{
    public enum RequestResponse
    {
        [Description("Success")]
        Success = 0,

        [Description("Error Occurred")]
        Error = 1,

        [Description("Record Not Found ")]
        NotFound = 2
    }
}
