﻿namespace FMS.Core.Enums
{
    public enum PaymentGatewayType
    {
        Stripe = 0,
        Cash = 1
    }
}
