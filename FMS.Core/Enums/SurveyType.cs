﻿using System.ComponentModel;

namespace FMS.Core.Enums
{
    public enum SurveyType
    {
        [Description("Form")]
        Form = 1,
        [Description("One By One")]
        OneByOne = 2,
        [Description("Groups")]
        Groups = 3
    }
}
