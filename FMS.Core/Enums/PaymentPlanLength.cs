﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMS.Core.Enums
{
    public enum PaymentPlanLength
    {
        Yearly = 12,
        Every_Six_Months = 6,
        Every_Three_Months = 3,
        Monthly = 1

    }
}
