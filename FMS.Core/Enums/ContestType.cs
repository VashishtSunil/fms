﻿using System.ComponentModel;

namespace FMS.Core.Enums
{
    public enum FMSType
    {
        [Description("Pin and Win")]
        PinAndWin = 1,
        [Description("Survey and Win")]
        SurveyAndWin = 2,
        [Description("InteractiveGame")]
        InteractiveGame = 3
    }
}
