﻿namespace FMS.Core.Enums
{
    public enum PaymentIntegrationStatus
    {
        Connected=0,
        None = 1,
        Pending_PreApproval_From_Paypal = 2,
        PreApproval_Confirmed_Paypal = 3,
        Stripe_Bank_Account = 4,
        Stripe_Connect_Account = 5,
        CardConnect_Connected = 6,
        CardConnect_Disconnect = 7,
        CardConnect_Reconnect = 8
    }
}
