﻿namespace FMS.Core.Enums
{
    public enum UserType
    {
        Admin = 1,
        Trainer = 2,
        Trainee = 3
    }
}
