﻿using System.ComponentModel.DataAnnotations.Schema;

namespace FMS.Core.DataModels.ApiUser
{
    [Table("ApiUsers")]
   public class ApiUser : BaseEntity
    {
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
        public string ApiKey { get; set; }
        public string ClientKey { get; set; }

   
    }
}
