﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace FMS.Core.DataModels.ApiUser
{
    [Table("ApiTokens")]
    public class ApiToken : BaseEntity
    {

        //this is used for client for which token is been created
        public string ClientId { get; set; }
        public string TokenId { get; set; }
        public DateTime ValidUntil { get; set; }
       

    }
}
