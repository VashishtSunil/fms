﻿using System.ComponentModel.DataAnnotations.Schema;

namespace FMS.Core.DataModels
{
    [Table("RoleFunctions")]
    public class RoleFunction : BaseEntity
    {
        //mapping for role and function 
        public int FunctionId { get; set; }
        public int RoleId { get; set; }

        //claim prop
        public int ClaimId { get; set; }
        public string ClaimName { get; set; }

        //Action Flag
        public bool IsCreate { get; set; }
        public bool IsView { get; set; }
        public bool IsEdit { get; set; }
        public bool IsDelete { get; set; }

        #region ForeignKey
        [ForeignKey("FunctionId")]
        public Function Function { get; set; }

        [ForeignKey("RoleId")]
        public Role Role { get; set; }

        #endregion


    }
}
