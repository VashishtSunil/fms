﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMS.Core.DataModels.Attachment
{
    [Table("Attachments")]
    public class Attachment : BaseEntity
    {
        public string Location { get; set; }
        public string FileName { get; set; }
        public string DummyFileName { get; set; }
        public int Size { get; set; }
        public int MIME { get; set; }
        public string ContentType { get; set; }
        public byte[] ImageBytes { get; set; }
    }
}
