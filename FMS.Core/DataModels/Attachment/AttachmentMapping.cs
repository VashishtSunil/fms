﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMS.Core.DataModels.Attachment
{
    [Table("AttachmentMapping")]
    public class AttachmentMapping : BaseEntity
    {
        public int AttachmentId { get; set; }
        public int UploaderId { get; set; }
        public int UplaoderType { get; set; }

        #region ForeignKey
        [ForeignKey("AttachmentId")]
        public Attachment Attachment { get; set; }
        #endregion
    }
}
