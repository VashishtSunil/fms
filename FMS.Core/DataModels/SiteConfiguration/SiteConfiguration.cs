﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FMS.Core.DataModels.SiteConfiguration
{
    [Table("SiteConfigurations")]
    public class SiteConfiguration:BaseEntity
    {
        [Required]
        public string Key { get; set; }
        [Required]
        public string Value { get; set; }

        public string Description { get; set; }
    }
}
