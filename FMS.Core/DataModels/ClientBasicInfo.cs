﻿using System.ComponentModel.DataAnnotations.Schema;

namespace FMS.Core.DataModels
{
    [Table("ClientBasicInfo")]
    public class ClientBasicInfo : BaseEntity
    {
        #region Properties
        public int ClientId { get; set; }
        public string Heading { get; set; }
        public string SubHeading { get; set; }
        public int LogoId { get; set; }
        public int? OwnerId { get; set; }
        public int SitePortalOwnerId { get; set; }
        public string FacebookUrl { get; set; }
        public string InstagramUrl { get; set; }
        public string TwitterUrl { get; set; }

        #endregion

        #region ForeignKey
        [ForeignKey("ClientId")]
        public User User { get; set; }

        [ForeignKey("LogoId")]
        public Attachment.Attachment Logo { get; set; }

        #endregion
    }
}
