﻿using System.ComponentModel.DataAnnotations.Schema;

namespace FMS.Core.DataModels
{
    [Table("Roles")]
    public class Role : BaseEntity
    {
        
        public string RoleName { get; set; }
        //public bool? IsDeleted { get; set; }
        public bool? IsSeeded { get; set; }

        //from which Role it's been derived 
        public int? ParentId { get; set; }//admin==>Admin1(Admin)==>Admin2(Admin1)
        //ReadOnly/Read Write
        public int? AccessLevel { get; set; }
        public int? OwnerId { get; set; }

        //categoryId will be used for tracking the subCategoryRole
        public int? CategoryId { get; set; }
    }
}
