﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using FMS.Core.DataModels.Attachment;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMS.Core.DataModels
{
    [Table("News")]
    public class News : BaseEntity
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime PublishDate { get; set; }
        public int AttachmentId { get; set; }

        public int? OwnerId { get; set; } // creted by 
        public int SitePortalOwnerId { get; set; } // i.e admin Id 

        [ForeignKey("AttachmentId")]
        public Core.DataModels.Attachment.Attachment Attachment { get; set; }

    }
}
