﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace FMS.Core.DataModels.Subscription
{
    [Table("UserSubscriptions")]
    public class UserSubscription : BaseEntity
    {
        #region Properties
        public int UserId { get; set; }
        public int PlanId { get; set; }

        /// <summary>
        /// Not_Started, Active, Canceled
        /// </summary>
        public int PlanStatus { get; set; }
        public decimal PricePaid { get; set; }
        public string Description { get; set; }
        public DateTime PlanExpirationDateTime { get; set; }
        public bool IsFreeTrailPeriod { get; set; }
        public bool IsTrailPeriodWarninEmailSent { get; set; }
        public bool IsCardDetailExpiredEmailSent { get; set; }
        public string ExpiredCardId { get; set; }
        public bool IsPaymentPending { get; set; }//for tracking pending payments for the subscription plan

        #region subscription using payment gateway
        public string GatewayCustomerId { get; set; }
        public string GatewaySubscriptionId { get; set; }
        #endregion
        
        public string ReasonForUnsubscription { get; set; }
        public bool IsPlanAutoRenewal { get; set; }
        public bool IsCardAddedAfterTrailExpired { get; set; }
        public DateTime? ExtendedPeriodDateTime { get; set; }
        public int NumberOfRetry { get; set; }
        #endregion

        #region ForeignKey
        [ForeignKey("PlanId")]
        public Plan.Plan Plan { get; set; }

        [ForeignKey("UserId")]
        public User User { get; set; }
        #endregion
    }
}
