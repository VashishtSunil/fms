﻿
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace FMS.Core.DataModels
{   
    [Table("RequestResponses")]
    public class RequestResponse : BaseEntity
    {
        public string Token { get; set; }
        public string UniqId { get; set; }
        public string Method { get; set; }
        public string Request { get; set; }
        public DateTime? RequestDate { get; set; }
        public string Response { get; set; }
        public DateTime? ResponseDate { get; set; }
        public string ResponseCode { get; set; }
       
    }
}
