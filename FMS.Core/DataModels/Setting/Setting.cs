﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMS.Core.DataModels.Setting
{
    [Table("SiteSettings")]
    public class SiteSetting : BaseEntity
    {        
        public string Key { get; set; }
        public string Value { get; set; }
        public string Value2 { get; set; }
        public string Description { get; set; }        
        public long ControlType { get; set; }
        public int OwnerId { get; set; }
        public int SitePortalOwnerId { get; set; }
    }
}
