﻿using System.ComponentModel.DataAnnotations.Schema;

namespace FMS.Core.DataModels.Stripe
{
    [Table("StripeCustomerCardDetails")]
    public class StripeCustomerCardDetail : BaseEntity
    {
        #region Properties
        public string StripeCardId { get; set; }
        /// <summary>
        /// foreign key w.r.t StripeCustomer table
        /// </summary>
        public int StripeCustomerId { get; set; }

        /// <summary>
        /// saved masked account number
        /// </summary>
        public string AccountNumber { get; set; }
        public string Cvc { get; set; }
        public int ExpirationMonth { get; set; }
        public int ExpirationYear { get; set; }
        public string Last4 { get; set; }
        /// <summary>
        /// token will be generated either using stripe.js or token stripe api
        /// </summary>
        public string CardToken { get; set; }
        public string CardType { get; set; }
        public bool CvcCheck { get; set; }
        public string Fingerprint { get; set; }

        public string JsonRequest { get; set; }
        public string JsonResponse { get; set; }
        #endregion

        #region ForeignKey
        [ForeignKey("StripeCustomerId")]
        public StripeCustomer StripeCustomer { get; set; }
        #endregion
    }
}
