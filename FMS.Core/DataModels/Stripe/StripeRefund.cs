﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace FMS.Core.DataModels.Stripe
{
    [Table("StripeRefunds")]
    public class StripeRefund : BaseEntity
    {
        #region Properties
        public string StripeRefundId { get; set; }
        public int StripeChargeId { get; set; }

        /// <summary>
        /// it will be send while doing charges using api.
        /// it's value depend upon country
        /// </summary>
        public int Currency { get; set; }
        public string Description { get; set; }

        /// <summary>
        /// as it is not necessary to send the application fee that is why it is null-able
        /// </summary>
        public int? FeeInCents { get; set; }
        public int PaidAmountInCents { get; set; }
        public decimal Amount { get; set; }
        public bool LiveMode { get; set; }
        public bool IsPaid { get; set; }

        public string TransactionId { get; set; }
        public string FailureCode { get; set; }
        public string FailureMessage { get; set; }
        public DateTime RequestDateTime { get; set; }
        public string JsonRequest { get; set; }
        public string JsonResponse { get; set; }
        #endregion

        #region ForeignKey
        [ForeignKey("StripeChargeId")]
        public virtual StripeCharge Charge { get; set; }
        #endregion
    }
}
