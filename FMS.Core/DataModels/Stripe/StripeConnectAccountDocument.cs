﻿using System.ComponentModel.DataAnnotations.Schema;

namespace FMS.Core.DataModels.Stripe
{
    [Table("StripeConnectAccountDocuments")]
    public class StripeConnectAccountDocument:BaseEntity
    {
        public int StripeConnectAccountId { get; set; }
        public string StripeFileId { get; set; }
        public int AttachmentId { get; set; }

        public bool IsFrontSide { get; set; }
        public bool IsBackSide { get; set; }
        public int DocumentSize { get; set; }
        public int DocumentPurpose { get; set; }//like we are uploading document for identity verification or for company verification
        public string JsonRequest { get; set; }
        public string JsonResponse { get; set; }

        #region foreign key
        [ForeignKey("StripeConnectAccountId")]
        public virtual StripeConnectAccount ConnectAccount { get; set; }

        [ForeignKey("AttachmentId")]
        public virtual Attachment.Attachment Document { get; set; }
        #endregion
    }
}
