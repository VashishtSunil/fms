﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace FMS.Core.DataModels.Stripe
{
    [Table("StripeCharges")]
    public class StripeCharge : BaseEntity
    {
        #region Properties
        public string StripeChargeId { get; set; }
        public int? StripeConnectAccountId { get; set; }
        public int StripeCardId { get; set; }

        /// <summary>
        /// it will be send while doing charges using api.
        /// it's value depend upon country
        /// </summary>
        public int Currency { get; set; }
        public string Description { get; set; }

        /// <summary>
        /// as it is not necessary to send the application fee that is why it is null-able
        /// </summary>
        public int? FeeInCents { get; set; }
        public int PaidAmountInCents { get; set; }
        public decimal Amount { get; set; }
        public decimal ApplicationFeeAmount { get; set; }
        public int ApplicationFeeAmountInCents { get; set; }
        public bool LiveMode { get; set; }
        public bool IsPaid { get; set; }

        public bool IsFullPaymentDone { get; set; }
        public bool IsAmountRefunded { get; set; }
        public decimal RefundedAmount { get; set; }

        public string TransactionId { get; set; }
        public string FailureCode { get; set; }
        public string FailureMessage { get; set; }
        public int PaymentStatus { get; set; }
        public DateTime RequestDateTime { get; set; }
        public string JsonRequest { get; set; }
        public string JsonResponse { get; set; }
        #endregion

        #region ForeignKey
        [ForeignKey("StripeCardId")]
        public virtual StripeCustomerCardDetail CardDetail { get; set; }

        [ForeignKey("StripeConnectAccountId")]
        public virtual StripeConnectAccount ConnectAccount { get; set; }
        #endregion
    }
}
