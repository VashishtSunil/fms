﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace FMS.Core.DataModels.Stripe
{
    [Table("StripeConnectAccounts")]
    public class StripeConnectAccount:BaseEntity
    {
        public int UserId { get; set; }
        public int UserType { get; set; }//define role basically
        public string ConnectAccountId { get; set; }
        public string PersonId { get; set; }
        
        public string BankName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime? DOB { get; set; }
        public string Country { get; set; }
        public string Currency { get; set; }
        public int AccountType { get; set; }
        public int BusinessType { get; set; }//like individual,company,non-profit and government_entity
        public bool IsChargeEnabled { get; set; }
        public bool IsPayoutEnabled { get; set; }
        public string VerificationStatus { get; set; }
        public bool IsAuthorized { get; set; }

        public DateTime StripeCreatedDate { get; set; }
        public string JsonRequest { get; set; }
        public string JsonResponse { get; set; }
        public string PersonJsonRequest { get; set; }
        public string PersonJsonResponse { get; set; }

        #region foreign key
        [ForeignKey("UserId")]
        public virtual User User { get; set; }
        #endregion
    }
}
