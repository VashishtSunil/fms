﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace FMS.Core.DataModels.Stripe
{
    [Table("StripeCustomers")]
    public class StripeCustomer:BaseEntity
    {
        #region Properties
        public int UserId { get; set; }
        public string StripeCustomerId { get; set; }

        public string UserName { get; set; }
        public string UserEmail { get; set; }

        public string Description { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string PostalCode { get; set; }
        public string PhoneNumber { get; set; }

        public bool LiveMode { get; set; }
        public DateTime StripeCreatedDateTime { get; set; }
        public string JsonRequest { get; set; }
        public string JsonResponse { get; set; }
        #endregion

        #region foreign key
        [ForeignKey("UserId")]
        public virtual User User { get; set; }
        #endregion
    }
}
