﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMS.Core.DataModels
{
    [Table("Countries")]
    public class Country : BaseEntity
    {


        #region Properties
        public string Name { get; set; }
        public string Code { get; set; }
        public virtual List<State> States { get; set; }
        #endregion


    }
}
