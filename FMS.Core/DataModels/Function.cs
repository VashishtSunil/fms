﻿using System.ComponentModel.DataAnnotations.Schema;

namespace FMS.Core.DataModels
{
    [Table("Functions")]
    public class Function : BaseEntity
    {
      
        //Can not changed 
        public string Key { get; set; }
        public string Title { get; set; }
        public string Icon { get; set; }
        public string Url { get; set; }
        public int DisplayOrder { get; set; }
    }

}
