﻿using System.ComponentModel.DataAnnotations.Schema;

namespace FMS.Core.DataModels
{


    [Table("States")]
    public class State : BaseEntity
    {
        #region Properties
        public string Name { get; set; }
        public string Code { get; set; }
        public int CountryId { get; set; }
        #endregion

        #region ForeignKey
        [ForeignKey("CountryId")]
        public Country Country { get; set; }
        #endregion
    }
}
