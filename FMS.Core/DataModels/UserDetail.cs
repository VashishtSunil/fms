﻿using System.ComponentModel.DataAnnotations.Schema;

namespace FMS.Core.DataModels
{
    [Table("UserDetails")]
    public class UserDetail : BaseEntity
    {
        #region Properties
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string PinCode { get; set; }
        public string ProfilePic { get; set; }
        public int UserId { get; set; }
        public int? ProfilePicId { get; set; }
        #endregion

        #region ForeignKey
        [ForeignKey("UserId")]
        public User User { get; set; }

        [ForeignKey("ProfilePicId")]
        public Attachment.Attachment Profile { get; set; }
        #endregion
    }
}
