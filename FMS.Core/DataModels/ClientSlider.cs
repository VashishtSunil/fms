﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMS.Core.DataModels
{
    [Table("ClientSliders")]
    public class ClientSlider : BaseEntity
    {
        #region Properties
        public int ClientId { get; set; }
        public string Heading { get; set; }
        public string SubHeading { get; set; }
        public int LogoId { get; set; }
        public bool IsButtonDisplayed { get; set; }
        public string ButtonText { get; set; }
        public string ButtonLink { get; set; }
        public int? OwnerId { get; set; }
        public int SitePortalOwnerId { get; set; }
        #endregion

        #region ForeignKey
        [ForeignKey("ClientId")]
        public User User { get; set; }

        [ForeignKey("LogoId")]
        public Attachment.Attachment Logo { get; set; }

        #endregion
    }
}
