﻿using FMS.Core.DataModels.Subscription;
using System.ComponentModel.DataAnnotations.Schema;

namespace FMS.Core.DataModels.Payment
{
    [Table("PaymentHistory")]
    public class PaymentHistory : BaseEntity
    {
        #region properties
        public int UserId { get; set; }
        public string GatewayCustomerId { get; set; }
        public string GatewayCardId { get; set; }
        public string GatewayChargeId { get; set; }

        public decimal PaidPrice { get; set; }
        /// <summary>
        /// as stripe accept payment in cents
        /// </summary>
        public long PaidPriceInCent { get; set; }
        /// <summary>
        /// like stripe and other payment gateway
        /// </summary>
        public int Gateway { get; set; }
        
        /// <summary>
        /// pending,done, not completed
        /// </summary>
        public int PaymentStatus { get; set; }

        public int PaymentMode { get; set; }
        public string Description { get; set; }

        /// <summary>
        /// payment can be done for active subscription or for other things
        /// </summary>
        public int? SubscriptionId { get; set; }
        #endregion

        #region foreign key 
        [ForeignKey("UserId")]
        public User User { get; set; }

        [ForeignKey("SubscriptionId")]
        public UserSubscription Subscription { get; set; }
        #endregion
    }
}
