﻿using FMS.Core.DataModels.Subscription;
using System.ComponentModel.DataAnnotations.Schema;

namespace FMS.Core.DataModels.Payment
{
    [Table("RefundHistory")]
    public class RefundHistory:BaseEntity
    {
        #region properties
        public int UserId { get; set; }
        public string GatewayCustomerId { get; set; }
        public string GatewayCardId { get; set; }
        public string GatewayChargeId { get; set; }
        public string GatewayRefundId { get; set; }

        public int Gateway { get; set; }
        public decimal PaidPrice { get; set; }
        /// <summary>
        /// as stripe accept payment in cents
        /// </summary>
        public long PaidPriceInCent { get; set; }
        /// <summary>
        /// like stripe and other payment gateway
        /// </summary>

        public int? SubscriptionId { get; set; }
        /// <summary>
        /// pending,done, not completed
        /// </summary>
        public int PaymentStatus { get; set; }
        public string Description { get; set; } //reason for refund
        public string JsonResponse { get; set; }
        #endregion

        #region foreign key
        [ForeignKey("SubscriptionId")]
        public UserSubscription Subscription { get; set; }

        [ForeignKey("UserId")]
        public User User { get; set; }
        #endregion
    }
}
