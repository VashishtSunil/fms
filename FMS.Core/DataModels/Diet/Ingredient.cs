﻿using System.ComponentModel.DataAnnotations.Schema;

namespace FMS.Core.DataModels.Diet
{
    [Table("Ingredients")]
    public class Ingredient:BaseEntity
    {        
        public string Name { get; set; }
        public decimal Calorie { get; set; }
        public decimal Fats { get; set; }
        public decimal Carbs { get; set; }
        public int Units { get; set; }
        public decimal Protein { get; set; }
        public decimal Fiber { get; set; }
        public decimal Sugar { get; set; }
    }
}
