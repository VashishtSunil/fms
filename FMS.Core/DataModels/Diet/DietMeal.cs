﻿using System.ComponentModel.DataAnnotations.Schema;

namespace FMS.Core.DataModels.Diet
{
    [Table("DietMeal")]
    public class DietMeal : BaseEntity
    {
        #region prop
        public int DietId { get; set; }
        public string MealName { get; set; }
        #endregion

        #region foreign key 

        [ForeignKey("DietId")]
        public Diet Diet { get; set; }
        
        #endregion
    }
}
