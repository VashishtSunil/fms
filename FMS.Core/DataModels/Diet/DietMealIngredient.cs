﻿using System.ComponentModel.DataAnnotations.Schema;

namespace FMS.Core.DataModels.Diet
{
    [Table("DietMealIngredient")]
    public class DietMealIngredient : BaseEntity 
    {
        #region prop
        public int MealId { get; set; }
        public int IngredientId { get; set; }
        public decimal Unit { get; set; }
        #endregion

        #region foreign key 
        [ForeignKey("MealId")]
        public DietMeal DietPlanMeal { get; set; }

        [ForeignKey("IngredientId")]
        public Ingredient Ingredient { get; set; }
        #endregion
    }
}
