﻿using System.ComponentModel.DataAnnotations.Schema;

namespace FMS.Core.DataModels.Diet
{
    [Table("Diet")]
    public class Diet : BaseEntity
    {
        #region prop
        public int UserId { get; set; }
        public string DietName { get; set; }
        #endregion

        #region foreign key 
        [ForeignKey("UserId")]
        public User User { get; set; }
        #endregion
    }
}
