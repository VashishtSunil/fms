﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace FMS.Core.DataModels.Plan
{
    [Table("Plans")]
    public class Plan : BaseEntity
    {
        #region Properties
        public string Name { get; set; }
        public decimal Price { get; set; }
        public string Description { get; set; }
        public bool IsDisplayOnPublicPage { get; set; }

        #region MyRegion
        public int PlanLengthInDays { get; set; }
        #endregion

        /// <summary>
        /// this will be the total price=plan price+ features prices
        /// </summary>
        public decimal TotalPrice { get; set; }
        public int? OwnerId { get; set; }
        public bool? IsSeeded { get; set; } //plan that will be used for identifying basic/pro client
        public int SitePortalOwnerId { get; set; }
        public virtual List<PlanFeature> PlanFeatures { get; set; }
        #endregion
    }
}
