﻿using System.ComponentModel.DataAnnotations.Schema;

namespace FMS.Core.DataModels.Plan
{
    [Table("PlanFeatures")]
    public class PlanFeature : BaseEntity
    {
        #region Properties
        public int PlanId { get; set; }
        public string Feature { get; set; }
        public int SitePortalOwnerId { get; set; }
        public int? OwnerId { get; set; }
        #endregion

        #region ForeignKey
        [ForeignKey("PlanId")]
        public Plan Plan { get; set; }

       // [ForeignKey("FeatureId")]
       // public DataModels.Feature.Feature Feature { get; set; }
        #endregion
    }
}
