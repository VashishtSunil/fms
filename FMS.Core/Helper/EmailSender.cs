﻿using System;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using FMS.Core.Models;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace FMS.Core.Helper
{
    public class EmailModel
    {
        public string FromEmail { get; set; }
        public string From { get; set; }
        public string ToEmail { get; set; }
        public string CcEmail { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
    }
    public class EmailSender
    {
        /// <summary>
        /// Send Email
        /// </summary>
        /// <param name="to"></param>
        /// <param name="subject"></param>
        /// <param name="htmlContent"></param>
        /// <returns></returns>
        public static async Task SendEmailAsync(string to,string subject,string htmlContent)
        {
            try
            {
                EmailServerSettingModel emailSetting = new EmailServerSettingModel
                {
                    FromEmail = "testdevsunil@gmail.com",
                    PortNo = "587",
                    IsSSLEnable = true,
                    SMTPAddress = "smtp.gmail.com",
                    Password = "shwscmdcfwqxgdiv"
                };
                var portNumber = Convert.ToInt32(emailSetting.PortNo);
                var enableSSL = Convert.ToBoolean(emailSetting.IsSSLEnable);
                var smtpAddress = Convert.ToString(emailSetting.SMTPAddress);
                var FromMailAddress = emailSetting.FromEmail.ToString();
                var password = emailSetting.Password;
                var client = new SmtpClient(smtpAddress, portNumber) //Port 8025, 587 and 25 can also be used.
                {
                    Credentials = new NetworkCredential(FromMailAddress, password),
                };
                client.UseDefaultCredentials = false;
                MailMessage _mailMessage = new MailMessage();
                _mailMessage.To.Add(new MailAddress(to));
                _mailMessage.From = new MailAddress(FromMailAddress, FromMailAddress);
                _mailMessage.Subject = subject;
                _mailMessage.IsBodyHtml = true;            
                AlternateView htmlView = AlternateView.CreateAlternateViewFromString(htmlContent, null, "text/html");
                _mailMessage.AlternateViews.Add(htmlView);
                using (SmtpClient smtp = new SmtpClient(smtpAddress, portNumber))
                {
                    smtp.Credentials = new NetworkCredential(FromMailAddress, password);
                    smtp.EnableSsl = enableSSL;
                    smtp.Send(_mailMessage);
                }
                //FOR THIS ERROR : The SMTP server requires a secure connection or the client was not authenticated. The server response was: 5.7.0 Authentication Required. Learn more at
                // Need to change your password and use App password insted of your gamil password
            }
            catch (Exception ex)
            {
                //RollbarLocator.RollbarInstance.Error(ex.Message);
                throw ex;
            }
        }

        /// <summary>
        /// send email by smtp
        /// </summary>
        /// <param name="emailModel"></param>
        /// <param name="LeadName"></param>
        /// <param name="ClientId"></param>
        /// <returns></returns>
        public static async Task SendMail(EmailModel emailModel, string LeadName, int ClientId)
        {
            try
            {               
                var fname = emailModel.From == "" ? "FMS Matters" : emailModel.From;
                var apiKey = "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";
                var client = new SendGridClient(apiKey);
                var from = new EmailAddress(emailModel.From, fname);
                var to = new EmailAddress(emailModel.ToEmail, LeadName);
                var plainTextContent = emailModel.Body;
                var htmlContent = emailModel.Body;
                var msg = MailHelper.CreateSingleEmail(from, to, emailModel.Subject, plainTextContent, htmlContent);
                var response = await client.SendEmailAsync(msg);
                if (response.StatusCode == HttpStatusCode.Accepted)
                {
                    //success
                }               
            }
            catch (Exception)
            {
            }
        }
    }
}
