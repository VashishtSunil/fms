﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
public static class TypeCast
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="val"></param>
    /// <returns></returns>
    public static DateTime? ToUniDateTime(this DateTime? val)
    {
        DateTime? alt = null;
        try
        {
            return val.Value.ToUniversalTime();
        }
        catch (Exception ex)
        {
            return alt;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="str"></param>
    /// <param name="StartString"></param>
    /// <param name="EndString"></param>
    /// <returns></returns>
    public static string Substring(this string str, string StartString, string EndString)
    {
        if (str.Contains(StartString))
        {
            int iStart = str.IndexOf(StartString) + StartString.Length;
            int iEnd = str.IndexOf(EndString, iStart);
            return str.Substring(iStart, (iEnd - iStart));
        }
        return string.Empty;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="val"></param>
    /// <param name="alt"></param>
    /// <returns></returns>
    public static T ToType<T>(this object val, T alt = default(T)) where T : struct, IConvertible
    {
        try
        {
            if (val == null) return alt;
            if (val is DBNull) return alt;
            return (T)Convert.ChangeType(val, typeof(T));
        }
        catch
        {
            return alt;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="val"></param>
    /// <param name="alt"></param>
    /// <returns></returns>
    public static T? ToTypeOrNull<T>(this object val, T? alt = null) where T : struct, IConvertible
    {
        try
        {
            if (val != null && !(val is DBNull))
            {
                return (T)Convert.ChangeType(val, typeof(T));
            }
            return null;
        }
        catch
        {
            return alt;
        }
    }
    /// <summary>
    /// used to get Enum From String
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="value"></param>
    /// <returns></returns>
    public static T ToEnum<T>(this object value)
    {
        return (T)Enum.Parse(typeof(T), value.ToString(), true);
    }
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="value"></param>
    /// <returns></returns>
    public static string ToEnumString<T>(this object value)
    {
        value = (T)Enum.Parse(typeof(T), value.ToString(), true);
        var fieldInfo = value.GetType().GetField(value.ToString());
        var descriptionAttributes = fieldInfo.GetCustomAttributes(
            typeof(DisplayAttribute), false) as DisplayAttribute[];
        if (descriptionAttributes == null) return string.Empty;
        return (descriptionAttributes.Length > 0) ? descriptionAttributes[0].Name : value.ToString();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="toCheck"></param>
    /// <param name="comp"></param>
    /// <returns></returns>
    public static bool Contains(this string source, string toCheck, StringComparison comp)
    {
        return source.IndexOf(toCheck, comp) >= 0;
    }

    /// <summary>
    /// comma separator
    /// </summary>
    /// <param name="commaSeperatedString"></param>
    /// <returns></returns>
    public static int[] StringToIntArray(this string commaSeperatedString)
    {
        List<int> myIntegers = new List<int>();
        Array.ForEach(commaSeperatedString.Split(",".ToCharArray()), s =>
        {
            int currentInt;
            if (Int32.TryParse(s, out currentInt))
                myIntegers.Add(currentInt);
        });
        return myIntegers.ToArray();
    }


    /// <summary>
    /// comma separator
    /// </summary>
    /// <param name="commaSeperatedString"></param>
    /// <returns></returns>
    public static int?[] StringToNullIntArray(this string commaSeperatedString)
    {
        List<int?> myIntegers = new List<int?>();
        Array.ForEach(commaSeperatedString.Split(",".ToCharArray()), s =>
        {
            int currentInt;
            if (Int32.TryParse(s, out currentInt))
                myIntegers.Add(currentInt);
        });
        return myIntegers.ToArray();
    }


    /// <summary>
    /// Array TO String (delimited)
    /// </summary>
    /// <param name="commaSeperatedString"></param>
    /// <returns></returns>
    public static string IntArrayToString(this int?[] IntArray, char delimeter = ',')
    {
        string _return = "";
        foreach (var item in IntArray)
        {
            _return += item.ToString() + delimeter;
        }

        if (_return.Length > 0 && _return.LastIndexOf(',') == _return.Length - 1) // there is an extra delimeter at the end
        {
            _return = _return.Substring(0, _return.Length - 1);
        }
        return _return;
    }

    /// <summary>
    /// Array TO String (delimeted)
    /// </summary>
    /// <param name="commaSeperatedString"></param>
    /// <returns></returns>
    public static string ToDelimetedString(this List<string> ValueList, char delimeter = ' ', bool InsertBreak = false)
    {
        string _return = "";
        foreach (var item in ValueList)
        {
            _return += item.ToString() + delimeter;
            if (InsertBreak)
                _return += "<br />";
        }

        if (_return.Length > 0 && _return.LastIndexOf(',') == _return.Length - 1) // there is an extra delimeter at the end
        {
            _return = _return.Substring(0, _return.Length - 1);
        }
        return _return;
    }

    /// <summary>
    /// | item 1 | item 4 | item 6 | item 8 |
    ///| item 2 | item 5 | item 7 | item 9 |
    ///| item 3 |        |        |        |
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="source"></param>
    /// <param name="parts"></param>
    /// <returns></returns>
    public static IEnumerable<IEnumerable<T>> SplitListVertically<T>(this IEnumerable<T> source, int parts)
    {
        var list = new List<T>(source);
        int defaultSize = (int)(list.Count / (double)parts);
        int offset = list.Count % parts;
        int position = 0;

        for (int i = 0; i < parts; i++)
        {
            int size = defaultSize;
            if (i < offset)
                size++; // Just add one to the size (it's enough).

            yield return list.GetRange(position, size);

            // Set the new position after creating a part list, so that it always start with position zero on the first yield return above.
            position += size;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="filePath"></param>
    /// <param name="pobject"></param>
    public static void SerializeXml<T>(string filePath, object pobject)
    {
        StreamWriter objStreamWriter = new StreamWriter(filePath);
        XmlSerializer serializer = new XmlSerializer(typeof(T));
        serializer.Serialize(objStreamWriter, pobject);
        objStreamWriter.Close();
        serializer = null;
        objStreamWriter.Dispose();
        objStreamWriter = null;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="xmlStream"></param>
    /// <returns></returns>
    public static T DeserializeXml<T>(System.IO.Stream xmlStream)
    {
        XmlSerializer serializer = new XmlSerializer(typeof(T));
        return (T)(serializer.Deserialize(xmlStream));
    }
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="input"></param>
    /// <returns></returns>
    public static T Deserialize<T>(string input) where T : class
    {
        System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(T));

        using (StringReader sr = new StringReader(input))
        {
            return (T)ser.Deserialize(sr);
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="path"></param>
    /// <returns></returns>
    public static T DeserializeXml<T>(string path, T alt = default(T))
    {
        if (!File.Exists(path)) return alt;

        Stream fs = File.OpenRead(path);
        XmlSerializer serializer = new XmlSerializer(typeof(T));
        try
        {
            return (T)(serializer.Deserialize(fs));
        }
        finally { fs.Close(); fs.Dispose(); }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <param name="find"></param>
    /// <param name="replace"></param>
    /// <param name="matchWholeWord"></param>
    /// <returns></returns>
    public static string SafeReplace(this string input, string find, string replace, bool matchWholeWord = true)
    {
        if (replace == null) replace = "";
        if (find == null) find = "";
        string textToFind = matchWholeWord ? string.Format(@"\b{0}\b", find) : find;
        string outPut = Regex.Replace(input, textToFind, replace, RegexOptions.IgnoreCase);
        //if (makeTextXmlSafeReplace)
        //{
        //    outPut = XmlSafeReplace(outPut);
        //}
        return outPut;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="str"></param>
    /// <returns></returns>
    public static string XmlSafeReplace(string str)
    {
        if (string.IsNullOrEmpty(str))
        {
            return str;
        }
        str = str.Replace("&", "&amp;");
        str = str.Replace("<", "&lt;");
        str = str.Replace(">", "&gt;");
        str = str.Replace("\"", "&quot;");
        str = str.Replace("'", "&#39;");

        return str;
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="filePath"></param>
    /// <param name="splitCharacter"></param>
    /// <returns></returns>
    public static DataTable CsvToDataTable(string filePath, char splitCharacter)
    {
        using (StreamReader sr = new StreamReader(filePath))
        {
            string myStringRow = sr.ReadLine();
            var rows = myStringRow.Split(splitCharacter);
            using (DataTable CsvData = new DataTable())
            {
                foreach (string column in rows)
                {
                    //creates the columns of new data table based on first row of csv
                    if (CsvData.Columns.Contains(column.Trim())) continue;
                    CsvData.Columns.Add(column.Trim());
                }
                myStringRow = sr.ReadLine();
                while (myStringRow != null)
                {
                    //runs until string reader returns null and adds rows to dt 
                    rows = myStringRow.Split(splitCharacter);
                    CsvData.Rows.Add(rows);
                    myStringRow = sr.ReadLine();
                }
                sr.Close();
                sr.Dispose();
                return CsvData;
            }
        }
    }
    ///// <summary>
    ///// 
    ///// </summary>
    ///// <param name="value"></param>
    ///// <returns></returns>
    public static string GetDescription(this Enum value)
    {
        return ((DescriptionAttribute)Attribute.GetCustomAttribute(
            value.GetType().GetFields(BindingFlags.Public | BindingFlags.Static)
                .Single(x => x.GetValue(null).Equals(value)),
            typeof(DescriptionAttribute)))?.Description ?? value.ToString();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="Linqlist"></param>
    /// <returns></returns>
    public static DataTable LinqResultToDataTable<T>(IEnumerable<T> Linqlist)
    {
        DataTable dt = new DataTable();
        PropertyInfo[] columns = null;
        if (Linqlist == null) return dt;
        foreach (T Record in Linqlist)
        {
            if (columns == null)
            {
                columns = Record.GetType().GetProperties();
                foreach (PropertyInfo GetProperty in columns)
                {
                    Type colType = GetProperty.PropertyType;
                    if ((colType.IsGenericType) && (colType.GetGenericTypeDefinition() == typeof(Nullable<>)))
                    {
                        colType = colType.GetGenericArguments()[0];
                    }
                    dt.Columns.Add(new DataColumn(GetProperty.Name, colType));
                }
            }
            DataRow dr = dt.NewRow();
            foreach (PropertyInfo pinfo in columns)
            {
                dr[pinfo.Name] = pinfo.GetValue(Record, null) == null ? DBNull.Value : pinfo.GetValue
                (Record, null);
            }
            dt.Rows.Add(dr);
        }
        return dt;
    }
    /// <summary>
    /// get display name of properties
    /// </summary>
    /// <param name="property"></param>
    /// <returns></returns>
    public static string GetAttributeDisplayName(PropertyInfo property)
    {
        var atts = property.GetCustomAttributes(
            typeof(DisplayNameAttribute), true);
        if (atts.Length == 0)
            return property.Name;
        return (atts[0] as DisplayNameAttribute).DisplayName;
    }
}
