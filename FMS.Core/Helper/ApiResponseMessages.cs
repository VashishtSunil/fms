﻿namespace FMS.Core.Helper
{
    public class ApiResponseMessages
    {
        public readonly static string SWW = "Something went wrong.";
        public readonly static string EmailFound = "Email is already register.";
        public readonly static string EmailNotFound = "Email not found.";
        public readonly static string Success = "Action performed successfully.";
        public readonly static string TraineeDeleted = "Account delete successfully.";
        public readonly static string TraineeStatusUpdated = "Status updated successfully.";
        public readonly static string TraineeUpdated = "Trainee Updated Successfully.";
        public readonly static string TraineeAdded = "Trainee Added Successfully.";
        public readonly static string Valid = "Valid";
        public readonly static string NotValid = "Not Valid";
        public readonly static string BadRequest = "Bad request";
        public readonly static string NoContent = "No content";
        #region Tokens
        public readonly static string TokenExp = "Token is expired.";
        #endregion
        #region Authenctication
        public readonly static string LoginFail = "The Email or Password is incorrect.";
        #endregion

    }

    public class ApiHeaderProperties
    {
        public readonly static string PortalOwnerId = "PortalOwnerId";
        public readonly static string GymKey = "GymKey";
        public readonly static string NewsId = "NewsId";
       

    }
}
