﻿using FMS.Core.Enums;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace FMS.Core.Models
{
    public class QuestionModel : BaseModel
    {
        #region Properties
        [Remote("IsQuestionDuplicate", "Question", HttpMethod = "POST", ErrorMessage = "Same question is already exists in database.", AdditionalFields = "Id")]
        [Required(ErrorMessage = "Please Enter Question")]
        public string Description { get; set; }
        public string Name { get; set; }
        
        [Required(ErrorMessage = "Please Select Question Type")]
        public int Type { get; set; }
        public string QuestionTypeString{ get; set; }

        public int GroupId { get; set; }
        public bool IsSelected { get; set; }
        public bool Mandatory { get; set; }
        public bool AllowOther { get; set; }
        public string OtherText { get; set; }
        public bool AllowNoAnswer { get; set; }
        public bool AllowUncertain { get; set; }
        public bool SavePiping { get; set; }
        public int PipingQuestionId { get; set; }

        public List<LookUpDomainValueModel> LstQuestionType { get; set; }
        public List<QuestionOptionModel> LstQuestionOption { get; set; }
        #endregion
    }
}
