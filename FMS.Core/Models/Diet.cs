﻿using System.Collections.Generic;

namespace FMS.Core.Models
{
    public class DietModel : BaseModel
    {
        public int UserId { get; set; }
        public int UserName { get; set; }
        public string DietName { get; set; }
        public List<DietPlanMealModel> Meals { get; set; }
    }
}
