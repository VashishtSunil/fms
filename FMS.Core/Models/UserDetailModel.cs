﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMS.Core.Models
{
   public class UserDetailModel : BaseModel
    {
        #region Properties
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string PinCode { get; set; }
        public string ProfilePic { get; set; }
        public int UserId { get; set; }
        public int? ProfilePicId { get; set; }
        public string FileName { get; set; }
        #endregion
    }
}
