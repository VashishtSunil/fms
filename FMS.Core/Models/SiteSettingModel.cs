﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMS.Core.Models
{
   public class SiteSettingModel : BaseModel
    {

        public string Key { get; set; }
        public string Value { get; set; }
        public string Value2 { get; set; }
        public string Description { get; set; }
        public long ControlType { get; set; }
        public int OwnerId { get; set; }
        public int SitePortalOwnerId { get; set; }

    }


    
}
