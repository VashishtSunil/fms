﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMS.Core.Models
{
   public class EditorAssetModel 
    {
        public string title { get; set; }
        public string value { get; set; }
    }
}
