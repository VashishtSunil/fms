﻿
using System;

namespace FMS.Core.Models
{   
    public class RequestResponseModel : Payloads    
    {
        //public int Id { get; set; }
        //public string CreatedBy { get; set; }
        //public string ModifiedBy { get; set; }
        public string Token { get; set; }
        public string UniqId { get; set; }
        public string Method { get; set; }
        public string Request { get; set; }
        public DateTime? RequestDate { get; set; }
        public string Response { get; set; }
        public DateTime? ResponseDate { get; set; }
        public string ResponseCode { get; set; }
       
    }
}
