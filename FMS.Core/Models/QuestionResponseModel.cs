﻿using FMS.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FMS.Core.Models
{
    public class QuestionResponseModel:BaseModel
    {
        public int QuestionId { get; set; }
        public int GroupId { get; set; }
        public int PipelineQuestionId { get; set; }
        public List<QuestionOptionModel> LstQuestionResponse { get; set; }
        public SelectList LstPipelineQuestion { get; set; }
    }
}
