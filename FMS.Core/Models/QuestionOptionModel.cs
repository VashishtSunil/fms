﻿using System.ComponentModel.DataAnnotations;

namespace FMS.Core.Models
{
    public class QuestionOptionModel : BaseModel
    {
        #region Properties
        public int QuestionId { get; set; }
        [Required(ErrorMessage = "Please enter option description")]
        public string Description { get; set; }
        public string Label { get; set; }
        //Display Order
        public int Order { get; set; }
        //[Range(0.1, int.MaxValue, ErrorMessage = "Please enter a value bigger than 0")]
        public int OptionScore { get; set; }
        public bool IsCorrectAnswer { get; set; }
        #endregion
    }
}
