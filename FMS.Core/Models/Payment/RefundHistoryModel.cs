﻿using FMS.Core.Enums;

namespace FMS.Core.Models.Payment
{
    public class RefundHistoryModel:BaseModel
    {
        #region properties
        public int UserId { get; set; }
        public string GatewayCustomerId { get; set; }
        public string GatewayCardId { get; set; }
        public string GatewayChargeId { get; set; }
        public string GatewayRefundId { get; set; }

        public decimal PaidPrice { get; set; }
        /// <summary>
        /// as stripe accept payment in cents
        /// </summary>
        public long PaidPriceInCent { get; set; }
        /// <summary>
        /// like stripe and other payment gateway
        /// </summary>
        public PaymentGatewayType Gateway { get; set; }
        /// <summary>
        /// pending,done, not completed
        /// </summary>
        public PaymentStatusType PaymentStatus { get; set; }
        public string Description { get; set; }

        /// <summary>
        /// payment can be done for active subscription or for other things
        /// </summary>
        public int? SubscriptionId { get; set; }
        #endregion
    }
}
