﻿namespace FMS.Core.Models
{
    public class ApiUserModel : BaseEntity
    {
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
        public string ApiKey { get; set; }
        public string ClientKey { get; set; }

        public int? BrandId { get; set; }
    }
}
