﻿using System;

namespace FMS.Core.Models
{
    public class UserQuizModel : BaseModel
    {
        public Guid UserQuizGuid { get; set; } //user who has started the quiz
        /// <summary>
        /// 
        /// </summary>
        public int UserId { get; set; } //user who has started the quiz
        public int? QuizId { get; set; }
        //The Quiz can contains Questions  from Quiz ->QuizQuestion 
        public int? FMSId { get; set; }
        // Quiz can contains Questions  from FMS ->FMSQuestion 
        
    }
}
