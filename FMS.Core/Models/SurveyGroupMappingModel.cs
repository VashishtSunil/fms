﻿using FMS.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMS.Core.Models
{
    public class SurveyGroupMappingModel:BaseModel
    {
        public int SurveyId { get; set; }
        public int GroupId { get; set; }
    }
}
