﻿using FMS.Core.Enums;
using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace FMS.Core.Models
{
    public class FMSModel : BaseModel
    {
        #region Properties
        [Remote("IsFMSDuplicate", "FMS", HttpMethod = "POST", ErrorMessage = "Same FMS is already exists in database.", AdditionalFields = "Id")]
        [RegularExpression(@"^[^<>.,?;:'()!~%\-_@#/*""\s]+$", ErrorMessage = "Special characters and white space are not allowed.")]
        [Required(ErrorMessage = "Please Enter Name")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Please Enter Start Date")]
        public DateTime StartDate { get; set; }
        public string StartDateString
        {
            get
            {
                if (StartDate != null)
                    return StartDate.ToString("MM/dd/yyyy");

                return string.Empty;

            }
        }
        [Required(ErrorMessage = "Please Enter End Date")]
        public DateTime EndDate { get; set; }
        public string EndDateString
        {
            get
            {
                if (EndDate != null)
                    return EndDate.ToString("MM/dd/yyyy");

                return string.Empty;

            }
        }

        [Range(1, int.MaxValue, ErrorMessage = "Please Select Question Type")]
        public FMSType Type { get; set; }

        public bool IsTest { get; set; }

        //will make it fk
        public int? SurveyId { get; set; }

        public string FMSUniqueId { get; set; }

        #endregion
    }
}
