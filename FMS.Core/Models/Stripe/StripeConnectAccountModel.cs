﻿using FMS.Core.Enums;
using System;

namespace FMS.Core.Models.Stripe
{
    public class StripeConnectAccountModel:BaseModel
    {
        public int UserId { get; set; }
        public UserType UserType { get; set; }//like reseller or clientId
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string CompanyName { get; set; }
        public string Phone { get; set; }
        public string Username { get; set; }

        public string Country { get; set; }
        public int CountryId { get; set; }
        public int StateId { get; set; }
        public string State { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Postal { get; set; }

        public string CompanyPhone { get; set; }
        public int BankCountryId { get; set; }
        public string BankCountryName { get; set; }
        public string BankName { get; set; }
        public string BranchName { get; set; }
        public string AccountNumber { get; set; }
        public string BusinessNumber { get; set; }
        public string OwnerFirstName { get; set; }
        public string OwnerLastName { get; set; }
        public DateTime? OwnerDOB { get; set; }
        //public string BankAccountId { get; set; }
        //public string Last4Digit { get; set; }
        //public string FingerPrint { get; set; }

        public int LogoId { get; set; }
        public int FrontDriverLicenseId { get; set; }
        public int BackDriverLicenseId { get; set; }
        public int ArticalOfIncorporationId { get; set; }

        public int ConnectAccountId { get; set; }
        public string StringConnectAccountId { get; set; }
        public int ResellerId { get; set; }

        public string JsonRequest { get; set; }
        public string JsonResponse { get; set; }
        public string PersonJsonRequest { get; set; }
        public string PersonJsonResponse { get; set; }
        public StripeAccountType ConnectAccountType { get; set; }

        #region MyRegion
        public string IPAddress { get; set; }
        public string Currency { get; set; }
        public bool IsChargeEnabled { get; set; }
        public bool IsPayoutEnabled { get; set; }
        public string VerificationStatus { get; set; }
        public bool IsAuthorized { get; set; }
        public string PersonId { get; set; }
        public DateTime StripeCreatedDate { get; set; }
        public string BusinessTypeString { get; set; }
        public StripeConnectBusinessType BusinessType { get; set; }//like individual,company,non-profit and government_entity
        #endregion

    }
}
