﻿using System.Collections.Generic;

namespace FMS.Core.Models.Stripe
{
    public class StripePersonRequestModel:BaseModel
    {
        public string ConnectAccountId { get; set; }
        public string PersonId { get; set; }
        public bool? IsUserDirector { get; set; }
        public bool? IsUserExecutive { get; set; }
        public bool? IsOwner { get; set; }
        public decimal? IsUserPercentOwnership { get; set; }
        public bool? IsRepresentative { get; set; }
        public string RelationshipTitle { get; set; }
        public string PoliticalExposure { get; set; }
        public string Phone { get; set; }
        public string PersonToken { get; set; }
        public Dictionary<string, string> Metadata { get; set; }
        public string MaidenName { get; set; }
        public string LastName { get; set; }
        public string IdNumber { get; set; }
        public string Gender { get; set; }
        public string FirstName { get; set; }
        public string Email { get; set; }
        public long? DobDay { get; set; }
        public long? DobMonth { get; set; }
        public long? DobYear { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string PostalCode { get; set; }
        public string State { get; set; }
        public string SsnLast4 { get; set; }

        public string JsonResponse { get; set; }
        public string VerificationStatus { get; set; }
    }
}
