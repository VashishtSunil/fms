﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMS.Core.Models.Stripe
{
    public class StripeKeyModel
    {
        public string StripeApiPublicKey { get; set; }
        public string StripeApiPrivateKey { get; set; }
    }
}
