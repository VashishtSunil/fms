﻿using FMS.Core.Enums;
using System;

namespace FMS.Core.Models.Stripe
{
    public class StripeRefundModel:BaseModel
    {
        public int ChargeId { get; set; }
        public string StripeChargeId { get; set; }
        public string StripeRefundId { get; set; }

        public CurrencyType Currency { get; set; }
        public string Description { get; set; }

        public int AmountInCents { get; set; }
        public decimal Amount { get; set; }
        public bool LiveMode { get; set; }

        public DateTime RequestDateTime { get; set; }
        public string JsonResponse { get; set; }
    }
}
