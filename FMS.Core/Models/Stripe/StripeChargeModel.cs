﻿using FMS.Core.Enums;
using System;

namespace FMS.Core.Models.Stripe
{
    public class StripeChargeModel:BaseModel
    {
        #region Properties
        public int CardId { get; set; }
        public int CustomerId { get; set; }
        public int? ConnectAccountId { get; set; }
        public string StripeChargeId { get; set; }
        public string StripeCardId { get; set; }
        public string StripeCustomerId { get; set; }
        public string StripeConnectAccountId { get; set; }
        public int ApplicationFeeAmount { get; set; }

        /// <summary>
        /// it will be send while doing charges using api.
        /// it's value depend upon country
        /// </summary>
        public CurrencyType Currency { get; set; }
        public string Description { get; set; }

        /// <summary>
        /// as it is not necessary to send the application fee that is why it is null-able
        /// </summary>
        public int? FeeInCents { get; set; }
        public int PaidAmountInCents { get; set; }
        public int ApplicationFeeAmountInCents { get; set; }
        public decimal Amount { get; set; }
        public bool LiveMode { get; set; }
        public bool IsPaid { get; set; }

        public bool IsFullPaymentDone { get; set; }
        public bool IsAmountRefunded { get; set; }
        public decimal RefundedAmount { get; set; }

        public string TransactionId { get; set; }
        public string FailureCode { get; set; }
        public string FailureMessage { get; set; }
        public PaymentStatusType PaymentStatus { get; set; }
        public DateTime RequestDateTime { get; set; }
        public string JsonResponse { get; set; }
        #endregion
    }
}
