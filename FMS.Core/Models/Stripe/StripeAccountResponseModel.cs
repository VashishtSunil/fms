﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMS.Core.Models.Stripe
{
    public class StripeAccountResponseModel:BaseModel
    {
        [JsonProperty("access_token")]
        public string StripeApiPrivateKey { get; set; }
        [JsonProperty("livemode")]
        public bool IsLiveMode { get; set; }
        [JsonProperty("refresh_token")]
        public string RefreshToken { get; set; }
        [JsonProperty("token_type")]
        public string AuthTokenType { get; set; }
        [JsonProperty("stripe_publishable_key")]
        public string StripeApiPublicKey { get; set; }
        [JsonProperty("stripe_user_id")]
        public string ConnectAccountId { get; set; }
        [JsonProperty("scope")]
        public string AccountScope { get; set; }
        [JsonProperty("error")]
        public string ErrorCode { get; set; }
        [JsonProperty("error_description")]
        public string ErrorMessage { get; set; }
    }
}
