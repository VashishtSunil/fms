﻿using Newtonsoft.Json;

namespace FMS.Core.Models.Stripe
{
    public class ConnectAccountResponseModel
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("_object")]
        public string Object { get; set; }
        [JsonProperty("business_profile")]
        public BusinessProfile BusinessProfile { get; set; }
        [JsonProperty("business_type")]
        public string BusinessType { get; set; }
        [JsonProperty("capabilities")]
        public Capabilities Capabilities { get; set; }
        [JsonProperty("charges_enabled")]
        public bool IsChargeEnabled { get; set; }
        [JsonProperty("company")]
        public Company Company { get; set; }
        [JsonProperty("country")]
        public string Country { get; set; }
        [JsonProperty("created")]
        public int CreatedDate { get; set; }
        [JsonProperty("default_currency")]
        public string DefaultCurrency { get; set; }
        [JsonProperty("details_submitted")]
        public bool IsDetailSubmitted { get; set; }
        [JsonProperty("email")]
        public string Email { get; set; }
        [JsonProperty("external_accounts")]
        public External_Accounts ExternalAccounts { get; set; }
        [JsonProperty("individual")]
        public object Individual { get; set; }
        [JsonProperty("metadata")]
        public Metadata MetaData { get; set; }
        [JsonProperty("payouts_enabled")]
        public bool IsPayoutEnabled { get; set; }
        [JsonProperty("requirements")]
        public Requirements Requirements { get; set; }
        [JsonProperty("settings")]
        public Settings Settings { get; set; }
        [JsonProperty("tos_acceptance")]
        public TosAcceptance Acceptance { get; set; }
        [JsonProperty("type")]
        public string AccountType { get; set; }
    }

    public class BusinessProfile
    {
        [JsonProperty("mcc")]
        public object MCC { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("product_description")]
        public object ProductInfo { get; set; }
        [JsonProperty("support_address")]
        public object Address { get; set; }
        [JsonProperty("support_email")]
        public object Email { get; set; }
        [JsonProperty("support_phone")]
        public object Phone { get; set; }
        [JsonProperty("support_url")]
        public object Url { get; set; }
        [JsonProperty("url")]
        public object BusinessUrl { get; set; }
    }

    public class Capabilities
    {
        [JsonProperty("au_becs_debit_payments")]
        public object AuBecsDebitPayments { get; set; }
        [JsonProperty("bacs_debit_payments")]
        public object BacsDebitPayments { get; set; }
        [JsonProperty("bancontact_payments")]
        public object BanContactPayments { get; set; }
        [JsonProperty("card_issuing")]
        public object CardIssuing { get; set; }
        [JsonProperty("card_payments")]
        public string CardPayments { get; set; }
        [JsonProperty("cartes_bancaires_payments")]
        public object CartesBancairesPayments { get; set; }
        [JsonProperty("eps_payments")]
        public object EpsPayments { get; set; }
        [JsonProperty("fpx_payments")]
        public object FpxPayments { get; set; }
        [JsonProperty("giropay_payments")]
        public object GiropayPayments { get; set; }
        [JsonProperty("grabpay_payments")]
        public object GrabpayPayments { get; set; }
        [JsonProperty("ideal_payments")]
        public object IdealPayments { get; set; }
        [JsonProperty("jcb_payments")]
        public object JcbPayments { get; set; }
        [JsonProperty("legacy_payments")]
        public object LegacyPayments { get; set; }
        [JsonProperty("oxxo_payments")]
        public object OxxoPayments { get; set; }
        [JsonProperty("p24_payments")]
        public object P24Payments { get; set; }
        [JsonProperty("sepa_debit_payments")]
        public object SepaDebitPayments { get; set; }
        [JsonProperty("sofort_payments")]
        public object SofortPayments { get; set; }
        [JsonProperty("tax_reporting_us_1099_k")]
        public object TaxReportingUs1099K { get; set; }
        [JsonProperty("tax_reporting_us_1099_misc")]
        public object TaxReportingUs1099Misc { get; set; }
        [JsonProperty("transfers")]
        public string Transfers { get; set; }
    }

    public class Company
    {
        [JsonProperty("address")]
        public Address Address { get; set; }
        [JsonProperty("address_kana")]
        public object AddressKana { get; set; }
        [JsonProperty("address_kanji")]
        public object AddressKanji { get; set; }
        [JsonProperty("transfers")]
        public bool directors_provided { get; set; }
        [JsonProperty("directors_provided")]
        public bool ExecutivesProvided { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("name_kana")]
        public object NameKana { get; set; }
        [JsonProperty("name_kanji")]
        public object NameKanji { get; set; }
        [JsonProperty("owners_provided")]
        public bool OwnersProvided { get; set; }
        [JsonProperty("phone")]
        public string PhoneNumber { get; set; }
        [JsonProperty("structure")]
        public object Structure { get; set; }
        [JsonProperty("tax_id_provided")]
        public bool IsTaxIdProvided { get; set; }
        [JsonProperty("tax_id_registrar")]
        public object TaxIdRegistrar { get; set; }
        [JsonProperty("vat_id_provided")]
        public bool IsVatIdProvided { get; set; }
        [JsonProperty("verification")]
        public Verification Verification { get; set; }
    }

    public class Address
    {
        [JsonProperty("city")]
        public string City { get; set; }
        [JsonProperty("country")]
        public string Country { get; set; }
        [JsonProperty("line1")]
        public string Address1 { get; set; }
        [JsonProperty("line2")]
        public object Address2 { get; set; }
        [JsonProperty("postal_code")]
        public string PostalCode { get; set; }
        [JsonProperty("state")]
        public string State { get; set; }
    }

    public class Verification
    {
        [JsonProperty("document")]
        public Document Document { get; set; }
    }

    public class Document
    {
        [JsonProperty("back")]
        public object BackFileDocument { get; set; }
        [JsonProperty("details")]
        public object Details { get; set; }
        [JsonProperty("details_code")]
        public object DetailCode { get; set; }
        [JsonProperty("front")]
        public object FrontFileDocument { get; set; }
    }

    public class External_Accounts
    {
        [JsonProperty("_object")]
        public string Object { get; set; }
        [JsonProperty("data")]
        public Datum[] BankDetails { get; set; }
        [JsonProperty("has_more")]
        public bool IsMoreDetail { get; set; }
        [JsonProperty("url")]
        public string ExternalAccountApiUrl { get; set; }
    }

    public class Datum
    {
        [JsonProperty("id")]
        public string BankId { get; set; }
        [JsonProperty("_object")]
        public string Object { get; set; }
        [JsonProperty("account")]
        public string AccountNumber { get; set; }
        [JsonProperty("account_holder_name")]
        public string AccountHolderName { get; set; }
        [JsonProperty("account_holder_type")]
        public string AccountHolderType { get; set; }
        [JsonProperty("available_payout_methods")]
        public string[] PayoutMethods { get; set; }
        [JsonProperty("bank_name")]
        public string BankName { get; set; }
        [JsonProperty("country")]
        public string Country { get; set; }
        [JsonProperty("currency")]
        public string Currency { get; set; }
        [JsonProperty("customer")]
        public object Customer { get; set; }
        [JsonProperty("default_for_currency")]
        public bool IsDefaultCurrency { get; set; }
        [JsonProperty("fingerprint")]
        public string FingerPrint { get; set; }
        [JsonProperty("last4")]
        public string Last4Digit { get; set; }
        [JsonProperty("metadata")]
        public Metadata MetaData { get; set; }
        [JsonProperty("routing_number")]
        public string RoutingNumber { get; set; }
        [JsonProperty("status")]
        public string Status { get; set; }
    }

    public class Metadata
    {
    }

    public class Requirements
    {
        [JsonProperty("current_deadline")]
        public object CurrentDeadline { get; set; }
        [JsonProperty("currently_due")]
        public string[] CurrentDue { get; set; }
        [JsonProperty("disabled_reason")]
        public string ReasonForDisabled { get; set; }
        [JsonProperty("errors")]
        public object[] ErrorObject { get; set; }
        [JsonProperty("eventually_due")]
        public string[] EventuallyDue { get; set; }
        [JsonProperty("past_due")]
        public string[] PastDue { get; set; }
        [JsonProperty("pending_verification")]
        public object[] PendingVerification { get; set; }
    }

    public class Settings
    {
        [JsonProperty("bacs_debit_payments")]
        public BacsDebitPayments bacsDebitPayments { get; set; }
        [JsonProperty("branding")]
        public Branding Branding { get; set; }
        [JsonProperty("card_payments")]
        public CardPayments CardPayments { get; set; }
        [JsonProperty("dashboard")]
        public Dashboard Dashboard { get; set; }
        [JsonProperty("payments")]
        public Payments Payments { get; set; }
        [JsonProperty("payouts")]
        public Payouts Payouts { get; set; }
        [JsonProperty("sepa_debit_payments")]
        public SepaDebitPayments SepaDebitPayments { get; set; }
    }

    public class BacsDebitPayments
    {
        [JsonProperty("display_name")]
        public object DisplayName { get; set; }
    }

    public class Branding
    {
        [JsonProperty("icon")]
        public object Icon { get; set; }
        [JsonProperty("logo")]
        public object Logo { get; set; }
        [JsonProperty("primary_color")]
        public object PrimaryColor { get; set; }
        [JsonProperty("secondary_color")]
        public object SecondaryColor { get; set; }
    }

    public class CardPayments
    {
        [JsonProperty("decline_on")]
        public DeclineOn DeclineOn { get; set; }
        [JsonProperty("statement_descriptor_prefix")]
        public object StatementDescriptorPrefix { get; set; }
    }

    public class DeclineOn
    {
        [JsonProperty("avs_failure")]
        public bool IsAvsFailure { get; set; }
        [JsonProperty("cvc_failure")]
        public bool IsCvcFailure { get; set; }
    }

    public class Dashboard
    {
        [JsonProperty("display_name")]
        public object DisplayName { get; set; }
        [JsonProperty("timezone")]
        public string Timezone { get; set; }
    }

    public class Payments
    {
        [JsonProperty("statement_descriptor")]
        public object StatementDescriptor { get; set; }
        [JsonProperty("statement_descriptor_kana")]
        public object StatementDescriptorKana { get; set; }
        [JsonProperty("statement_descriptor_kanji")]
        public object StatementDescriptorKanji { get; set; }
    }

    public class Payouts
    {
        [JsonProperty("debit_negative_balances")]
        public bool DebitNegativeBalances { get; set; }
        [JsonProperty("schedule")]
        public Schedule Schedule { get; set; }
        [JsonProperty("statement_descriptor")]
        public object StatementDescriptor { get; set; }
    }

    public class Schedule
    {
        [JsonProperty("delay_days")]
        public int DelayDays { get; set; }
        [JsonProperty("interval")]
        public string Interval { get; set; }
        [JsonProperty("monthly_anchor")]
        public int MonthlyAnchor { get; set; }
        [JsonProperty("weekly_anchor")]
        public object WeeklyAnchor { get; set; }
    }

    public class SepaDebitPayments
    {
        [JsonProperty("creditor_id")]
        public object CreditorId { get; set; }
    }

    public class TosAcceptance
    {
        [JsonProperty("date")]
        public int Date { get; set; }
        [JsonProperty("ip")]
        public string IpAddress { get; set; }
        [JsonProperty("service_agreement")]
        public object ServiceAgreement { get; set; }
        [JsonProperty("user_agent")]
        public object UserAgent { get; set; }
    }
}