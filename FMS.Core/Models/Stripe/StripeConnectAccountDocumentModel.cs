﻿using FMS.Core.Enums;
using System.Collections.Generic;

namespace FMS.Core.Models.Stripe
{
    public class StripeConnectAccountDocumentModel:BaseModel
    {
        public int ConnectAccountId { get; set; }
        public string StripeConnectAccountId { get; set; }
        public string StripePersonId { get; set; }
        public string StripeFileId { get; set; }
        public int AttachmentId { get; set; }

        public bool IsFrontSide { get; set; }
        public bool IsBackSide { get; set; }
        public byte[] DocumentBytes { get; set; }
        public int DocumentSize { get; set; }
        public FileUploadingPurpose DocumentPurpose { get; set; }//like we are uploading document for identity verification or for company verification
        public string JsonRequest { get; set; }
        public string JsonResponse { get; set; }
    }

    public class StripeConnectAccountDocumentViewModel:BaseModel
    {
        public string StripeConnectAccountId { get; set; }
        public int ConnectAccountId { get; set; }
        public string StripePersonId { get; set; }
        public List<StripeConnectAccountDocumentModel> lstDocument { get; set; }
    }
}
