﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace FMS.Core.Models
{
    public class FMSPinModel : BaseModel
    {
        #region Properties
        public int FMSId { get; set; }
        [Required(ErrorMessage = "Please Enter Number")]
        public string Number { get; set; }
        public SelectList FMSList { get; set; }    
        #endregion

    }

    public class FMSPinViewModel : BaseModel
    {
        #region Properties
        public int FMSId { get; set; }
        [Required(ErrorMessage = "Please Enter Number")]
        public string Number { get; set; }
        public SelectList FMSList { get; set; }


        public SelectList SurveyList { get; set; }
        public int? SurveyId { get; set; }
        #endregion

    }
}
