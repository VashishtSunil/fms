using System.Collections.Generic;
using System;
using System.ComponentModel.DataAnnotations;

namespace FMS.Core.Models
{
    public class ClientSliderViewModel : BaseModel
    {
        #region Properties
        public int ClientId { get; set; }
        [Required(ErrorMessage = "Please enter heading")]
        public string Heading { get; set; }
        [Required(ErrorMessage = "Please enter sub heading")]
        public string SubHeading { get; set; }
        public int LogoId { get; set; }
        public bool IsButtonDisplayed { get; set; }
        public string ButtonText { get; set; }
        public string ButtonLink { get; set; }
        public int? OwnerId { get; set; }
        public int SitePortalOwnerId { get; set; }
        public Core.DataModels.Attachment.Attachment SliderLogo { get; set; }
        public string Logo
        {
            get
            {
                if (SliderLogo != null)
                    return "data:" + SliderLogo.ContentType + ";base64," + Convert.ToBase64String(SliderLogo.ImageBytes);
                return string.Empty;
            }
        }

        #endregion
    }
}
