﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FMS.Core.Models
{
    public class QuizModel : BaseModel
    {
        [Remote("IsQuizDuplicate", "Quiz", HttpMethod = "POST", ErrorMessage = "Same quiz is already exists in database.", AdditionalFields = "Id")]
        [Required(ErrorMessage = "Please Enter Name")]
        public string Name { get; set; }
        public int Questions { get; set; }
        public string SelectedQuizQuestion { get; set; }
        public int? QuizId { get; set; }
        public int? QuestionId { get; set; }

        public int? FMSId { get; set; }
    }
}
