﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FMS.Core.Models
{
public    class NewsModel : BaseModel
    {
        [Required(ErrorMessage = "Please enter title")]
        public string Title { get; set; }
        
        [AllowHtml]
        public string Description { get; set; }
       
        public DateTime PublishDate { get; set; }
        public string PublishDateString
        {
            get
            {
                if (PublishDate != null)
                    return PublishDate.ToShortDateString();

                return string.Empty;
            }
        }
        public string CreatedOnString
        {
            get
            {
                if (CreatedOn != null)
                    return CreatedOn.Value.ToShortDateString();

                return string.Empty;
            }
        }

        public int? OwnerId { get; set; } // creted by 
        public int SitePortalOwnerId { get; set; }// i.e admin Id 
        public int AttachmentId { get; set; }
        
        public Core.DataModels.Attachment.Attachment Attachment { get; set; }

        public string Image
        {
            get
            {
                if (Attachment != null)
                    return "data:" + Attachment.ContentType + ";base64," + Convert.ToBase64String(Attachment.ImageBytes);
                return string.Empty;
            }
        }


    }
}
