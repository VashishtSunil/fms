﻿using FMS.Core.Enums;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace FMS.Core.Models
{
    public class FMSPageModel : BaseModel
    {
        #region Properties
        public int FMSId { get; set; }
        [Remote("IsFMSPageDuplicate", "FMS", HttpMethod = "POST", ErrorMessage = "Same page name is already exists in this FMS.", AdditionalFields = "FMSId")]
        [Required(ErrorMessage = "Please Enter Name")]
        public string Name { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "Please Select FMS Type")]
        public PageType Type { get; set; }
        public string CSS { get; set; }
        public string HTML { get; set; }


        #endregion

    }
}
