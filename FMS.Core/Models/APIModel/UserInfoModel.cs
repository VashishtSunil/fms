namespace FMS.Core.Models.APIModel
{
    public class UserInfoModel : Payloads
    {
        public long UserId { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string CompanyName { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }
        public string Phone { get; set; }
        public string ProfilePic { get; set; }
    }
}