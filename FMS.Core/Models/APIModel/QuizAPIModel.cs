﻿namespace FMS.Core.Models.APIModel
{
    public class QuizAPIModel : Payloads
    {
        public string Name { get; set; }

        public int UserId { get; set; }
        public int ProgramId { get; set; }
        public int FMSId { get; set; }
    }
}
