﻿using System.Web.Mvc;

namespace FMS.Core.Models.APIModel
{
    public class FMSAPIModel : Payloads
    {
        #region Properties

        public string Name { get; set; }
        public int ProgramId { get; set; }

        public int Order { get; set; }
        #endregion
    }
}
