﻿using System;

namespace FMS.Core.Models.APIModel
{
    class NewsApiModel : BaseModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime PublishDate { get; set; }
        public int AttachmentId { get; set; }
        public Core.DataModels.Attachment.Attachment Attachment { get; set; }
    }
}
