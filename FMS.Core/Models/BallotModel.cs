﻿namespace FMS.Core.Models
{
    public class BallotModel : BaseModel
    {
        #region Properties
        public int FMSId { get; set; }
        public int SurveyId { get; set; }
        public string Email { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Address { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string Postal { get; set; }
        public string Phone { get; set; }
        public string IP { get; set; }
        public string Code { get; set; }
        public string Age { get; set; }
        public string FMS { get; set; }
        public string Language { get; set; }
        public string Gender { get; set; }
        public bool TermsFlag { get; set; }
        public bool AgeFlag { get; set; }
        public bool CaslFlag { get; set; }
        public string Custom1 { get; set; }
        public string Custom2 { get; set; }
        public string Custom3 { get; set; }
        public string Custom4 { get; set; }
        public string Custom5 { get; set; }

        #endregion
    }
}
