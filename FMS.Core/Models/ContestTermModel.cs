﻿using System.ComponentModel.DataAnnotations;

namespace FMS.Core.Models
{
    public class FMSTermModel : BaseModel
    {
        #region Properties
        public int FMSId { get; set; }
        [Required(ErrorMessage = "Please Enter Description")]
        public string Description { get; set; }
        public string CSS { get; set; }
        #endregion
    }
}
