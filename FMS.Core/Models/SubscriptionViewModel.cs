﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FMS.Core.Models
{
   public class SubscriptionViewModel
    {

        public List<PlanModel> paymentPlans { get; set; }
        public SelectList PlanList { get; set; }
        public int PlanId { get; set; }
        //public virtual List<PaymentModel> LstPlan { get; set; }
    }
}
