﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace FMS.Core.Models
{
    public class PlanModel : BaseModel
    {
        public PlanModel()
        {
            Price = Convert.ToDecimal("0.00");
        }

        [Required(ErrorMessage = "Please enter plan name")]
        [Remote("IsPlanDuplicate", "Plan", HttpMethod = "POST", ErrorMessage = "Plan name is already exists in database.", AdditionalFields = "Id")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Please enter price")]
        [Range(1.00, double.MaxValue, ErrorMessage = "Please enter a valid price")]
        public decimal Price { get; set; }
        public string Description { get; set; }
        public bool IsDisplayOnPublicPage { get; set; }
        public int FeatureCount { get; set; }
        public string PriceString { get; set; }
        public bool IsFeatureScreenUsed { get; set; }
        public int? OwnerId { get; set; }
        public int PlanLengthInDays { get; set; }
        public bool? IsSeeded { get; set; }
        public string Feature { get; set; }
        public string planFeature { get; set; }
        public int SitePortalOwnerId { get; set; }
        public virtual List<ListItemModel> LstFeature { get; set; }
        public virtual List<PlanFeatureModel> LstPlanFeature { get; set; }
    }
}
