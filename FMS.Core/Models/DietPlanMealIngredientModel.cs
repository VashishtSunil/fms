﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace FMS.Core.Models
{
    public class DietPlanMealIngredientModel : BaseModel
    {
        public int DietId { get; set; }
        public int MealId { get; set; }
        public string MealName { get; set; }
        public int IngredientId { get; set; }
        public string Ingredient { get; set; }
        public decimal Unit { get; set; }
        public List<SelectListItem> Ingredients { get; set; }
    }

   


}
