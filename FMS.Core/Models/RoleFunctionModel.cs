﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMS.Core.Models
{
   public class RoleFunctionModel : BaseModel
    {
        //mapping for role and function 
        public int FunctionId { get; set; }
        public int RoleId { get; set; }

        //claim prop
        public int ClaimId { get; set; }
        public string ClaimName { get; set; }

        //Action Flag
        public bool IsCreate { get; set; }
        public bool IsView { get; set; }
        public bool IsEdit { get; set; }
        public bool IsDelete { get; set; }
    }
}
