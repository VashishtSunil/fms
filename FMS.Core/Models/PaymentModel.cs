﻿using FMS.Core.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMS.Core.Models
{
    public class PaymentModel:BaseModel
    {
        public long OwnerId { get; set; }
        public int UserId { get; set; }    
        public string CardNumber { get; set; }
        public int ExMonth { get; set; }
        public int ExYear { get; set; }
        public string Cvc { get; set; }
        public int CustomerId { get; set; }
        public string GatewayCustomerId { get; set; }
        // for subscriptionCharge 
        public decimal Charge { get; set; }
        public PaymentGatewayType PaymentMethod { get; set; }
        public int PlanId { get; set; }
        public string PlanName { get; set; }
    }
}
