﻿using FMS.Core.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace FMS.Core.Models
{
    public class SurveyModel:BaseModel
    {
        [Required(ErrorMessage = "Please enter survey name")]
        //[Remote("IsSurveyDuplicate", "Survey", HttpMethod = "POST", ErrorMessage = "Survey name is already exists in database", AdditionalFields = "Id")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Please select survey type")]
        public SurveyType Type { get; set; }
        [Required(ErrorMessage = "Please select start date")]
        public DateTime? StartDate { get; set; }
        [Required(ErrorMessage = "Please select end date")]
        public DateTime? EndDate { get; set; }
        public bool ShowProgressText { get; set; }
        public bool ShowProgressBar { get; set; }
        public bool BackwardNavigation { get; set; }
        public bool SaveAnswers { get; set; }
        public int OwnerId { get; set; }
        public int QuestionCount { get; set; }
        public string StartDateString
        {
            get
            {
                if (StartDate != null)
                    return StartDate.Value.ToShortDateString();

                return string.Empty;
            }
        }
        public string EndDateString
        {
            get
            {
                if (EndDate != null)
                    return EndDate.Value.ToShortDateString();

                return string.Empty;
            }
        }
        public bool IsSurveyActive
        {
            get
            {
                if (StartDate != null && EndDate != null && StartDate.Value.Date <= DateTime.Now.Date && EndDate.Value.Date >= DateTime.Now.Date)
                    return true;

                return false;
            }
        }
        public List<SelectListItem> SurveyTypeList { get; set; }
    }
}
