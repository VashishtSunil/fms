﻿namespace FMS.Core.Models
{
    public class PortalModel : BaseModel
    {
        public string Name { get; set; }
        public string Url { get; set; }
        public string Icon { get; set; }
        public string ConnectionString { get; set; }
        public string SecondaryUserId { get; set; }
    }
}
