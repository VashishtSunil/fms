﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMS.Core.Models
{
    public class UserQuizQuestionModel : BaseModel
    {
        public int UserQuizId { get; set; }
        public Guid UserQuizGuid { get; set; }

        public string Description { get; set; }
        public string Name { get; set; }
        //Save QuestionType from enum 
        public int Type { get; set; }

        //Flag that user has attempted this question
        public bool IsAttempted { get; set; }
        //Flag that user has skipped this question 
        public bool IsSkipped { get; set; }
        //when user will select the option during test will 
        //set this flag
        public bool IsAnswerCorrect { get; set; }
        //will calcluate this during ans submit and by adding all 
        //calculate total marks 
        public float ObtainedMarks { get; set; }

        //For UI check 
        public int NextQuestionId { get; set; }
        public int PreviousQuestionId { get; set; }

        public bool IsFirstQuestion { get; set; }
        public bool IsLastQuestion { get; set; }

        public bool AllQuestionCompleted { get; set; }

        public List<UserQuizQuestionOptionModel> LstUserQuizQuestionOptions { get; set; }
        public string SelectedOption { get; set; }
        public bool IsQuizCompleted { get; set; }

    }
}
