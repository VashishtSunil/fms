﻿using FMS.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FMS.Core.Models
{
    public class GroupModel: BaseModel
    {
        [Required(ErrorMessage = "Please enter group name")]
        [Remote("IsGroupDuplicate", "Group", HttpMethod = "POST", ErrorMessage = "Group name is already exists in database", AdditionalFields = "Id")]
        public string Name { get; set; }
        public int OwnerId { get; set; }
        public int SurveyId { get; set; }
    }
}
