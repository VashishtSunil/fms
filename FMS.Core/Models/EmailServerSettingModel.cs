﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMS.Core.Models
{
    public class EmailServerSettingModel : BaseModel
    {

        public long Id { get; set; }

        //Id from Api for further recognisation 
        [Required]
        public string PortNo { get; set; } //from api header can be used further 
        [Required]
        public bool IsSSLEnable { get; set; } // this is the Id that system will assighn -https://sendgrid.com/docs/for-developers/sending-email/unique-arguments/
        [Required]
        public string SMTPAddress { get; set; }
        [Required]
        public string FromEmail { get; set; }
        [Required]
        public string Password { get; set; }
        public string Name { get; set; } //from candidate name
        // 0 for sendgrid and 1 for ssl setting 
       

        // for sendgrid 
        [Required]
        public string SendgridApiKey { get; set; }
        public string OwnerId { get; set; }


    }
}
