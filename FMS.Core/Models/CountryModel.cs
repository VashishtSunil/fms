﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMS.Core.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class CountryModel : BaseModel
    {
        #region Properties
        public string Name { get; set; }
        public string Code { get; set; }
        //public virtual List<State> States { get; set; }
        #endregion

    }
}
