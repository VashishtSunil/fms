﻿using System;

namespace FMS.Core.Models
{
    public class WinnerModel : BaseModel
    {
        public DateTime StartDate { get; set; }
        public string StartDateString
        {
            get
            {
                if (StartDate != null)
                    return StartDate.ToString("MM/dd/yyyy");

                return string.Empty;

            }
        }
        public DateTime EndDate { get; set; }
        public string EndDateString
        {
            get
            {
                if (EndDate != null)
                    return EndDate.ToString("MM/dd/yyyy");

                return string.Empty;

            }
        }
        public int Picks { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public int FMS { get; set; }
        public string Mobile { get; set; }
        public string Age { get; set; }
        public string Flavor { get; set; }
        public int FMSId { get; set; }

        public int RandomNumber { get; set; }
    }
}
