﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace FMS.Core.Models
{
    public class RoleModel : BaseModel
    {

        [Remote("IsRoleDuplicate", "Role", HttpMethod = "POST", ErrorMessage = "Same role is already exists in database.", AdditionalFields = "Id")]
        [Required(ErrorMessage = "Please enter role name")]
        public string RoleName { get; set; }    
        public int? ParentId { get; set; }
        public int? AccessLevel { get; set; }
        public int? OwnerId { get; set; }
        public int? CategoryId { get; set; }
        public int Users { get; set; }      
        public List<FunctionModel> LstFunction { get; set; }
    }
}
