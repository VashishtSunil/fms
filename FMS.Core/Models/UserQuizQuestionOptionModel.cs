﻿namespace FMS.Core.Models
{
    public class UserQuizQuestionOptionModel : BaseModel
    {
        public int QuestionId { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        //Display Order
        public int Order { get; set; }

        public bool IsCorrectAnswer { get; set; }

        public bool IsMarkedCorrect { get; set; }
    }
}
