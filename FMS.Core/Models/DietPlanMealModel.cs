﻿using FMS.Core.DataModels.Diet;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace FMS.Core.Models
{
    public class DietPlanMealModel : BaseModel
    {
        public int DietId { get; set; }
        public int UserName { get; set; }
        public string MealName { get; set; }
        public List<DietPlanMealIngredientModel> Ingredient { get; set; }
    }
}
