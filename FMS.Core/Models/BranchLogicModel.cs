﻿using FMS.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FMS.Core.Models
{
    public class BranchLogicModel:BaseModel
    {
        public int QuestionId { get; set; }
        public string QuestionName { get; set; }
        [Required(ErrorMessage ="Please select the branch question")]
        public int BranchQuestionId { get; set; }
        public int RuleId { get; set; }
        public bool IsAndRule { get; set; }
        public int? ReplaceQuestionId { get; set; }
        [Required(ErrorMessage = "Please select the responses")]
        public int[] ResponseIdArray { get; set; }

        public SelectList LstBranchQuestion { get; set; }
        public MultiSelectList LstQuestionOption { get; set; }
        public SelectList LstReplaceQuestion { get; set; }
    }
}
