﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMS.Core.Models
{
   public class QuestionFMSModel : BaseModel
    {
        #region Properties
        public int QuestionId { get; set; }
        public int FMSId { get; set; }

        #endregion
    }
}
