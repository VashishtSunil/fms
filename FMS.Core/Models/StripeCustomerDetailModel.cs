﻿using FMS.Core.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMS.Core.Models
{
    public class StripeCustomerDetailModel : BaseModel
    {
        #region Properties
        public int CustomerId { get; set; }
        public string StripeCustomerId { get; set; }
        public string StripeCardId { get; set; }

        /// <summary>
        /// saved masked credit card number
        /// </summary>
        /// 
        public string CardNumber { get; set; }
        public string UserId { get; set; }
        public string Cvc { get; set; }
        public int ExpirationMonth { get; set; }
        public int ExpirationYear { get; set; }
        public string Last4 { get; set; }
        /// <summary>
        /// token will be generated either using stripe.js or token stripe api
        /// </summary>
        public string CardToken { get; set; }
        public string CardType { get; set; }
        public bool CvcCheck { get; set; }
        public string Fingerprint { get; set; }
        public string Country { get; set; }
        public string JsonResponse { get; set; }
        public int PlanId { get; set; }
        public string PlanName { get; set; }
        public string Charge { get; set; }
        //customer owner address
        public string UserName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        #endregion
    }
}
