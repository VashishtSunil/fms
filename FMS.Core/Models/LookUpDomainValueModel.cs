﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace FMS.Core.Models
{
    public class LookUpDomainValueModel : BaseModel
    {
        public int DomainId { get; set; }
        public int DomainValueParentId { get; set; }
        // only add first time work as unique key 
        [Remote("IsSettingDuplicate", "Settings", HttpMethod = "POST", ErrorMessage = "Same name is already exists in database.", AdditionalFields = "Id")]
        [Required(ErrorMessage = "Please enter Setting Name")]
        public string DomainValue { get; set; }
        //on change we will keep the change value
        [Required(ErrorMessage = "Please enter Setting Value")]
        public string DomainText { get; set; }
        public int GroupId { get; set; }
    }
}
