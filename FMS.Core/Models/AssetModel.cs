﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace FMS.Core.Models
{
    public class AssetModel : BaseModel
    {
        public int MasterId { get; set; }
        [Remote("IsAssetDuplicate", "Asset", HttpMethod = "POST", ErrorMessage = "Same asset is already exists in database.", AdditionalFields = "Id")]
        [MaxLength(100)]
        [Required(ErrorMessage = "Please enter company")]
        public string Name { get; set; }

        public bool IsImage { get; set; }
        public bool IsVideo { get; set; }
        public bool IsDoc { get; set; }

        public string CreatedOnString
        {
            get
            {
                if (CreatedOn != null)
                    return CreatedOn.Value.ToShortDateString();

                return string.Empty;
            }
        }

        public string ModifiedOnString
        {
            get
            {
                if (ModifiedOn != null)
                    return ModifiedOn.Value.ToShortDateString();

                return string.Empty;
            }
        }
        public string Location { get; set; }
        public string AssetType { get; set; }
    }
}
