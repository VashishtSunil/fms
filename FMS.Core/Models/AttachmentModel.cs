﻿using FMS.Core.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMS.Core.Models
{
    public class AttachmentModel : BaseModel
    {
        public string Location { get; set; }
        public string FileName { get; set; }
        public string DummyFileName { get; set; }
        public int Size { get; set; }
        public MimeType MIME { get; set; }
        public int UploaderId { get; set; }
        public AttachmentUploaderType UplaoderType { get; set; }
        public int[] AttachmentIds { get; set; }
        public byte[] ImageBytes { get; set; }
        public string ContentType { get; set; }
    }
}
