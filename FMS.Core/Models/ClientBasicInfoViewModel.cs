﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FMS.Core.Models
{
    public class ClientBasicInfoViewModel : BaseModel
    {
        #region Properties
        public int ClientId { get; set; }
        [Required(ErrorMessage = "Please enter heading")]
        public string Heading { get; set; }
        [Required(ErrorMessage = "Please enter sub heading")]
        public string SubHeading { get; set; }
        public string Logo
        {
            get
            {
                if (BasicInfoLogo != null)
                    return "data:" + BasicInfoLogo.ContentType + ";base64," + Convert.ToBase64String(BasicInfoLogo.ImageBytes);
                return string.Empty;
            }
        }
        public int LogoId { get; set; }
        public int? OwnerId { get; set; }
        public int SitePortalOwnerId { get; set; }
        public string FacebookUrl { get; set; }
        public string InstagramUrl { get; set; }
        public string TwitterUrl { get; set; }
        public Core.DataModels.Attachment.Attachment BasicInfoLogo { get; set; }
        #endregion
    }
}
