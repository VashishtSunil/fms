﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMS.Core.Models
{
  public  class ContactUsModel : BaseModel
    {
        [Required(ErrorMessage = "Please enter name")]
        public string Name { get; set; }
        public string LastName { get; set; }
        [Required(ErrorMessage = "Please enter email")]
        public string Email { get; set; }
        public string Subject { get; set; }
        [Required(ErrorMessage = "Please enter phone number")]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$",
        ErrorMessage = "Please enter a valid 10 digit phone number.")]
        public string Phone { get; set; }
        public int OwnerId { get; set; }
        public string Message { get; set; }
    }
}
