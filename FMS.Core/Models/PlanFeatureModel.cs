﻿using System.Collections.Generic;

namespace FMS.Core.Models
{
    public class PlanFeatureModel : BaseModel
    {
        public int PlanId { get; set; }
        public string Feature { get; set; }
        public int? OwnerId { get; set; }
        public int SitePortalOwnerId { get; set; }
        public virtual List<ListItemModel> LstFeature { get; set; }
    }
}
