﻿using FMS.Core.Enums;
using System;

namespace FMS.Core.Models.Subscription
{
    public class UserSubscriptionModel:BaseModel
    {
        public int PlanId { get; set; }
        public int UserId { get; set; }

        public decimal PricePaid { get; set; }
        public decimal PlanPrice { get; set; }
        public int PlanLengthDays { get; set; }
        public DateTime PlanExpirationDateTime { get; set; }
        public int RemainingSubscriptionExpirationDays { get; set; }
        public bool IsTrailPeriodWarninEmailSent { get; set; }
        public bool IsCardDetailExpiredEmailSent { get; set; }
        public string ExpiredCardId { get; set; }
        public string GatewayCustomerId { get; set; }

        public int DefaultSubscriptionExpirationDays { get; set; }
        /// <summary>
        /// used for gateway subscription Id
        /// </summary>
        public string GatewaySubscriptionId { get; set; }
        public bool IsFreeTrailPeriod { get; set; }
        public bool IsPlanAutoRenewal { get; set; }

        public int NumberOfRetry { get; set; }
        public bool IsCardAddedAfterTrailExpired { get; set; }//when card detail will be added after trail period is expired
        public DateTime? ExtendedPeriodDateTime { get; set; }//date when card detail added after trail period is expired
        public bool IsPaymentPending { get; set; }
        public SubscriptionPlanStatus PlanStatus { get; set; }//pending,payment done etc
        public string Description { get; set; }
        
        /// <summary>
        /// If User Un-subscribed this is the  Reason 
        /// </summary>
        public string ReasonForUnsubscription { get; set; }
    }
}
