﻿using System.ComponentModel.DataAnnotations;

namespace FMS.Core.Models
{
    public class IngredientsModel : BaseModel
    {
        [Required(ErrorMessage = "Please ingredient name")]
        public string Name { get; set; }
        public decimal Calorie { get; set; }
        public decimal Fats { get; set; }
        public decimal Carbs { get; set; }
        public int Units { get; set; }
        public decimal Protein { get; set; }
        public decimal Fiber { get; set; }
        public decimal Sugar { get; set; }
    }
}
