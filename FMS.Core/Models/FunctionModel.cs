﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMS.Core.Models
{
    public class FunctionModel : BaseModel
    {
        public int PortalId { get; set; }
        //Can not changed 
        public string Key { get; set; }
        public string Title { get; set; }
        public string Icon { get; set; }
        public string Url { get; set; }
        public int DisplayOrder { get; set; }
        public int RoleId { get; set; }

        //Action Flag
        public bool IsCreate { get; set; }
        public bool IsView { get; set; }
        public bool IsEdit { get; set; }
        public bool IsDelete { get; set; }

    }
}
