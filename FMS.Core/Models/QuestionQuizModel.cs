﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMS.Core.Models
{
   public class QuestionQuizModel : BaseModel
    {
        public int QuestionId { get; set; }
        public int QuizId { get; set; }
    }
}
