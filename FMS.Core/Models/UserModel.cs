using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using FMS.Core.Enums;

namespace FMS.Core.Models
{
    public class UserModel : BaseModel
    {

        // public string Name { get; set; }
        [Required(ErrorMessage = "Please enter email")]
        [EmailAddress(ErrorMessage = "Please enter valid email")]
        [Remote("CheckUserEmail", "User", HttpMethod = "POST", ErrorMessage = "Email Id already exists in database.", AdditionalFields = "Id")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Please enter first name")]
        public string FirstName { get; set; }
        //[Required(ErrorMessage = "Please enter company name")]
        public string CompanyName { get; set; }
        
        [Required(ErrorMessage = "Please enter address")]
        public string Address { get; set; }
        [Required(ErrorMessage = "Please enter last name")]
        public string LastName { get; set; }
        public string Password { get; set; }
       [Required(ErrorMessage = "Please enter phone number")]
       [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$",
       ErrorMessage = "Please enter a valid 10 digit phone number.")]
        public string Phone { get; set; }
        [Required(ErrorMessage = "Please Select atleast one role")]
        public int? RoleId { get; set; }
        [Required(ErrorMessage = "Please Select atleast one Plan")]
        public int PlanId { get; set; }

        //[Required(ErrorMessage = "Please select Reseller/Client")]
        public int? MainRoleId { get; set; }
        //User For Min Role Name Like Reseller/Client
        public string Role { get; set; }
        public DateTime PlanExpirationDateTime { get; set; }
        public int RemainingSubscriptionExpirationDays { get; set; }
        public string PlanName { get; set; }

        // [Required(ErrorMessage = "Please enter name")]
        public string Username { get; set; }
        public string Code { get; set; }
        public SelectList RoleList { get; set; }
        public SelectList PlanList { get; set; }
        //Mail Role List 

        public string UserTypeString { get
            { return TypeId.ToString(); }
        }
        public List<SelectListItem> MainRoleList { get; set; }
        public bool IsCodeUsed { get; set; }
        public bool IsUpdated { get; set; }
        public int ResellerId { get; set; }
        public int UserTypeId { get; set; }
        public int ? OwnerId { get; set; }
        [Required, Range(1, int.MaxValue, ErrorMessage = "Please select user type")]
        public TraineeType TypeId { get; set; }
       
        public int SitePortalOwnerId { get; set; }
        
        public string RoleName { get; set; }
        [Required(ErrorMessage = "Please enter site key")]
        ///[MaxLength(10, ErrorMessage = "The field SiteKey must be a Text  with a maximum length of '10'.")]
        [RegularExpression(@"^(?!\d+$)(?:[a-zA-Z0-9][a-zA-Z0-9 @&$]*)?$", ErrorMessage = "White space,special character are not allowed")]
        [Remote("CheckSiteKey", "Client", HttpMethod = "POST", ErrorMessage = "SiteKey already exists in database.")]
        public string SiteKey { get; set; }

       

        //public string FullName { get; set; }


        public string FullName
        {
            get
            {
                return FirstName + " " + LastName;
            }
        }
        public SelectList UserTypeList { get; set; }
        public SelectList ResellerList { get; set; }

        #region UserProgram
        public int[] ProgramIdArray { get; set; }
        public MultiSelectList LstPrograms { get; set; }
        public int Programs { get; set; }
        public int? ProgramId { get; set; }

        #endregion
    }

    public class RegisterModel
    {
        [Required(ErrorMessage = "Please enter email")]
        [EmailAddress(ErrorMessage = "Please enter valid email")]
        [Remote("CheckEmail", "Account", HttpMethod = "POST", ErrorMessage = "Email Id already exists in database.")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Password")]
        public string Password { get; set; }


        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public UserRole UserRole { get; set; }

        public string Id { get; set; }
        public string Code { get; set; }
    }
    
}