﻿using System.Collections.Generic;

namespace FMS.Core.Models
{
    public class LandingViewModel
    {
        public virtual List<PlanModel> LstPlan { get; set; }
        public virtual List<ClientSliderViewModel> LstSlider { get; set; }
        public string Key { get; set; }
        public int Client { get; set; }
        public int Trainer { get; set; }
        public int Years { get; set; }

    }
}
