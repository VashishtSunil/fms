﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace FMS.Core.Models
{

    public class ApiTokenModel : Payloads
    {
        //public int Id { get; set; }
        public string ClientId { get; set; }
        public string TokenId { get; set; }
        public DateTime ValidUntil { get; set; }

        //public string CreatedBy { get; set; }
        //public string ModifiedBy { get; set; }
        

    }
}
