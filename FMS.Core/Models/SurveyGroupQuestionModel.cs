﻿using FMS.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FMS.Core.Models
{
    public class SurveyGroupQuestionModel : BaseModel
    {
        public int SurveyId { get; set; }
        public int GroupId { get; set; }
        [Required(ErrorMessage ="Please select question")]
        public int? QuestionId { get; set; }
        public SelectList LstQuestion { get; set; }
    }
}
