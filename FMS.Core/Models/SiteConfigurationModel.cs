﻿namespace FMS.Core.Models
{
    public class SiteConfigurationModel:BaseModel
    {
        public string Key { get; set; }
        public string Value { get; set; }

        public string Description { get; set; }
    }
}
