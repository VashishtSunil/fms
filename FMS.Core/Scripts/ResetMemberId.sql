select * from Members

--query for reset the identity column
-- now the first inserted record Id will be set as 100000001 
DBCC CHECKIDENT ('Members', RESEED, 1000)  