import 'package:connectivity/connectivity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_overlay_loader/flutter_overlay_loader.dart';
import 'package:fms_mobile/auth_screens/login.dart';
import 'package:fms_mobile/helper/apiResponseHandler.dart';
import 'package:fms_mobile/helper/utility.dart';
import 'package:fms_mobile/services/authenticateService.dart';

class Gym extends StatefulWidget {
  final Color? primaryColor;
  final Color? backgroundColor;
  final AssetImage backgroundImage;
  Gym({
    Key? key,
    this.primaryColor = Colors.pinkAccent,
    this.backgroundColor = Colors.white,
    this.backgroundImage = const AssetImage("assets/images/login_bg.jpg"),
  });

  @override
  _Gym createState() => new _Gym();
}

// ignore: must_be_immutable
class _Gym extends State<Gym> {
  final _formKey = GlobalKey<FormState>();
  var _apiResponse;
  final _gymkeyCtrl = TextEditingController();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  bool _obscureText = false;

  void _handleSubmitted(BuildContext context) async {
    final FormState? form = _formKey.currentState;
    Loader.show(context,
        isSafeAreaOverlay: false,
        isAppbarOverlay: true,
        isBottomBarOverlay: false,
        progressIndicator: CircularProgressIndicator(),
        themeData: Theme.of(context).copyWith(accentColor: Colors.black38),
        overlayColor: Color(0x99E8EAF6));
    if (!form!.validate()) {
      Utility.snackbarWidget(context, 'Please enter valid key.', Colors.white);
    } else {
      var connectivityResult = await (Connectivity().checkConnectivity());
      if (connectivityResult == ConnectivityResult.mobile ||
          connectivityResult == ConnectivityResult.wifi) {
        form.save();
        _apiResponse = await isGymTokenValid(_gymkeyCtrl.text);
        print(_apiResponse);
        var apiStatusResp = _apiResponse as ApiStatus;
        var apiStatus = apiStatusResp.toJson();
        if (apiStatus['Status']) {
          Navigator.pushReplacement(
              context,
              new MaterialPageRoute(
                builder: (ctxt) => Login(),
              ));
        } else {
          Utility.snackbarWidget(context, apiStatus['Message'], Colors.white);
        }
      } else {
        Utility.snackbarWidget(context, Utility.noNetwork, Colors.white);
      }
    }
  }

  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      resizeToAvoidBottomInset: false,
      body: SingleChildScrollView(
        child: new Container(
            height: MediaQuery.of(context).size.height,
            decoration: BoxDecoration(
              color: widget.backgroundColor,
            ),
            child: Form(
              autovalidateMode: AutovalidateMode.disabled,
              key: _formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Expanded(
                    child: new ClipPath(
                      clipper: MyClipper(),
                      child: Container(
                        height: MediaQuery.of(context).size.height,
                        decoration: BoxDecoration(
                          image: new DecorationImage(
                            image: widget.backgroundImage,
                            fit: BoxFit.cover,
                          ),
                        ),
                        alignment: Alignment.center,
                        padding: EdgeInsets.only(top: 100.0, bottom: 100.0),
                        child: Column(
                          children: <Widget>[
                            Text(
                              "FMS",
                              style: TextStyle(
                                  fontSize: 50.0,
                                  fontWeight: FontWeight.bold,
                                  color: widget.primaryColor),
                            ),
                            Text(
                              "Lorem ipsum dolor sit amet.",
                              style: TextStyle(
                                  fontSize: 20.0,
                                  fontWeight: FontWeight.bold,
                                  color: widget.primaryColor),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                        left: 20.0, right: 20.0, top: 50.0, bottom: 100.0),
                    child: TextFormField(
                      key: Key("_gymkey"),
                      controller: _gymkeyCtrl,
                      validator: (value) => (value == null || value.isEmpty)
                          ? 'Please enter valid key'
                          : null,
                      obscureText: _obscureText, //hide text
                      decoration: InputDecoration(
                        labelText: 'Key',
                        prefixIcon: Icon(Icons.lock),
                        suffixIcon: IconButton(
                          onPressed: () => _handleSubmitted(context),
                          icon: Icon(Icons.arrow_forward),
                        ),
                        hintText: "Enter key",
                      ),
                    ),
                  ),
                ],
              ),
            )),
      ),
    );
  }

  @override
  void dispose() {
    Loader.hide();
    super.dispose();
  }
}

class MyClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path p = new Path();
    p.lineTo(size.width, 0.0);
    p.lineTo(size.width, size.height * 0.85);
    p.arcToPoint(
      Offset(0.0, size.height * 0.85),
      radius: const Radius.elliptical(50.0, 10.0),
      rotation: 0.0,
    );
    p.lineTo(0.0, 0.0);
    p.close();
    return p;
  }

  @override
  bool shouldReclip(CustomClipper oldClipper) {
    return true;
  }
}
