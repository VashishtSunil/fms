import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fms_mobile/auth_screens/login.dart';
import 'package:fms_mobile/auth_screens/registerSuccess.dart';
import 'package:fms_mobile/helper/apiResponseHandler.dart';
import 'package:fms_mobile/helper/utility.dart';
import 'package:fms_mobile/services/authenticateService.dart';
import 'package:flutter_overlay_loader/flutter_overlay_loader.dart';
import 'package:connectivity/connectivity.dart';

class Register extends StatefulWidget {
  final Color? primaryColor;
  final Color? backgroundColor;
  final AssetImage backgroundImage;
  Register({
    Key? key,
    this.primaryColor = Colors.pinkAccent,
    this.backgroundColor = Colors.white,
    this.backgroundImage = const AssetImage("assets/images/login_bg.jpg"),
  });

  @override
  _Register createState() => _Register();
}

class _Register extends State<Register> {
  final _formKey = GlobalKey<FormState>();
  final _fnameCtrl = TextEditingController();
  final _lnameCtrl = TextEditingController();
  final _emailCtrl = TextEditingController();
  final _phoneCtrl = TextEditingController();
  final _addressCtrl = TextEditingController();

  void _handleSubmitted(BuildContext context) async {
    final FormState? form = _formKey.currentState;
    Loader.show(context,
        isSafeAreaOverlay: false,
        isAppbarOverlay: true,
        isBottomBarOverlay: false,
        progressIndicator: CircularProgressIndicator(),
        themeData: Theme.of(context).copyWith(accentColor: Colors.black38),
        overlayColor: Color(0x99E8EAF6));
    if (form!.validate()) {
      var connectivityResult = await (Connectivity().checkConnectivity());
      if (connectivityResult == ConnectivityResult.mobile ||
          connectivityResult == ConnectivityResult.wifi) {
        bool isDuplictaeEmail = await isEmailDuplicate(_emailCtrl.text);
        print(isDuplictaeEmail);
        if (isDuplictaeEmail) {
          Utility.snackbarWidget(context,
              'Email is already registered. Please login.', Colors.white);
        } else {
          form.save();
          var resp = await registerUser(_fnameCtrl.text, _lnameCtrl.text,
              _emailCtrl.text, _phoneCtrl.text, _addressCtrl.text);
          var apiStatusResp = resp.ApiStatus as ApiStatus;
          var apiStatus = apiStatusResp.toJson();
          if (apiStatus['Status']) {
            Navigator.pushReplacement(
                context,
                new MaterialPageRoute(
                  builder: (ctxt) => RegisterSuccess(),
                ));
          } else {
            Utility.snackbarWidget(context, Utility.errSWW, Colors.white);
          }
        }
      } else {
        Utility.snackbarWidget(context, Utility.noNetwork, Colors.white);
      }
    } else {
      Utility.snackbarWidget(context, 'Please check errors.', Colors.white);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        physics: NeverScrollableScrollPhysics(),
        child: new Container(
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
            color: widget.backgroundColor,
          ),
          child: Form(
            autovalidateMode: AutovalidateMode.disabled,
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Expanded(
                  child: new ClipPath(
                    clipper: MyClipper(),
                    child: Container(
                      decoration: BoxDecoration(
                        image: new DecorationImage(
                          image: widget.backgroundImage,
                          fit: BoxFit.cover,
                        ),
                      ),
                      alignment: Alignment.center,
                      padding: EdgeInsets.only(top: 100.0, bottom: 80.0),
                      child: Column(
                        children: <Widget>[
                          Text(
                            "FMS",
                            style: TextStyle(
                                fontSize: 50.0,
                                fontWeight: FontWeight.bold,
                                color: widget.primaryColor),
                          ),
                          Text(
                            "Lorem ipsum dolor sit amet.",
                            style: TextStyle(
                                fontSize: 20.0,
                                fontWeight: FontWeight.bold,
                                color: widget.primaryColor),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: TextFormField(
                    key: Key("_fname"),
                    controller: _fnameCtrl,
                    validator: (value) => (value == null || value.isEmpty)
                        ? 'Please enter firstname'
                        : null,
                    decoration: InputDecoration(
                      labelText: 'Firstname',
                      prefixIcon: Icon(Icons.person),
                      hintText: "Enter Firstname",
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      left: 20.0, right: 20.0, bottom: 20.0),
                  child: TextFormField(
                      key: Key("_lname"),
                      controller: _lnameCtrl,
                      validator: (value) => (value == null || value.isEmpty)
                          ? 'Please enter lastname'
                          : null,
                      decoration: InputDecoration(
                        labelText: 'Lastname',
                        prefixIcon: Icon(Icons.person),
                        hintText: "Enter Lastname",
                      )),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      left: 20.0, right: 20.0, bottom: 20.0),
                  child: TextFormField(
                    key: Key("_email"),
                    controller: _emailCtrl,
                    validator: (value) => (value == null ||
                            value.isEmpty ||
                            !RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                                .hasMatch(value))
                        ? 'Please enter valid email'
                        : null,
                    decoration: InputDecoration(
                      labelText: 'Email',
                      prefixIcon: Icon(Icons.email),
                      hintText: "Enter Email",
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      left: 20.0, right: 20.0, bottom: 20.0),
                  child: TextFormField(
                    key: Key("_phone"),
                    controller: _phoneCtrl,
                    validator: (value) => (value == null ||
                            value.isEmpty ||
                            !RegExp(r"^[6-9]\d{9}$").hasMatch(value))
                        ? 'Please enter valid number'
                        : null,
                    decoration: InputDecoration(
                      labelText: 'Phone',
                      prefixIcon: Icon(Icons.phone),
                      hintText: "Enter Phone",
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      left: 20.0, right: 20.0, bottom: 20.0),
                  child: TextFormField(
                    key: Key("_address"),
                    controller: _addressCtrl,
                    validator: (value) => (value == null || value.isEmpty)
                        ? 'Please enter address'
                        : null,
                    decoration: InputDecoration(
                      labelText: 'Address',
                      prefixIcon: Icon(Icons.map_rounded),
                      hintText: "Enter Address",
                    ),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(top: 10.0),
                  padding: const EdgeInsets.only(left: 20.0, right: 20.0),
                  child: new Row(
                    children: <Widget>[
                      new Expanded(
                          child: ElevatedButton.icon(
                        icon: Icon(Icons.arrow_forward, color: Colors.white),
                        label: Text("Register"),
                        onPressed: () {
                          _handleSubmitted(context);
                        },
                        style: ElevatedButton.styleFrom(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(28.0),
                            ),
                            primary: widget.primaryColor),
                      )),
                    ],
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(top: 10.0),
                  padding: const EdgeInsets.only(left: 20.0, right: 20.0),
                  child: new Row(
                    children: <Widget>[
                      new Expanded(
                        child: TextButton(
                          style: TextButton.styleFrom(
                            primary: Colors.red,
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(30.0)),
                          ),
                          child: Container(
                            padding: const EdgeInsets.only(left: 20.0),
                            alignment: Alignment.center,
                            child: Text(
                              "ALREADY HAVE AN ACCOUNT?",
                              style: TextStyle(color: widget.primaryColor),
                            ),
                          ),
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => Login()),
                            );
                          }, //action
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    Loader.hide();
    super.dispose();
  }
}

class MyClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path p = new Path();
    p.lineTo(size.width, 0.0);
    p.lineTo(size.width, size.height * 0.85);
    p.arcToPoint(
      Offset(0.0, size.height * 0.85),
      radius: const Radius.elliptical(50.0, 10.0),
      rotation: 0.0,
    );
    p.lineTo(0.0, 0.0);
    p.close();
    return p;
  }

  @override
  bool shouldReclip(CustomClipper oldClipper) {
    return true;
  }
}
