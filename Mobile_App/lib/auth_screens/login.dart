import 'package:connectivity/connectivity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_overlay_loader/flutter_overlay_loader.dart';
import 'package:fms_mobile/auth_screens/register.dart';
import 'package:fms_mobile/helper/utility.dart';
import 'package:fms_mobile/services/authenticateService.dart';
import 'package:fms_mobile/views/home.dart';

class Login extends StatefulWidget {
  final Color? primaryColor;
  final Color? backgroundColor;
  final AssetImage backgroundImage;
  Login({
    Key? key,
    this.primaryColor = Colors.pinkAccent,
    this.backgroundColor = Colors.white,
    this.backgroundImage = const AssetImage("assets/images/login_bg.jpg"),
  });

  @override
  _Login createState() => new _Login();
}

// ignore: must_be_immutable
class _Login extends State<Login> {
  final _formKey = GlobalKey<FormState>();
  var _apiResponse;
  final _passwordCtrl = TextEditingController();
  final _usernameCtrl = TextEditingController();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  bool _obscureText = true;

  void _togglePasswordVisibility() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  void _handleSubmitted(BuildContext context) async {
    final FormState? form = _formKey.currentState;
    Loader.show(context,
        isSafeAreaOverlay: false,
        isAppbarOverlay: true,
        isBottomBarOverlay: false,
        progressIndicator: CircularProgressIndicator(),
        themeData: Theme.of(context).copyWith(accentColor: Colors.black38),
        overlayColor: Color(0x99E8EAF6));
    if (!form!.validate()) {
      Utility.snackbarWidget(
          context, 'Please enter valid email and password.', Colors.white);
    } else {
      var connectivityResult = await (Connectivity().checkConnectivity());
      if (connectivityResult == ConnectivityResult.mobile ||
          connectivityResult == ConnectivityResult.wifi) {
        form.save();
        _apiResponse =
            await authenticateUser(_usernameCtrl.text, _passwordCtrl.text);
        if (_apiResponse.ApiError != null) {
          var apiError = _apiResponse.ApiError.toJson();
          Utility.snackbarWidget(context, apiError["Message"], Colors.white);
        } else {
          Navigator.pushReplacement(
              context,
              new MaterialPageRoute(
                builder: (ctxt) => Home(title: "Home"),
              ));
        }
      } else {
        Utility.snackbarWidget(
            context, 'No network. Please check your internet', Colors.white);
      }
    }
  }

  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      resizeToAvoidBottomInset: false,
      body: SingleChildScrollView(
        child: new Container(
            height: MediaQuery.of(context).size.height,
            decoration: BoxDecoration(
              color: widget.backgroundColor,
            ),
            child: Form(
              autovalidateMode: AutovalidateMode.disabled,
              key: _formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Expanded(
                    child: new ClipPath(
                      clipper: MyClipper(),
                      child: Container(
                        decoration: BoxDecoration(
                          color: Color.fromRGBO(58, 66, 86, .8),
                          image: new DecorationImage(
                            image: widget.backgroundImage,
                            fit: BoxFit.cover,
                          ),
                        ),
                        alignment: Alignment.center,
                        padding: EdgeInsets.only(top: 100.0, bottom: 100.0),
                        child: Column(
                          children: <Widget>[
                            Text(
                              "FMS",
                              style: TextStyle(
                                  fontSize: 50.0,
                                  fontWeight: FontWeight.bold,
                                  color: widget.primaryColor),
                            ),
                            Text(
                              "Lorem ipsum dolor sit amet.",
                              style: TextStyle(
                                  fontSize: 20.0,
                                  fontWeight: FontWeight.bold,
                                  color: widget.primaryColor),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20.0),
                    child: TextFormField(
                      key: Key("_username"),
                      controller: _usernameCtrl,
                      validator: (value) => (value == null ||
                              value.isEmpty ||
                              !RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                                  .hasMatch(value))
                          ? 'Please enter valid email'
                          : null,
                      decoration: InputDecoration(
                        labelText: 'Email',
                        prefixIcon: Icon(Icons.person),
                        hintText: "Enter email",
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 20.0, vertical: 20.0),
                    child: TextFormField(
                      key: Key("_password"),
                      controller: _passwordCtrl,
                      validator: (value) => (value == null || value.isEmpty)
                          ? 'Please enter password'
                          : null,
                      obscureText: _obscureText, //hide text
                      decoration: InputDecoration(
                        labelText: 'Password',
                        prefixIcon: Icon(Icons.lock),
                        suffixIcon: IconButton(
                          onPressed: _togglePasswordVisibility,
                          icon: !_obscureText
                              ? Icon(Icons.visibility)
                              : Icon(Icons.visibility_off),
                        ),
                        hintText: "Enter password",
                      ),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 20.0),
                    padding: const EdgeInsets.only(left: 20.0, right: 20.0),
                    child: new Row(
                      children: <Widget>[
                        new Expanded(
                            child: ElevatedButton.icon(
                          icon: Icon(Icons.arrow_forward, color: Colors.white),
                          label: Text("LOGIN "),
                          onPressed: () => _handleSubmitted(context),
                          style: ElevatedButton.styleFrom(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(28.0),
                              ),
                              primary: widget.primaryColor),
                        )),
                      ],
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 20.0),
                    padding: const EdgeInsets.only(left: 20.0, right: 20.0),
                    child: new Row(
                      children: <Widget>[
                        new Expanded(
                          child: TextButton(
                            style: TextButton.styleFrom(
                              primary: Colors.red,
                              shape: new RoundedRectangleBorder(
                                  borderRadius:
                                      new BorderRadius.circular(30.0)),
                            ),
                            child: Container(
                              padding: const EdgeInsets.only(left: 20.0),
                              alignment: Alignment.center,
                              child: Text(
                                "DON'T HAVE AN ACCOUNT?",
                                style: TextStyle(color: widget.primaryColor),
                              ),
                            ),
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => Register()),
                              );
                            }, //action
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            )),
      ),
    );
  }

  @override
  void dispose() {
    Loader.hide();
    super.dispose();
  }
}

class MyClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path p = new Path();
    p.lineTo(size.width, 0.0);
    p.lineTo(size.width, size.height * 0.85);
    p.arcToPoint(
      Offset(0.0, size.height * 0.85),
      radius: const Radius.elliptical(50.0, 10.0),
      rotation: 0.0,
    );
    p.lineTo(0.0, 0.0);
    p.close();
    return p;
  }

  @override
  bool shouldReclip(CustomClipper oldClipper) {
    return true;
  }
}
