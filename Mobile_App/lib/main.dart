import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:fms_mobile/helper/apiResponseHandler.dart';
import 'package:fms_mobile/services/authenticateService.dart';
import 'package:fms_mobile/views/home.dart';
import 'package:is_first_run/is_first_run.dart';
import 'auth_screens/gymKey.dart';
import 'auth_screens/login.dart';
import '../enum/authentication.dart';
import 'noInternet.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'FMS',
      theme: new ThemeData(
        primarySwatch: Colors.pink,
        primaryColor: Colors.pinkAccent,
        inputDecorationTheme: inputDecorationTheme(),
      ),
      home: HomePage(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => new _HomePageState();
}

class _HomePageState extends State<HomePage> {
  late final Future<List<dynamic>> _future;
  var colorizeColors = [
    Colors.purple,
    Colors.white,
    Colors.yellow,
    Colors.red,
  ];

  var colorizeTextStyle = TextStyle(
    fontSize: 50.0,
    fontFamily: 'alison',
  );

  Future<void> isFirstRunClearLocalStorage() async {
    bool isFirstRun = await IsFirstRun.isFirstRun();
    if (isFirstRun) {
      await storage.deleteAll();
    }
  }

  initState() {
    super.initState();
    isFirstRunClearLocalStorage();
    _future = Future.wait([
      storage.read(key: authentication.gymKey.toShortString()),
      Connectivity().checkConnectivity(),
      isTokenValid(),
    ]);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: FutureBuilder(
            builder: (context, AsyncSnapshot<List<dynamic>> snapshot) {
              switch (snapshot.connectionState) {
                case ConnectionState.waiting:
                  return Container(
                    height: MediaQuery.of(context).size.height,
                    decoration: BoxDecoration(
                      color: Colors.pinkAccent,
                    ),
                    child: Center(
                        child: AnimatedTextKit(
                      animatedTexts: [
                        ColorizeAnimatedText(
                          'Go Hard',
                          textStyle: colorizeTextStyle,
                          colors: colorizeColors,
                        ),
                        ColorizeAnimatedText(
                          'Or Go Home',
                          textStyle: colorizeTextStyle,
                          colors: colorizeColors,
                        ),
                      ],
                      isRepeatingAnimation: true,
                      onTap: () {},
                    )),
                  );
                default:
                  return (snapshot.data![1] == ConnectivityResult.mobile ||
                          snapshot.data![1] ==
                              ConnectivityResult
                                  .wifi) //chekcing network connectivity
                      ? (snapshot.data![0] ==
                              null) //checking if gym key is present
                          ? Gym()
                          : (snapshot.data![2] ==
                                  true) // checking if the user is already loggedin
                              ? Home(title: "FMS")
                              : Login()
                      : NoInternet();
              }
            },
            future: _future));
  }
}

InputDecorationTheme inputDecorationTheme() {
  return InputDecorationTheme(
    hintStyle: TextStyle(color: Colors.grey),
    labelStyle: TextStyle(color: Colors.pink),
    enabledBorder: OutlineInputBorder(
      borderRadius: BorderRadius.all(Radius.circular(20.0)),
      borderSide: BorderSide(color: Colors.grey, width: 2.0),
    ),
    focusedBorder: OutlineInputBorder(
      borderRadius: BorderRadius.all(Radius.circular(20.0)),
      borderSide: BorderSide(color: Colors.pinkAccent, width: 2.0),
    ),
    errorBorder: OutlineInputBorder(
      borderRadius: BorderRadius.all(Radius.circular(20.0)),
      borderSide: BorderSide(color: Colors.red, width: 2.0),
    ),
    focusedErrorBorder: OutlineInputBorder(
      borderRadius: BorderRadius.all(Radius.circular(20.0)),
      borderSide: BorderSide(color: Colors.red, width: 2.0),
    ),
  );
}
