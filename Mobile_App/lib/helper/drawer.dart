import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:fms_mobile/auth_screens/login.dart';
import 'package:fms_mobile/services/authenticateService.dart';

class CustomDrawerWidget extends StatefulWidget {
  _CustomDrawerWidgetState createState() => _CustomDrawerWidgetState();
}

class _CustomDrawerWidgetState extends State<CustomDrawerWidget> {
  var _future;
  void initState() {
    super.initState();
    final storage = new FlutterSecureStorage();
    _future = storage.read(key: 'username');
  }

  @override
  Widget build(BuildContext context) {
    return new Drawer(
      child: FutureBuilder(
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            var data = snapshot.data as String;
            return new ListView(
              // Important: Remove any padding from the ListView.
              padding: EdgeInsets.zero,
              children: [
                DrawerHeader(
                  decoration: BoxDecoration(
                    color: Colors.pinkAccent,
                  ),
                  child: Text(
                    data,
                    style: TextStyle(
                        fontStyle: FontStyle.italic,
                        color: Colors.white,
                        fontSize: 22),
                  ),
                ),
                ListTile(
                  leading: Icon(Icons.logout),
                  title: const Text('Logout'),
                  onTap: () async {
                    logout();
                    Navigator.pop(context);
                    Navigator.pushReplacement(context,
                        new MaterialPageRoute(builder: (ctxt) => new Login()));
                  },
                ),
              ],
            );
          } else {
            return new Text("loading");
          }
        },
        future: _future,
      ),
    );
  }
}
