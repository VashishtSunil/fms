//Api response hanlder
class ApiResponse {
  // _data will hold any response converted into
  // its own object. For example user.
  Object? _data;
  // _apiError will hold the error object
  Object? _apiError;

  Object? _apiStatus;

  Object? get Data => _data;
  // ignore: non_constant_identifier_names
  set Data(Object? data) => _data = data;

  Object? get ApiError => _apiError;
  set ApiError(Object? error) => _apiError = error;

  Object? get ApiStatus => _apiStatus;
  set ApiStatus(Object? status) => _apiStatus = status;
}

// Api error handler

class ApiError {
  late String _error;

  ApiError({required String error}) {
    this._error = error;
  }

  ApiError.fromJson(Map<String, dynamic> json) {
    _error = json['Message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Message'] = this._error;
    return data;
  }
}

class ApiStatus {
  late String _message;
  late bool _status;

  ApiStatus({required String message, required bool status}) {
    this._message = message;
    this._status = status;
  }

  ApiStatus.fromJson(Map<String, dynamic> json) {
    _message = json['Message'];
    _status = json['Status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Message'] = this._message;
    data['Status'] = this._status;
    return data;
  }
}
