import 'package:fab_circular_menu/fab_circular_menu.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:fms_mobile/auth_screens/login.dart';
import 'package:fms_mobile/auth_screens/register.dart';
import 'package:fms_mobile/landing.dart';

class CustomFabWidget extends StatefulWidget {
  _CustomFabWidgetState createState() => _CustomFabWidgetState();
}

class _CustomFabWidgetState extends State<CustomFabWidget> {
  var _future;
  void initState() {
    super.initState();
    final storage = new FlutterSecureStorage();
    _future = storage.read(key: 'token');
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          var data = snapshot.data as String;
          return new FabCircularMenu(
              fabOpenColor: Colors.pinkAccent,
              fabCloseColor: Colors.pinkAccent,
              ringColor: Colors.pinkAccent,
              ringDiameter: 300,
              ringWidth: 50,
              children: <Widget>[
                IconButton(
                    icon: Icon(Icons.logout),
                    onPressed: () async {
                      final storage = new FlutterSecureStorage();
                      await storage.delete(key: "token");
                      Navigator.pop(context);
                      Navigator.push(
                          context,
                          new MaterialPageRoute(
                              builder: (ctxt) =>
                                  new Landing(title: "Landing")));
                    }),
              ]);
        } else {
          print("data22");
          return new FabCircularMenu(
              fabOpenColor: Colors.pinkAccent,
              fabCloseColor: Colors.pinkAccent,
              ringColor: Colors.pinkAccent,
              ringDiameter: 300,
              ringWidth: 50,
              children: <Widget>[
                IconButton(
                    icon: Icon(Icons.login),
                    onPressed: () {
                      Navigator.pop(context);
                      Navigator.push(
                          context,
                          new MaterialPageRoute(
                              builder: (ctxt) => new Login()));
                    }),
                IconButton(
                    icon: Icon(Icons.app_registration),
                    onPressed: () {
                      Navigator.pop(context);
                      Navigator.push(
                          context,
                          new MaterialPageRoute(
                              builder: (ctxt) => new Register()));
                    })
              ]);
        }
      },
      future: _future,
    );
  }
}
