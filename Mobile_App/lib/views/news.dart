import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:fms_mobile/helper/apiResponseHandler.dart';
import 'package:fms_mobile/helper/drawer.dart';
import 'package:fms_mobile/helper/utility.dart';
import 'package:fms_mobile/models/newsModel.dart';
import 'package:fms_mobile/services/newsService.dart';

class NewsView extends StatefulWidget {
  final String title;
  final Color? primaryColor;
  final Color? backgroundColor;
  final int newsId;
  NewsView({
    Key? key,
    required this.title,
    this.primaryColor = Colors.pinkAccent,
    this.backgroundColor = Colors.white,
    required this.newsId,
  }) : super(key: key);
  @override
  _News createState() => _News();
}

class _News extends State<NewsView> {
  late final Future<dynamic> _news;
  initState() {
    super.initState();
    _news = NewsService.getNews(widget.newsId.toString());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
          backgroundColor: widget.primaryColor,
        ),
        body: FutureBuilder(
            builder: (context, snapshot) {
              Widget widgetView;
              if (snapshot.hasData) {
                var apiResp = snapshot.data as ApiResponse;
                if (apiResp.ApiError != null) {
                  var errs = apiResp.ApiError as ApiError;
                  var err = errs.toJson();
                  widgetView = Utility.errorWidget(err['Message'].toString());
                } else {
                  var newsData = apiResp.Data as News;
                  var news = newsData.toJson();
                  var imgBase64String = (news['attachmentImg'] != null)
                      ? news['attachmentImg'].split("base64,")[1]
                      : Utility.demoImgB64;

                  final topContentText = Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Text(
                        news['title'],
                        style: TextStyle(color: Colors.white, fontSize: 30.0),
                      ),
                      SizedBox(height: 30.0),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Expanded(
                              flex: 1,
                              child: Column(
                                children: [
                                  Icon(
                                    Icons.publish,
                                    color: Colors.pinkAccent,
                                    size: 30.0,
                                  ),
                                  Text(
                                    news['publishDate'],
                                    style: TextStyle(
                                      color: Colors.white,
                                    ),
                                  )
                                ],
                              )),
                          Expanded(
                              flex: 1,
                              child: Padding(
                                  padding: EdgeInsets.only(left: 10.0),
                                  child: Text(
                                    news['title'],
                                    style: TextStyle(color: Colors.white),
                                  ))),
                          Expanded(
                              flex: 1,
                              child: Text(
                                news['title'],
                                style: TextStyle(color: Colors.white),
                              ))
                        ],
                      ),
                    ],
                  );

                  final topContent = Stack(
                    children: <Widget>[
                      Image.memory(
                        base64Decode(imgBase64String),
                        fit: BoxFit.cover,
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.height / 2,
                      ),
                      Container(
                        height: MediaQuery.of(context).size.height / 2,
                        padding: EdgeInsets.all(40.0),
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                            color: Color.fromRGBO(58, 66, 86, .8)),
                        child: Center(
                          child: topContentText,
                        ),
                      ),
                    ],
                  );

                  final bottomContent = Container(
                      //color: Theme.of(context).primaryColor,
                      padding: EdgeInsets.all(40.0),
                      child: Center(
                          child: Html(data: news['description'], style: {
                        "p": Style(fontSize: FontSize.rem(1.3)),
                      })));

                  widgetView = SingleChildScrollView(
                      child: Column(
                    children: <Widget>[topContent, bottomContent],
                  ));
                }
              } else if (snapshot.hasError) {
                //error widget
                widgetView = Utility.errorWidget(snapshot.error.toString());
              } else {
                //loader widget
                widgetView = Utility.loaderWidget('Loading ${widget.title}  ');
              }

              return widgetView;
            },
            future: _news),
        drawer: CustomDrawerWidget());
  }
}
