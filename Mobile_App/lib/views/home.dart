import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:fms_mobile/helper/apiResponseHandler.dart';
import 'package:fms_mobile/helper/drawer.dart';
import 'package:fms_mobile/helper/utility.dart';
import 'package:fms_mobile/models/newsModel.dart';
import 'package:fms_mobile/services/newsService.dart';

import 'news.dart';

class Home extends StatefulWidget {
  final String title;
  final Color? primaryColor;
  final Color? backgroundColor;
  Home({
    Key? key,
    required this.title,
    this.primaryColor = Colors.pinkAccent,
    this.backgroundColor = Colors.white,
  }) : super(key: key);
  @override
  _Home createState() => _Home();
}

class _Home extends State<Home> {
  late final Future<dynamic> _future;
  initState() {
    super.initState();
    _future = NewsService.getAllNews();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
          backgroundColor: widget.primaryColor,
        ),
        body: FutureBuilder(
            builder: (context, snapshot) {
              Widget widgetView;
              if (snapshot.hasData) {
                var apiResp = snapshot.data as ApiResponse;
                var newsList;
                if (apiResp.Data != null) {
                  newsList = apiResp.Data as List<News>;
                }
                var err;
                if (apiResp.ApiError != null) {
                  var errs = apiResp.ApiError as ApiError;
                  err = errs.toJson();
                }

                widgetView = Column(
                  children: [
                    Container(
                        height: MediaQuery.of(context).size.height / 3,
                        child: _imageSlider(context)),
                    if (apiResp.Data != null)
                      Container(
                          height: MediaQuery.of(context).size.height / 2,
                          child: _scrollList(newsList)),
                    if (apiResp.ApiError != null)
                      Container(
                        height: MediaQuery.of(context).size.height / 2,
                        child: Padding(
                            padding: EdgeInsets.only(top: 16),
                            child: Utility.errorWidget(err["Message"])),
                      )
                  ],
                );
              } else if (snapshot.hasError) {
                widgetView = Utility.errorWidget(snapshot.error.toString());
              } else {
                //loader widget
                widgetView = Utility.loaderWidget('Loading ${widget.title}  ');
              }
              //return widgetView;
              return Container(
                  height: MediaQuery.of(context).size.height / 3,
                  child: _imageSlider(context));
            },
            future: _future),
        drawer: CustomDrawerWidget());
  }
}

// slider widget

final List<String> imgList = [
  'https://images.unsplash.com/photo-1520342868574-5fa3804e551c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=6ff92caffcdd63681a35134a6770ed3b&auto=format&fit=crop&w=1951&q=80',
  'https://images.unsplash.com/photo-1522205408450-add114ad53fe?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=368f45b0888aeb0b7b08e3a1084d3ede&auto=format&fit=crop&w=1950&q=80',
  'https://images.unsplash.com/photo-1519125323398-675f0ddb6308?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=94a1e718d89ca60a6337a6008341ca50&auto=format&fit=crop&w=1950&q=80',
  'https://images.unsplash.com/photo-1523205771623-e0faa4d2813d?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=89719a0d55dd05e2deae4120227e6efc&auto=format&fit=crop&w=1953&q=80',
  'https://images.unsplash.com/photo-1508704019882-f9cf40e475b4?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=8c6e5e3aba713b17aa1fe71ab4f0ae5b&auto=format&fit=crop&w=1352&q=80',
  'https://images.unsplash.com/photo-1519985176271-adb1088fa94c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=a0c8d632e977f94e5d312d9893258f59&auto=format&fit=crop&w=1355&q=80'
];

Widget _imageSlider(context) {
  return new CarouselSlider(
    options: CarouselOptions(),
    items: imgList
        .map(
          (item) => Container(
              child: Image.network(item,
                  fit: BoxFit.cover, width: MediaQuery.of(context).size.width)),
        )
        .toList(),
  );
}

Widget _buildListView(news) {
  return ListView.builder(
    itemCount: news.length,
    itemBuilder: (BuildContext context, int index) {
      var newsDetails = news[index] as News;
      var nws = newsDetails.toJson();
      print(nws);
      var imgBase64String = (nws['attachmentImg'] != null)
          ? nws['attachmentImg'].split("base64,")[1]
          : Utility.demoImgB64;
      return Card(
          child: ListTile(
              leading: CircleAvatar(
                child: ClipOval(
                    child: Image.memory(base64Decode(imgBase64String))),
              ),
              title: Text(
                nws['title'],
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                softWrap: false,
              ),
              subtitle: Text(
                nws['description'],
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                softWrap: false,
              ),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => NewsView(
                            title: nws['title'],
                            newsId: nws['id'],
                          )),
                );
              }));
    },
  );
}

Widget _scrollList(news) {
  return Scrollbar(
    child: _buildListView(news),
  );
}
