class User {
  late String _userId;
  late String _name;
  late String _email;
  late String _token;

  User({
    required String userId,
    required String name,
    required String email,
    required String token,
  }) {
    this._userId = userId;
    this._name = name;
    this._email = email;
    this._token = token;
  }

  // create the user object from json input
  User.fromJson(Map<String, dynamic> json) {
    _userId = json['userId'];
    _name = json['name'];
    _email = json['email'];
    _token = json['token'];
  }

  // exports to json
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['userId'] = this._userId;
    data['name'] = this._name;
    data['email'] = this._email;
    data['token'] = this._token;
    return data;
  }
}
