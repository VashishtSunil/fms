class News {
  late int _id;
  late String _title;
  late String _description;
  late String _publishDate;
  late String _attachmentImg;
  late int _attachmentId;
  late Attachment _attachment;
  News({
    required int id,
    required String title,
    required String description,
    required String publishDate,
    required String attachmentImg,
    required int attachmentId,
    required Attachment attachment,
  }) {
    this._id = id;
    this._title = title;
    this._description = description;
    this._publishDate = publishDate;
    this._attachmentId = attachmentId;
    this._attachment = attachment;
    this._attachmentImg = attachmentImg;
  }

  // create the user object from json input
  News.fromJson(Map<String, dynamic> json) {
    _id = json['Id'];
    _title = json['Title'];
    _description = json['Description'];
    _publishDate = json['PublishDateString'];
    _attachmentId = json['AttachmentId'];
    _attachment = Attachment.fromJson(json['Attachment']);
    _attachmentImg = json['Image'];
  }

  // exports to json

  Map<String, dynamic> toJson() {
    print(this._attachment);
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['title'] = this._title;
    data['description'] = this._description;
    data['publishDate'] = this._publishDate;
    data['attachmentId'] = this._attachmentId;
    data['attachment'] = this._attachment;
    data['attachmentImg'] = this._attachmentImg;
    return data;
  }
}

class Attachment {
  late int _id;
  late String _location;
  late String _fileName;
  late String _dummyFileName;
  late dynamic _size;
  late int _mIME;
  late String _contentType;
  late String _imageBytes;

  Attachment({
    required int id,
    required String location,
    required String fileName,
    required String dummyFileName,
    required dynamic size,
    required int mIME,
    required String contentType,
    required String imageBytes,
  }) {
    this._id = id;
    this._location = location;
    this._fileName = fileName;
    this._dummyFileName = dummyFileName;
    this._size = size;
    this._mIME = mIME;
    this._contentType = contentType;
    this._imageBytes = imageBytes;
  }

  // create the user object from json input
  Attachment.fromJson(Map<String, dynamic> json) {
    _id = json['Id'];
    _location = json['Location'];
    _fileName = json['FileName'];
    _dummyFileName = json['DummyFileName'];
    _size = json['Size'];
    _mIME = json['MIME'];
    _contentType = json['ContentType'];
    _imageBytes = json['ImageBytes'] ?? "no bytes";
  }

  // exports to json
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['location'] = this._location;
    data['filename'] = this._fileName;
    data['dummyFileName'] = this._dummyFileName;
    data['size'] = this._size;
    data['mime'] = this._mIME;
    data['contentType'] = this._contentType;
    data['imageBytes'] = this._imageBytes;
    return data;
  }
}
