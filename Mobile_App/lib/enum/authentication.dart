enum authentication { token, isLoggedIn, username, gymKey, gymPortalOwnerId }

extension ParseToString on authentication {
  String toShortString() => this.toString().split('.').last;
}
