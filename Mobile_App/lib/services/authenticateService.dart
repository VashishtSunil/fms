import 'dart:convert';
import 'dart:io';
//import 'package:http/http.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';
import '../models/userModel.dart';
import '../helper/apiResponseHandler.dart';
import '../enum/authentication.dart';
import '../helper/utility.dart';

final storage = new FlutterSecureStorage();
String _baseUrl = Utility.baseUrl;
String _nullToken = Utility.nullToken;

//return ApiResponse (Data or ApiError)
Future<ApiResponse> authenticateUser(String username, String password) async {
  ApiResponse _apiResponse = new ApiResponse();
  var url = Uri.parse('${_baseUrl}account/LoginUser');
  try {
    bool trustSelfSigned = true;
    HttpClient httpClient = new HttpClient()
      ..badCertificateCallback =
          ((X509Certificate cert, String host, int port) => trustSelfSigned);
    IOClient ioClient = new IOClient(httpClient);

    final response = await ioClient.post(url, body: {
      'Email': username,
      'Password': password,
    });

    switch (response.statusCode) {
      case 200:
        _apiResponse.Data = loginSuccessHalder(json.decode(response.body));
        break;
      case 400:
        _apiResponse.ApiError =
            ApiError(error: "Please check your email or passowrd");
        break;
      case 401:
        _apiResponse.ApiError = ApiError.fromJson(json.decode(response.body));
        break;
      default:
        _apiResponse.ApiError = ApiError(error: "Server error. Please retry");
        break;
    }
  } on SocketException {
    _apiResponse.ApiError = ApiError(error: "Server error. Please retry");
  }
  return _apiResponse;
}

//login handler method
loginSuccessHalder(data) async {
  var modal = User.fromJson(data);
  var user = modal.toJson(); //parsing model
  // Create storage
  await storage.write(
      key: authentication.token.toShortString(), value: user["token"]);
  await storage.write(
      key: authentication.isLoggedIn.toShortString(), value: "true");
  await storage.write(
      key: authentication.username.toShortString(), value: user["email"]);
  return modal;
}

//return bool
Future<bool> isTokenValid() async {
  bool resp = false;
  var url = Uri.parse('${_baseUrl}account/ValidationCheck');
  try {
    bool trustSelfSigned = true;
    HttpClient httpClient = new HttpClient()
      ..badCertificateCallback =
          ((X509Certificate cert, String host, int port) => trustSelfSigned);
    IOClient ioClient = new IOClient(httpClient);

    String? token =
        await storage.read(key: authentication.token.toShortString());
    String userToken = token ??= _nullToken;
    final response = await ioClient.post(url, headers: {
      HttpHeaders.authorizationHeader: userToken,
    });
    switch (response.statusCode) {
      case 200:
        resp = true;
        break;
      case 401:
        resp = await tokenErrorHandlder(json.decode(response.body));
        break;
      default:
        resp = false;
        break;
    }
  } on SocketException {
    resp = false;
  }
  return resp;
}

tokenErrorHandlder(data) async {
  var error = ApiError.fromJson(data);
  var errors = error.toJson(); //parsing model
  var resp = false;
  if (errors.containsValue("Un-Authorized")) {
    resp = false;
  } else {
    resp = await refreshTokenValid();
  }
  return resp;
}

//return bool
Future<bool> refreshTokenValid() async {
  var url = Uri.parse('${_baseUrl}account/RefreshToken');
  var resp = false;
  try {
    bool trustSelfSigned = true;
    HttpClient httpClient = new HttpClient()
      ..badCertificateCallback =
          ((X509Certificate cert, String host, int port) => trustSelfSigned);
    IOClient ioClient = new IOClient(httpClient);
    final storage = new FlutterSecureStorage();
    String? token =
        await storage.read(key: authentication.token.toShortString());
    String userToken = token ??= _nullToken;
    final response = await ioClient.post(url, headers: {
      HttpHeaders.authorizationHeader: userToken,
    });

    switch (response.statusCode) {
      case 200:
        resp = true;
        loginSuccessHalder(json.decode(response.body));
        break;
      case 500:
        resp = false;
        break;
      default:
        resp = false;
        break;
    }
  } on SocketException {
    resp = false;
  }
  return resp;
}

Future<void> logout() async {
  final storage = new FlutterSecureStorage();
  await storage.delete(key: authentication.token.toShortString());
}

//return ApiStatus
Future<ApiResponse> registerUser(String fname, String lname, String email,
    String phone, String address) async {
  var apiResp;
  ApiResponse _apiResponse = new ApiResponse();
  var url = Uri.parse('${_baseUrl}trainee/Add');
  try {
    bool trustSelfSigned = true;
    HttpClient httpClient = new HttpClient()
      ..badCertificateCallback =
          ((X509Certificate cert, String host, int port) => trustSelfSigned);
    IOClient ioClient = new IOClient(httpClient);

    final response = await ioClient.post(url, body: {
      'FirstName': fname,
      'LastName': lname,
      'Email': email,
      'Phone': phone,
      'Address': address,
    });
    apiResp = json.decode(response.body);

    switch (response.statusCode) {
      case 200:
        apiResp["Status"] = true;
        break;
      default:
        apiResp["Status"] = false;
        break;
    }
  } on SocketException {
    apiResp["Status"] = false;
  }
  _apiResponse.ApiStatus = ApiStatus.fromJson(apiResp);
  return _apiResponse;
}

//return bool
Future<bool> isEmailDuplicate(String email) async {
  bool resp = true;
  var url = Uri.parse('${_baseUrl}trainee/CheckUserEmail');
  try {
    bool trustSelfSigned = true;
    HttpClient httpClient = new HttpClient()
      ..badCertificateCallback =
          ((X509Certificate cert, String host, int port) => trustSelfSigned);
    IOClient ioClient = new IOClient(httpClient);

    final storage = new FlutterSecureStorage();
    String? token =
        await storage.read(key: authentication.token.toShortString());
    String userToken = token ??= _nullToken;
    final response = await ioClient.post(url, headers: {
      HttpHeaders.authorizationHeader: userToken,
    }, body: {
      "Email": email
    });
    switch (response.statusCode) {
      case 200:
        resp = false;
        break;
      case 302:
        resp = true;
        break;
      default:
        resp = true;
        break;
    }
  } on SocketException {
    resp = true;
  }
  return resp;
}

//return ApiStatus
Future<ApiStatus> isGymTokenValid(String gymToken) async {
  var apiResp;
  var url = Uri.parse('${_baseUrl}account/IsValidGymKey');
  try {
    bool trustSelfSigned = true;
    HttpClient httpClient = new HttpClient()
      ..badCertificateCallback =
          ((X509Certificate cert, String host, int port) => trustSelfSigned);
    IOClient ioClient = new IOClient(httpClient);
    final response = await ioClient.get(url, headers: {'GymKey': gymToken});

    switch (response.statusCode) {
      case 200:
        isGymTokenValidSuccessHalder(json.decode(response.body));
        apiResp = ApiStatus(message: "valid key", status: true);
        break;
      case 204:
        apiResp = ApiStatus(
            message: "Invalid key. Please enter correct key", status: false);
        break;
      case 400:
        apiResp = ApiStatus(message: Utility.errSWW, status: false);
        break;
      default:
        apiResp = ApiStatus(message: Utility.errSWW, status: false);
        break;
    }
  } on SocketException {
    apiResp = ApiStatus(message: Utility.errSWW, status: false);
  }
  return apiResp;
}

//gym key handler method
void isGymTokenValidSuccessHalder(data) async {
  // saving site key and site portal owner id in local storage
  await storage.write(
      key: authentication.gymPortalOwnerId.toShortString(),
      value: data["SitePortalOwnerId"].toString());
  await storage.write(
      key: authentication.gymKey.toShortString(), value: data["SiteKey"]);
}
