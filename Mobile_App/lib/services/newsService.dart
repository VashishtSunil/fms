import 'dart:convert';
import 'dart:io';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:fms_mobile/helper/apiResponseHandler.dart';
import 'package:fms_mobile/models/newsModel.dart';
import 'package:http/io_client.dart';
import '../enum/authentication.dart';
import '../helper/utility.dart';

String _baseUrl = Utility.baseUrl;
String _nullToken = Utility.nullToken;
final storage = new FlutterSecureStorage();

class NewsService {
  static Future<ApiResponse> getAllNews() async {
    ApiResponse _apiResponse = new ApiResponse();
    var url = Uri.parse('${_baseUrl}news/getall');
    try {
      bool trustSelfSigned = true;
      HttpClient httpClient = new HttpClient()
        ..badCertificateCallback =
            ((X509Certificate cert, String host, int port) => trustSelfSigned);
      IOClient ioClient = new IOClient(httpClient);
      String? token =
          await storage.read(key: authentication.token.toShortString());
      String userToken = token ??= _nullToken;
      String? portalOwnerId = await storage.read(
          key: authentication.gymPortalOwnerId.toShortString());
      final response = await ioClient.get(url, headers: {
        HttpHeaders.authorizationHeader: userToken,
        "PortalOwnerId": portalOwnerId ?? "0"
      });
      switch (response.statusCode) {
        case 200:
          Iterable newsList = json.decode(response.body);
          _apiResponse.Data =
              List<News>.from(newsList.map((model) => News.fromJson(model)));
          //News.fromJson(json.decode(response.body));
          break;
        case 204:
          _apiResponse.ApiError = ApiError(error: "No news found");
          break;
        case 401:
          _apiResponse.ApiError = ApiError(error: "Unauthorized");
          break;
        case 500:
        case 502:
          _apiResponse.ApiError = ApiError(error: Utility.errISE);
          break;
        default:
          _apiResponse.ApiError = ApiError(error: Utility.errSWW);
          break;
      }
    } on SocketException {
      _apiResponse.ApiError = ApiError(error: Utility.errISE);
    }
    return _apiResponse;
  }

  static Future<ApiResponse> getNews(String newsId) async {
    ApiResponse _apiResponse = new ApiResponse();
    var url = Uri.parse('${_baseUrl}news/get');
    try {
      bool trustSelfSigned = true;
      HttpClient httpClient = new HttpClient()
        ..badCertificateCallback =
            ((X509Certificate cert, String host, int port) => trustSelfSigned);
      IOClient ioClient = new IOClient(httpClient);
      String? token =
          await storage.read(key: authentication.token.toShortString());
      String userToken = token ??= _nullToken;
      final response = await ioClient.get(url, headers: {
        HttpHeaders.authorizationHeader: userToken,
        "NewsId": newsId
      });
      print(response.body);
      switch (response.statusCode) {
        case 200:
          _apiResponse.Data = News.fromJson(json.decode(response.body));
          break;
        case 401:
          _apiResponse.ApiError = ApiError.fromJson(json.decode(response.body));
          break;
        case 502:
          _apiResponse.ApiError = ApiError(error: "Server error. Please retry");
          break;
        default:
          _apiResponse.ApiError = ApiError.fromJson(json.decode(response.body));
          break;
      }
    } on SocketException {
      _apiResponse.ApiError = ApiError(error: "Server error. Please retry");
    }
    return _apiResponse;
  }
}
