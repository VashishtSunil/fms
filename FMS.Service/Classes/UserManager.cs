﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using FMS.Core.DataModels;
using FMS.Core.DataModels.Setting;
using FMS.Core.Enums;
using FMS.Core.Helper;
using FMS.Core.Models;
using FMS.Data.Context;
using Rollbar;
using Serilog;

namespace FMS.Services.Classes
{
    public class UserManager
    {
        #region User

        /// <summary>
        /// Login user 
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        public static async Task<UserModel> Login(LoginModel login)
        {
            UserModel loginUser = new UserModel();
            try
            {
                //hash password 
                var hashPassword = CipherHelper.EncryptPassword(login.Password + "_" + login.Email.ToLower());
                var dc = new FMSContext();
                RollbarLocator.RollbarInstance.Info("Login Start !");
                var user = dc.Users.FirstOrDefault(x => x.Email == login.Email && x.Password == hashPassword && x.IsActive && x.IsEmailVerified);
                if (user != null)
                {
                    //var roles = await GetUserRoles(user.Id);
                    //string role = string.Join(",", roles);
                    loginUser.Id = user.Id;
                    loginUser.FirstName = user.FirstName;
                    loginUser.LastName = user.LastName;
                    loginUser.Username = user.Username;
                    loginUser.Email = user.Email;
                    loginUser.Role = user.RoleId.ToString();
                    loginUser.RoleName = user.Role;
                    loginUser.Password = user.Password;
                }
            }
            catch (Exception ex)
            {
                RollbarLocator.RollbarInstance.AsBlockingLogger(TimeSpan.FromSeconds(1)).Error(ex);

                Log.Error(ex, "UserManager=>>Login");
            }
            return loginUser;
        }

        /// <summary>
        /// Is Code Used
        /// </summary>
        /// <param name="email"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        public static async Task<bool> IsCodeUsed(string email, string code)
        {
            bool status = false;
            try
            {
                var dc = new FMSContext();
                var user = await dc.Users.FirstOrDefaultAsync(x => x.Email == email && x.Code == code);
                if (user != null)
                {
                    status = user.IsCodeUsed;
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "UserManager=>>IsCodeUsed");
            }
            return status;
        }

        /// <summary>
        /// Reset Password
        /// </summary>
        /// <param name="email"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public static async Task<bool> ResetPassword(string email, string password)
        {
            bool status = false;
            try
            {
                var hashPassword = CipherHelper.EncryptPassword(password + "_" + email.ToLower());
                var dc = new FMSContext();
                var user = dc.Users.FirstOrDefault(x => x.Email == email);
                if (user != null)
                {
                    user.Password = hashPassword;
                    user.IsCodeUsed = true;
                    user.IsActive = true;
                    user.IsEmailVerified = true;
                    status = await dc.SaveChangesAsync() > 0;
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "UserManager=>>ResetPassword");
            }
            return status;
        }

        /// <summary>
        /// Update user code 
        /// </summary>
        /// <param name="email"></param>
        /// <param name="code"></param>
        /// <param name="isUsed"></param>
        /// <returns></returns>
        public static async Task<bool> UpdateUserCode(string email, string code, bool isUsed)
        {
            bool status = false;
            try
            {
                var dc = new FMSContext();
                var user = dc.Users.FirstOrDefault(x => x.Email == email);
                if (user != null)
                {
                    user.Code = code;
                    user.IsCodeUsed = isUsed;
                    status = await dc.SaveChangesAsync() > 0;
                }
            }
            catch (Exception ex)
            {
                status = false;
                Serilog.Log.Error(ex, "UserManager=>>UpdateUserCode");
            }
            return status;
        }

        /// <summary>
        /// Is Email Exist
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public static async Task<UserModel> IsEmailExist(string email)
        {
            UserModel userModel = new UserModel();
            var userEmail = email;
            try
            {
                var dc = new FMSContext();
                var user = await dc.Users.FirstOrDefaultAsync(x => x.Email == userEmail);
                if (user != null && user.Id > 0)
                {
                    userModel.Id = user.Id;
                    userModel.FirstName = user.FirstName;
                    userModel.LastName = user.LastName;
                    userModel.Email = user.Email;
                    userModel.Password = user.Password;
                    userModel.Phone = user.Phone;
                    userModel.RoleId = user.RoleId;
                    userModel.IsDeleted = (bool)user.IsDeleted;
                    userModel.Role = user.Role;
                    userModel.Username = user.Username;
                    userModel.Code = user.Code;
                    userModel.IsCodeUsed = user.IsCodeUsed;
                    userModel.IsActive = user.IsActive;
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "UserManager=>>IsEmailExist");
            }
            return userModel;
        }

        /// <summary>
        /// Register
        /// </summary>
        /// <param name="registerModel"></param>
        /// <returns></returns>
        public static async Task<Tuple<bool, string, FMS.Core.DataModels.User>> Register(RegisterViewModel registerViewModel)
        {
            bool status = false;
            string message = string.Empty;
            var newuser = new FMS.Core.DataModels.User();
            try
            {
                //hash password 
                //var hashPassword = CipherHelper.EncryptPassword(registerModel.Password + "_" + registerModel.Email.ToLower());
                var dc = new FMSContext();

                var user = dc.Users.FirstOrDefault(x => x.Email == registerViewModel.Email);
                var role = RoleManager.GetRoleByName(UserRole.Trainee.ToString());
                if (user == null)
                {
                    newuser.Id = registerViewModel.Id;
                    newuser.Email = registerViewModel.Email;
                    newuser.Role = role.RoleName;
                    newuser.RoleId = role.Id;
                    newuser.OwnerId = registerViewModel.OwnerId;
                    newuser.Code = registerViewModel.Code;
                    newuser.PlanExpirationDateTime = DateTime.Now; // Need to update this date for now using Date Time now
                    dc.Users.Add(newuser);
                    status = dc.SaveChanges() > 0;
                    registerViewModel.Id = newuser.Id;
                    registerViewModel.RoleId = (int)newuser.RoleId;
                    message = (status) ? "Please check you email to verify your account." : "Something went wrong!";
                }
                else
                {
                    message = "User already exist";
                }
            }
            catch (Exception ex)
            {
                status = false;
                message = "Something went wrong!";
                Serilog.Log.Error(ex, "UserManager=>>Login");
            }
            return Tuple.Create(status, message, newuser);
        }

        /// <summary>
        /// Verify Email
        /// </summary>
        /// <param name="Email"></param>
        /// <param name="Code"></param>
        /// <returns></returns>
        public static Tuple<bool, string> VerifyEmail(string Email, string Code)
        {
            var dc = new FMSContext();
            var user = dc.Users.FirstOrDefault(x => x.Email == Email && x.Code == Code);
            bool status = false;
            string message = string.Empty;
            try
            {
                if (user != null)
                {
                    if (user.IsCodeUsed == false)
                    {
                        if (user.IsEmailVerified == false)
                        {
                            user.IsEmailVerified = true;
                            user.IsCodeUsed = true;
                            status = dc.SaveChanges() > 0;
                            message = (status) ? "Your email is verified. Now you can login." : "Something went worng! Please try again";
                        }
                        else
                            message = "Email is already verified!";
                    }
                    else
                        message = "Token is expired!";
                }
                else
                    message = "No record found!";
            }
            catch (Exception ex)
            {
                status = false;
                message = "Something went worng! Please try again";
            }
            return Tuple.Create(status, message);
        }

        /// <summary>
        /// Get User By Id
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static UserModel GetUserById(long userId)
        {
            try
            {
                var dc = new FMSContext();
                return (from p in dc.Users
                        join userDeatail in dc.UserDetails on p.Id equals userDeatail.UserId
                        where p.IsDeleted == false
                        where p.IsDeleted == false && p.Id == userId
                        //Binding Data with Model
                        select new UserModel
                        {
                            Id = p.Id,
                            FirstName = userDeatail.FirstName,
                            LastName = userDeatail.LastName,
                            Email = p.Email,
                            Password = p.Password,
                            Phone = userDeatail.Phone,
                            Role = p.Role,
                            RoleId = p.RoleId,
                            CompanyName = p.Company,
                            Code = p.Code,
                            Username = p.Username,
                            OwnerId = p.OwnerId,
                            IsActive = p.IsActive,
                            Address = userDeatail.Address,
                            SitePortalOwnerId = p.SitePortalOwnerId,
                            SiteKey = p.SiteKey,
                            TypeId = (TraineeType)p.Type,
                            PlanExpirationDateTime = p.PlanExpirationDateTime
                        }).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }           
        }


        /// <summary>
        /// List Of User
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public static async Task<Tuple<List<UserModel>, string, int>> GetUsersDataTable(jQueryDataTableParamModel param, int roleId = 0)
        {
            var dc = new FMSContext();
            var sortColumnIndex = param.iSortCol_0;
            var qry = from user in dc.Users
                      join userDeatail in dc.UserDetails on user.Id equals userDeatail.UserId                  
                      where user.IsDeleted == false
                      select new UserModel
                      {
                          Id = user.Id,
                          FirstName = userDeatail.FirstName,
                          LastName = userDeatail.LastName,
                          Email = user.Email,
                          Phone = userDeatail.Phone,
                          Address = userDeatail.Address,
                          RoleId = user.RoleId,
                          OwnerId = user.OwnerId,
                          RoleName = user.Role,
                          SiteKey = user.SiteKey,
                          SitePortalOwnerId = user.SitePortalOwnerId,
                          TypeId = (TraineeType)user.Type,
                          IsActive = user.IsActive,                  
                      };
            var userInfo = GetUserById(Convert.ToInt32(param.id));
            if (userInfo != null)
            {
                if (userInfo.RoleId == 2)
                {
                    int sitePortalOwnerId = Convert.ToInt32(param.id);//set this while user is not supradmin 

                    if (sitePortalOwnerId > 1)
                    {
                        qry = qry.Where(x => x.SitePortalOwnerId == sitePortalOwnerId);
                    }
                }
                if (userInfo.RoleId == 3)
                {
                    int ownerId = Convert.ToInt32(param.id);//set this while user is not admin 
                    if (ownerId > 1)
                    {
                        qry = qry.Where(x => x.OwnerId == ownerId);
                    }
                }
            }
            // specific to role 
            if (roleId > 0)
            {
                qry = qry.Where(x => x.RoleId == roleId);
            }
            //Get total count
            var totalRecords = await qry.CountAsync();

            #region Searching 
            if (!string.IsNullOrEmpty(param.sSearch))
            {
                var toSearch = param.sSearch.ToLower();
                qry = qry.Where(c => c.Email.ToLower().Contains(toSearch)
                );
            }
            #endregion

            #region  Sorting 
            switch (sortColumnIndex)
            {
                //Sort by Name
                case 0:
                    switch (param.sSortDir_0)
                    {
                        case "desc":
                            qry = qry.OrderByDescending(a => a.FirstName);
                            break;
                        case "asc":
                            qry = qry.OrderBy(a => a.FirstName);
                            break;
                        default:
                            qry = qry.OrderBy(a => a.FirstName);
                            break;
                    }

                    break;
                //Sort by Name
                case 1:
                    switch (param.sSortDir_0)
                    {
                        case "desc":
                            qry = qry.OrderByDescending(a => a.Email);
                            break;
                        case "asc":
                            qry = qry.OrderBy(a => a.Email);
                            break;
                        default:
                            qry = qry.OrderBy(a => a.Email);
                            break;
                    }
                    break;
                default:
                    qry = qry.OrderBy(a => a.FirstName);
                    break;
            }
            #endregion

            #region  Paging 
            if (param.iDisplayLength != -1)
                qry = qry.Skip(param.iDisplayStart).Take(param.iDisplayLength);
            #endregion           
            return new Tuple<List<UserModel>, string, int>(
                await
                   (qry).ToListAsync(),
                param.sEcho,
                totalRecords);
        }

        /// <summary>
        /// Used to add Update/Add User
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static bool Update(UserModel model)
        {
            var dc = new FMSContext();
            var user = dc.Users.FirstOrDefault(x => x.Id == model.Id);
            var userDetails = dc.UserDetails.FirstOrDefault(x => x.UserId == model.Id);
            try
            {
                // for add new record
                if (user == null)
                {
                    user = new User
                    {
                        FirstName = model.FirstName,
                        LastName = model.LastName,
                        Username = model.Username,
                        Password = model.Password,
                        Email = model.Email,
                        Phone = model.Phone,
                        Company = model.CompanyName,
                        Role = model.RoleName,
                        RoleId = model.RoleId,
                        Code = model.Code,
                        IsCodeUsed = model.IsCodeUsed,
                        IsDeleted = false,
                        OwnerId = model.OwnerId,
                        IsActive = model.IsActive,
                        SiteKey = model.SiteKey,
                        SitePortalOwnerId = model.SitePortalOwnerId,
                        Type = (int)model.TypeId,
                        PlanExpirationDateTime = DateTime.Now //Need to update this date for now using DateTime.now
                    };
                    dc.Users.Add(user);
                }
                // for update 
                else
                {
                    user.FirstName = model.FirstName;
                    user.LastName = model.LastName;
                    user.Username = model.Username;
                    user.Password = model.Password;
                    user.Email = model.Email;
                    user.Phone = model.Phone;
                    user.Company = model.CompanyName;
                    user.Role = model.RoleName;
                    user.RoleId = model.RoleId;
                    user.Code = model.Code;
                    user.SiteKey = model.SiteKey;
                    user.IsCodeUsed = model.IsCodeUsed;
                    user.IsActive = model.IsActive;
                    user.SitePortalOwnerId = model.SitePortalOwnerId;
                }
                dc.SaveChanges();
                model.Id = user.Id;
                return true;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "UserManager=>>Update");
                return false;
            }
        }

        /// <summary>
        /// Is Email Duplicate
        /// </summary>
        /// <param name="userModel"></param>
        /// <returns></returns>
        public static bool IsEmailDuplicate(UserModel userModel)
        {
            var dc = new FMSContext();
            var user = dc.Users.FirstOrDefault(x => x.Email == userModel.Email && x.IsDeleted == false);
            if (user == null)
                return false;
            //Same email  already occupied 
            return (user.Id != userModel.Id) ? true : false;
        }

        /// <summary>
        /// Soft delete selected User According to Id 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns>Boolean Result</returns>
        public static bool DeleteUser(long id)
        {
            var dc = new FMSContext();
            var roles = dc.Users.FirstOrDefault(x => x.Id == id && x.IsDeleted == false);
            if (roles == null)
                return true;
            roles.IsDeleted = true;
            return dc.SaveChanges() > 0;
        }

        /// <summary>
        /// Update User Status
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static bool UpdateUserStatus(long id)
        {
            var dc = new FMSContext();
            var news = dc.Users.FirstOrDefault(x => x.Id == id && x.IsDeleted == false);
            if (news == null)
                return true;
            news.IsActive = !news.IsActive;
            return dc.SaveChanges() > 0;
        }

        /// <summary>
        /// Check for Duplicate email 
        /// </summary>
        /// <param name="userModel"></param>
        /// <returns></returns>
        public static bool IsUserEmailDuplicate(UserModel userModel)
        {
            var dc = new FMSContext();
            var brand = dc.Users.FirstOrDefault(x => x.Email == userModel.Email && x.IsDeleted == false);
            if (brand == null)
                return false;
            //Same brand name already occupied 
            if (brand.Id == userModel.Id)
                return true;
            return false;
        }

        /// <summary>
        /// delete all the previous mapping between attachment and uploader info
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static Tuple<bool, string> DeleteUserRoles(int userId)
        {
            bool status = false;
            string message = "Something Went Wrong!";
            try
            {
                var dc = new FMSContext();
                var roleMapping = dc.UserInRole.Where(x => x.UserId == userId).ToList();
                if (roleMapping != null && roleMapping.Count > 0)
                {
                    roleMapping.ForEach(p => dc.Entry(p).State = EntityState.Deleted);
                    dc.SaveChanges();
                }
                status = true;
                message = "Role Mapping Deleted Successfully!";
            }
            catch (Exception ex)
            {
                status = false;
                message = "Something Went Wrong!";
                Serilog.Log.Error(ex, "UserManager=>>DeleteUserRoles");
            }
            return Tuple.Create(status, message);
        }

        /// <summary>
        /// Update User In role
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static bool UpdateUserInRole(UserInRoleModel model)
        {
            UserInRoleModel userInRoleModel = new UserInRoleModel
            {
                UserId = model.UserId,
                RoleId = model.RoleId,
                CreatedBy = model.CreatedBy,
                ModifiedBy = model.ModifiedBy
            };
            return RoleManager.AddUserRole(userInRoleModel);
        }

        /// <summary>
        ///Soft delete selected Delete All User According to Role Id  
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public static Tuple<bool, string> DeleteAllUserByRoleId(long roleId)
        {
            bool status = false;
            string message = "Something Went Wrong!";
            try
            {
                var dc = new FMSContext();
                var UserRoleMapping = dc.Users.Where(x => x.RoleId == roleId).ToList();
                if (UserRoleMapping != null && UserRoleMapping.Count > 0)
                {
                    UserRoleMapping.ForEach(p => p.IsDeleted = true);
                    dc.SaveChanges();
                }
                status = true;
                message = "User Role Mapping Deleted Successfully!";
            }
            catch (Exception ex)
            {
                status = false;
                message = "Something Went Wrong!";
                Serilog.Log.Error(ex, "UserManager=>>DeleteAllUserByRoleId");
            }
            return Tuple.Create(status, message);
        }

        /// <summary>
        /// Check for Duplicate sitekey 
        /// </summary>
        /// <param name="userModel"></param>
        /// <returns></returns>
        public static async Task<UserModel> IsSiteKeyExist(string siteKey)
        {
            UserModel userModel = new UserModel();
            var clientKey = siteKey;
            try
            {
                var dc = new FMSContext();
                var user = await dc.Users.FirstOrDefaultAsync(x => x.SiteKey == siteKey);
                if (user != null && user.Id > 0)
                {
                    userModel.Id = user.Id;
                    userModel.FirstName = user.FirstName;
                    userModel.LastName = user.LastName;
                    userModel.Email = user.Email;
                    userModel.Password = user.Password;
                    userModel.Phone = user.Phone;
                    userModel.RoleId = user.RoleId;
                    userModel.IsDeleted = user.IsDeleted;
                    userModel.Role = user.Role;
                    userModel.Username = user.Username;
                    userModel.Code = user.Code;
                    userModel.IsCodeUsed = user.IsCodeUsed;
                    userModel.IsActive = user.IsActive;
                    userModel.SiteKey = user.SiteKey;
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "UserManager=>>IsEmailExist");
            }
            return userModel;
        }

        /// <summary>
        /// Get User By Key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static UserModel GetClientAdminByKey(string key)
        {
            var dc = new FMSContext();
            return (from p in dc.Users
                    join userDeatail in dc.UserDetails on p.Id equals userDeatail.UserId
                    where p.IsDeleted == false && p.SiteKey == key
                    //Binding Data with Model
                    select new UserModel
                    {
                        Id = p.Id,
                        OwnerId = p.OwnerId,
                        Phone = p.Phone,
                        SitePortalOwnerId = p.SitePortalOwnerId,
                        SiteKey = p.SiteKey,
                        Email = p.Email,
                        Address = userDeatail.Address
                    }).FirstOrDefault();
        }
        #endregion

        #region User Detail

        /// <summary>
        /// Get User Detail By Id
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static UserDetailModel GetUserDetailById(long userId)
        {
            var dc = new FMSContext();
            return (from p in dc.UserDetails
                    where p.IsDeleted == false && p.UserId == userId
                    //Binding Data with Model
                    select new UserDetailModel
                    {
                        Id = p.Id,
                        FirstName = p.FirstName,
                        LastName = p.LastName,
                        Address = p.Address,
                        PinCode = p.PinCode,
                        Phone = p.Phone,
                        UserId = p.UserId,
                        IsActive = p.IsActive,
                        ProfilePicId = p.ProfilePicId,
                        ProfilePic = p.ProfilePic,
                    }).FirstOrDefault();
        }

        /// <summary>
        /// get user detail along with uploaded file
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static UserDetailModel GetUserDetail(long userId)
        {
            var dc = new FMSContext();
            var userDetail = (from p in dc.UserDetails
                              where p.IsDeleted == false && p.UserId == userId
                              //Binding Data with Model
                              select new UserDetailModel
                              {
                                  Id = p.Id,
                                  FirstName = p.FirstName,
                                  LastName = p.LastName,
                                  Address = p.Address,
                                  PinCode = p.PinCode,
                                  Phone = p.Phone,
                                  UserId = p.UserId,
                                  IsActive = p.IsActive,
                                  ProfilePicId = p.ProfilePicId
                              }).FirstOrDefault();
            if (userDetail != null && userDetail.ProfilePicId > 0)
            {
                var fileDetail = AttachmentManager.GetById(userDetail.ProfilePicId.Value);
                if (fileDetail != null)
                {
                    userDetail.ProfilePic = "data:" + fileDetail.ContentType + ";base64," + Convert.ToBase64String(fileDetail.ImageBytes);
                    userDetail.FileName = fileDetail.FileName;
                }
            }
            return userDetail;
        }

        /// <summary>
        /// Used to add Update/Add User Detail 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static bool UpdateUserDetail(UserDetailModel model)
        {
            var dc = new FMSContext();
            var userDetails = dc.UserDetails.FirstOrDefault(x => x.Id == model.Id);
            try
            {
                // for add new record
                if (userDetails == null)
                {
                    userDetails = new UserDetail
                    {
                        Id = model.Id,
                        FirstName = model.FirstName,
                        LastName = model.LastName,
                        Address = model.Address,
                        Phone = model.Phone,
                        PinCode = model.PinCode,
                        UserId = model.UserId,
                        IsActive = model.IsActive,
                        ProfilePicId = model.ProfilePicId,
                        ProfilePic = model.ProfilePic
                    };
                    dc.UserDetails.Add(userDetails);
                }
                // for update 
                else
                {
                    userDetails.FirstName = model.FirstName;
                    userDetails.LastName = model.LastName;
                    userDetails.Address = model.Address;
                    userDetails.Phone = model.Phone;
                    userDetails.UserId = model.UserId;
                    userDetails.PinCode = model.PinCode;
                    userDetails.IsActive = model.IsActive;
                    userDetails.ProfilePicId = model.ProfilePicId;
                    userDetails.ProfilePic = model.ProfilePic;
                }
                dc.SaveChanges();
                model.Id = userDetails.Id;
                return true;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "UserManager=>>UpdateUserDetail");
                return false;
            }
        }

        /// <summary>
        /// Delete User Detail By selected User Id
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static Tuple<bool, string> DeleteUserDetail(int userId)
        {
            bool status = false;
            string message = "Something Went Wrong!";
            try
            {
                var dc = new FMSContext();
                var userDetailMapping = dc.UserDetails.Where(x => x.UserId == userId).ToList();
                if (userDetailMapping != null && userDetailMapping.Count > 0)
                {
                    userDetailMapping.ForEach(p => dc.Entry(p).State = EntityState.Deleted);
                    dc.SaveChanges();
                }
                status = true;
                message = "user Detail Mapping Deleted Successfully!";
            }
            catch (Exception ex)
            {
                status = false;
                message = "Something Went Wrong!";
                Serilog.Log.Error(ex, "UserManager=>>DeleteUserDetail");
            }
            return Tuple.Create(status, message);
        }

        /// <summary>
        /// Get user detail with token
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public static UserModel GetUserDetailWithToken(string token)
        {
            var dc = new FMSContext();
            var tokenDbUser = (from t in dc.ApiTokens
                               join u in dc.Users on t.ClientId equals u.Id.ToString() into uGJ
                               from user in uGJ.DefaultIfEmpty()
                               where t.TokenId == token
                               select new UserModel
                               {
                                   Id = user.Id,
                                   FirstName = user.FirstName,
                                   LastName = user.LastName,
                                   Username = user.Username,
                                   Email = user.Email,
                                   Role = user.RoleId.ToString(),
                                   RoleName = user.Role,
                                   Password = user.Password,
                               }).FirstOrDefault();
            return tokenDbUser;
        }

        /// <summary>
        /// Get Count By UserId
        /// </summary>
        /// <param name="ownerId"></param>
        /// <returns></returns>
        public static async Task<Tuple<int, int, int>> GetCountByUserId(int sitePortalOwnerId)
        {
            var dc = new FMSContext();
            var qry = dc.Users.Where(x => x.IsDeleted == false && x.SitePortalOwnerId == sitePortalOwnerId);
            int clientCount = await qry.Where(x => x.Role == UserRole.Trainee.ToString()).CountAsync();
            int trainerCount = await qry.Where(x => x.Role == UserRole.Trainer.ToString()).CountAsync();
            // get Created date
            DateTime? createdDate = await qry.Select(x => x.CreatedOn).FirstOrDefaultAsync();
            int yearExprienceCount = 0;
            if (createdDate != null)
            {
                yearExprienceCount = DateTime.Now.Year - createdDate.Value.Year;
            }
            return new Tuple<int, int, int>(clientCount, trainerCount, yearExprienceCount);
        }
        #endregion

        #region UserMap

        /// <summary>
        /// Add or update every user entry on UserMap table 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static bool UpdateUserMap(UserMapModel model)
        {
            var dc = new FMSContext();
            var userMap = dc.UserMap.FirstOrDefault(x => x.Id == model.Id);
            try
            {
                // for add new record
                if (userMap == null)
                {
                    userMap = new UserMap
                    {
                        Id = model.Id,
                        OwnerId = model.OwnerId,
                        UserDetailId = model.UserDetailId,
                        UserType = model.UserType,
                        UserId = model.UserId,
                        IsActive = model.IsActive,
                    };
                    dc.UserMap.Add(userMap);
                }
                // for update 
                else
                {
                    userMap.Id = model.Id;
                    userMap.OwnerId = model.OwnerId;
                    userMap.UserDetailId = model.UserDetailId;
                    userMap.UserType = model.UserType;
                    userMap.UserId = model.UserId;
                    userMap.IsActive = model.IsActive;
                }
                dc.SaveChanges();
                model.Id = userMap.Id;
                return true;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "UserManager=>>UpdateUserDetail");
                return false;
            }
        }

        #endregion

        #region ForgotPassword

        /// <summary>
        /// Update Forgot Password
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        /// 
        public static bool UpdateForgotPassword(ForgotPasswordViewModel model)
        {
            try
            {
                //hash password 
                var hashPassword = CipherHelper.EncryptPassword(model.Password + "_" + model.Email.ToLower());
                var dc = new FMSContext();
                var user = dc.Users.FirstOrDefault(x => x.Id == model.Id);
                if (user != null)
                {
                    user.Password = hashPassword;
                    user.IsActive = model.IsActive;
                    user.IsEmailVerified = true;
                }
                dc.SaveChanges();
                model.Id = user.Id;
                return true;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "UserManager=>>UpdateForgotPassword");
                return false;
            }
        }

        #endregion

        #region SubscriptionSetting

        /// <summary>
        /// Used to add Update/Site Setting
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static bool UpdateSubscriptionSetting(SiteSettingModel model)
        {
            var dc = new FMSContext();
            var setting = dc.SiteSettings.FirstOrDefault(x => x.Id == model.Id);
            //var userDetails = dc.UserDetails.FirstOrDefault(x => x.UserId == model.Id);
            try
            {
                // for add new record
                if (setting == null)
                {
                    setting = new SiteSetting
                    {
                        Key = model.Key,
                        Value = model.Value,
                        Value2 = model.Value2,
                        Description = model.Description,
                        ControlType = model.ControlType,
                        OwnerId = model.OwnerId,
                        SitePortalOwnerId = model.SitePortalOwnerId,                        
                    };
                    dc.SiteSettings.Add(setting);
                }
                // for update 
                else
                {
                    setting.Key = model.Key;
                    setting.Value = model.Value;
                    setting.Value2 = model.Value2;
                    setting.Description = model.Description;
                    setting.ControlType = model.ControlType;
                    setting.OwnerId = model.OwnerId;
                    setting.SitePortalOwnerId = model.SitePortalOwnerId;           
                }
                dc.SaveChanges();
                model.Id = setting.Id;
                return true;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "UserManager=>>UpdateSubscriptionSetting");
                return false;
            }
        }
        #endregion

    }
}
