﻿using FMS.Core.DataModels.Attachment;
using FMS.Core.Enums;
using FMS.Core.Models;
using FMS.Data.Context;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FMS.Core.Models;

namespace FMS.Services.Classes
{
    public class AttachmentManager
    {
        /// <summary>
        /// upload attachment
        /// </summary>
        /// <param name="attachmentModel"></param>
        /// <returns></returns>
        public static Tuple<bool, string> UpdateAttachments(AttachmentModel attachmentModel)
        {
            bool status = false;
            string message = "Something Went Wrong!";
            try
            {
                var dc = new FMSContext();
                var attachment = dc.Attachments.FirstOrDefault(x => x.Id == attachmentModel.Id);
                if (attachment == null)
                {
                    attachment = new Attachment()
                    {
                        FileName = attachmentModel.FileName,
                        DummyFileName = attachmentModel.DummyFileName,
                        Location = attachmentModel.Location,
                        MIME = (int)attachmentModel.MIME,
                        Size = attachmentModel.Size,
                        ImageBytes = attachmentModel.ImageBytes,
                        ContentType = attachmentModel.ContentType,
                        CreatedBy = attachmentModel.CreatedBy
                    };
                    dc.Attachments.Add(attachment);
                    if (dc.SaveChanges() > 0)
                    {
                        attachmentModel.Id = attachment.Id;
                        status = true;
                        message = "File Uploaded Successfully!";
                    }
                }
            }
            catch (Exception ex)
            {
                status = false;
                message = "Something Went Wrong!";
                // Log.Error(ex, "AttachmentManager=>>UpdateAttachments");
            }
            return Tuple.Create(status, message);
        }

        /// <summary>
        /// save the mapping between up-loader info and attachment
        /// </summary>
        /// <param name="attachmentModel"></param>
        /// <returns></returns>
        public static Tuple<bool, string> SaveAttachmentUploaderMapping(AttachmentModel attachmentModel)
        {
            bool status = false;
            string message = "Something Went Wrong!";
            try
            {
                var dc = new FMSContext();
                int uploaderType = (int)attachmentModel.UplaoderType;
                var deleteAttachmentUploaderMappingResponse = DeleteAttachmentUploaderMapping(attachmentModel.UploaderId, uploaderType);
                if (deleteAttachmentUploaderMappingResponse.Item1)
                {
                    if (attachmentModel.AttachmentIds != null && attachmentModel.AttachmentIds.Length > 0)
                    {
                        var attachmentMappingDb = new List<AttachmentMapping>();
                        foreach (var ids in attachmentModel.AttachmentIds)
                        {
                            var attachmentMedia = new AttachmentMapping()
                            {
                                AttachmentId = ids,
                                UplaoderType = uploaderType,
                                UploaderId = attachmentModel.UploaderId,
                                CreatedBy = attachmentModel.CreatedBy
                            };
                            attachmentMappingDb.Add(attachmentMedia);
                        }
                        if (attachmentMappingDb != null && attachmentMappingDb.Count > 0)
                        {
                            dc.AttachmentMappings.AddRange(attachmentMappingDb);
                            if (dc.SaveChanges() > 0)
                            {
                                status = true;
                                message = "AttachmentUploader Mapping Save Successfully!";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                status = false;
                message = "Something Went Wrong!";
                //Log.Error(ex, "AttachmentManager=>>SaveAttachmentUploaderMapping");
            }
            return Tuple.Create(status, message);
        }

        /// <summary>
        /// delete all the previous mapping between attachment and uploader info
        /// </summary>
        /// <param name="uploaderId"></param>
        /// <param name="uploaderType"></param>
        /// <returns></returns>
        public static Tuple<bool, string> DeleteAttachmentUploaderMapping(int uploaderId, int uploaderType)
        {
            bool status = false;
            string message = "Something Went Wrong!";
            try
            {
                var dc = new FMSContext();
                var attachmentMapping = dc.AttachmentMappings.Where(x => x.UploaderId == uploaderId && x.UplaoderType == uploaderType).ToList();
                if (attachmentMapping != null && attachmentMapping.Count > 0)
                {
                    attachmentMapping.ForEach(p => dc.Entry(p).State = EntityState.Deleted);
                    dc.SaveChanges();
                }
                status = true;
                message = "AtttachmentUploader Mapping Deleted Successfully!";
            }
            catch (Exception ex)
            {
                status = false;
                message = "Something Went Wrong!";
                // Log.Error(ex, "AttachmentManager=>>DeleteAttachmentUploaderMapping");
            }
            return Tuple.Create(status, message);
        }

        /// <summary>
        /// get attachment list by uploader Id
        /// </summary>
        /// <param name="uploaderId"></param>
        /// <param name="uploaderType"></param>
        /// <returns></returns>
        public static List<AttachmentModel> GetAttachmentsByUploaderId(long uploaderId, int uploaderType)
        {
            List<AttachmentModel> lstAttachmentModel = new List<AttachmentModel>();
            try
            {
                var dc = new FMSContext();
                var attachmentMappingList = dc.AttachmentMappings.Where(x => x.UploaderId == uploaderId && x.UplaoderType == uploaderType).ToList();
                if (attachmentMappingList != null && attachmentMappingList.Count > 0)
                {
                    foreach (var attachmentInfo in attachmentMappingList)
                    {
                        var attachment = dc.Attachments.FirstOrDefault(x => x.Id == attachmentInfo.AttachmentId);
                        if (attachment != null)
                        {
                            AttachmentModel attachmentModel = new AttachmentModel()
                            {
                                Id = attachment.Id,
                                FileName = attachment.FileName,
                                DummyFileName = attachment.DummyFileName,
                                Location = attachment.Location,
                                MIME = (MimeType)attachment.MIME,
                                Size = attachment.Size,
                                ContentType = attachment.ContentType,
                                ImageBytes = attachment.ImageBytes,
                                CreatedBy = attachment.CreatedBy
                            };
                            lstAttachmentModel.Add(attachmentModel);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                lstAttachmentModel = null;
                //Log.Error(ex, "AttachmentManager=>>GetAttachmentsByUploaderId");
            }
            return lstAttachmentModel;
        }

        /// <summary>
        /// delete attachment by Id and also its mapping
        /// </summary>
        /// <param name="attachmentId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static Tuple<bool, string> DeleteAttachmentById(int attachmentId, string userId)
        {
            bool status = false;
            string message = "Something Went Wrong!";
            try
            {
                if (attachmentId > 0)
                {
                    var dc = new FMSContext();
                    var attachment = dc.Attachments.FirstOrDefault(x => x.Id == attachmentId && x.CreatedBy == userId);
                    if (attachment != null)
                    {
                        attachment.IsDeleted = true;
                        var attachmentMappings = dc.AttachmentMappings.Where(x => x.AttachmentId == attachmentId && x.CreatedBy == userId);
                        if (attachmentMappings != null)
                        {
                            foreach (var attachmentMapping in attachmentMappings)
                            {
                                attachmentMapping.IsDeleted = true;
                            }
                        }
                        dc.SaveChanges();
                    }
                    status = true;
                    message = "Attachment Deleted Successfully!";
                }
            }
            catch (Exception ex)
            {
                status = false;
                message = "Something Went Wrong!";
                //Log.Error(ex, "AttachmentManager=>>DeleteAttachmentById");
            }
            return Tuple.Create(status, message);
        }
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static AttachmentModel GetById(int id)
        {
            
                var dc = new FMSContext();
                var image =  dc.Attachments.Where(x => x.Id == id).Select(x => new AttachmentModel()
                {
                    Id = x.Id,
                    FileName = x.FileName,
                    DummyFileName = x.DummyFileName,
                    Location = x.Location,
                    MIME = (MimeType)x.MIME,
                    ContentType = x.ContentType,
                    ImageBytes = x.ImageBytes,
                    Size = x.Size,
                    CreatedBy = x.CreatedBy
                }).FirstOrDefault();

            return image;
           

        }
        

        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async static Task<AttachmentModel> GetByIdAsync(int id)
        {
            try
            {
                var dc = new FMSContext();
                return await dc.Attachments.Where(x => x.Id == id).Select(x => new AttachmentModel()
                {
                    Id = x.Id,
                    FileName = x.FileName,
                    DummyFileName = x.DummyFileName,
                    Location = x.Location,
                    MIME = (MimeType)x.MIME,
                    ContentType = x.ContentType,
                    ImageBytes = x.ImageBytes,
                    Size = x.Size,
                    CreatedBy = x.CreatedBy
                }).FirstOrDefaultAsync();

            }
            catch (Exception ex)
            {
                //Log.Error(ex, "AttachmentManager=>>GetById");
                return null;
            }

        }
    }
}
