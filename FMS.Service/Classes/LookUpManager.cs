﻿using FMS.Core.DataModels;
using FMS.Core.Enums;
using FMS.Core.Models;
using FMS.Data.Context;
using Serilog;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FMS.Services.Classes
{
    public class LookUpManager
    {
        #region Look Up Domain

        /// <summary>
        /// Used to add Update /Add LookUpDomain
        /// </summary>
        /// <param name="lookUpDomainModel"></param>
        /// <returns></returns>
        public static bool UpdateLookUpDomain(LookUpDomainModel lookUpDomainModel)
        {
            var dc = new FMSContext();
            var lookUpDomain = dc.LookUpDomains.FirstOrDefault(x => x.Code == lookUpDomainModel.Code && !x.IsDeleted);
            try
            {
                // for add new record
                if (lookUpDomain == null)
                {
                    lookUpDomain = new LookUpDomain
                    {
                        // Id = Guid.NewGuid(),
                        Code = lookUpDomainModel.Code,
                        Description = lookUpDomainModel.Description
                    };
                    dc.LookUpDomains.Add(lookUpDomain);
                    dc.SaveChanges();
                }
                // for update 
                else
                {
                    lookUpDomain.Code = lookUpDomainModel.Code;
                    lookUpDomain.Description = lookUpDomainModel.Description;
                    lookUpDomain.IsDeleted = false;
                    lookUpDomain.ModifiedOn = DateTime.Now;
                    dc.SaveChanges();
                }
                lookUpDomainModel.Id = lookUpDomain.Id;
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// Get Look Up Domain By Code
        /// </summary>
        /// <param name="Code"></param>
        /// <returns></returns>
        public static LookUpDomainModel GetLookUpDomainByCode(string Code)
        {
            var dc = new FMSContext();
            return (from p in dc.LookUpDomains
                    where p.IsDeleted == false && p.Code == Code
                    select new LookUpDomainModel
                    {
                        Id = p.Id,
                        Code = p.Code,
                        Description = p.Description
                    }).FirstOrDefault();
        }

        /// <summary>
        /// Get Look Up Domain Value By Domain Id
        /// </summary>
        /// <param name="DomainId"></param>
        /// <returns></returns>
        public static List<LookUpDomainValueModel> GetLookUpDomainValueByDomainId(long DomainId)
        {
            var dc = new FMSContext();
            return (from p in dc.LookUpDomainValues
                    where p.IsDeleted == false && p.DomainId == DomainId
                    select new LookUpDomainValueModel
                    {
                        Id = p.Id,
                        DomainId = p.DomainId,
                        DomainText = p.DomainText,
                        IsActive = p.IsActive,
                        DomainValue = p.DomainValue,
                        DomainValueParentId = p.DomainValueParentId
                    }).ToList();
        }

        /// <summary>
        /// get look Up Domain Value based on domain Value 
        /// </summary>
        /// <param name="domainValue"></param>
        /// <returns></returns>
        public static LookUpDomainValueModel GetLookUpDomainValueByDomainValue(string domainValue)
        {
            var dc = new FMSContext();
            return (from p in dc.LookUpDomainValues
                    where p.IsDeleted == false && p.DomainValue == domainValue
                    select new LookUpDomainValueModel
                    {
                        Id = p.Id,
                        DomainId = p.DomainId,
                        DomainText = p.DomainText,
                        IsActive = p.IsActive,
                        DomainValue = p.DomainValue,
                        DomainValueParentId = p.DomainValueParentId
                    }).FirstOrDefault();
        }

        /// <summary>
        /// get look Up Domain Value based on id 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static LookUpDomainValueModel GetLookUpDomainValueById(int id)
        {
            var dc = new FMSContext();
            return (from p in dc.LookUpDomainValues
                    where p.IsDeleted == false && p.Id == id
                    select new LookUpDomainValueModel
                    {
                        Id = p.Id,
                        DomainId = p.DomainId,
                        DomainText = p.DomainText,
                        IsActive = p.IsActive,
                        DomainValue = p.DomainValue,
                        DomainValueParentId = p.DomainValueParentId
                    }).FirstOrDefault();
        }

        /// <summary>
        /// Update Look Up Domain Value
        /// </summary>
        /// <param name="lookUpDomainValueModel"></param>
        /// <returns></returns>
        public static bool UpdateLookUpDomainValue(LookUpDomainValueModel lookUpDomainValueModel)
        {
            var dc = new FMSContext();
            var lookUpDomainValue = dc.LookUpDomainValues.FirstOrDefault(x => x.Id == lookUpDomainValueModel.Id &&
            !x.IsDeleted);
            try
            {
                // for add new record
                if (lookUpDomainValue == null)
                {
                    lookUpDomainValue = new LookUpDomainValue
                    {
                        // Id = Guid.NewGuid(),
                        DomainId = lookUpDomainValueModel.DomainId,
                        DomainValueParentId = lookUpDomainValueModel.DomainValueParentId,
                        DomainValue = lookUpDomainValueModel.DomainValue,
                        DomainText = lookUpDomainValueModel.DomainText,
                        IsActive = lookUpDomainValueModel.IsActive,
                        CreatedBy = lookUpDomainValueModel.CreatedBy,
                        CreatedOn = DateTime.Now
                    };
                    dc.LookUpDomainValues.Add(lookUpDomainValue);
                }
                // for update 
                else
                {
                    lookUpDomainValue.DomainValueParentId = lookUpDomainValueModel.DomainValueParentId;
                    lookUpDomainValue.DomainValue = lookUpDomainValueModel.DomainValue;
                    lookUpDomainValue.DomainText = lookUpDomainValueModel.DomainText;
                    lookUpDomainValue.IsActive = lookUpDomainValueModel.IsActive;
                    lookUpDomainValue.IsDeleted = false;
                    lookUpDomainValue.ModifiedOn = DateTime.Now;
                    lookUpDomainValue.ModifiedBy = lookUpDomainValueModel.ModifiedBy;

                }
                dc.SaveChanges();
                lookUpDomainValueModel.Id = lookUpDomainValue.Id;
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// check whether template category(domain value) is duplicate or not
        /// </summary>
        /// <param name="lookUpDomainValueModel"></param>
        /// <returns></returns>
        public static bool IsDomainValueDuplicate(LookUpDomainValueModel lookUpDomainValueModel)
        {
            var dc = new FMSContext();
            var detail = dc.LookUpDomainValues.FirstOrDefault(x => x.DomainValue == lookUpDomainValueModel.DomainValue && !x.IsDeleted);
            if (detail == null)
                return false;
            if (detail.Id != lookUpDomainValueModel.Id)
                return true;
            return false;
        }

        /// <summary>
        /// get look up domain value select list by domain code
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public static async Task<SelectList> GetLookUpDomainValueSelectListByDomainCode(string code)
        {
            SelectList selectListItFMS = null;
            try
            {
                var dc = new FMSContext();
                var lstValues = await (from domain in dc.LookUpDomains
                                       join domainValue in dc.LookUpDomainValues
                                       on domain.Id equals domainValue.DomainId
                                       where domain.IsDeleted == false
                                       && domain.Code == code
                                       select new LookUpDomainValueModel
                                       {
                                           Id = domainValue.Id,
                                           DomainId = domainValue.DomainId,
                                           DomainText = domainValue.DomainText,
                                           IsActive = domainValue.IsActive,
                                           DomainValue = domainValue.DomainValue,
                                           DomainValueParentId = domainValue.DomainValueParentId
                                       }).ToListAsync();
                if (lstValues != null && lstValues.Count > 0)
                {
                    selectListItFMS = new SelectList(lstValues, "DomainId", "DomainText");
                }
            }
            catch (Exception ex)
            {
                selectListItFMS = null;
                Log.Error(ex, "LookUpManager=>>GetLookUpDomainValueSelectListByDomainCode");
            }
            return selectListItFMS;
        }

        /// <summary>
        /// Get all Data of Look Up Domain Values to bind with data-table 
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public static async Task<Tuple<List<LookUpDomainValueModel>, string, int>> GetDataTableList(jQueryDataTableParamModel param, int domainId)
        {
            var dc = new FMSContext();
            var sortColumnIndex = param.iSortCol_0;
            var qry = dc.LookUpDomainValues
             .Where(x => x.IsDeleted == false && x.DomainId == domainId);
            //Get total count
            var totalRecords = await qry.CountAsync();

            #region Searching 
            if (!string.IsNullOrEmpty(param.sSearch))
            {
                var toSearch = param.sSearch.ToLower();
                qry = qry.Where(c => c.DomainValue.ToLower().Contains(toSearch)
                    || c.DomainText.ToLower().Contains(toSearch));
            }
            #endregion

            #region  Sorting 
            switch (sortColumnIndex)
            {
                case 0:
                    switch (param.sSortDir_0)
                    {
                        case "desc":
                            qry = qry.OrderByDescending(a => a.DomainValue);
                            break;
                        case "asc":
                            qry = qry.OrderBy(a => a.DomainValue);
                            break;
                        default:
                            qry = qry.OrderBy(a => a.DomainValue);
                            break;
                    }

                    break;
                case 1:
                    switch (param.sSortDir_0)
                    {
                        case "desc":
                            qry = qry.OrderByDescending(a => a.DomainText);
                            break;
                        case "asc":
                            qry = qry.OrderBy(a => a.DomainText);
                            break;
                        default:
                            qry = qry.OrderBy(a => a.DomainText);
                            break;
                    }

                    break;

                default:
                    qry = qry.OrderBy(a => a.DomainValue);
                    break;
            }
            #endregion

            #region  Paging 
            if (param.iDisplayLength != -1)
                qry = qry.Skip(param.iDisplayStart).Take(param.iDisplayLength);
            #endregion

            return new Tuple<List<LookUpDomainValueModel>, string, int>(
                await
                    (from p in qry
                     select new LookUpDomainValueModel
                     {
                         Id = p.Id,
                         DomainId = p.DomainId,
                         DomainValue = p.DomainValue,
                         DomainText = p.DomainText,
                         CreatedOn = p.CreatedOn,
                         ModifiedOn = p.ModifiedOn,
                         IsActive = p.IsActive

                     }).ToListAsync(),
                param.sEcho,
                totalRecords);
        }

        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static LookUpDomainValueModel GetById(long id)
        {
            var dc = new FMSContext();
            return (from p in dc.LookUpDomainValues
                    where p.IsDeleted == false && p.Id == id
                    //Binding Data with Model
                    select new LookUpDomainValueModel
                    {
                        Id = p.Id,
                        DomainId = p.DomainId,
                        DomainValue = p.DomainValue,
                        DomainText = p.DomainText,
                        IsActive = p.IsActive
                    }).FirstOrDefault();
        }

        /// <summary>
        /// Soft delete selected Look Up Domain Value According to Id 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns>Boolean Result</returns>
        public static bool DeleteLookUpDomainValue(long id)
        {
            var dc = new FMSContext();
            var lessons = dc.LookUpDomainValues.FirstOrDefault(x => x.Id == id && x.IsDeleted == false);
            if (lessons == null)
                return true;
            lessons.IsDeleted = true;
            return dc.SaveChanges() > 0;
        }

        /// <summary>
        /// Update Look Up Domain Value Status
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static bool UpdateLookUpDomainValueStatus(long id)
        {
            var dc = new FMSContext();
            var lookUpDomainValues = dc.LookUpDomainValues.FirstOrDefault(x => x.Id == id && x.IsDeleted == false);
            if (lookUpDomainValues == null)
                return true;

            lookUpDomainValues.IsActive = !lookUpDomainValues.IsActive;
            return dc.SaveChanges() > 0;
        }

        /// <summary>
        /// get domain value list by domain code
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public static List<LookUpDomainValueModel> GetLookUpDomainValueListByDomainCode(string code)
        {
            List<LookUpDomainValueModel> lstDomainValues = new List<LookUpDomainValueModel>();
            try
            {
                var dc = new FMSContext();
                lstDomainValues = (from domain in dc.LookUpDomains
                                       join domainValue in dc.LookUpDomainValues
                                       on domain.Id equals domainValue.DomainId
                                       where domain.IsDeleted == false
                                       && domain.Code == code
                                       && !domainValue.IsDeleted
                                       select new LookUpDomainValueModel
                                       {
                                           Id = domainValue.Id,
                                           DomainId = domainValue.DomainId,
                                           DomainText = domainValue.DomainText,
                                           GroupId=domainValue.GroupId,
                                           IsActive = domainValue.IsActive,
                                           DomainValue = domainValue.DomainValue,
                                           DomainValueParentId = domainValue.DomainValueParentId
                                       }).ToList();
            }
            catch (Exception ex)
            {
                lstDomainValues = null;
                Log.Error(ex, "LookUpManager=>>GetLookUpDomainValueListByDomainCode");
            }
            return lstDomainValues;
        }
        #endregion
    }
}
