﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using FMS.Core.DataModels;
using FMS.Core.DataModels.Diet;
using FMS.Core.Enums;
using FMS.Core.Models;
using FMS.Data.Context;
using Serilog;


namespace FMS.Services.Classes
{
    public class DietPlanManager
    {
        #region Diet
        /// <summary>
        /// Add/Update Meal
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static bool? AddDiet(DietModel model)
        {
            bool? status = null;
            try
            {
                using (var dc = new FMSContext())
                {

                    var diet = dc.Diets.FirstOrDefault(x => x.Id == model.Id && !x.IsDeleted);
                    if (model.IsActive)
                    {
                        var diets = dc.Diets.Where(x => x.UserId == model.UserId && !x.IsDeleted).ToList();
                        diets.ForEach(x => x.IsActive = false);
                    }
                    
                    // for add new record
                    if (diet == null)
                    {
                        diet = new FMS.Core.DataModels.Diet.Diet
                        {
                            UserId = model.UserId,
                            DietName = model.DietName,
                            CreatedBy = model.CreatedBy,
                            CreatedOn = DateTime.Now,
                            IsActive = model.IsActive,
                        };
                        dc.Diets.Add(diet);
                    }
                    // for update 
                    else
                    {
                        diet.UserId = model.UserId;
                        diet.DietName = model.DietName;
                        diet.ModifiedBy = model.CreatedBy;
                        diet.ModifiedOn = DateTime.Now;
                        diet.IsActive = model.IsActive;
                    }
                    status =  dc.SaveChanges() > 0;
                    model.Id = diet.Id;
                }

                return status;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "DietPlanManager=>>AddDiet");
                return status;
            }
        }

        /// <summary>
        /// Get trainee diet list 
        /// </summary>
        /// <param name="param"></param>
        /// <param name="tarineeId"></param>
        /// <returns></returns>
        public static Tuple<List<DietModel>, string, int> GetDietByUserDataTable(jQueryDataTableParamModel param)
        {
            using (var dc = new FMSContext())
            {
                bool parsed = int.TryParse(param.id, out int traineeId);
                if (parsed)
                {
                    var sortColumnIndex = param.iSortCol_0;
                    var qry = dc.Diets.Where(x => x.UserId == traineeId && !x.IsDeleted).Select(x => new DietModel
                    {
                        UserId = x.UserId,
                        DietName = x.DietName,
                        Id = x.Id,
                        IsActive = x.IsActive
                    });
                    //Get total count
                    var totalRecords = qry.Count();

                    #region Searching 
                    if (!string.IsNullOrEmpty(param.sSearch))
                    {
                        var toSearch = param.sSearch.ToLower();
                        qry = qry.Where(c => c.DietName.ToLower().Contains(toSearch)
                        );
                    }
                    #endregion

                    #region  Sorting 
                    switch (sortColumnIndex)
                    {
                        //Sort by Name
                        case 0:
                            switch (param.sSortDir_0)
                            {
                                case "desc":
                                    qry = qry.OrderByDescending(a => a.DietName);
                                    break;
                                case "asc":
                                    qry = qry.OrderBy(a => a.DietName);
                                    break;
                                default:
                                    qry = qry.OrderBy(a => a.DietName);
                                    break;
                            }

                            break;
                        default:
                            qry = qry.OrderBy(a => a.DietName);
                            break;
                    }
                    #endregion

                    #region  Paging 
                    if (param.iDisplayLength != -1)
                        qry = qry.Skip(param.iDisplayStart).Take(param.iDisplayLength);
                    #endregion
                    return new Tuple<List<DietModel>, string, int>(
                      (qry).ToList(),
                   param.sEcho,
                   totalRecords);
                }
                return new Tuple<List<DietModel>, string, int>(
                     new List<DietModel>(),
                  param.sEcho,
                  0);
            }
        }

        /// <summary>
        /// Add/Update Meal
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static DietModel GetDiet(int id)
        {
            DietModel model = new DietModel();
            try
            {
                using (var dc = new FMSContext())
                {
                    model = dc.Diets.Where(x => x.Id == id && !x.IsDeleted).Select(x => new DietModel
                    {
                        DietName = x.DietName,
                        Id = x.Id,
                        UserId = x.UserId,
                        IsActive = x.IsActive
                    }).FirstOrDefault();
                    
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "DietPlanManager=>>AddDiet");
                
            }
            return model;
        }

        /// <summary>
        /// delete diet meals ingredient 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static bool  DeleteDiet(int id)
        {
            bool status = false;
            try
            {
                using (var dc = new FMSContext())
                {
                    var diet = dc.Diets.FirstOrDefault(x => x.Id == id && !x.IsDeleted); //delete diet
                    if (diet != null)
                    {
                        diet.IsDeleted = true;
                        status = dc.SaveChanges() > 0;
                        if (status)
                        {
                            var meals = dc.DietMeals.Where(x => x.DietId == id && !x.IsDeleted).ToList(); //delete diet meals 
                            if (meals.Count > 0)
                            {
                                meals.ForEach(x => x.IsDeleted = true);
                                status = dc.SaveChanges() > 0;
                                if (status)
                                {
                                    var mealIds = meals.Select(x => x.Id).ToList();
                                    var ingredients = dc.DietMealIngredients.Where(x => mealIds.Contains(x.MealId)).ToList(); //delete diet meals ingredient
                                    if(ingredients.Count > 0)
                                    {
                                        ingredients.ForEach(x => x.IsDeleted = true);
                                        status = dc.SaveChanges() > 0;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "DietPlanManager=>>DeleteDiet");
            }
            return status;
        }
        #endregion

        #region Meals

        /// <summary>
        /// Add/Update Meal
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static bool AddMeal(DietPlanMealModel model)
        {
            try
            {
                using (var dc = new FMSContext())
                {
                    var meal = dc.DietMeals.FirstOrDefault(x => x.Id == model.Id && !x.IsDeleted);
                    // for add new record
                    if (meal == null)
                    {
                        int count = dc.DietMeals.Count(x => x.DietId == model.DietId && !x.IsDeleted);
                        meal = new FMS.Core.DataModels.Diet.DietMeal
                        {
                            DietId = model.DietId,
                            MealName = $"Meal {count+1}",
                            CreatedBy = model.CreatedBy,
                            CreatedOn = DateTime.Now
                        };
                        dc.DietMeals.Add(meal);
                    }
                    // for update 
                    else
                    {
                        meal.DietId = model.DietId;
                        meal.MealName = model.MealName;
                        meal.ModifiedBy = model.CreatedBy;
                        meal.ModifiedOn = DateTime.Now;
                    }
                    dc.SaveChanges();
                    model.Id = meal.Id;
                    model.MealName = meal.MealName;
                }

                return true;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "DietPlanManager=>>AddMeal");
                return false;
            }
        }

        public static List<DietPlanMealModel> GetMealsByUser(int dietId)
        {
            List<DietPlanMealModel> meals = new List<DietPlanMealModel>();
            try
            {
                using (var dc = new FMSContext())
                {
                    meals = dc.DietMeals.Where(x => x.DietId == dietId).Select(x => new DietPlanMealModel
                    {
                        DietId = x.DietId,
                        MealName = x.MealName,
                        Id = x.Id
                    }).ToList();
                };

            }
            catch (Exception ex)
            {
                Log.Error(ex, "DietPlanManager=>>GetMealsByUser");

            }
            return meals;
        }

        public static Tuple<List<DietPlanMealModel>, string, int> GetMealsByUserDataTable(jQueryDataTableParamModel param, int dietId = 0)
        {
            using (var dc = new FMSContext())
            {
                var sortColumnIndex = param.iSortCol_0;
                var qry = dc.DietMeals.Where(x => x.DietId == dietId && !x.IsDeleted).Select(x => new DietPlanMealModel
                {
                    DietId = x.DietId,
                    MealName = x.MealName,
                    Id = x.Id
                });
                //Get total count
                var totalRecords = qry.Count();

                #region Searching 
                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    var toSearch = param.sSearch.ToLower();
                    qry = qry.Where(c => c.MealName.ToLower().Contains(toSearch)
                    );
                }
                #endregion

                #region  Sorting 
                switch (sortColumnIndex)
                {
                    //Sort by Name
                    case 0:
                        switch (param.sSortDir_0)
                        {
                            case "desc":
                                qry = qry.OrderByDescending(a => a.MealName);
                                break;
                            case "asc":
                                qry = qry.OrderBy(a => a.MealName);
                                break;
                            default:
                                qry = qry.OrderBy(a => a.MealName);
                                break;
                        }

                        break;
                    default:
                        qry = qry.OrderBy(a => a.MealName);
                        break;
                }
                #endregion

                #region  Paging 
                if (param.iDisplayLength != -1)
                    qry = qry.Skip(param.iDisplayStart).Take(param.iDisplayLength);
                #endregion
                return new Tuple<List<DietPlanMealModel>, string, int>(
                       (qry).ToList(),
                    param.sEcho,
                    totalRecords);
            }
        }

        /// <summary>
        /// Delete Meal 
        /// </summary>
        /// <param name="mealId"></param>
        /// <returns></returns>
        public static bool DeleteMeal(int mealId)
        {
            bool status = true;
            try
            {
                using (var dc = new FMSContext())
                {
                    var meal = dc.DietMeals.Where(x => x.Id == mealId).FirstOrDefault();
                    if (meal != null)
                    {
                        meal.IsDeleted = true;
                        if (dc.SaveChanges() > 0)
                        {
                            var ingredients = dc.DietMealIngredients.Where(x => x.MealId == mealId).ToList();
                            ingredients.ForEach(x => x.IsDeleted = true);
                            status = dc.SaveChanges() > 0;
                        }
                    }
                };

            }
            catch (Exception ex)
            {
                Log.Error(ex, "DietPlanManager=>>DeleteMeal");
                status = false;
            }
            return status;
        }
        #endregion

        #region Meal Ingredients
        /// <summary>
        /// Add/Update Meal Ingredient
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static bool AddMealIngredient(List<DietPlanMealIngredientModel> model)
        {
            bool status = true;
            try
            {
                if (model != null && model.Count > 0)
                {
                    using (var dc = new FMSContext())
                    {
                        var mealIds = model.Select(x => x.MealId).ToList();
                        var mealIngredientIds = dc.DietMealIngredients.Where(x => mealIds.Contains(x.MealId)).Select(x => x.Id).ToList();
                        var addIngredients = model.Where(x => x.Id == 0).ToList();
                        if (mealIngredientIds.Count > 0)
                        {
                            var removedIngredients = model.Where(x => !mealIngredientIds.Contains(x.Id)).Select(x => x.Id).ToList();
                            var updateIngredients = model.Where(x => mealIngredientIds.Contains(x.Id)).Select(x => x.Id).ToList();
                            if (removedIngredients.Count > 0)
                            {
                                var rmvdIng = dc.DietMealIngredients.Where(x => removedIngredients.Contains(x.Id)).ToList();
                                rmvdIng.ForEach(r => r.IsDeleted = true);
                            }

                            if (updateIngredients.Count > 0)
                            {
                                var updateIng = dc.DietMealIngredients.Where(x => updateIngredients.Contains(x.Id)).ToList();

                                foreach (var u in updateIng)
                                {
                                    var data = model.FirstOrDefault(x => x.Id == u.Id);
                                    u.IngredientId = data.IngredientId;
                                    u.Unit = data.Unit;
                                    u.ModifiedBy = data.ModifiedBy;
                                    u.ModifiedOn = DateTime.Now;
                                }
                            }
                        }
                        if (addIngredients.Count > 0)
                        {
                            List<DietMealIngredient> dataModel = new List<DietMealIngredient>();
                            foreach (var a in addIngredients)
                            {
                                dataModel.Add(new DietMealIngredient
                                {
                                    MealId = a.MealId,
                                    IngredientId = a.IngredientId,
                                    Unit = a.Unit,
                                    CreatedBy = a.ModifiedBy,
                                    CreatedOn = DateTime.Now,
                                });
                            }
                            dc.DietMealIngredients.AddRange(dataModel);
                        }
                        status = dc.SaveChanges() > 0;
                    }
                }
               
            }
            catch (Exception ex)
            {
                Log.Error(ex, "DietPlanManager=>>AddMealIngredient");
            }
            return status;
        }

        public static List<DietPlanMealIngredientModel> GetMealIngredientsByMeal(int mealId)
        {
            List<DietPlanMealIngredientModel> ingredient = new List<DietPlanMealIngredientModel>();
            try
            {
                using (var dc = new FMSContext())
                {
                    ingredient = dc.DietMealIngredients.Where(x => x.MealId == mealId).Select(x => new DietPlanMealIngredientModel
                    {
                        Id = x.Id,
                        MealId = x.MealId,
                        IngredientId = x.IngredientId,
                        Unit = x.Unit,
                    }).ToList();
                };
            }
            catch (Exception ex)
            {
                Log.Error(ex, "DietPlanManager=>>GetMealIngredientsByUser");

            }
            return ingredient;
        }

        public static List<DietPlanMealModel> GetMealsWithIngredientByUser(int dietId)
        {
            List<DietPlanMealModel> mealIngredient = new List<DietPlanMealModel>();
            try
            {
                using (var dc = new FMSContext())
                {

                    var data = (from meal in dc.DietMeals
                                join ing in dc.DietMealIngredients on meal.Id equals ing.MealId into ingGroup
                                from ing in ingGroup.DefaultIfEmpty()
                                join i in dc.Ingredients on ing.IngredientId equals i.Id into iGroup
                                from i in iGroup.DefaultIfEmpty()
                                where meal.DietId == dietId && !meal.IsDeleted
                                select new DietPlanMealIngredientModel
                                {
                                    MealId = meal.Id,
                                    MealName = meal.MealName,
                                    IngredientId = ing != null ? ing.IngredientId : 0,
                                    Ingredient = i != null ? i.Name : "",
                                    Unit = ing != null ? ing.Unit : 0,
                                    Id = ing != null ? ing.Id : 0,

                                }).GroupBy(x => x.MealId).ToList();

                    foreach(var dd in data)
                    {
                        mealIngredient.Add(new DietPlanMealModel
                        {
                            MealName = dd.FirstOrDefault()?.MealName,
                            Id = dd.FirstOrDefault()?.MealId ?? 0,
                            Ingredient = dd.ToList()
                        });
                    }

                };

            }
            catch (Exception ex)
            {
                Log.Error(ex, "DietPlanManager=>>GetMealsByUser");
            }
            return mealIngredient;
        }

        /// <summary>
        /// Delete Meal Ingredient
        /// </summary>
        /// <param name="mealId"></param>
        /// <returns></returns>
        public static bool DeleteMealIngredient(int mealIngredientId)
        {
            bool status = true;
            try
            {
                using (var dc = new FMSContext())
                {
                    var mealIngredient = dc.DietMealIngredients.Where(x => x.Id == mealIngredientId).FirstOrDefault();
                    if (mealIngredient != null)
                    {
                        mealIngredient.IsDeleted = true;
                        status = dc.SaveChanges() > 0;
                    }
                };

            }
            catch (Exception ex)
            {
                Log.Error(ex, "DietPlanManager=>>DeleteMealIngredient");
                status = false;
            }
            return status;
        }
        #endregion


        #region Select List
        public static List<SelectListItem> GetIngredientsSelectList()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            try
            {
                using (var dc = new FMSContext())
                {
                    list = dc.Ingredients.Where(x => !x.IsDeleted).Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.Id.ToString(),
                    }).ToList();
                };
            }
            catch (Exception ex)
            {
                Log.Error(ex, "DietPlanManager=>>DeleteMeal");
            }
            return list;
        }
        #endregion
    }
}
