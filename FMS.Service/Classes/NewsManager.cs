﻿using FMS.Core.DataModels;
using FMS.Core.Models;
using FMS.Data.Context;
using Serilog;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMS.Services.Classes
{
    public class NewsManager
    {

        /// <summary>
        /// get by selected id 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static NewsModel GetById(int id)
        {
            var dc = new FMSContext();
           return (from p in dc.News
                    join a in dc.Attachments on p.AttachmentId equals a.Id
                    into attachmentGJ
                    from attchment in attachmentGJ.DefaultIfEmpty()
                     
                    where p.IsDeleted == false && p.Id == id
                    //Binding Data with Model
                    select new NewsModel
                    {
                        Id = p.Id,
                        Title = p.Title,
                        Description = p.Description,
                        AttachmentId = p.AttachmentId,
                        PublishDate = p.PublishDate,
                        CreatedOn = p.CreatedOn,
                        Attachment = attchment,
                        IsActive = p.IsActive
                    }).FirstOrDefault();
           


            
        }

        /// <summary>
        /// Get all async
        /// </summary>
        /// <returns></returns>
        public static async Task<List<NewsModel>> GetAllAsync(int portalOwnerId)
        {
            var dc = new FMSContext();
            return  await(from news in dc.News
                          join a in dc.Attachments on news.AttachmentId equals a.Id into aGJ
                          from attachment in aGJ.DefaultIfEmpty()
                          where !news.IsDeleted && news.SitePortalOwnerId == portalOwnerId
                          select new NewsModel
                          {
                              Id = news.Id,
                              Title = news.Title,
                              Description = news.Description,
                              PublishDate = news.PublishDate,
                              AttachmentId = news.AttachmentId,
                              Attachment = attachment,      
                              IsActive = news.IsActive
                          }).ToListAsync();      
        }



        /// <summary>
        /// Used to add Update/Add news
        /// </summary>
        /// <param name="roleModel"></param>
        /// <returns></returns>
        public static bool Update(NewsModel newsModel)
        {
            var dc = new FMSContext();
            var news = dc.News.FirstOrDefault(x => x.Id == newsModel.Id);
            try
            {
                // for add new record
                if (news == null)
                {
                    news = new News
                    {
                        Id = newsModel.Id,
                        Title = newsModel.Title,
                        PublishDate = newsModel.PublishDate,
                        Description = newsModel.Description,
                        AttachmentId = newsModel.AttachmentId,
                        IsDeleted = newsModel.IsDeleted,
                        CreatedOn = DateTime.UtcNow,
                        CreatedBy = newsModel.CreatedBy,
                        OwnerId = newsModel.OwnerId,
                        SitePortalOwnerId = newsModel.SitePortalOwnerId,
                        IsActive = newsModel.IsActive
                    };
                    dc.News.Add(news);
                }
                // for update 
                else
                {
                    news.Id = newsModel.Id;
                    news.Title = newsModel.Title;
                    news.PublishDate = newsModel.PublishDate;
                    news.Description = newsModel.Description;
                    news.AttachmentId = newsModel.AttachmentId;
                    news.IsDeleted = newsModel.IsDeleted;
                    news.CreatedOn = DateTime.Now;
                    news.CreatedBy = newsModel.CreatedBy;
                    news.ModifiedOn = DateTime.Now;
                    news.ModifiedBy = newsModel.ModifiedBy;
                    news.OwnerId = newsModel.OwnerId;
                    news.SitePortalOwnerId = newsModel.SitePortalOwnerId;
                    news.IsActive = newsModel.IsActive;
                }
                dc.SaveChanges();
                newsModel.Id = news.Id;
                return true;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "RoleManager=>>Update");
                return false;
            }
        }



        ///// <summary>
        ///// Get all Data of news to bind with data-table 
        ///// </summary>
        ///// <param name="param"></param>
        ///// <returns></returns>
        public static async Task<Tuple<List<NewsModel>, string, int>> GetDataTableList(jQueryDataTableParamModel param)
        {
            List<NewsModel> roleModels = new List<NewsModel>();
            var dc = new FMSContext();
            var sortColumnIndex = param.iSortCol_0;

            var qry = from news in dc.News
                      join a in dc.Attachments on news.AttachmentId equals a.Id
                      into attachmentGJ
                      from attchment in attachmentGJ.DefaultIfEmpty()
                      where news.IsDeleted == false
                      select new NewsModel
                      {
                          Id = news.Id,
                          Title = news.Title,
                          Description = news.Description,
                          PublishDate = news.PublishDate,
                          AttachmentId = news.AttachmentId,
                          IsActive = news.IsActive,
                          Attachment = attchment
                      };

            //Get total count
            var totalRecords = await qry.CountAsync();

            #region Searching 
            if (!string.IsNullOrEmpty(param.sSearch))
            {
                var toSearch = param.sSearch.ToLower();
                qry = qry.Where(c => c.Title.ToLower().Contains(toSearch)
                || c.AttachmentId.ToString().Contains(toSearch));
            }
            #endregion

            #region  Sorting 
            switch (sortColumnIndex)
            {
                //Sort by Title
                case 0:
                    switch (param.sSortDir_0)
                    {
                        case "desc":
                            qry = qry.OrderByDescending(a => a.Title);
                            break;
                        case "asc":
                            qry = qry.OrderBy(a => a.Title);
                            break;
                        default:
                            qry = qry.OrderBy(a => a.Title);
                            break;
                    }

                    break;

                case 1:
                    switch (param.sSortDir_0)
                    {
                        case "desc":
                            qry = qry.OrderByDescending(a => a.Description);
                            break;
                        case "asc":
                            qry = qry.OrderBy(a => a.Description);
                            break;
                        default:
                            qry = qry.OrderBy(a => a.Description);
                            break;
                    }

                    break;

                default:
                    qry = qry.OrderBy(a => a.Title);
                    break;
            }
            #endregion

            #region  Paging 
            if (param.iDisplayLength != -1)
                qry = qry.Skip(param.iDisplayStart).Take(param.iDisplayLength);
            #endregion

            return new Tuple<List<NewsModel>, string, int>(
                await
                   (qry).ToListAsync(),
                param.sEcho,
                totalRecords);

        }

        /// <summary>
        /// Update News Status
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static bool UpdateNewsStatus(long id)
        {
            var dc = new FMSContext();
            var news = dc.News.FirstOrDefault(x => x.Id == id && x.IsDeleted == false);
            if (news == null)
                return true;

            news.IsActive = !news.IsActive;
            return dc.SaveChanges() > 0;
        }

        /// <summary>
        /// Soft delete selected News According to Id 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns>Boolean Result</returns>
        public static bool DeleteNews(long id)
        {
            var dc = new FMSContext();
            var news = dc.News.FirstOrDefault(x => x.Id == id && x.IsDeleted == false);
            if (news == null)
                return true;
            news.IsDeleted = true;
            return dc.SaveChanges() > 0;
        }

    }
}
