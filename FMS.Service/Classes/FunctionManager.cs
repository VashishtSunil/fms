﻿using System.Collections.Generic;
using System.Linq;
using FMS.Core.Models;
using FMS.Data.Context;

namespace FMS.Services.Classes
{
    public class FunctionManager
    {

        /// <summary>
        /// Get all Portals Data 
        /// </summary>
        /// <returns></returns>
        public static List<FunctionModel> GetAll(string userRoles)
        {
            int[] integerRoleIds = new int[] { -1 };

            if (!string.IsNullOrEmpty(userRoles))
            {
                var roleIdArray = userRoles.Split(',');
                integerRoleIds = roleIdArray.Select(int.Parse).ToArray();
            }

            var dc = new FMSContext();
            return (from roleFunction in dc.RoleFunction
                    join function in dc.Functions.OrderBy(x => x.DisplayOrder)
                    on roleFunction.FunctionId equals function.Id

                    where function.IsDeleted == false && function.IsActive ==true && integerRoleIds.Contains(roleFunction.RoleId)
                    select new FunctionModel
                    {
                        Id = function.Id,
                        Title = function.Title,
                        Url = function.Url,
                        Icon = function.Icon,
                        DisplayOrder = function.DisplayOrder
                    }).ToList();
        }

        /// <summary>
        /// Get All Function List
        /// </summary>
        /// <returns></returns>
        public static List<FunctionModel> GetAllFunctionList(long roleId)
        {
            var dc = new FMSContext();
            return (from function in dc.Functions
                    join roleFunction in dc.RoleFunction
                    on function.Id equals roleFunction.FunctionId
                    into roleFunctionList
                    //from roleFunctionData in roleFunctionList.DefaultIfEmpty()
                    where function.IsDeleted == false
                    select new FunctionModel
                    {
                        Id = function.Id,
                        Title = function.Title,
                        Url = function.Url,
                        Icon = function.Icon,
                        DisplayOrder = function.DisplayOrder,
                        IsCreate = roleFunctionList.Any(x => x.IsCreate == true && x.RoleId == roleId),
                        IsDelete = roleFunctionList.Any(x => x.IsDelete == true && x.RoleId == roleId),
                        IsEdit = roleFunctionList.Any(x => x.IsEdit == true && x.RoleId == roleId),
                        IsView = roleFunctionList.Any(x => x.IsView == true && x.RoleId == roleId),
                    }).ToList();
        }

        /// <summary>
        ///  Get Function By Url
        /// </summary>
        /// <returns></returns>
        public static FunctionModel GetPermissionByUrl(string path, string userRoles)
        {
            int[] integerRoleIds = new int[] { -1 };
            if (!string.IsNullOrEmpty(userRoles))
            {
                var roleIdArray = userRoles.Split(',');
                integerRoleIds = roleIdArray.Select(int.Parse).ToArray();
            }
            var dc = new FMSContext();
            return (from function in dc.Functions
                    join roleFunction in dc.RoleFunction
                    on function.Id equals roleFunction.FunctionId
                    where function.IsDeleted == false && function.Url.ToLower().Contains(path.ToLower())
                    && integerRoleIds.Contains(roleFunction.RoleId)
                    select new FunctionModel
                    {
                        Id = function.Id,
                        Title = function.Title,
                        Url = function.Url,
                        Icon = function.Icon,
                        DisplayOrder = function.DisplayOrder,
                        RoleId = roleFunction.RoleId,
                        IsCreate = roleFunction.IsCreate,
                        IsEdit = roleFunction.IsEdit,
                        IsDelete = roleFunction.IsDelete,
                        IsView = roleFunction.IsView
                    }).FirstOrDefault();


        }

    }
}
