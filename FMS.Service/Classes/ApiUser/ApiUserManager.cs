﻿using FMS.Core.Models;
using FMS.Data.Context;
using Serilog;
using System;
using System.Linq;

namespace FMS.Services.Classes.ApiUser
{
    public class ApiUserManager
    {

        /// <summary>
        /// Validate Api user Keys
        /// </summary>
        /// <param name="apiUserModel"></param>
        public static bool IsAPICredentilaValid(ApiUserModel apiUserModel)
        {
            var dc = new FMSContext();
            var ApiUsers = dc.ApiUsers.FirstOrDefault(x => x.ClientId == apiUserModel.ClientId &&
                                                           x.ClientSecret == apiUserModel.ClientSecret &&
                                                           x.ApiKey == apiUserModel.ApiKey &&
                                                           x.ClientKey == apiUserModel.ClientKey
                                                          );
            return (ApiUsers != null) ? true : false;
        }

        //Getting Client Secret Key
        public static string GetClientSecretKey(ApiUserModel apiUserModel)
        {
            var dc = new FMSContext();
            var clientSecret = dc.ApiUsers.FirstOrDefault(x => x.ClientId == apiUserModel.ClientId).ClientSecret;
            return (!string.IsNullOrEmpty(clientSecret)) ? clientSecret : "Not Found";
        }

        /// <summary>
        /// Add / Update Api Keys
        /// </summary>
        /// <param name="apiUserModel"></param>
        /// <returns>Boolean Result</returns>
        public static bool UpdateApiKeys(ApiUserModel apiUserModel)
        {
            var dc = new FMSContext();
            var keys = dc.ApiUsers.FirstOrDefault(x => x.Id == apiUserModel.Id);
            try
            {
                // for add new record
                if (keys == null)
                {
                    keys = new Core.DataModels.ApiUser.ApiUser
                    {
                        ClientSecret = apiUserModel.ClientSecret,
                        ClientId = apiUserModel.ClientId,
                        ClientKey = apiUserModel.ClientKey,
                        ApiKey = apiUserModel.ApiKey,
                        // BrandId=apiUserModel.BrandId,
                        CreatedBy = apiUserModel.CreatedBy
                    };
                    dc.ApiUsers.Add(keys);
                    dc.SaveChanges();
                }
                // for update 
                else
                {
                    keys.ClientSecret = apiUserModel.ClientSecret;
                    keys.ClientId = apiUserModel.ClientId;
                    keys.ClientKey = apiUserModel.ClientKey;
                    keys.ApiKey = apiUserModel.ApiKey;
                    // keys.BrandId = apiUserModel.BrandId;
                    keys.ModifiedBy = apiUserModel.CreatedBy;
                    keys.ModifiedOn = DateTime.Now;

                    dc.SaveChanges();
                }
                apiUserModel.Id = keys.Id;
                return true;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "ApiUserManager=>>UpdateBrand");
                return false;
            }
        }

        /// <summary>
        /// Delete Api Keys by Client Id
        /// </summary>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public static bool DeleteKey(long clientId)
        {
            var dc = new FMSContext();
            var key = dc.ApiUsers.FirstOrDefault(x => x.Id == clientId && x.IsDeleted == false);
            if (key == null)
                return true;

            key.IsDeleted = true;
            return dc.SaveChanges() > 0;
        }
    }
}
