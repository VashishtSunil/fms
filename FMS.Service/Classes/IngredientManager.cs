﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using FMS.Core.DataModels.Diet;
using FMS.Core.Models;
using FMS.Data.Context;
using Serilog;

namespace FMS.Services.Classes
{
    public class IngredientManager
    {
        #region Ingredient       

        /// <summary>
        /// Get all Data of Ingredient to bind with data-table 
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public static async Task<Tuple<List<IngredientsModel>, string, int>> GetDataTableList(jQueryDataTableParamModel param)
        {
            List<IngredientsModel> IngredientModels = new List<IngredientsModel>();
            var dc = new FMSContext();
            var sortColumnIndex = param.iSortCol_0;
            var qry = from Ingredient in dc.Ingredients
                      where Ingredient.IsDeleted == false
                      select new IngredientsModel
                      {
                          Id = Ingredient.Id,
                          Name = Ingredient.Name,
                          Calorie = Ingredient.Calorie,
                          Fats = Ingredient.Fats,
                          Sugar = Ingredient.Sugar,
                          Protein = Ingredient.Protein,
                          Fiber = Ingredient.Fiber,
                          Carbs = Ingredient.Carbs,
                          IsDeleted = Ingredient.IsDeleted,
                          IsActive = Ingredient.IsActive,
                          CreatedOn = DateTime.Now,
                          CreatedBy = Ingredient.CreatedBy
                      };
            //Get total count
            var totalRecords = await qry.CountAsync();
            #region Searching 
            if (!string.IsNullOrEmpty(param.sSearch))
            {
                var toSearch = param.sSearch.ToLower();
                qry = qry.Where(c => c.Name.ToLower().Contains(toSearch));
            }
            #endregion

            #region  Sorting 
            switch (sortColumnIndex)
            {
                //Sort by IngredientName
                case 0:
                    switch (param.sSortDir_0)
                    {
                        case "desc":
                            qry = qry.OrderByDescending(a => a.Name);
                            break;
                        case "asc":
                            qry = qry.OrderBy(a => a.Name);
                            break;
                        default:
                            qry = qry.OrderBy(a => a.Name);
                            break;
                    }
                    break;
                default:
                    qry = qry.OrderBy(a => a.Name);
                    break;
            }
            #endregion

            #region  Paging 
            if (param.iDisplayLength != -1)
                qry = qry.Skip(param.iDisplayStart).Take(param.iDisplayLength);
            #endregion


            return new Tuple<List<IngredientsModel>, string, int>(
                 await
                    (qry).ToListAsync(),
                 param.sEcho,
                 totalRecords);

        }

        /// <summary>
        /// get by selected id 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static IngredientsModel GetById(long id)
        {
            var dc = new FMSContext();
            return (from p in dc.Ingredients
                    where p.IsDeleted == false && p.Id == id
                    //Binding Data with Model
                    select new IngredientsModel
                    {
                        Name = p.Name,
                        Calorie = p.Calorie,
                        Fats = p.Fats,
                        Sugar = p.Sugar,
                        Protein = p.Protein,
                        Fiber = p.Fiber,
                        Carbs = p.Carbs,
                        IsDeleted = p.IsDeleted,
                        IsActive = p.IsActive,
                        CreatedOn = DateTime.Now,
                        CreatedBy = p.CreatedBy
                    }).FirstOrDefault();
        }

        /// <summary>
        /// Used to add Update/Add Ingredient
        /// </summary>
        /// <param name="IngredientModel"></param>
        /// <returns></returns>
        public static bool Update(IngredientsModel IngredientModel)
        {
            var dc = new FMSContext();
            var Ingredients = dc.Ingredients.FirstOrDefault(x => x.Id == IngredientModel.Id);
            try
            {
                // for add new record
                if (Ingredients == null)
                {
                    Ingredients = new Ingredient
                    {
                        Name = IngredientModel.Name,
                        Calorie = IngredientModel.Calorie,
                        Fats = IngredientModel.Fats,
                        Sugar = IngredientModel.Sugar,
                        Protein = IngredientModel.Protein,
                        Fiber = IngredientModel.Fiber,
                        Carbs = IngredientModel.Carbs,
                        IsDeleted = IngredientModel.IsDeleted,
                        IsActive = IngredientModel.IsActive,
                        CreatedOn = DateTime.Now,
                        CreatedBy = IngredientModel.CreatedBy
                    };
                    dc.Ingredients.Add(Ingredients);
                }
                // for update 
                else
                {
                    Ingredients.Name = IngredientModel.Name;
                    Ingredients.Carbs = IngredientModel.Carbs;
                    Ingredients.Sugar = IngredientModel.Sugar;
                    Ingredients.Protein = IngredientModel.Protein;
                    Ingredients.Calorie = IngredientModel.Calorie;
                    Ingredients.Fats = IngredientModel.Fats;
                    Ingredients.Fiber = IngredientModel.Fiber;
                    //Base Entities            
                    Ingredients.IsActive = IngredientModel.IsActive;
                    Ingredients.IsDeleted = IngredientModel.IsDeleted;
                    Ingredients.ModifiedOn = DateTime.Now;
                    Ingredients.ModifiedBy = IngredientModel.ModifiedBy;
                }
                dc.SaveChanges();
                IngredientModel.Id = Ingredients.Id;
                return true;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "IngredientManager=>>Update");
                return false;
            }
        }

        /// <summary>
        /// Soft delete selected Ingredient According to Id 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns>Boolean Result</returns>
        public static bool DeleteIngredient(long id)
        {
            var dc = new FMSContext();
            var Ingredients = dc.Ingredients.FirstOrDefault(x => x.Id == id && x.IsDeleted == false);
            if (Ingredients == null)
                return true;
            Ingredients.IsDeleted = true;
            return dc.SaveChanges() > 0;
        }



        /// <summary>
        /// check is Ingredient is duplicate or not
        /// </summary>
        /// <param name="IngredientModel"></param>
        /// <returns></returns>
        public static bool IsIngredientDuplicate(IngredientsModel IngredientModel)
        {
            var dc = new FMSContext();
            var IngredientDeatil = dc.Ingredients.FirstOrDefault(x => x.Name == IngredientModel.Name && x.IsDeleted != true);
            if (IngredientDeatil == null)
                return false;
            if (IngredientDeatil.Id != IngredientModel.Id)
                return true;
            return false;
        }


        /// <summary>
        /// Ingredient list for drop down
        /// </summary>
        /// <returns></returns>
        public static SelectList GetSelectList()
        {
            SelectList selectListItFMS = null;
            try
            {
                var dc = new FMSContext();
                var Ingredient = dc.Ingredients.Where(x => x.IsDeleted == false).ToList();

                if (Ingredient != null && Ingredient.Count > 0)
                {
                    selectListItFMS = new SelectList(Ingredient, "Id", "IngredientName");
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "IngredientManager=>>GetSelectList");
            }
            return selectListItFMS;
        }

        /// <summary>
        /// get Ingredient by selected IngredientName 
        /// </summary>
        /// <param name="IngredientName"></param>
        /// <returns></returns>
        public static IngredientsModel GetIngredientByName(string IngredientName)
        {
            var dc = new FMSContext();
            return (from p in dc.Ingredients
                    where p.IsDeleted == false
                    select new IngredientsModel
                    {

                    }).FirstOrDefault();
        }
        #endregion
    }
}
