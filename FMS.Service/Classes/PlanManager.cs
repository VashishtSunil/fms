﻿using FMS.Core.DataModels.Plan;
using FMS.Core.Models;
using FMS.Data.Context;
using Serilog;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FMS.Services.Classes
{
    public class PlanManager
    {


        #region Plan

        /// <summary>
        /// add/update plan
        /// </summary>
        /// <param name="planModel"></param>
        /// <returns></returns>
        public static async Task<Tuple<bool, string>> UpdatePlan(PlanModel planModel)
        {
            bool status = false;
            string message = "Something Went Wrong!";
            try
            {
                var dc = new FMSContext();
                var planDetail = dc.Plans.FirstOrDefault(x => x.Id == planModel.Id && !x.IsDeleted);
                if (planDetail == null)
                {
                    planDetail = new Plan()
                    {
                        Name = planModel.Name,
                        Price = planModel.Price,
                        Description = planModel.Description,
                        IsDisplayOnPublicPage = planModel.IsDisplayOnPublicPage,
                        CreatedBy = planModel.CreatedBy,
                        OwnerId = planModel.OwnerId,
                        SitePortalOwnerId = planModel.SitePortalOwnerId,
                        PlanLengthInDays = planModel.PlanLengthInDays
                    };
                    dc.Plans.Add(planDetail);
                    await dc.SaveChangesAsync();
                    planModel.Id = planDetail.Id;

                    status = true;
                    message = "Plan Added Successfully!," + planModel.Id;
                }
                else
                {
                    planDetail.Name = planModel.Name;
                    planDetail.Price = planModel.Price;
                    planDetail.Description = planModel.Description;
                    planDetail.IsDisplayOnPublicPage = planModel.IsDisplayOnPublicPage;
                    planDetail.SitePortalOwnerId = planModel.SitePortalOwnerId;
                    planDetail.ModifiedBy = planModel.ModifiedBy;
                    planDetail.ModifiedOn = DateTime.Now;
                    await dc.SaveChangesAsync();
                    status = true;
                    message = "Plan Updated Successfully!," + planModel.Id;
                }
            }
            catch (Exception ex)
            {
                status = false;
                message = "Something Went Wrong!";
            }
            return Tuple.Create(status, message);
        }

        /// <summary>
        /// Soft delete selected Plan According to Id 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static bool DeletePlan(long id)
        {
            var dc = new FMSContext();
            var plans = dc.Plans.FirstOrDefault(x => x.Id == id && x.IsDeleted == false);
            if (plans == null)
                return true;
            plans.IsDeleted = true;
            return dc.SaveChanges() > 0;
        }

        /// <summary>
        /// get plan detail by id
        /// </summary>
        /// <param name="planId"></param>
        /// <returns></returns>
        public static async Task<PlanModel> GetPlanById(int planId)
        {
            PlanModel planModel = new PlanModel();
            try
            {
                var dc = new FMSContext();
                planModel = await dc.Plans.Where(x => x.Id == planId && !x.IsDeleted).Select(x => new PlanModel()
                {
                    Id = x.Id,
                    Name = x.Name,
                    Price = x.Price,
                    Description = x.Description,
                    IsDisplayOnPublicPage = x.IsDisplayOnPublicPage,
                    SitePortalOwnerId = x.SitePortalOwnerId,
                    CreatedBy = x.CreatedBy,
                    ModifiedBy = x.ModifiedBy,
                    OwnerId = x.OwnerId,
                    IsSeeded = x.IsSeeded,
                    PlanLengthInDays = x.PlanLengthInDays
                }).FirstOrDefaultAsync();
            }
            catch (Exception ex)
            {
                planModel = null;
                Log.Error(ex, "PlanManager=>>GetPlanById");
            }
            return planModel;
        }

        /// <summary>
        /// get all the plan for datatable
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public static async Task<Tuple<List<PlanModel>, string, int>> GetPlansDataTable(jQueryDataTableParamModel param)
        {
            var dc = new FMSContext();
            var sortColumnIndex = param.iSortCol_0;
            var qry = from plan in dc.Plans
                      join planFeature in dc.PlanFeatures on
                      plan.Id equals planFeature.PlanId into
                      planFeatureList
                      where !plan.IsDeleted
                      select new PlanModel
                      {
                          Id = plan.Id,
                          Name = plan.Name,
                          Price = plan.Price,
                          OwnerId = plan.OwnerId,
                          SitePortalOwnerId = plan.SitePortalOwnerId,
                          FeatureCount = planFeatureList.Count(),
                          IsActive = plan.IsActive,
                          PlanLengthInDays = plan.PlanLengthInDays
                      };
            //Get total count
            var totalRecords = await qry.CountAsync();

            #region Searching 
            if (!string.IsNullOrEmpty(param.sSearch) && param.sSearch != "$")
            {
                var toSearch = param.sSearch.ToLower();
                toSearch = toSearch.Replace("$", "");
                qry = qry.Where(c => c.Name.ToLower().Contains(toSearch)
                || c.Price.ToString().Contains(toSearch));
                totalRecords = await qry.CountAsync();
            }
            #endregion

            #region  Sorting 
            switch (sortColumnIndex)
            {
                //Sort by Name
                case 0:
                    switch (param.sSortDir_0)
                    {
                        case "desc":
                            qry = qry.OrderByDescending(a => a.Name);
                            break;
                        case "asc":
                            qry = qry.OrderBy(a => a.Name);
                            break;
                        default:
                            qry = qry.OrderBy(a => a.Name);
                            break;
                    }
                    break;
                //Sort by price
                case 1:
                    switch (param.sSortDir_0)
                    {
                        case "desc":
                            qry = qry.OrderByDescending(a => a.Price);
                            break;
                        case "asc":
                            qry = qry.OrderBy(a => a.Price);
                            break;
                        default:
                            qry = qry.OrderBy(a => a.Price);
                            break;
                    }
                    break;
                default:
                    qry = qry.OrderBy(a => a.Name);
                    break;
            }
            #endregion

            #region  Paging 
            if (param.iDisplayLength != -1)
                qry = qry.Skip(param.iDisplayStart).Take(param.iDisplayLength);
            #endregion


            return new Tuple<List<PlanModel>, string, int>(
                 await
                    (qry).ToListAsync(),
                 param.sEcho,
                 totalRecords);
        }

        /// <summary>
        /// check whether plan is duplicate or not
        /// </summary>
        /// <param name="planModel"></param>
        /// <returns></returns>
        public static bool IsPlanDuplicate(PlanModel planModel)
        {
            var dc = new FMSContext();
            var planDetail = dc.Plans.FirstOrDefault(x => x.Name == planModel.Name && !x.IsDeleted);
            if (planDetail == null)
                return false;
            if (planDetail.Id != planModel.Id)
                return true;
            return false;
        }

        #endregion 

        #region PlanFeature

        /// <summary>
        /// Used to add Update/Add Plan Feature
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static bool UpdatePlanFeature(PlanFeatureModel model)
        {
            var dc = new FMSContext();
            var planFeatures = dc.PlanFeatures.FirstOrDefault(x => x.Id == model.Id);
            try
            {
                // for add new record
                if (planFeatures == null)
                {
                    planFeatures = new PlanFeature
                    {
                        PlanId = model.PlanId,
                        Feature = model.Feature,
                        OwnerId = model.OwnerId,
                        SitePortalOwnerId = model.SitePortalOwnerId,
                        IsDeleted = false,
                        IsActive = model.IsActive
                    };
                    dc.PlanFeatures.Add(planFeatures);

                }
                // for update 
                else
                {
                    planFeatures.PlanId = model.PlanId;
                    planFeatures.Feature = model.Feature;
                    planFeatures.OwnerId = model.OwnerId;
                    planFeatures.SitePortalOwnerId = model.SitePortalOwnerId;
                    planFeatures.IsActive = model.IsActive;
                }
                dc.SaveChanges();
                model.Id = planFeatures.Id;
                return true;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "PlanManager=>>UpdatePlanFeature");
                return false;
            }
        }

        /// <summary>
        /// get all the plan feature for datatable
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public static async Task<Tuple<List<PlanFeatureModel>, string, int>> GetPlanFeatureDataTable(jQueryDataTableParamModel param)
        {
            var dc = new FMSContext();
            var sortColumnIndex = param.iSortCol_0;
            int planId = Convert.ToInt32(param.id);
            var qry = dc.PlanFeatures.Where(x => x.Id > 0 && x.IsDeleted == false && x.PlanId == planId);

            //Get total count
            var totalRecords = await qry.CountAsync();

            #region Searching 
            if (!string.IsNullOrEmpty(param.sSearch))
            {
                var toSearch = param.sSearch.ToLower();
                qry = qry.Where(c => c.Feature.ToLower().Contains(toSearch));
            }
            #endregion

            #region  Sorting 
            switch (sortColumnIndex)
            {
                //Sort by Feature
                case 0:
                    switch (param.sSortDir_0)
                    {
                        case "desc":
                            qry = qry.OrderByDescending(a => a.Feature);
                            break;
                        case "asc":
                            qry = qry.OrderBy(a => a.Feature);
                            break;
                        default:
                            qry = qry.OrderBy(a => a.Feature);
                            break;
                    }

                    break;

                default:
                    qry = qry.OrderBy(a => a.Feature);
                    break;
            }
            #endregion

            #region  Paging 
            if (param.iDisplayLength != -1)
                qry = qry.Skip(param.iDisplayStart).Take(param.iDisplayLength);
            #endregion

            return new Tuple<List<PlanFeatureModel>, string, int>(
                await
                    (from plan in qry
                     select new PlanFeatureModel
                     {
                         Id = plan.Id,
                         Feature = plan.Feature,
                         OwnerId = plan.OwnerId,
                         SitePortalOwnerId = plan.SitePortalOwnerId,
                         IsActive = plan.IsActive
                     }).ToListAsync(),
                param.sEcho,
                totalRecords);
        }

        /// <summary>
        /// Soft delete selected Plan Feature According to Id 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns>Boolean Result</returns>
        public static bool DeletePlanFeature(long id)
        {
            var dc = new FMSContext();
            var roles = dc.PlanFeatures.FirstOrDefault(x => x.Id == id && x.IsDeleted == false);
            if (roles == null)
                return true;
            roles.IsDeleted = true;
            return dc.SaveChanges() > 0;
        }

        /// <summary>
        /// Soft delete selected Delete All Plan feature According to Plan Id  
        /// </summary>
        /// <param name="planId"></param>
        /// <returns></returns>
        public static Tuple<bool, string> DeleteAllPlanFeatureByPlanId(long planId)
        {
            bool status = false;
            string message = "Something Went Wrong!";
            try
            {
                var dc = new FMSContext();
                var planFeatureMapping = dc.PlanFeatures.Where(x => x.PlanId == planId).ToList();
                if (planFeatureMapping != null && planFeatureMapping.Count > 0)
                {
                    planFeatureMapping.ForEach(p => p.IsDeleted = true);
                    dc.SaveChanges();
                }
                status = true;
                message = "Plan Feature Mapping Deleted Successfully!";
            }
            catch (Exception ex)
            {
                status = false;
                message = "Something Went Wrong!";
                Serilog.Log.Error(ex, "PlanManager=>>DeleteAllPlanFeatureByPlanId");
            }
            return Tuple.Create(status, message);
        }

        /// <summary>
        /// get List of all plan
        /// </summary>
        /// <returns></returns>
        public static List<PlanModel> GetPlansList(int ownerId)
        {
            var dc = new FMSContext();
            List<PlanModel> planModels = new List<PlanModel>();
            var plans = dc.Plans.Include(x => x.PlanFeatures).Where(x => !x.IsDeleted && x.SitePortalOwnerId == ownerId).ToList();

            foreach (var plan in plans)
            {
                // add Plans 
                PlanModel planModel = new PlanModel
                {
                    Id = plan.Id,
                    Name = plan.Name,
                    Price = plan.Price,
                    OwnerId = plan.OwnerId,
                    SitePortalOwnerId = plan.SitePortalOwnerId,
                    PlanLengthInDays = plan.PlanLengthInDays
                };
                // add plans Features
                planModel.LstPlanFeature = plan.PlanFeatures.Select(x => new PlanFeatureModel
                {
                    Id = x.Id,
                    Feature = x.Feature,
                    PlanId = x.PlanId,
                    OwnerId = plan.OwnerId,
                    SitePortalOwnerId = plan.SitePortalOwnerId
                }).ToList();
                planModels.Add(planModel);
            }
            return planModels;

        }


        /// <summary>
        /// Update Plan Status
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static bool UpdatePlanStatus(long id)
        {
            var dc = new FMSContext();
            var news = dc.Plans.FirstOrDefault(x => x.Id == id && x.IsDeleted == false);
            if (news == null)
                return true;

            news.IsActive = !news.IsActive;
            return dc.SaveChanges() > 0;
        }



        public static SelectList GetSelectPlanList()
        {
            SelectList selectListItFMS = null;
            try
            {
                var dc = new FMSContext();
                var plan = dc.Plans.Where(x => x.IsDeleted == false && x.IsActive == true).ToList();

                if (plan != null && plan.Count > 0)
                {
                    selectListItFMS = new SelectList(plan, "Id", "Name");
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "PlanManager=>>GetSelectPlanList");
            }
            return selectListItFMS;
        }
        #endregion


    }
}
