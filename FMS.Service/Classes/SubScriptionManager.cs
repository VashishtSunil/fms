﻿using FMS.Core.DataModels.Subscription;
using FMS.Core.Enums;
using FMS.Core.Models;
using FMS.Core.Models.Payment;
using FMS.Core.Models.Subscription;
using FMS.Data.Context;
using Serilog;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace FMS.Services.Classes
{
    public class SubscriptionManager
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="planId"></param>
        /// <returns></returns>
        public static PlanModel GetPaymentPlanByPlanId(long planId)
        {
            var dc = new FMSContext();
            return dc.Plans.Where(x => x.Id == planId && !x.IsDeleted).Select(
                x => new PlanModel
                {
                    Id = x.Id,
                    Name = x.Name,
                    PlanLengthInDays = x.PlanLengthInDays,
                    Price = x.Price,
                    Description = x.Description
                }).FirstOrDefault();
        }

        #region Subscription
        /// <summary>
        /// add/update subscription
        /// </summary>
        /// <param name="userSubscriptionModel"></param>
        /// <returns></returns>
        public static async Task<Tuple<bool, string>> UpdateUserSubscription(UserSubscriptionModel userSubscriptionModel)
        {
            bool status = false;
            string message = "Something Went Wrong!";
            try
            {
                var dc = new FMSContext();
                var subscriptionDetail = dc.UserSubscriptions.Where(x => x.Id == userSubscriptionModel.Id && !x.IsDeleted).FirstOrDefault();
                if (subscriptionDetail == null)
                {
                    subscriptionDetail = new UserSubscription()
                    {
                        PlanId = userSubscriptionModel.PlanId,
                        UserId = userSubscriptionModel.UserId,

                        PricePaid = userSubscriptionModel.PricePaid,
                        PlanExpirationDateTime = userSubscriptionModel.PlanExpirationDateTime,
                        IsFreeTrailPeriod = userSubscriptionModel.IsFreeTrailPeriod,
                        IsTrailPeriodWarninEmailSent = userSubscriptionModel.IsTrailPeriodWarninEmailSent,
                        IsCardDetailExpiredEmailSent = userSubscriptionModel.IsCardDetailExpiredEmailSent,
                        ExpiredCardId = userSubscriptionModel.ExpiredCardId,

                        IsCardAddedAfterTrailExpired = userSubscriptionModel.IsCardAddedAfterTrailExpired,

                        GatewayCustomerId = userSubscriptionModel.GatewayCustomerId,
                        GatewaySubscriptionId = userSubscriptionModel.GatewaySubscriptionId,
                        Description = userSubscriptionModel.Description,
                        PlanStatus = (int)userSubscriptionModel.PlanStatus,
                        IsPaymentPending = userSubscriptionModel.IsPaymentPending,
                        IsPlanAutoRenewal = userSubscriptionModel.IsPlanAutoRenewal,
                        CreatedOn = DateTime.UtcNow,
                        CreatedBy = userSubscriptionModel.CreatedBy,
                        IsActive = userSubscriptionModel.IsActive
                    };
                    dc.UserSubscriptions.Add(subscriptionDetail);
                    await dc.SaveChangesAsync();
                    userSubscriptionModel.Id = subscriptionDetail.Id;
                    message = "Subscription Detail Added Successfully!";
                }
                else
                {
                    subscriptionDetail.UserId = userSubscriptionModel.UserId;
                    subscriptionDetail.PlanId = userSubscriptionModel.PlanId;

                    subscriptionDetail.PlanStatus = (int)userSubscriptionModel.PlanStatus;
                    subscriptionDetail.PlanExpirationDateTime = userSubscriptionModel.PlanExpirationDateTime;
                    subscriptionDetail.IsFreeTrailPeriod = userSubscriptionModel.IsFreeTrailPeriod;
                    subscriptionDetail.IsPlanAutoRenewal = userSubscriptionModel.IsPlanAutoRenewal;

                    subscriptionDetail.IsTrailPeriodWarninEmailSent = userSubscriptionModel.IsTrailPeriodWarninEmailSent;
                    subscriptionDetail.IsCardDetailExpiredEmailSent = userSubscriptionModel.IsCardDetailExpiredEmailSent;
                    subscriptionDetail.ExpiredCardId = userSubscriptionModel.ExpiredCardId;
                    subscriptionDetail.IsPaymentPending = userSubscriptionModel.IsPaymentPending;

                    subscriptionDetail.IsCardAddedAfterTrailExpired = userSubscriptionModel.IsCardAddedAfterTrailExpired;
                    subscriptionDetail.ExtendedPeriodDateTime = userSubscriptionModel.ExtendedPeriodDateTime;
                    subscriptionDetail.NumberOfRetry = userSubscriptionModel.NumberOfRetry;

                    subscriptionDetail.PricePaid = userSubscriptionModel.PricePaid;
                    subscriptionDetail.GatewayCustomerId = userSubscriptionModel.GatewayCustomerId;
                    subscriptionDetail.GatewaySubscriptionId = userSubscriptionModel.GatewaySubscriptionId;
                    subscriptionDetail.Description = userSubscriptionModel.Description;

                    subscriptionDetail.ModifiedOn = DateTime.UtcNow;
                    subscriptionDetail.ModifiedBy = userSubscriptionModel.ModifiedBy;
                    subscriptionDetail.IsActive = userSubscriptionModel.IsActive;
                    message = "Subscription Detail Updated Successfully!";

                    await dc.SaveChangesAsync();
                }
                status = true;
            }
            catch (Exception ex)
            {
                status = false;
                message = "Something Went Wrong!";
                Log.Error(ex, "SubScriptionManager=>>UpdateUserSubscription");
            }
            return Tuple.Create(status, message);
        }

        /// <summary>
        /// get the active subscription detail by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static async Task<UserSubscriptionModel> GetActiveSubscriptionDetailById(int id)
        {
            UserSubscriptionModel userSubscriptionModel = new UserSubscriptionModel();
            try
            {
                var dc = new FMSContext();
                userSubscriptionModel = await dc.UserSubscriptions.Where(x => x.Id == id && !x.IsDeleted && x.PlanStatus == SubscriptionPlanStatus.Active.GetHashCode()).Select(x => new UserSubscriptionModel()
                {
                    Id = x.Id,
                    PlanId = x.PlanId,
                    UserId = x.UserId,
                    PricePaid = x.PricePaid,
                    GatewayCustomerId = x.GatewayCustomerId,
                    GatewaySubscriptionId = x.GatewaySubscriptionId,
                    PlanStatus = (SubscriptionPlanStatus)x.PlanStatus,
                    IsFreeTrailPeriod = x.IsFreeTrailPeriod,
                    PlanExpirationDateTime = x.PlanExpirationDateTime,
                    IsPlanAutoRenewal = x.IsPlanAutoRenewal,
                    Description = x.Description,
                    CreatedBy = x.CreatedBy,
                    ModifiedBy = x.ModifiedBy
                }).FirstOrDefaultAsync();
            }
            catch (Exception ex)
            {
                userSubscriptionModel = null;
                Log.Error(ex, "SubScriptionManager=>>GetActiveSubscriptionDetailById");
            }
            return userSubscriptionModel;
        }

        /// <summary>
        /// get the active subscription detail by userId
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static UserSubscriptionModel GetActiveSubscriptionDetailByUserId(long userId)
        {
            UserSubscriptionModel userSubscriptionModel = new UserSubscriptionModel();
            try
            {
                var dc = new FMSContext();
                userSubscriptionModel = dc.UserSubscriptions.Where(x => x.UserId == userId && !x.IsDeleted && x.PlanStatus == SubscriptionPlanStatus.Active.GetHashCode()).Select(x => new UserSubscriptionModel()
                {
                    Id = x.Id,
                    PlanId = x.PlanId,
                    UserId = x.UserId,
                    PricePaid = x.PricePaid,
                    GatewayCustomerId = x.GatewayCustomerId,
                    GatewaySubscriptionId = x.GatewaySubscriptionId,
                    PlanStatus = (SubscriptionPlanStatus)x.PlanStatus,
                    IsFreeTrailPeriod = x.IsFreeTrailPeriod,
                    PlanExpirationDateTime = x.PlanExpirationDateTime,
                    IsPlanAutoRenewal = x.IsPlanAutoRenewal,
                    Description = x.Description,
                    CreatedBy = x.CreatedBy,
                    ModifiedBy = x.ModifiedBy
                }).FirstOrDefault();
            }
            catch (Exception ex)
            {
                userSubscriptionModel = null;
                Log.Error(ex, "SubScriptionManager=>>GetActiveSubscriptionDetailByPlanId");
            }
            return userSubscriptionModel;
        }

        /// <summary>
        /// get user subscription detail
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static UserSubscriptionModel GetUserSubscriptionDetail(long userId)
        {
            UserSubscriptionModel userSubscriptionModel = new UserSubscriptionModel();
            try
            {
                var dc = new FMSContext();
                userSubscriptionModel = dc.UserSubscriptions.Where(x => x.UserId == userId && !x.IsDeleted).Select(x => new UserSubscriptionModel()
                {
                    Id = x.Id,
                    PlanId = x.PlanId,
                    UserId = x.UserId,
                    PricePaid = x.IsFreeTrailPeriod ? (x.Plan != null ? x.Plan.Price : x.PricePaid) : x.PricePaid,
                    GatewayCustomerId = x.GatewayCustomerId,
                    GatewaySubscriptionId = x.GatewaySubscriptionId,
                    PlanStatus = (SubscriptionPlanStatus)x.PlanStatus,
                    IsFreeTrailPeriod = x.IsFreeTrailPeriod,
                    PlanExpirationDateTime = x.PlanExpirationDateTime,
                    IsTrailPeriodWarninEmailSent = x.IsTrailPeriodWarninEmailSent,
                    IsCardAddedAfterTrailExpired = x.IsCardAddedAfterTrailExpired,
                    IsCardDetailExpiredEmailSent = x.IsCardDetailExpiredEmailSent,
                    NumberOfRetry = x.NumberOfRetry,
                    IsPaymentPending = x.IsPaymentPending,
                    ExtendedPeriodDateTime = x.ExtendedPeriodDateTime,
                    IsPlanAutoRenewal = x.IsPlanAutoRenewal,
                    Description = x.Description,
                    CreatedBy = x.CreatedBy,
                    ModifiedBy = x.ModifiedBy
                }).FirstOrDefault();
            }
            catch (Exception ex)
            {
                userSubscriptionModel = null;
                Log.Error(ex, "SubScriptionManager=>>GetUserSubscriptionDetail");
            }
            return userSubscriptionModel;
        }

        /// <summary>
        /// get all user subscription details
        /// </summary>
        /// <returns></returns>
        public static async Task<List<UserSubscriptionModel>> GetUserSubscriptions()
        {
            List<UserSubscriptionModel> lstUserSubscription = new List<UserSubscriptionModel>();
            try
            {
                var dc = new FMSContext();
                lstUserSubscription = await (from subscription in dc.UserSubscriptions
                                             join user in dc.Users
                                             on subscription.UserId equals user.Id
                                             where subscription.IsDeleted != true
                                             && user.IsDeleted != true
                                             select new UserSubscriptionModel()
                                             {
                                                 Id = subscription.Id,
                                                 PlanId = subscription.PlanId,
                                                 UserId = subscription.UserId,
                                                 PricePaid = subscription.PricePaid,
                                                 PlanPrice = subscription.Plan != null ? subscription.Plan.Price : 0,
                                                 PlanLengthDays = subscription.Plan != null ? subscription.Plan.PlanLengthInDays : 0,
                                                 GatewayCustomerId = subscription.GatewayCustomerId,
                                                 GatewaySubscriptionId = subscription.GatewaySubscriptionId,
                                                 PlanStatus = (SubscriptionPlanStatus)subscription.PlanStatus,
                                                 IsFreeTrailPeriod = subscription.IsFreeTrailPeriod,
                                                 IsTrailPeriodWarninEmailSent = subscription.IsTrailPeriodWarninEmailSent,
                                                 IsCardDetailExpiredEmailSent = subscription.IsCardDetailExpiredEmailSent,
                                                 ExpiredCardId = subscription.ExpiredCardId,
                                                 PlanExpirationDateTime = subscription.PlanExpirationDateTime,
                                                 IsPlanAutoRenewal = subscription.IsPlanAutoRenewal,
                                                 Description = subscription.Description,
                                                 IsCardAddedAfterTrailExpired = subscription.IsCardAddedAfterTrailExpired,
                                                 IsPaymentPending = subscription.IsPaymentPending,
                                                 ExtendedPeriodDateTime = subscription.ExtendedPeriodDateTime,
                                                 CreatedBy = subscription.CreatedBy,
                                                 ModifiedBy = subscription.ModifiedBy
                                             }).ToListAsync();
            }
            catch (Exception ex)
            {
                lstUserSubscription = null;
                Log.Error(ex, "SubScriptionManager=>>GetUserSubscriptions");
            }
            return lstUserSubscription;
        }

        /// <summary>
        /// update the charge payment status for all the in progress payments by using stripe webhook
        /// </summary>
        /// <param name="chargeId"></param>
        /// <returns></returns>
        public static async Task<Tuple<bool, string>> UpdateInProgressChargeDetail(string chargeId, bool isSuccessfull)
        {
            bool status = false;
            string message = "Something Went Wrong!";
            try
            {
                var dc = new FMSContext();
                var paymentDetail = await dc.PaymentHistories.Where(x => x.GatewayChargeId == chargeId && !x.IsDeleted).FirstOrDefaultAsync();
                if (paymentDetail != null)
                {
                    var userSubscriptionDetail = await dc.UserSubscriptions.Where(x => x.UserId == paymentDetail.UserId && !x.IsDeleted).FirstOrDefaultAsync();
                    if (isSuccessfull)
                    {
                        paymentDetail.PaymentStatus = (int)PaymentStatusType.Completed;
                        if (userSubscriptionDetail != null)
                        {
                            userSubscriptionDetail.IsPaymentPending = false;
                            userSubscriptionDetail.PlanStatus = (int)SubscriptionPlanStatus.Active;
                            userSubscriptionDetail.IsFreeTrailPeriod = false;
                            userSubscriptionDetail.IsCardDetailExpiredEmailSent = false;

                            if (userSubscriptionDetail.IsCardAddedAfterTrailExpired)
                            {
                                userSubscriptionDetail.IsCardAddedAfterTrailExpired = false;
                                var deductedTime = DateTime.Now - userSubscriptionDetail.ExtendedPeriodDateTime.Value;
                                userSubscriptionDetail.PlanExpirationDateTime = DateTime.Now.AddDays(userSubscriptionDetail.Plan.PlanLengthInDays) - deductedTime;
                            }
                            else
                            {
                                userSubscriptionDetail.PlanExpirationDateTime = userSubscriptionDetail.PlanExpirationDateTime.AddDays(userSubscriptionDetail.Plan.PlanLengthInDays);
                            }
                        }
                    }
                    else
                    {
                        paymentDetail.PaymentStatus = (int)PaymentStatusType.Failed;
                        if (userSubscriptionDetail != null)
                        {
                            userSubscriptionDetail.NumberOfRetry = userSubscriptionDetail.NumberOfRetry + 1;
                        }
                    }
                }
                await dc.SaveChangesAsync();
                status = true;
                message = "Charges Status Updated Successfully!";
            }
            catch (Exception ex)
            {
                Log.Error(ex, "SubScriptionManager=>>UpdateInProgressChargeDetail");
            }
            return Tuple.Create(status, message);
        }


        #region check for free period and subscription Plan 
        /// <summary>
        ///Lastly, we need to allow the user to see ALL features for 2 months, when they first join.
        //This 2 months should be a config setting.
        //On the date that the STORE was created, they get all features for 2 months.
        //After two months, they can only see the Admin Section and Dashboard section.
        //Once they purchase a subscription, they should again be able to see ALL features.
        //So the subscription should allow them to see features, but if they don’t have one, they shouldn’t see the features anymore.        
        /// </summary>
        public static UserSubscriptionModel IsStoreFreeTrialAndSubscriptionExpire(int userId)
        {
            UserSubscriptionModel storeModel = new UserSubscriptionModel();
            try
            {


                //check for database the subscription plan info 
                //storeModel = SubscriptionManager.GetUserSubscriptionDetail(storeId);
                //check for active subscription 
                var user = UserManager.GetUserById(userId);
                var storeSubscription = GetUserSubscriptionDetail(userId);
                    if (storeSubscription != null)
                    {
                        storeModel.IsActive = true;
                        storeModel.PlanExpirationDateTime = storeSubscription.PlanExpirationDateTime;
                    }


                
                

                if (user != null)
                {
                    storeModel.DefaultSubscriptionExpirationDays = (user.PlanExpirationDateTime - DateTime.Now).Days;
                    if (user.PlanExpirationDateTime < DateTime.Today)
                    {
                        storeModel.IsActive = false;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "StorSubScriptionManager=>>IsStoreFreeTrialAndSubscriptionExpire");
            }
            return storeModel;
        }

        #endregion


        #region PaymentByCash



        #endregion
        #endregion

        #region Payment History


        /// <summary>
        /// Get all Data of Ingredient to bind with data-table 
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public static async Task<Tuple<List<PaymentHistoryModel>, string, int>> GetDataTableList(jQueryDataTableParamModel param, string userId)
        {

                var dc = new FMSContext();
                var sortColumnIndex = param.iSortCol_0;
                var qry = from payHistory in dc.PaymentHistories
                          where !payHistory.IsDeleted && payHistory.UserId == Convert.ToInt32(userId)
                          select new PaymentHistoryModel
                          {
                              PaidPrice = payHistory.PaidPrice,
                              PaymentMode = (PaymentGatewayType)payHistory.PaymentMode,
                              PaymentStatus = (PaymentStatusType)payHistory.PaymentStatus,
                              SubscriptionId = payHistory.SubscriptionId,
                              Id = payHistory.Id,
                              CreatedOn = payHistory.CreatedOn,
                          };
                //Get total count
                var totalRecords = await qry.CountAsync();
                #region Searching 
                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    var toSearch = param.sSearch.ToLower();
                    qry = qry.Where(c => c.Name.ToLower().Contains(toSearch));
                }
                #endregion

                #region  Sorting 
                switch (sortColumnIndex)
                {
                    //Sort by IngredientName
                    case 0:
                        switch (param.sSortDir_0)
                        {
                            case "desc":
                                qry = qry.OrderByDescending(a => a.PaymentStatus);
                                break;
                            case "asc":
                                qry = qry.OrderBy(a => a.PaymentMode);
                                break;
                            default:
                                qry = qry.OrderBy(a => a.PaymentMode);
                                break;
                        }
                        break;
                    default:
                        qry = qry.OrderBy(a => a.CreatedOn);
                        break;
                }
                #endregion

                #region  Paging 
                if (param.iDisplayLength != -1)
                    qry = qry.Skip(param.iDisplayStart).Take(param.iDisplayLength);
                #endregion
                return new Tuple<List<PaymentHistoryModel>, string, int>(
                 await
                    (qry).ToListAsync(),
                 param.sEcho,
                 totalRecords);
            }           
        }
        #endregion
    }

