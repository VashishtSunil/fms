﻿using FMS.Core.DataModels.Payment;
using FMS.Core.Enums;
using FMS.Core.Models;
using FMS.Core.Models.Payment;
using FMS.Data.Context;
using Serilog;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace FMS.Services.Classes
{
    public class PaymentManager
    {
        #region Payment History
        /// <summary>
        /// get all payment history
        /// </summary>
        /// <returns></returns>
        public static List<PaymentHistoryModel> GetAllPaymentHistories()
        {
            List<PaymentHistoryModel> lstPaymentHistory = new List<PaymentHistoryModel>();
            try
            {
                var dc = new FMSContext();
                lstPaymentHistory = dc.PaymentHistories.Where(x => x.IsDeleted != true).Select(x => new PaymentHistoryModel()
                {
                    UserId = x.UserId,
                    GatewayCustomerId = x.GatewayCustomerId,
                    GatewayCardId = x.GatewayCardId,
                    GatewayChargeId = x.GatewayChargeId,

                    PaidPrice = x.PaidPrice,
                    PaidPriceInCent = x.PaidPriceInCent,
                    Gateway = (PaymentGatewayType)x.Gateway,
                    Description = x.Description,
                    SubscriptionId = x.SubscriptionId,
                    PaymentStatus = (PaymentStatusType)x.PaymentStatus,
                    ModifiedBy = x.ModifiedBy,
                    CreatedBy = x.CreatedBy
                }).ToList();


            }
            catch (Exception ex)
            {
                lstPaymentHistory = null;
                Log.Error(ex, "PaymentManager=>>GetAllPaymentHistories");
            }
            return lstPaymentHistory;
        }

        /// <summary>
        /// get payment history by Id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public static async Task<PaymentHistoryModel> GetPaymentHistory(int id)
        {
            PaymentHistoryModel paymentHistoryModel = new PaymentHistoryModel();
            try
            {
                var dc = new FMSContext();
                paymentHistoryModel = await dc.PaymentHistories.Where(u => u.Id == id && !u.IsDeleted).Select(x => new PaymentHistoryModel()
                {
                    UserId = x.UserId,
                    GatewayCustomerId = x.GatewayCustomerId,
                    GatewayCardId = x.GatewayCardId,
                    GatewayChargeId = x.GatewayChargeId,
                    PaidPrice = x.PaidPrice,
                    PaidPriceInCent = x.PaidPriceInCent,
                    Gateway = (PaymentGatewayType)x.Gateway,
                    Description = x.Description,
                    SubscriptionId = x.SubscriptionId,
                    PaymentStatus = (PaymentStatusType)x.PaymentStatus,
                    ModifiedBy = x.ModifiedBy,
                    CreatedBy = x.CreatedBy
                }).FirstOrDefaultAsync();
            }
            catch (Exception ex)
            {
                paymentHistoryModel = null;
                Log.Error(ex, "PaymentManager=>>GetPaymentHistory");
            }
            return paymentHistoryModel;
        }

        /// <summary>
        /// get payment history detail by gateway info
        /// </summary>
        /// <param name="gatewayCustomerId"></param>
        /// <param name="gatewayCardId"></param>
        /// <param name="userId"></param>
        /// <param name="subsriptionId"></param>
        /// <returns></returns>
        public static async Task<List<PaymentHistoryModel>> GetPaymentHistoryByGatewayDetail(string gatewayCustomerId, string gatewayCardId, int userId, int? subsriptionId)
        {
            List<PaymentHistoryModel> lstPaymentHistory = new List<PaymentHistoryModel>();
            try
            {
                var dc = new FMSContext();
                lstPaymentHistory = await dc.PaymentHistories.Where(x => x.GatewayCustomerId == gatewayCustomerId && x.GatewayCardId == gatewayCardId && x.UserId == userId && x.SubscriptionId == subsriptionId && !x.IsDeleted).Select(x => new PaymentHistoryModel()
                {
                    UserId = x.UserId,
                    GatewayCustomerId = x.GatewayCustomerId,
                    GatewayCardId = x.GatewayCardId,
                    GatewayChargeId = x.GatewayChargeId,

                    PaidPrice = x.PaidPrice,
                    PaidPriceInCent = x.PaidPriceInCent,
                    Gateway = (PaymentGatewayType)x.Gateway,
                    Description = x.Description,
                    SubscriptionId = x.SubscriptionId,
                    PaymentStatus = (PaymentStatusType)x.PaymentStatus,
                    ModifiedBy = x.ModifiedBy,
                    CreatedBy = x.CreatedBy,
                    CreatedOn = x.CreatedOn
                }).ToListAsync();
            }
            catch (Exception ex)
            {
                lstPaymentHistory = null;
                Log.Error(ex, "PaymentManager=>>GetPaymentHistoryByGatewayDetail");
            }
            return lstPaymentHistory;
        }

        /// <summary>
        /// add/update payment history records
        /// </summary>
        /// <param name="paymentHistoryModel"></param>
        /// <returns></returns>
        public static async Task<Tuple<bool, string>> UpdatePaymentHistory(PaymentHistoryModel paymentHistoryModel)
        {
            bool status = false;
            string message = "Something Went Wrong!";
            try
            {
                var dc = new FMSContext();
                var paymentDetail = await dc.PaymentHistories.Where(x => x.Id == paymentHistoryModel.Id).FirstOrDefaultAsync();
                if (paymentDetail == null)
                {
                    paymentDetail = new PaymentHistory()
                    {
                        UserId = paymentHistoryModel.UserId,
                        GatewayCustomerId = paymentHistoryModel.GatewayCustomerId,
                        GatewayCardId = paymentHistoryModel.GatewayCardId,
                        GatewayChargeId = paymentHistoryModel.GatewayChargeId,
                        PaidPrice = paymentHistoryModel.PaidPrice,
                        PaidPriceInCent = paymentHistoryModel.PaidPriceInCent,
                        Gateway = (int)paymentHistoryModel.Gateway,
                        Description = paymentHistoryModel.Description,
                        SubscriptionId = paymentHistoryModel.SubscriptionId,
                        PaymentStatus = (int)paymentHistoryModel.PaymentStatus,
                        CreatedBy = paymentHistoryModel.CreatedBy,
                        CreatedOn = DateTime.UtcNow,
                        PaymentMode = (int)paymentHistoryModel.PaymentMode
                    };
                    dc.PaymentHistories.Add(paymentDetail);
                    message = "Payment History Save Successfully!";
                }
                else
                {
                    paymentDetail.UserId = paymentHistoryModel.UserId;
                    paymentDetail.GatewayCustomerId = paymentHistoryModel.GatewayCustomerId;
                    paymentDetail.GatewayCardId = paymentHistoryModel.GatewayCardId;
                    paymentDetail.GatewayChargeId = paymentHistoryModel.GatewayChargeId;

                    paymentDetail.PaidPrice = paymentHistoryModel.PaidPrice;
                    paymentDetail.PaidPriceInCent = paymentHistoryModel.PaidPriceInCent;
                    paymentDetail.Gateway = (int)paymentHistoryModel.Gateway;
                    paymentDetail.Description = paymentHistoryModel.Description;
                    paymentDetail.SubscriptionId = paymentHistoryModel.SubscriptionId;
                    paymentDetail.PaymentStatus = (int)paymentHistoryModel.PaymentStatus;
                    paymentDetail.ModifiedBy = paymentHistoryModel.ModifiedBy;
                    paymentDetail.ModifiedOn = DateTime.Now;
                    message = "Payment History Update Successfully!";
                }
                status = true;
                await dc.SaveChangesAsync();
                paymentHistoryModel.Id = paymentDetail.Id;
            }
            catch (Exception ex)
            {
                status = false;
                message = "Something Went Wrong!";
                Log.Error(ex, "PaymentManager=>>UpdatePaymentHistory");
            }
            return Tuple.Create(status, message);
        }
        #endregion

        #region Refund History
        /// <summary>
        /// add refund history record
        /// </summary>
        /// <param name="refundHistoryModel"></param>
        /// <returns></returns>
        public static async Task<Tuple<bool, string>> UpdateRefundHistory(RefundHistoryModel refundHistoryModel)
        {
            bool status = false;
            string message = "Something Went Wrong!";
            try
            {
                var dc = new FMSContext();
                var refundDetail = dc.RefundHistories.Where(x => x.Id == refundHistoryModel.Id).FirstOrDefault();
                if (refundDetail == null)
                {
                    refundDetail = new RefundHistory()
                    {
                        UserId = refundHistoryModel.UserId,
                        GatewayCustomerId = refundHistoryModel.GatewayCustomerId,
                        GatewayCardId = refundHistoryModel.GatewayCardId,
                        GatewayChargeId = refundHistoryModel.GatewayChargeId,
                        GatewayRefundId = refundHistoryModel.GatewayRefundId,

                        PaidPrice = refundHistoryModel.PaidPrice,
                        PaidPriceInCent = refundHistoryModel.PaidPriceInCent,
                        Gateway = (int)refundHistoryModel.Gateway,
                        Description = refundHistoryModel.Description,
                        SubscriptionId = refundHistoryModel.SubscriptionId,
                        PaymentStatus = (int)refundHistoryModel.PaymentStatus,
                        CreatedBy = refundHistoryModel.CreatedBy,
                        CreatedOn = DateTime.UtcNow
                    };

                    dc.RefundHistories.Add(refundDetail);
                    await dc.SaveChangesAsync();

                    message = "Refund Payment History Added Successfully!";
                }
                else
                {
                    refundDetail.UserId = refundHistoryModel.UserId;
                    refundDetail.GatewayCustomerId = refundHistoryModel.GatewayCustomerId;
                    refundDetail.GatewayCardId = refundHistoryModel.GatewayCardId;
                    refundDetail.GatewayChargeId = refundHistoryModel.GatewayChargeId;
                    refundDetail.GatewayRefundId = refundHistoryModel.GatewayRefundId;

                    refundDetail.PaidPrice = refundHistoryModel.PaidPrice;
                    refundDetail.PaidPriceInCent = refundHistoryModel.PaidPriceInCent;
                    refundDetail.Gateway = (int)refundHistoryModel.Gateway;
                    refundDetail.Description = refundHistoryModel.Description;
                    refundDetail.SubscriptionId = refundHistoryModel.SubscriptionId;
                    refundDetail.PaymentStatus = (int)refundHistoryModel.PaymentStatus;
                    refundDetail.ModifiedBy = refundHistoryModel.ModifiedBy;
                    refundDetail.ModifiedOn = DateTime.UtcNow;
                    dc.RefundHistories.Add(refundDetail);
                    await dc.SaveChangesAsync();

                    message = "Refund Payment History Updated Successfully!";
                }
                status = true;
                refundHistoryModel.Id = refundDetail.Id;
            }
            catch (Exception ex)
            {
                status = false;
                message = "Something Went Wrong!";
                Log.Error(ex, "PaymentManager=>>UpdateRefundHistory");
            }
            return Tuple.Create(status, message);
        }



        public static async Task<Tuple<List<PaymentHistoryModel>, string, int>> GetTraineePaymentHistroy(jQueryDataTableParamModel param, int userId)
        {

            var dc = new FMSContext();
            var sortColumnIndex = param.iSortCol_0;
            var qry = from history in dc.PaymentHistories
                      join user in dc.Users on history.UserId equals user.Id
                      join userDeatail in dc.UserDetails on history.UserId equals userDeatail.UserId
                      where history.IsDeleted == false && history.UserId == userId

                      select new PaymentHistoryModel
                      {
                          UserId = history.UserId,
                          GatewayCustomerId = history.GatewayCustomerId,
                          GatewayCardId = history.GatewayCardId,
                          GatewayChargeId = history.GatewayChargeId,
                          PaidPrice = history.PaidPrice,
                          PaidPriceInCent = history.PaidPriceInCent,
                          Description = history.Description,
                          SubscriptionId = history.SubscriptionId,
                          PaymentStatus = (PaymentStatusType)history.PaymentStatus,
                          ModifiedBy = history.ModifiedBy,
                          CreatedBy = history.CreatedBy,
                          Phone = userDeatail.Phone,
                          Name = userDeatail.FirstName,
                          Email = user.Email,
                          PaymentMode = (PaymentGatewayType)history.PaymentMode,
                          CreatedOn = history.CreatedOn
                          
                      };    
            //Get total count
            var totalRecords = await qry.CountAsync();

            #region Searching 
            if (!string.IsNullOrEmpty(param.sSearch))
            {
                var toSearch = param.sSearch.ToLower();
                qry = qry.Where(c => c.Email.ToLower().Contains(toSearch)
                );
            }
            #endregion

            #region  Sorting 
            switch (sortColumnIndex)
            {
                //Sort by Name
                case 0:
                    switch (param.sSortDir_0)
                    {
                        case "desc":
                            qry = qry.OrderByDescending(a => a.Name);
                            break;
                        case "asc":
                            qry = qry.OrderBy(a => a.Name);
                            break;
                        default:
                            qry = qry.OrderBy(a => a.Name);
                            break;
                    }

                    break;
                //Sort by Name
                case 1:
                    switch (param.sSortDir_0)
                    {
                        case "desc":
                            qry = qry.OrderByDescending(a => a.Email);
                            break;
                        case "asc":
                            qry = qry.OrderBy(a => a.Email);
                            break;
                        default:
                            qry = qry.OrderBy(a => a.Email);
                            break;
                    }
                    break;
                default:
                    qry = qry.OrderBy(a => a.Name);
                    break;
            }
            #endregion

            #region  Paging 
            if (param.iDisplayLength != -1)
                qry = qry.Skip(param.iDisplayStart).Take(param.iDisplayLength);
            #endregion           
            return new Tuple<List<PaymentHistoryModel>, string, int>(
                await
                   (qry).ToListAsync(),
                param.sEcho,
                totalRecords);
        }
        #endregion
    }
}
