﻿using FMS.Core;
using FMS.Core.Enums;
using FMS.Core.Helper;
using FMS.Core.Models;
using FMS.Data.Context;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FMS.Services.Classes.Token
{
    public class TokenManager
    {
        #region Generate token 
        public static string Secret;
        public static Tuple<bool, Dictionary<string, string>, string> GenerateToken(Payloads model)
        {
            bool status = false;
            Dictionary<string, string> errorResponse = new Dictionary<string, string>();
            string newToken = "";
            try
            {
                //check for valid key 
                //var secretKeyResponse = GetSecretKey(model);
                //if (secretKeyResponse.Item1)
                //{
                //    // if we have valid client key and secret then will able to generate the token 
                //    if (!string.IsNullOrWhiteSpace(Secret))
                //    {                       
                //        status = true;

                //        newToken = DataEncryptionManager.EncryptSHA512(DataEncryptionManager.EncodeBase64(model.UserId) + ":" + DataEncryptionManager.EncodeBase64(model.Key) + DataEncryptionManager.EncodeBase64(Guid.NewGuid().ToString()));
                //    }
                //}
                //else
                //{
                //    errorResponse = secretKeyResponse.Item2;
                //}
                newToken = DataEncryptionManager.EncryptSHA512(DataEncryptionManager.EncodeBase64(model.UserId) + ":" + DataEncryptionManager.EncodeBase64(model.Password) + DataEncryptionManager.EncodeBase64(Guid.NewGuid().ToString()));
                status = true;
            }
            catch (Exception ex)
            {
                int errorCode = ErrorMessageCode.InternalServerError.GetHashCode();
                var errorMessage = ErrorMessageCode.InternalServerError.GetDescription();

                errorResponse.Add("errornum", errorCode.ToString());
                errorResponse.Add("errormsg", errorMessage != null ? errorMessage : "");
                Log.Error(ex, "TokenManager=>>GenerateToken");
            }
            return Tuple.Create(status, errorResponse, newToken);
        }

        /// <summary>
        /// Get Client Secret Key and Assigning it to Secret Property
        /// </summary>
        /// <param name="model"></param>
        public static Tuple<bool, Dictionary<string, string>> GetSecretKey(Payloads model)
        {
            bool status = false;
            Dictionary<string, string> errorResponse = new Dictionary<string, string>();
            try
            {
                var dc = new FMSContext();
                var keys = dc.ApiUsers.FirstOrDefault(x => x.ClientId == model.UserId && x.ClientKey == model.Key);
                if (keys != null)
                {
                    status = true;
                    Secret = keys.ClientSecret.ToString();
                }
                else
                {
                    int errorCode = ErrorMessageCode.InvalidClientIdAndSecretKey.GetHashCode();
                    var errorMessage = ErrorMessageCode.InvalidClientIdAndSecretKey.GetDescription();
                    errorResponse.Add("errornum", errorCode.ToString());
                    errorResponse.Add("errormsg", errorMessage != null ? errorMessage : "");
                }
            }
            catch (Exception ex)
            {
                int errorCode = ErrorMessageCode.InternalServerError.GetHashCode();
                var errorMessage = ErrorMessageCode.InternalServerError.GetDescription();
                errorResponse.Add("errornum", errorCode.ToString());
                errorResponse.Add("errormsg", errorMessage != null ? errorMessage : "");
                Log.Error(ex, "TokenManager=>>GetSecretKey");
            }
            return Tuple.Create(status, errorResponse);
        }
        #endregion


        #region Validate API token

        /// <summary>
        /// Used to add Update/Add Retailer
        /// </summary>
        /// <param name="retailerModel"></param>
        /// <returns></returns>
        public static bool UpdateApiToken(ApiTokenModel apiTokenModel)
        {
            var dc = new FMSContext();
            var token = dc.ApiTokens.FirstOrDefault(x => x.Id == apiTokenModel.Id);
            try
            {
                // for add new record
                if (token == null)
                {
                    token = new Core.DataModels.ApiUser.ApiToken
                    {
                        ClientId = apiTokenModel.ClientId,
                        TokenId = apiTokenModel.TokenId,
                        ValidUntil = apiTokenModel.ValidUntil,
                        //UserName = retailerModel.UserName,
                        //LastAccess = DateTime.Now,
                        CreatedBy = apiTokenModel.CreatedBy
                    };
                    dc.ApiTokens.Add(token);
                    dc.SaveChanges();
                }
                // for update 
                else
                {
                    token.ClientId = apiTokenModel.ClientId;
                    token.TokenId = apiTokenModel.TokenId;
                    token.ValidUntil = apiTokenModel.ValidUntil;
                    token.ModifiedBy = apiTokenModel.ModifiedBy;
                    token.ModifiedOn = DateTime.Now;

                    dc.SaveChanges();
                }
                apiTokenModel.Id = token.Id;
                return true;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "TokenManager=>>UpdateApiToken");
                return false;
            }
        }

        /// <summary>
        /// token is valid or not 
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public static bool IsTokenValid(string token)
        {
            var dc = new FMSContext();
            var tokenDb = dc.ApiTokens.FirstOrDefault(x => x.TokenId == token && x.ValidUntil > DateTime.Now);
            return (tokenDb != null);
        }

        /// <summary>
        /// token is expired or not 
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public static bool IsTokenExpired(string token)
        {
            var dc = new FMSContext();
            DateTime expiryTime = DateTime.Now.AddMinutes(-15);
            var tokenDb = dc.ApiTokens.FirstOrDefault(x => x.TokenId == token && x.CreatedOn >= expiryTime);
            return (tokenDb != null);
        }

        /// <summary>
        /// Delete expired token
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public static async Task<bool> DeleteToken(string token)
        {
            var dc = new FMSContext();
            bool status = false;
            var tokenDb = dc.ApiTokens.FirstOrDefault(x => x.TokenId == token);
            
            if(tokenDb != null)
            {
                tokenDb.IsDeleted = true;
                var resp = await dc.SaveChangesAsync();
                status = resp > 0;
            }
            return status;
        }


        #endregion
    }
}
