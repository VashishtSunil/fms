﻿using FMS.Core.DataModels;
using FMS.Core.Helper;
using FMS.Data.Context;
using System;
using System.Linq;

namespace FMS.Services.Classes
{
    public class AccountManager
    {
        /// <summary>
        /// Verify the site key 
        /// </summary>
        /// <param name="key"></param>
        /// <param name="Code"></param>
        /// <returns></returns>
        public static User VerifyGymKey(string key)
        {
            var dc = new FMSContext();
           return dc.Users.FirstOrDefault(x => x.SiteKey == key && !x.IsDeleted);
        }

        /// <summary>
        /// Get User By Email And Code
        /// </summary>
        /// <param name="Email"></param>
        /// <param name="Code"></param>
        /// <returns></returns>
        public static User GetUserByEmailAndCode(string Email, string Code)
        {
            var dc = new FMSContext();
            var user = dc.Users.FirstOrDefault(x => x.Email == Email && x.Code == Code);
            try
            {
                return user;
            }
            catch (Exception ex)
            {
                return user;
            }
        }

        /// <summary>
        /// Reset Admin Password
        /// </summary>
        /// <param name="Password"></param>
        /// <param name="Email"></param>
        public static Tuple<bool, string> ResetUserPassword(string Password, string Email)
        {
            var dc = new FMSContext();
            var user = dc.Users.FirstOrDefault(x => x.Email == Email && x.IsCodeUsed == false);
            bool status = false;
            string message = string.Empty;
            try
            {
                if(user != null)
                {
                    var hashPassword = CipherHelper.EncryptPassword(Password + "_" + Email.ToLower());
                    user.Password = hashPassword;
                    user.IsCodeUsed = true;
                    status = dc.SaveChanges() > 0;
                    message = "Password changed successfully";
                }
                else
                {
                    status = false;
                    message = "Token is expired";
                }
            }
            catch(Exception ex)
            {
                status = false;
                message = "Something went wrong";
            }

            return Tuple.Create(status, message);
        }
    }
}
