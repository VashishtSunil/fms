﻿using FMS.Core.Models;
using FMS.Data.Context;
using Serilog;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMS.Services.Classes
{
    public class ClientBasicInfoManager
    {
        #region Basic Information

        /// <summary>
        /// get user detail along with uploaded file
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static ClientBasicInfoViewModel GetClientBasicInfoByUserId(long userId)
        {
            var dc = new FMSContext();
            return (from p in dc.ClientBasicInfos
                    join a in dc.Attachments on p.LogoId equals a.Id
                    into attachmentGJ
                    from attchment in attachmentGJ.DefaultIfEmpty()
                    where p.IsDeleted == false && p.ClientId == userId
                    //Binding Data with Model
                    select new ClientBasicInfoViewModel
                    {
                        Id = p.Id,
                        ClientId = p.ClientId,
                        Heading = p.Heading,
                        SubHeading = p.SubHeading,
                        FacebookUrl = p.FacebookUrl,
                        TwitterUrl = p.TwitterUrl,
                        InstagramUrl = p.InstagramUrl,
                        LogoId = p.LogoId,
                        IsActive = p.IsActive,
                        BasicInfoLogo = attchment
                    }).FirstOrDefault();
        }

        /// <summary>
        /// Used to add Update/Add Client Basic Info
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static bool UpdateClientBasicInfo(ClientBasicInfoViewModel model)
        {
            var dc = new FMSContext();
            var clientBasicInfos = dc.ClientBasicInfos.FirstOrDefault(x => x.Id == model.Id);
            try
            {
                // for add new record
                if (clientBasicInfos == null)
                {
                    clientBasicInfos = new Core.DataModels.ClientBasicInfo
                    {
                        Id = model.Id,
                        ClientId = model.ClientId,
                        Heading = model.Heading,
                        SubHeading = model.SubHeading,
                        LogoId = model.LogoId,
                        FacebookUrl = model.FacebookUrl,
                        InstagramUrl = model.InstagramUrl,
                        TwitterUrl = model.TwitterUrl,
                        OwnerId = model.OwnerId,
                        SitePortalOwnerId = model.SitePortalOwnerId,
                    };
                    dc.ClientBasicInfos.Add(clientBasicInfos);
                }
                // for update 
                else
                {
                    clientBasicInfos.ClientId = model.ClientId;
                    clientBasicInfos.Heading = model.Heading;
                    clientBasicInfos.SubHeading = model.SubHeading;
                    clientBasicInfos.LogoId = model.LogoId;
                    clientBasicInfos.FacebookUrl = model.FacebookUrl;
                    clientBasicInfos.InstagramUrl = model.InstagramUrl;
                    clientBasicInfos.TwitterUrl = model.TwitterUrl;
                    clientBasicInfos.OwnerId = model.OwnerId;
                    clientBasicInfos.SitePortalOwnerId = model.SitePortalOwnerId;
                }
                dc.SaveChanges();
                model.Id = clientBasicInfos.Id;
                return true;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "ClientBasicInfoManager=>>UpdateClientBasicInfo");
                return false;
            }
        }

        /// <summary>
        /// Get Basic Infos List
        /// </summary>
        /// <param name="ownerId"></param>
        /// <returns></returns>

        public static ClientBasicInfoViewModel GetBasicInfosList(int ownerId)
        {
            var dc = new FMSContext();
            var basicInfoDetail = (from p in dc.ClientBasicInfos
                                   where p.IsDeleted == false && p.SitePortalOwnerId == ownerId
                                   //Binding Data with Model
                                   select new ClientBasicInfoViewModel
                                   {
                                       Id = p.Id,
                                       Heading = p.Heading,
                                       SubHeading = p.SubHeading,
                                       OwnerId = p.OwnerId,
                                       LogoId = p.LogoId,
                                       FacebookUrl = p.FacebookUrl,
                                       InstagramUrl = p.InstagramUrl,
                                       TwitterUrl = p.TwitterUrl,
                                       SitePortalOwnerId = p.SitePortalOwnerId,
                                   }).FirstOrDefault();
            
            return basicInfoDetail;
        }

        #endregion

        #region Slider

        /// <summary>
        /// Get all Data of Slider to bind with data-table 
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public static async Task<Tuple<List<ClientSliderViewModel>, string, int>> GetDataTableList(jQueryDataTableParamModel param)
        {
            var dc = new FMSContext();
            var userId = Convert.ToInt32(param.id);
            var sortColumnIndex = param.iSortCol_0;
            var qry = dc.ClientSliders
             .Where(x => x.IsDeleted == false && x.ClientId == userId);
            //Get total count
            var totalRecords = await qry.CountAsync();

            #region Searching 
            if (!string.IsNullOrEmpty(param.sSearch))
            {
                var toSearch = param.sSearch.ToLower();
                qry = qry.Where(c => c.Heading.ToLower().Contains(toSearch)
                || c.SubHeading.ToLower().Contains(toSearch));

            }
            #endregion

            #region  Sorting 
            switch (sortColumnIndex)
            {
                //Sort by Name
                case 0:
                    switch (param.sSortDir_0)
                    {
                        case "desc":
                            qry = qry.OrderByDescending(a => a.Heading);
                            break;
                        case "asc":
                            qry = qry.OrderBy(a => a.Heading);
                            break;
                        default:
                            qry = qry.OrderBy(a => a.Heading);
                            break;
                    }
                    break;
                //Sort by price
                case 1:
                    switch (param.sSortDir_0)
                    {
                        case "desc":
                            qry = qry.OrderByDescending(a => a.SubHeading);
                            break;
                        case "asc":
                            qry = qry.OrderBy(a => a.SubHeading);
                            break;
                        default:
                            qry = qry.OrderBy(a => a.SubHeading);
                            break;
                    }
                    break;
                default:
                    qry = qry.OrderBy(a => a.Heading);
                    break;
            }
            #endregion

            #region  Paging 
            if (param.iDisplayLength != -1)
                qry = qry.Skip(param.iDisplayStart).Take(param.iDisplayLength);
            #endregion


            return new Tuple<List<ClientSliderViewModel>, string, int>(
               await
                   (from p in qry
                    select new ClientSliderViewModel
                    {
                        Id = p.Id,
                        Heading = p.Heading,
                        SubHeading = p.SubHeading,
                        CreatedOn = p.CreatedOn,
                        ModifiedOn = p.ModifiedOn,
                        IsActive = p.IsActive

                    }).ToListAsync(),
               param.sEcho,
               totalRecords);
        }

        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static ClientSliderViewModel GetById(long id)
        {
            var dc = new FMSContext();
            return (from p in dc.ClientSliders
                    join a in dc.Attachments on p.LogoId equals a.Id
                          into attachmentGJ
                    from attchment in attachmentGJ.DefaultIfEmpty()
                    where p.IsDeleted == false && p.Id == id && attchment.IsDeleted == false
                    //Binding Data with Model
                    select new ClientSliderViewModel
                    {
                        Id = p.Id,
                        Heading = p.Heading,
                        SubHeading = p.SubHeading,
                        ClientId = p.ClientId,
                        LogoId = p.LogoId,
                        ButtonLink = p.ButtonLink,
                        ButtonText = p.ButtonText,
                        IsButtonDisplayed = p.IsButtonDisplayed,
                        IsActive = p.IsActive,
                        SliderLogo = attchment
                    }).FirstOrDefault();
        }

        /// <summary>
        /// Used to add Update/Add Client Slider
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static bool UpdateClientSlider(ClientSliderViewModel model)
        {
            var dc = new FMSContext();
            var clientSliders = dc.ClientSliders.FirstOrDefault(x => x.Id == model.Id);

            try
            {
                // for add new record
                if (clientSliders == null)
                {
                    clientSliders = new Core.DataModels.ClientSlider
                    {
                        Id = model.Id,
                        ClientId = model.ClientId,
                        Heading = model.Heading,
                        SubHeading = model.SubHeading,
                        LogoId = model.LogoId,
                        ButtonText = model.ButtonText,
                        ButtonLink = model.ButtonLink,
                        OwnerId = model.OwnerId,
                        SitePortalOwnerId = model.SitePortalOwnerId,
                        IsButtonDisplayed = model.IsButtonDisplayed
                    };
                    dc.ClientSliders.Add(clientSliders);
                }
                // for update 
                else
                {
                    clientSliders.ClientId = model.ClientId;
                    clientSliders.Heading = model.Heading;
                    clientSliders.SubHeading = model.SubHeading;
                    clientSliders.LogoId = model.LogoId;
                    clientSliders.ButtonText = model.ButtonText;
                    clientSliders.ButtonLink = model.ButtonLink;
                    clientSliders.OwnerId = model.OwnerId;
                    clientSliders.SitePortalOwnerId = model.SitePortalOwnerId;
                    clientSliders.IsButtonDisplayed = model.IsButtonDisplayed;
                }
                dc.SaveChanges();
                model.Id = clientSliders.Id;
                return true;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "ClientBasicInfoManager=>>UpdateClientSlider");
                return false;
            }
        }

        /// <summary>
        /// Update Client Slider Status
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static bool UpdateClientSliderStatus(long id)
        {
            var dc = new FMSContext();
            var clientSliders = dc.ClientSliders.FirstOrDefault(x => x.Id == id && x.IsDeleted == false);
            if (clientSliders == null)
                return true;

            clientSliders.IsActive = !clientSliders.IsActive;
            return dc.SaveChanges() > 0;
        }

        /// <summary>
        /// Soft delete selected Client Slider According to Id 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns>Boolean Result</returns>
        public static bool DeleteClientSlider(long id)
        {
            var dc = new FMSContext();
            var clientSliders = dc.ClientSliders.FirstOrDefault(x => x.Id == id && x.IsDeleted == false);
            if (clientSliders == null)
                return true;
            clientSliders.IsDeleted = true;
            return dc.SaveChanges() > 0;
        }

        /// <summary>
        /// Get Sliders List
        /// </summary>
        /// <param name="ownerId"></param>
        /// <returns></returns>
        /// 
        public static List<ClientSliderViewModel> GetSlidersList(int ownerId)
        {
            var dc = new FMSContext();
            List<ClientSliderViewModel> clientSliderViewModels = new List<ClientSliderViewModel>();
            var attachments = dc.ClientSliders.Include(x => x.Logo).Where(x => !x.IsDeleted && x.SitePortalOwnerId == ownerId).ToList();

            foreach (var attachment in attachments)
            {
                // add Slider 
                ClientSliderViewModel clientSliderViewModel = new ClientSliderViewModel
                {
                    Id = attachment.Id,
                    Heading = attachment.Heading,
                    SubHeading = attachment.SubHeading,
                    ButtonText = attachment.ButtonText,
                    ButtonLink = attachment.ButtonLink,
                    LogoId = attachment.LogoId,
                    OwnerId = attachment.OwnerId,
                    SliderLogo = attachment.Logo,
                    SitePortalOwnerId = attachment.SitePortalOwnerId,
                };

                clientSliderViewModels.Add(clientSliderViewModel);
            }
            return clientSliderViewModels;
        }
        #endregion
    }
}
