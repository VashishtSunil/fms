﻿using FMS.Core.DataModels.Stripe;
using FMS.Core.Enums;
using FMS.Core.Helper;
using FMS.Core.Models;
using FMS.Core.Models.Stripe;
using FMS.Data.Context;
using Newtonsoft.Json;
using Serilog;
using Stripe;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace FMS.Services.Classes
{
    public class StripeManager
    {
        StripeKeyModel _options;
        public StripeManager(StripeKeyModel options)
        {
            _options = options;
        }

        #region CRUD function for stripe table
        /// <summary>
        /// update stripe connect account info
        /// </summary>
        /// <param name="stripeConnectAccountModel"></param>
        /// <returns></returns>
        public static async Task<Tuple<bool, string>> UpdateStripeConnectAccount(StripeConnectAccountModel stripeConnectAccountModel)
        {
            bool status = false;
            string message = "Something Went Wrong!";
            try
            {
                var dc = new FMSContext();
                var connectAccount = await dc.StripeConnectAccounts.Where(x => x.ConnectAccountId == stripeConnectAccountModel.StringConnectAccountId && x.IsDeleted != true).FirstOrDefaultAsync();
                if (connectAccount == null)
                {
                    connectAccount = new StripeConnectAccount
                    {
                        ConnectAccountId = stripeConnectAccountModel.StringConnectAccountId,// stripe connect id 
                        UserId = stripeConnectAccountModel.UserId,
                        UserType = (int)stripeConnectAccountModel.UserType,
                        PersonId = stripeConnectAccountModel.PersonId,
                        FirstName = stripeConnectAccountModel.OwnerFirstName,
                        LastName = stripeConnectAccountModel.OwnerLastName,
                        Email = stripeConnectAccountModel.Email,
                        DOB = stripeConnectAccountModel.OwnerDOB,
                        Country = stripeConnectAccountModel.Country,
                        Currency = stripeConnectAccountModel.Currency,
                        BusinessType = (int)stripeConnectAccountModel.BusinessType,
                        AccountType = (int)stripeConnectAccountModel.ConnectAccountType,
                        IsChargeEnabled = stripeConnectAccountModel.IsChargeEnabled,
                        IsPayoutEnabled = stripeConnectAccountModel.IsPayoutEnabled,
                        VerificationStatus = stripeConnectAccountModel.VerificationStatus,
                        IsAuthorized = stripeConnectAccountModel.IsAuthorized,
                        StripeCreatedDate = stripeConnectAccountModel.StripeCreatedDate,

                        JsonRequest = stripeConnectAccountModel.JsonRequest,
                        JsonResponse = stripeConnectAccountModel.JsonResponse,
                        PersonJsonRequest = stripeConnectAccountModel.PersonJsonRequest,
                        PersonJsonResponse = stripeConnectAccountModel.PersonJsonResponse,

                        CreatedBy = stripeConnectAccountModel.CreatedBy,
                        CreatedOn = DateTime.UtcNow
                    };
                    connectAccount.StripeCreatedDate = DateTime.Now;
                    dc.StripeConnectAccounts.Add(connectAccount);
                    await dc.SaveChangesAsync();

                    message = "Connect Account Added Successfully!";
                }
                else
                {
                    connectAccount.ConnectAccountId = stripeConnectAccountModel.StringConnectAccountId;// stripe connect id 
                    connectAccount.UserType = (int)stripeConnectAccountModel.UserType;
                    connectAccount.UserId = stripeConnectAccountModel.UserId;
                    connectAccount.PersonId = stripeConnectAccountModel.PersonId;
                    connectAccount.FirstName = stripeConnectAccountModel.OwnerFirstName;
                    connectAccount.LastName = stripeConnectAccountModel.OwnerLastName;
                    connectAccount.Email = stripeConnectAccountModel.Email;
                    connectAccount.DOB = stripeConnectAccountModel.OwnerDOB;
                    connectAccount.Country = stripeConnectAccountModel.Country;
                    connectAccount.Currency = stripeConnectAccountModel.Currency;
                    connectAccount.BusinessType = (int)stripeConnectAccountModel.BusinessType;
                    connectAccount.AccountType = (int)stripeConnectAccountModel.ConnectAccountType;
                    connectAccount.IsChargeEnabled = stripeConnectAccountModel.IsChargeEnabled;
                    connectAccount.IsPayoutEnabled = stripeConnectAccountModel.IsPayoutEnabled;
                    connectAccount.VerificationStatus = stripeConnectAccountModel.VerificationStatus;
                    connectAccount.IsAuthorized = stripeConnectAccountModel.IsAuthorized;

                    connectAccount.JsonRequest = stripeConnectAccountModel.JsonRequest;
                    connectAccount.JsonResponse = stripeConnectAccountModel.JsonResponse;
                    connectAccount.PersonJsonRequest = stripeConnectAccountModel.PersonJsonRequest;
                    connectAccount.PersonJsonResponse = stripeConnectAccountModel.PersonJsonResponse;

                    connectAccount.ModifiedBy = stripeConnectAccountModel.ModifiedBy;
                    connectAccount.ModifiedOn = DateTime.UtcNow;

                    await dc.SaveChangesAsync();
                    message = "Connect Account Updated Successfully!";
                }

                stripeConnectAccountModel.ConnectAccountId = connectAccount.Id;
                status = true;
            }
            catch (Exception ex)
            {
                status = false;
                message = "Something Went Wrong!";
                Log.Error(ex, "StripeManager=>>UpdateStripeConnectAccount");
            }
            return Tuple.Create(status, message);
        }

        /// <summary>
        /// update authorization or deauthorization status
        /// </summary>
        /// <param name="accountId"></param>
        /// <returns></returns>
        public static async Task<Tuple<bool, string>> UpdateAccountAuthorization(string accountId, bool isAuthorized)
        {
            bool status = false;
            string message = "Something Went Wrong!";
            try
            {
                var dc = new FMSContext();
                var accountDetail = await dc.StripeConnectAccounts.Where(x => x.ConnectAccountId == accountId && !x.IsDeleted).FirstOrDefaultAsync();
                if (accountDetail != null)
                {
                    accountDetail.IsAuthorized = isAuthorized;
                    await dc.SaveChangesAsync();
                }
                status = true;
                message = "Connect Account Authorization Updated Successful!";
            }
            catch (Exception ex)
            {
                Log.Error(ex, "StripeManager==>UpdateAccountAuthorization");
            }
            return Tuple.Create(status, message);
        }

        /// <summary>
        /// get stripe connect account detail by userId
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static async Task<StripeConnectAccountModel> GetStripeConnectAccountDetailByUserId(int userId)
        {
            StripeConnectAccountModel stripeConnectAccountModel = new StripeConnectAccountModel();
            try
            {
                var dc = new FMSContext();
                stripeConnectAccountModel = await dc.StripeConnectAccounts.Where(x => x.UserId == userId && x.IsDeleted != true).Select(x => new StripeConnectAccountModel()
                {
                    ConnectAccountId = x.Id,
                    StringConnectAccountId = x.ConnectAccountId,// stripe connect id 
                    UserId = x.UserId,
                    UserType = (UserType)x.UserType,
                    PersonId = x.PersonId,
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                    Email = x.Email,
                    OwnerDOB = x.DOB,
                    Country = x.Country,
                    Currency = x.Currency,
                    BusinessType = (StripeConnectBusinessType)x.BusinessType,
                    ConnectAccountType = (StripeAccountType)x.AccountType,
                    IsChargeEnabled = x.IsChargeEnabled,
                    IsPayoutEnabled = x.IsPayoutEnabled,
                    VerificationStatus = x.VerificationStatus,
                    IsAuthorized = x.IsAuthorized,
                    StripeCreatedDate = x.StripeCreatedDate,

                    JsonRequest = x.JsonRequest,
                    JsonResponse = x.JsonResponse,

                    CreatedBy = x.CreatedBy,
                    ModifiedBy = x.ModifiedBy

                }).FirstOrDefaultAsync();
            }
            catch (Exception ex)
            {
                stripeConnectAccountModel = null;
                Log.Error(ex, "StripeManager=>>UpdateStripeConnectAccountDocuments");
            }
            return stripeConnectAccountModel;
        }

        /// <summary>
        /// get stripe connect account based on userDetailId and userType
        /// </summary>
        /// <param name="userDetailId"></param>
        /// <param name="userType"></param>
        /// <returns></returns>
        public static async Task<StripeConnectAccountModel> GetStripeConnectAccountDetailByUserType(int userDetailId, int userType)
        {
            StripeConnectAccountModel stripeConnectAccountModel = new StripeConnectAccountModel();
            try
            {
                var dc = new FMSContext();
                stripeConnectAccountModel = await (from userMap in dc.UserMap
                                                   join user in dc.Users
                                                   on userMap.UserId equals user.Id
                                                   where userMap.UserType == userType
                                                   && userMap.UserDetailId == userDetailId
                                                   join connectAccount in dc.StripeConnectAccounts
                                                   on user.Id equals connectAccount.UserId
                                                   where userMap.IsDeleted != true && user.IsDeleted != true
                                                   && connectAccount.IsDeleted != true
                                                   select new StripeConnectAccountModel()
                                                   {
                                                       ConnectAccountId = connectAccount.Id,
                                                       StringConnectAccountId = connectAccount.ConnectAccountId,// stripe connect id 
                                                       UserId = connectAccount.UserId,
                                                       UserType = (UserType)connectAccount.UserType,
                                                       PersonId = connectAccount.PersonId,
                                                       FirstName = connectAccount.FirstName,
                                                       LastName = connectAccount.LastName,
                                                       Email = connectAccount.Email,
                                                       OwnerDOB = connectAccount.DOB,
                                                       Country = connectAccount.Country,
                                                       Currency = connectAccount.Currency,
                                                       BusinessType = (StripeConnectBusinessType)connectAccount.BusinessType,
                                                       ConnectAccountType = (StripeAccountType)connectAccount.AccountType,
                                                       IsChargeEnabled = connectAccount.IsChargeEnabled,
                                                       IsPayoutEnabled = connectAccount.IsPayoutEnabled,
                                                       VerificationStatus = connectAccount.VerificationStatus,
                                                       IsAuthorized = connectAccount.IsAuthorized,
                                                       StripeCreatedDate = connectAccount.StripeCreatedDate,

                                                       JsonRequest = connectAccount.JsonRequest,
                                                       JsonResponse = connectAccount.JsonResponse,

                                                       CreatedBy = connectAccount.CreatedBy,
                                                       ModifiedBy = connectAccount.ModifiedBy
                                                   }).FirstOrDefaultAsync();
            }
            catch (Exception ex)
            {
                stripeConnectAccountModel = null;
                Log.Error(ex, "StripeManager=>>GetStripeConnectAccountDetailByUserType");
            }
            return stripeConnectAccountModel;
        }

        /// <summary>
        /// get trainee connect account detail
        /// </summary>
        /// <returns></returns>
        public static async Task<List<StripeConnectAccountModel>> GetTraineeStripeConnectAccountDetail()
        {
            List<StripeConnectAccountModel> lstStripeConnectAccountModel = new List<StripeConnectAccountModel>();
            try
            {
                var dc = new FMSContext();
                int traineeType = UserType.Trainee.GetHashCode();
                lstStripeConnectAccountModel = await (from reseller in dc.Users
                                                      //reseller in dc.Trainees
                                                      join userMap in dc.UserMap
                                                      on reseller.Id equals userMap.UserDetailId
                                                      where userMap.UserType == traineeType
                                                      && userMap.IsDeleted != true
                                                      join user in dc.Users
                                                      on userMap.UserId equals user.Id
                                                      join connectAccount in dc.StripeConnectAccounts
                                                      on user.Id equals connectAccount.UserId
                                                      where connectAccount.UserType == traineeType
                                                      && connectAccount.IsDeleted != true
                                                      //set the join according to data models
                                                      //join resellerBank in dc.ResellerBankingInfos
                                                      //on reseller.Id equals resellerBank.ResellerId
                                                      where user.IsDeleted != true
                                                      && reseller.IsDeleted != true
                                                      //&& resellerBank.IsDeleted != true
                                                      && connectAccount.IsDeleted != true
                                                      //&& reseller.IsUpdated
                                                      select new StripeConnectAccountModel()
                                                      {
                                                          ResellerId = reseller.Id,
                                                          Username = user.Username,
                                                          //BankCountryId = resellerBank.CountryId != null ? resellerBank.CountryId.Value : 0,
                                                          //BankCountryName = resellerBank.Country != null ? resellerBank.Country.Code : "",
                                                          Email = user.Email,
                                                          CompanyName = user.Company,
                                                          CompanyPhone = user.Phone,
                                                          Phone = user.Phone,
                                                          //CountryId = reseller.CountryId != null ? reseller.CountryId.Value : 0,
                                                          //Country = reseller.Country != null ? reseller.Country.Code : "",
                                                          //StateId = reseller.StateId != null ? reseller.StateId.Value : 0,
                                                          //State = reseller.State != null ? reseller.State.Code : "",
                                                          //City = reseller.City,
                                                          //Address = reseller.Address,
                                                          //Postal = reseller.Postal,
                                                          //BankName = resellerBank.BankName,
                                                          //BranchName = resellerBank.BranchName,
                                                          //AccountNumber = resellerBank.AccountNumber,
                                                          //BusinessNumber = resellerBank.BusinessNumber,
                                                          //OwnerFirstName = resellerBank.OwnerFirstName,
                                                          //OwnerLastName = resellerBank.OwnerLastName,
                                                          //OwnerDOB = resellerBank.OwnerDOB,
                                                          //FrontDriverLicenseId = resellerBank.FrontDriverLicenseId,
                                                          //BackDriverLicenseId = resellerBank.BackDriverLicenseId,
                                                          //ArticalOfIncorporationId = resellerBank.ArticalOfIncorporationId,
                                                          UserId = user.Id,
                                                          StringConnectAccountId = connectAccount.ConnectAccountId,
                                                          PersonId = connectAccount.PersonId,
                                                          ConnectAccountId = connectAccount.Id,// stripe connect id 
                                                          UserType = (UserType)connectAccount.UserType,
                                                          //FirstName = resellerBank.OwnerFirstName,
                                                          //LastName = resellerBank.OwnerLastName,
                                                          BusinessType = (StripeConnectBusinessType)connectAccount.BusinessType,
                                                          ConnectAccountType = (StripeAccountType)connectAccount.AccountType,
                                                          IsChargeEnabled = connectAccount.IsChargeEnabled,
                                                          IsPayoutEnabled = connectAccount.IsPayoutEnabled,
                                                          VerificationStatus = connectAccount.VerificationStatus,
                                                          IsAuthorized = connectAccount.IsAuthorized,

                                                          JsonRequest = connectAccount.JsonRequest,
                                                          JsonResponse = connectAccount.JsonResponse,
                                                          PersonJsonRequest = connectAccount.PersonJsonRequest,
                                                          PersonJsonResponse = connectAccount.PersonJsonResponse,

                                                          CreatedBy = connectAccount.CreatedBy,
                                                          ModifiedBy = "1", //as superadmin
                                                      }).ToListAsync();
            }
            catch (Exception ex)
            {
                Log.Error(ex, "StripeManager==>GetResellersStripeConnectAccountDetail");
            }
            return lstStripeConnectAccountModel;
        }

        /// <summary>
        /// get stripe connect account detail
        /// </summary>
        /// <returns></returns>
        public static async Task<List<StripeConnectAccountModel>> GetAllStripeConnectAccountDetail()
        {
            List<StripeConnectAccountModel> lstStripeConnectAccount = new List<StripeConnectAccountModel>();
            try
            {
                var dc = new FMSContext();
                lstStripeConnectAccount = await dc.StripeConnectAccounts.Where(x => x.IsDeleted != true).Select(x => new StripeConnectAccountModel()
                {
                    ConnectAccountId = x.Id,
                    StringConnectAccountId = x.ConnectAccountId,// stripe connect id 
                    UserId = x.UserId,
                    UserType = (UserType)x.UserType,
                    PersonId = x.PersonId,
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                    Email = x.Email,
                    OwnerDOB = x.DOB,
                    Country = x.Country,
                    Currency = x.Currency,
                    BusinessType = (StripeConnectBusinessType)x.BusinessType,
                    ConnectAccountType = (StripeAccountType)x.AccountType,
                    IsChargeEnabled = x.IsChargeEnabled,
                    IsPayoutEnabled = x.IsPayoutEnabled,
                    VerificationStatus = x.VerificationStatus,
                    IsAuthorized = x.IsAuthorized,
                    StripeCreatedDate = x.StripeCreatedDate,

                    JsonRequest = x.JsonRequest,
                    JsonResponse = x.JsonResponse,

                    CreatedBy = x.CreatedBy,
                    ModifiedBy = x.ModifiedBy

                }).ToListAsync();
            }
            catch (Exception ex)
            {
                lstStripeConnectAccount = null;
                Log.Error(ex, "StripeManager=>>GetAllStripeConnectAccountDetail");
            }
            return lstStripeConnectAccount;
        }

        /// <summary>
        ///  update stripe connect account document
        /// </summary>
        /// <param name="connectAccountId"></param>
        /// <param name="lstDocuments"></param>
        /// <returns></returns>
        public static async Task<Tuple<bool, string>> UpdateStripeConnectAccountDocuments(StripeConnectAccountDocumentModel stripeConnectAccountDocumentModel)
        {
            bool status = false;
            string message = "Something Went Wrong!";
            try
            {
                var dc = new FMSContext();
                var fileDetail = await dc.StripeConnectAccountDocuments.Where(x => x.StripeConnectAccountId == stripeConnectAccountDocumentModel.ConnectAccountId && x.AttachmentId == stripeConnectAccountDocumentModel.AttachmentId && x.IsDeleted != true).FirstOrDefaultAsync();
                if (fileDetail == null)
                {
                    fileDetail = new StripeConnectAccountDocument()
                    {
                        StripeConnectAccountId = stripeConnectAccountDocumentModel.ConnectAccountId,
                        AttachmentId = stripeConnectAccountDocumentModel.AttachmentId,
                        StripeFileId = stripeConnectAccountDocumentModel.StripeFileId,
                        IsFrontSide = stripeConnectAccountDocumentModel.IsFrontSide,
                        IsBackSide = stripeConnectAccountDocumentModel.IsBackSide,
                        DocumentSize = stripeConnectAccountDocumentModel.DocumentSize,
                        DocumentPurpose = (int)stripeConnectAccountDocumentModel.DocumentPurpose,
                        JsonRequest = stripeConnectAccountDocumentModel.JsonRequest,
                        JsonResponse = stripeConnectAccountDocumentModel.JsonResponse,

                        CreatedBy = stripeConnectAccountDocumentModel.CreatedBy,
                        CreatedOn = DateTime.UtcNow
                    };
                    dc.StripeConnectAccountDocuments.Add(fileDetail);
                    status = await dc.SaveChangesAsync() > 0;

                    message = "Connect Account File Mapping Added Successfully!";
                }
                else
                {
                    fileDetail.StripeConnectAccountId = stripeConnectAccountDocumentModel.ConnectAccountId;
                    fileDetail.AttachmentId = stripeConnectAccountDocumentModel.AttachmentId;
                    fileDetail.StripeFileId = stripeConnectAccountDocumentModel.StripeFileId;
                    fileDetail.IsFrontSide = stripeConnectAccountDocumentModel.IsFrontSide;
                    fileDetail.IsBackSide = stripeConnectAccountDocumentModel.IsBackSide;
                    fileDetail.DocumentSize = stripeConnectAccountDocumentModel.DocumentSize;
                    fileDetail.DocumentPurpose = (int)stripeConnectAccountDocumentModel.DocumentPurpose;
                    fileDetail.JsonRequest = stripeConnectAccountDocumentModel.JsonRequest;
                    fileDetail.JsonResponse = stripeConnectAccountDocumentModel.JsonResponse;

                    fileDetail.ModifiedBy = stripeConnectAccountDocumentModel.ModifiedBy;
                    fileDetail.ModifiedOn = DateTime.Now;

                    status = await dc.SaveChangesAsync() > 0;
                    message = "Connect Account File Mapping Updated Successfully!";
                }
            }
            catch (Exception ex)
            {
                status = false;
                message = "Something Went Wrong!";
                Log.Error(ex, "StripeManager=>>UpdateStripeConnectAccountDocuments");
            }
            return Tuple.Create(status, message);
        }

        /// <summary>
        /// get all stripe connect account documents
        /// </summary>
        /// <param name="connectAccountId"></param>
        /// <returns></returns>
        public static async Task<List<StripeConnectAccountDocumentModel>> GetConnectAccountDocuments(int connectAccountId)
        {
            try
            {
                var dc = new FMSContext();
                return await dc.StripeConnectAccountDocuments.Where(x => x.StripeConnectAccountId == connectAccountId && !x.IsDeleted).Select(x => new StripeConnectAccountDocumentModel()
                {
                    Id = x.Id,
                    ConnectAccountId = x.StripeConnectAccountId,
                    AttachmentId = x.AttachmentId,
                    StripeFileId = x.StripeFileId,
                    IsFrontSide = x.IsFrontSide,
                    IsBackSide = x.IsBackSide,
                    DocumentSize = x.DocumentSize,
                    DocumentPurpose = (FileUploadingPurpose)x.DocumentPurpose,
                    JsonRequest = x.JsonRequest,
                    JsonResponse = x.JsonResponse,

                    CreatedBy = x.CreatedBy,
                    ModifiedBy = x.ModifiedBy
                }).ToListAsync();
            }
            catch (Exception ex)
            {
                Log.Error(ex, "StripeManager==>GetConnectAccountDocuments");
                return null;
            }
        }

        /// <summary>
        /// update stripe connect account documents
        /// </summary>
        /// <param name="lstDocuments"></param>
        /// <returns></returns>
        public static async Task<Tuple<bool, string>> UpdateStripeConnectAccountDocumentList(List<StripeConnectAccountDocumentModel> lstDocuments)
        {
            bool status = false;
            string message = "Something Went Wrong!";
            try
            {
                if (lstDocuments != null && lstDocuments.Count > 0)
                {
                    var dc = new FMSContext();
                    foreach (var document in lstDocuments)
                    {
                        var fileDetail = dc.StripeConnectAccountDocuments.Where(x => x.StripeConnectAccountId == document.ConnectAccountId && x.AttachmentId == document.AttachmentId && x.IsDeleted != true).FirstOrDefault();
                        if (fileDetail == null)
                        {
                            fileDetail = new StripeConnectAccountDocument()
                            {
                                StripeConnectAccountId = document.ConnectAccountId,
                                AttachmentId = document.AttachmentId,
                                StripeFileId = document.StripeFileId,
                                IsFrontSide = document.IsFrontSide,
                                IsBackSide = document.IsBackSide,
                                DocumentSize = document.DocumentSize,
                                DocumentPurpose = (int)document.DocumentPurpose,
                                JsonRequest = document.JsonRequest,
                                JsonResponse = document.JsonResponse,

                                CreatedBy = document.CreatedBy,
                                CreatedOn = DateTime.UtcNow
                            };
                            dc.StripeConnectAccountDocuments.Add(fileDetail);
                            status = await dc.SaveChangesAsync() > 0;

                            message = "Connect Account File Mapping Added Successfully!";
                        }
                        else
                        {
                            fileDetail.StripeConnectAccountId = document.ConnectAccountId;
                            fileDetail.AttachmentId = document.AttachmentId;
                            fileDetail.StripeFileId = document.StripeFileId;
                            fileDetail.IsFrontSide = document.IsFrontSide;
                            fileDetail.IsBackSide = document.IsBackSide;
                            fileDetail.DocumentSize = document.DocumentSize;
                            fileDetail.DocumentPurpose = (int)document.DocumentPurpose;
                            fileDetail.JsonRequest = document.JsonRequest;
                            fileDetail.JsonResponse = document.JsonResponse;

                            fileDetail.ModifiedBy = document.ModifiedBy;
                            fileDetail.ModifiedOn = DateTime.Now;

                            status = await dc.SaveChangesAsync() > 0;
                            message = "Connect Account File Mapping Updated Successfully!";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                status = false;
                message = "Something Went Wrong!";
                Log.Error(ex, "StripeManager=>>UpdateStripeConnectAccountDocumentList");
            }
            return Tuple.Create(status, message);
        }

        /// <summary>
        /// add/update stripe customer detail
        /// </summary>
        /// <param name="stripeCustomerModel"></param>
        /// <returns></returns>
        public static async Task<Tuple<bool, string>> UpdateStripeCustomerDetail(StripeCustomerModel stripeCustomerModel)
        {
            bool status = false;
            string message = "Something Went Wrong!";
            try
            {
                var dc = new FMSContext();
                var stripeCustomer = dc.StripeCustomers.Where(x => x.Id == stripeCustomerModel.Id && x.IsDeleted != true).FirstOrDefault();
                if (stripeCustomer == null)
                {
                    stripeCustomer = new StripeCustomer
                    {
                        StripeCustomerId = stripeCustomerModel.StripeCustomerId,// stripe customer Id In Stripe Board 
                        UserId = stripeCustomerModel.UserId,
                        UserName = stripeCustomerModel.UserName,
                        UserEmail = stripeCustomerModel.UserEmail,
                        Description = stripeCustomerModel.Description,
                        Address1 = stripeCustomerModel.Address1,
                        Address2 = stripeCustomerModel.Address2,
                        City = stripeCustomerModel.City,
                        State = stripeCustomerModel.State,
                        Country = stripeCustomerModel.Country,
                        PostalCode = stripeCustomerModel.PostalCode,
                        PhoneNumber = stripeCustomerModel.PhoneNumber,

                        LiveMode = stripeCustomerModel.LiveMode,
                        StripeCreatedDateTime = stripeCustomerModel.StripeCreatedDateTime,
                        JsonResponse = stripeCustomerModel.JsonResponse,
                        CreatedBy = stripeCustomerModel.CreatedBy,
                        CreatedOn = DateTime.UtcNow
                    };
                    dc.StripeCustomers.Add(stripeCustomer);
                    await dc.SaveChangesAsync();

                    message = "Stripe Customer Added Successfully!";
                }
                else
                {
                    stripeCustomerModel.UserId = stripeCustomerModel.UserId;
                    stripeCustomer.StripeCustomerId = stripeCustomerModel.StripeCustomerId;// stripe 

                    stripeCustomer.UserName = stripeCustomerModel.UserName;
                    stripeCustomer.UserEmail = stripeCustomerModel.UserEmail;
                    stripeCustomer.Description = stripeCustomerModel.Description;
                    stripeCustomer.Address1 = stripeCustomerModel.Address1;
                    stripeCustomer.Address2 = stripeCustomerModel.Address2;
                    stripeCustomer.City = stripeCustomerModel.City;
                    stripeCustomer.State = stripeCustomerModel.State;
                    stripeCustomer.Country = stripeCustomerModel.Country;
                    stripeCustomer.PostalCode = stripeCustomerModel.PostalCode;
                    stripeCustomer.PhoneNumber = stripeCustomerModel.PhoneNumber;

                    stripeCustomer.LiveMode = stripeCustomerModel.LiveMode;
                    stripeCustomer.StripeCreatedDateTime = stripeCustomerModel.StripeCreatedDateTime;
                    stripeCustomer.JsonResponse = stripeCustomerModel.JsonResponse;
                    stripeCustomer.ModifiedBy = stripeCustomerModel.ModifiedBy;
                    stripeCustomer.ModifiedOn = DateTime.UtcNow;

                    await dc.SaveChangesAsync();
                    message = "Stripe Customer Updated Successfully!";
                }


                stripeCustomerModel.Id = stripeCustomer.Id;
                status = true;
            }
            catch (Exception ex)
            {
                status = false;
                message = "Something Went Wrong!";
                Log.Error(ex, "StripeManager=>>UpdateStripeCustomerDetail");
            }
            return Tuple.Create(status, message);
        }

        /// <summary>
        /// get stripe customer detail by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static StripeCustomerModel GetStripeCustomerDetail(long id)
        {
            StripeCustomerModel stripeCustomerModel = new StripeCustomerModel();
            try
            {
                var dc = new FMSContext();
                stripeCustomerModel = dc.StripeCustomers.Where(x => x.Id == id && x.IsDeleted != true).Select(x => new StripeCustomerModel()
                {
                    Id = x.Id,
                    UserId = x.UserId,
                    StripeCustomerId = x.StripeCustomerId,// stripe customer Id In Stripe Board 

                    UserName = x.UserName,
                    UserEmail = x.UserEmail,
                    Description = x.Description,
                    Address1 = x.Address1,
                    Address2 = x.Address2,
                    City = x.City,
                    State = x.State,
                    Country = x.Country,
                    PostalCode = x.PostalCode,

                    LiveMode = x.LiveMode,
                    StripeCreatedDateTime = x.StripeCreatedDateTime,
                    JsonResponse = x.JsonResponse,
                    CreatedBy = x.CreatedBy,
                    ModifiedBy = x.ModifiedBy
                }).FirstOrDefault();
            }
            catch (Exception ex)
            {
                stripeCustomerModel = null;
                Log.Error(ex, "StripeManager=>>GetStripeCustomerDetail");
            }
            return stripeCustomerModel;
        }

        /// <summary>
        /// get stripe customer detail by user-id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static StripeCustomerModel GetStripeCustomerDetailByUserId(long userId)
        {
            StripeCustomerModel stripeCustomerModel = new StripeCustomerModel();
            try
            {
                var dc = new FMSContext();
                stripeCustomerModel = dc.StripeCustomers.Where(x => x.UserId == userId && x.IsDeleted != true).Select(x => new StripeCustomerModel()
                {
                    Id = x.Id,
                    UserId = x.UserId,
                    StripeCustomerId = x.StripeCustomerId,// stripe customer Id In Stripe Board 

                    UserName = x.UserName,
                    UserEmail = x.UserEmail,
                    Description = x.Description,
                    Address1 = x.Address1,
                    Address2 = x.Address2,
                    City = x.City,
                    State = x.State,
                    Country = x.Country,
                    PostalCode = x.PostalCode,

                    LiveMode = x.LiveMode,
                    StripeCreatedDateTime = x.StripeCreatedDateTime,
                    JsonResponse = x.JsonResponse,
                    CreatedBy = x.CreatedBy,
                    ModifiedBy = x.ModifiedBy
                }).FirstOrDefault();
            }
            catch (Exception ex)
            {
                stripeCustomerModel = null;
                Log.Error(ex, "StripeManager=>>GetStripeCustomerDetailByUserId");
            }
            return stripeCustomerModel;
        }

        /// <summary>
        /// get stripe customer detail by stripeCardId
        /// </summary>
        /// <param name="stripeCardId"></param>
        /// <returns></returns>
        public static async Task<StripeCustomerModel> GetStripeCustomerDetailByCardId(string stripeCardId)
        {
            StripeCustomerModel stripeCustomerModel = new StripeCustomerModel();
            try
            {
                var dc = new FMSContext();
                stripeCustomerModel = await (from card in dc.StripeCustomerCardDetails
                                             join cust in dc.StripeCustomers
                                             on card.StripeCustomerId equals cust.Id
                                             join user in dc.Users
                                             on cust.UserId equals user.Id
                                             where card.StripeCardId == stripeCardId
                                             && !card.IsDeleted && !cust.IsDeleted
                                             && user.IsDeleted != true
                                             select new StripeCustomerModel()
                                             {
                                                 Id = cust.Id,
                                                 UserId = cust.UserId,
                                                 StripeCustomerId = cust.StripeCustomerId,// stripe customer Id In Stripe Board 

                                                 UserName = user.Username,
                                                 UserEmail = user.Email,
                                                 Description = cust.Description,
                                                 Address1 = cust.Address1,
                                                 Address2 = cust.Address2,
                                                 City = cust.City,
                                                 State = cust.State,
                                                 Country = cust.Country,
                                                 PostalCode = cust.PostalCode,

                                                 LiveMode = cust.LiveMode,
                                                 StripeCreatedDateTime = cust.StripeCreatedDateTime,
                                                 JsonResponse = cust.JsonResponse,
                                                 CreatedBy = cust.CreatedBy,
                                                 ModifiedBy = cust.ModifiedBy
                                             }).FirstOrDefaultAsync();
            }
            catch (Exception ex)
            {
                stripeCustomerModel = null;
                Log.Error(ex, "StripeManager=>>GetStripeCustomerDetailByCardId");
            }
            return stripeCustomerModel;
        }

        /// <summary>
        /// add/update stripe customer credit card detail
        /// </summary>
        /// <param name="stripeCustomerCardDetailModel"></param>
        /// <returns></returns>
        public static async Task<Tuple<bool, string>> UpdateStripeCustomerCardDetail(StripeCustomerCardDetailModel stripeCustomerCardDetailModel)
        {
            bool status = false;
            string message = "Something Went Wrong!";
            try
            {
                var dc = new FMSContext();
                var cardDetail = dc.StripeCustomerCardDetails.Where(x => x.Id == stripeCustomerCardDetailModel.Id && x.IsDeleted != true).FirstOrDefault();
                if (cardDetail == null)
                {
                    cardDetail = new StripeCustomerCardDetail()
                    {
                        StripeCustomerId = stripeCustomerCardDetailModel.CustomerId,
                        StripeCardId = stripeCustomerCardDetailModel.StripeCardId,
                        AccountNumber = stripeCustomerCardDetailModel.CardNumber,
                        ExpirationMonth = stripeCustomerCardDetailModel.ExpirationMonth,
                        ExpirationYear = stripeCustomerCardDetailModel.ExpirationYear,
                        Cvc = stripeCustomerCardDetailModel.Cvc,
                        CardToken = stripeCustomerCardDetailModel.CardToken,
                        Last4 = stripeCustomerCardDetailModel.Last4,
                        CvcCheck = stripeCustomerCardDetailModel.CvcCheck,
                        Fingerprint = stripeCustomerCardDetailModel.Fingerprint,
                        CardType = stripeCustomerCardDetailModel.CardType,
                        JsonResponse = stripeCustomerCardDetailModel.JsonResponse,
                        CreatedBy = stripeCustomerCardDetailModel.CreatedBy,
                        CreatedOn = DateTime.UtcNow
                    };
                    dc.StripeCustomerCardDetails.Add(cardDetail);
                    await dc.SaveChangesAsync();

                    message = "Customer Card Added Successfully!";
                }
                else
                {
                    cardDetail.StripeCustomerId = stripeCustomerCardDetailModel.CustomerId;
                    cardDetail.StripeCardId = stripeCustomerCardDetailModel.StripeCardId;
                    cardDetail.AccountNumber = stripeCustomerCardDetailModel.CardNumber;
                    cardDetail.ExpirationMonth = stripeCustomerCardDetailModel.ExpirationMonth;
                    cardDetail.ExpirationYear = stripeCustomerCardDetailModel.ExpirationYear;
                    cardDetail.Cvc = stripeCustomerCardDetailModel.Cvc;
                    cardDetail.CardToken = stripeCustomerCardDetailModel.CardToken;
                    cardDetail.Last4 = stripeCustomerCardDetailModel.Last4;
                    cardDetail.CvcCheck = stripeCustomerCardDetailModel.CvcCheck;
                    cardDetail.Fingerprint = stripeCustomerCardDetailModel.Fingerprint;
                    cardDetail.CardType = stripeCustomerCardDetailModel.CardType;
                    cardDetail.JsonResponse = stripeCustomerCardDetailModel.JsonResponse;
                    cardDetail.ModifiedBy = stripeCustomerCardDetailModel.ModifiedBy;
                    cardDetail.ModifiedOn = DateTime.UtcNow;

                    dc.StripeCustomerCardDetails.Add(cardDetail);
                    await dc.SaveChangesAsync();
                }
                stripeCustomerCardDetailModel.Id = cardDetail.Id;

                status = true;
            }
            catch (Exception ex)
            {
                status = false;
                message = "Something Went Wrong!";
                Log.Error(ex, "StripeManager=>>UpdateStripeCustomerCardDetail");
            }
            return Tuple.Create(status, message);
        }

        /// <summary>
        /// delete stripe customer card detail
        /// </summary>
        /// <param name="cardId"></param>
        /// <returns></returns>
        public static async Task<Tuple<bool, string>> DeleteStripeCustomerCard(int cardId)
        {
            bool status = false;
            string message = "Something Went Wrong!";
            try
            {
                var dc = new FMSContext();
                var cardDetail = await dc.StripeCustomerCardDetails.Where(x => x.Id == cardId && !x.IsDeleted).FirstOrDefaultAsync();
                if (cardDetail != null)
                {
                    cardDetail.IsDeleted = true;
                    await dc.SaveChangesAsync();
                }
                status = true;
                message = "Card Deleted Successfully!";
            }
            catch (Exception ex)
            {
                status = false;
                message = "Something Went Wrong!";
                Log.Error(ex, "StripeManager=>>DeleteStripeCustomerCard");
            }
            return Tuple.Create(status, message);
        }

        /// <summary>
        /// get customer card detail by cardId
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static async Task<StripeCustomerCardDetailModel> GetStripeCustomerCardDetail(long cardId)
        {
            StripeCustomerCardDetailModel stripeCustomerCardDetailModel = new StripeCustomerCardDetailModel();
            try
            {
                var dc = new FMSContext();
                stripeCustomerCardDetailModel = await dc.StripeCustomerCardDetails.Where(x => x.Id == cardId && x.IsDeleted != true).Select(x => new StripeCustomerCardDetailModel()
                {
                    Id = x.Id,
                    CustomerId = x.StripeCustomerId,
                    StripeCustomerId = x.StripeCustomer.StripeCustomerId,
                    StripeCardId = x.StripeCardId,
                    Country = x.StripeCustomer.Country,
                    CardNumber = x.AccountNumber,
                    ExpirationMonth = x.ExpirationMonth,
                    ExpirationYear = x.ExpirationYear,
                    Cvc = x.Cvc,
                    CardToken = x.CardToken,
                    Last4 = x.Last4,
                    CvcCheck = x.CvcCheck,
                    Fingerprint = x.Fingerprint,
                    CardType = x.CardType,
                    JsonResponse = x.JsonResponse,
                    CreatedBy = x.CreatedBy,
                    ModifiedBy = x.ModifiedBy
                }).FirstOrDefaultAsync();
            }
            catch (Exception ex)
            {
                stripeCustomerCardDetailModel = null;
                Log.Error(ex, "StripeManager=>>GetStripeCustomerCardDetail");
            }
            return stripeCustomerCardDetailModel;
        }

        /// <summary>
        /// get stripe customer and card detail by userId
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static async Task<StripeCustomerCardDetailModel> GetStripeCustomerCardDetailByUserId(int userId)
        {
            StripeCustomerCardDetailModel stripeCustomerCardDetailModel = new StripeCustomerCardDetailModel();
            try
            {
                var dc = new FMSContext();
                stripeCustomerCardDetailModel = await (from stripeCustomer in dc.StripeCustomers
                                                       join stripeCard in dc.StripeCustomerCardDetails
                                                       on stripeCustomer.Id equals stripeCard.StripeCustomerId
                                                       where stripeCustomer.UserId == userId
                                                       && stripeCustomer.IsDeleted != true
                                                       && stripeCard.IsDeleted != true
                                                       select new StripeCustomerCardDetailModel()
                                                       {
                                                           Id = stripeCard.Id,
                                                           CustomerId = stripeCustomer.Id,
                                                           StripeCustomerId = stripeCustomer.StripeCustomerId,
                                                           StripeCardId = stripeCard.StripeCardId,
                                                           Country = stripeCustomer.Country,
                                                           CardNumber = stripeCard.AccountNumber,
                                                           ExpirationMonth = stripeCard.ExpirationMonth,
                                                           ExpirationYear = stripeCard.ExpirationYear,
                                                           Cvc = stripeCard.Cvc,
                                                           CardToken = stripeCard.CardToken,
                                                           Last4 = stripeCard.Last4,
                                                           CvcCheck = stripeCard.CvcCheck,
                                                           Fingerprint = stripeCard.Fingerprint,
                                                           CardType = stripeCard.CardType,
                                                           JsonResponse = stripeCard.JsonResponse,
                                                           CreatedBy = stripeCard.CreatedBy,
                                                           ModifiedBy = stripeCard.ModifiedBy
                                                       }).FirstOrDefaultAsync();
            }
            catch (Exception ex)
            {
                stripeCustomerCardDetailModel = null;
                Log.Error(ex, "StripeManager=>>GetStripeCustomerCardDetailByUserId");
            }
            return stripeCustomerCardDetailModel;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="cardId"></param>
        /// <returns></returns>
        public static async Task<StripeCustomerCardDetailModel> GetStripeCustomerCardDetailByCustomerId(int customerId)
        {
            StripeCustomerCardDetailModel stripeCustomerCardDetailModel = new StripeCustomerCardDetailModel();
            try
            {
                var dc = new FMSContext();
                stripeCustomerCardDetailModel = await dc.StripeCustomerCardDetails.Where(x => x.StripeCustomerId == customerId && x.IsDeleted != true).Select(x => new StripeCustomerCardDetailModel()
                {
                    Id = x.Id,
                    CustomerId = x.StripeCustomerId,
                    StripeCustomerId = x.StripeCustomer.StripeCustomerId,
                    StripeCardId = x.StripeCardId,
                    Country = x.StripeCustomer.Country,
                    CardNumber = x.AccountNumber,
                    ExpirationMonth = x.ExpirationMonth,
                    ExpirationYear = x.ExpirationYear,
                    Cvc = x.Cvc,
                    CardToken = x.CardToken,
                    Last4 = x.Last4,
                    CvcCheck = x.CvcCheck,
                    Fingerprint = x.Fingerprint,
                    CardType = x.CardType,
                    JsonResponse = x.JsonResponse,
                    CreatedBy = x.CreatedBy,
                    ModifiedBy = x.ModifiedBy
                }).FirstOrDefaultAsync();
            }
            catch (Exception ex)
            {
                stripeCustomerCardDetailModel = null;
                Log.Error(ex, "StripeManager=>>GetStripeCustomerCardDetail");
            }
            return stripeCustomerCardDetailModel;
        }

        /// <summary>
        /// add/update stripe charges detail
        /// </summary>
        /// <param name="stripeChargeModel"></param>
        /// <returns></returns>
        public static async Task<Tuple<bool, string>> SaveStripeCustomerCharge(StripeChargeModel stripeChargeModel)
        {
            bool status = false;
            string message = "Something Went Wrong!";
            try
            {
                var dc = new FMSContext();
                var chargeDetail = dc.StripeCharges.Where(x => x.Id == stripeChargeModel.Id && x.IsDeleted != true).FirstOrDefault();
                if (chargeDetail == null)
                {
                    chargeDetail = new StripeCharge()
                    {
                        Id = stripeChargeModel.Id,
                        StripeChargeId = stripeChargeModel.StripeChargeId,
                        StripeCardId = stripeChargeModel.CardId,
                        StripeConnectAccountId = stripeChargeModel.ConnectAccountId,
                        Currency = (int)stripeChargeModel.Currency,
                        Description = stripeChargeModel.Description,
                        FeeInCents = stripeChargeModel.FeeInCents,
                        PaidAmountInCents = stripeChargeModel.PaidAmountInCents,
                        Amount = stripeChargeModel.Amount,
                        ApplicationFeeAmount = stripeChargeModel.ApplicationFeeAmount,
                        ApplicationFeeAmountInCents = stripeChargeModel.ApplicationFeeAmountInCents,
                        LiveMode = stripeChargeModel.LiveMode,
                        IsPaid = stripeChargeModel.IsPaid,
                        TransactionId = stripeChargeModel.TransactionId,
                        FailureCode = stripeChargeModel.FailureCode,
                        FailureMessage = stripeChargeModel.FailureMessage,
                        RequestDateTime = stripeChargeModel.RequestDateTime,
                        JsonResponse = stripeChargeModel.JsonResponse,
                        PaymentStatus = (int)stripeChargeModel.PaymentStatus,
                        CreatedBy = stripeChargeModel.CreatedBy,
                        CreatedOn = DateTime.UtcNow
                    };
                    dc.StripeCharges.Add(chargeDetail);
                    message = "Charge Added Successfully!";
                }
                else
                {
                    chargeDetail.StripeCardId = stripeChargeModel.CardId;
                    chargeDetail.StripeChargeId = stripeChargeModel.StripeChargeId;
                    chargeDetail.StripeConnectAccountId = stripeChargeModel.ConnectAccountId;
                    chargeDetail.Currency = (int)stripeChargeModel.Currency;
                    chargeDetail.Description = stripeChargeModel.Description;
                    chargeDetail.FeeInCents = stripeChargeModel.FeeInCents;
                    chargeDetail.PaidAmountInCents = stripeChargeModel.PaidAmountInCents;
                    chargeDetail.Amount = stripeChargeModel.Amount;
                    chargeDetail.LiveMode = stripeChargeModel.LiveMode;
                    chargeDetail.IsPaid = stripeChargeModel.IsPaid;
                    chargeDetail.TransactionId = stripeChargeModel.TransactionId;
                    chargeDetail.FailureCode = stripeChargeModel.FailureCode;
                    chargeDetail.FailureMessage = stripeChargeModel.FailureMessage;
                    chargeDetail.RequestDateTime = stripeChargeModel.RequestDateTime;
                    chargeDetail.JsonResponse = stripeChargeModel.JsonResponse;
                    chargeDetail.PaymentStatus = (int)stripeChargeModel.PaymentStatus;
                    chargeDetail.ModifiedBy = stripeChargeModel.ModifiedBy;
                    chargeDetail.ModifiedOn = DateTime.UtcNow;
                    message = "Charge Updated Successfully!";
                }
                await dc.SaveChangesAsync();
                status = true;
                stripeChargeModel.Id = chargeDetail.Id;
            }
            catch (Exception ex)
            {
                status = false;
                message = "Something Went Wrong!";
                Log.Error(ex, "StripeManager=>>SaveStripeCustomerCharge");
            }
            return Tuple.Create(status, message);
        }

        /// <summary>
        /// 
        /// fetch stripe customer charge detail
        /// </summary>
        /// <param name="chargeId"></param>
        /// <returns></returns>
        public static StripeChargeModel GetStripeCustomerChargeDetail(long chargeId)
        {
            StripeChargeModel stripeChargeModel = new StripeChargeModel();
            try
            {
                var dc = new FMSContext();
                stripeChargeModel = dc.StripeCharges.Where(x => x.Id == chargeId && x.IsDeleted != true).Select(x => new StripeChargeModel()
                {
                    Id = x.Id,
                    StripeChargeId = x.StripeChargeId,
                    CardId = x.CardDetail.Id,
                    StripeCardId = x.CardDetail.StripeCardId,
                    CustomerId = x.CardDetail.StripeCustomer.Id,
                    Currency = (CurrencyType)x.Currency,
                    Description = x.Description,
                    FeeInCents = x.FeeInCents,
                    PaidAmountInCents = x.PaidAmountInCents,
                    Amount = x.Amount,
                    LiveMode = x.LiveMode,
                    IsPaid = x.IsPaid,
                    TransactionId = x.TransactionId,
                    FailureCode = x.FailureCode,
                    FailureMessage = x.FailureMessage,
                    RequestDateTime = x.RequestDateTime,
                    JsonResponse = x.JsonResponse,
                    CreatedBy = x.CreatedBy,
                    ModifiedBy = x.ModifiedBy
                }).FirstOrDefault();
            }
            catch (Exception ex)
            {
                stripeChargeModel = null;
                Log.Error(ex, "StripeManager=>>GetStripeCustomerChargeDetail");
            }
            return stripeChargeModel;
        }

        /// <summary>
        /// get stripe customer charge detail by gateway charge id
        /// </summary>
        /// <param name="gatewayChargeId"></param>
        /// <returns></returns>
        public static async Task<StripeChargeModel> GetStripeCustomerChargeDetailByGatewayChargeId(string gatewayChargeId)
        {
            StripeChargeModel stripeChargeModel = new StripeChargeModel();
            try
            {
                var dc = new FMSContext();
                stripeChargeModel = await dc.StripeCharges.Where(x => x.StripeChargeId == gatewayChargeId && x.IsDeleted != true).Select(x => new StripeChargeModel()
                {
                    Id = x.Id,
                    StripeChargeId = x.StripeChargeId,
                    CardId = x.CardDetail.Id,
                    StripeCardId = x.CardDetail.StripeCardId,
                    CustomerId = x.CardDetail.StripeCustomer.Id,
                    Currency = (CurrencyType)x.Currency,
                    Description = x.Description,
                    FeeInCents = x.FeeInCents,
                    PaidAmountInCents = x.PaidAmountInCents,
                    Amount = x.Amount,
                    LiveMode = x.LiveMode,
                    IsPaid = x.IsPaid,
                    TransactionId = x.TransactionId,
                    FailureCode = x.FailureCode,
                    FailureMessage = x.FailureMessage,
                    RequestDateTime = x.RequestDateTime,
                    JsonResponse = x.JsonResponse,
                    CreatedBy = x.CreatedBy,
                    ModifiedBy = x.ModifiedBy
                }).FirstOrDefaultAsync();
            }
            catch (Exception ex)
            {
                stripeChargeModel = null;
                Log.Error(ex, "StripeManager=>>GetStripeCustomerChargeDetailByGatewayChargeId");
            }
            return stripeChargeModel;
        }

        /// <summary>
        /// get stripe charge detail by cardId
        /// </summary>
        /// <param name="cardId"></param>
        /// <returns></returns>
        public static StripeChargeModel GetStripeCustomerChargeDetailByCardId(long cardId)
        {
            StripeChargeModel stripeChargeModel = new StripeChargeModel();
            try
            {
                var dc = new FMSContext();
                stripeChargeModel = dc.StripeCharges.Where(x => x.StripeCardId == cardId && x.IsDeleted != true).Select(x => new StripeChargeModel()
                {
                    Id = x.Id,
                    StripeChargeId = x.StripeChargeId,
                    CardId = x.CardDetail.Id,
                    StripeCardId = x.CardDetail.StripeCardId,
                    CustomerId = x.CardDetail.StripeCustomer.Id,
                    Currency = (CurrencyType)x.Currency,
                    Description = x.Description,
                    FeeInCents = x.FeeInCents,
                    PaidAmountInCents = x.PaidAmountInCents,
                    Amount = x.Amount,
                    LiveMode = x.LiveMode,
                    IsPaid = x.IsPaid,
                    TransactionId = x.TransactionId,
                    FailureCode = x.FailureCode,
                    FailureMessage = x.FailureMessage,
                    RequestDateTime = x.RequestDateTime,
                    JsonResponse = x.JsonResponse,
                    CreatedBy = x.CreatedBy,
                    ModifiedBy = x.ModifiedBy
                }).OrderByDescending(x => x.CreatedOn).FirstOrDefault();
            }
            catch (Exception ex)
            {
                stripeChargeModel = null;
                Log.Error(ex, "StripeManager=>>GetStripeCustomerChargeDetail");
            }
            return stripeChargeModel;
        }


        /// <summary>
        /// get connect account charges
        /// </summary>
        /// <param name="integrationId"></param>
        /// <returns></returns>
        public static async Task<List<StripeChargeModel>> GetConnectAccountChargeDetail(int integrationId)
        {
            List<StripeChargeModel> stripeChargesList = new List<StripeChargeModel>();
            try
            {
                var dc = new FMSContext();
                stripeChargesList = await dc.StripeCharges.Where(x => x.StripeConnectAccountId == integrationId && x.IsDeleted != true).Select(x => new StripeChargeModel()
                {
                    Id = x.Id,
                    StripeChargeId = x.StripeChargeId,
                    CardId = x.CardDetail.Id,
                    StripeCardId = x.CardDetail.StripeCardId,
                    CustomerId = x.CardDetail.StripeCustomer.Id,
                    Currency = (CurrencyType)x.Currency,
                    Description = x.Description,
                    FeeInCents = x.FeeInCents,
                    PaidAmountInCents = x.PaidAmountInCents,
                    Amount = x.Amount,
                    LiveMode = x.LiveMode,
                    IsPaid = x.IsPaid,
                    TransactionId = x.TransactionId,
                    FailureCode = x.FailureCode,
                    FailureMessage = x.FailureMessage,
                    RequestDateTime = x.RequestDateTime,
                    JsonResponse = x.JsonResponse,
                    CreatedBy = x.CreatedBy,
                    ModifiedBy = x.ModifiedBy
                }).ToListAsync();
            }
            catch (Exception ex)
            {
                stripeChargesList = null;
                Log.Error(ex, "StripeManager=>>GetConnectAccountChargeDetail");
            }
            return stripeChargesList;
        }

        /// <summary>
        /// get all the save card by UserId
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static async Task<List<ListItemModel>> GetSaveCardsByUserId(long userId)
        {
            List<ListItemModel> lstCards = new List<ListItemModel>();
            try
            {
                var dc = new FMSContext();
                lstCards = await dc.StripeCustomerCardDetails.Where(x => !x.IsDeleted && x.StripeCustomer.UserId == userId && x.ExpirationMonth <= DateTime.Now.Month && x.ExpirationYear <= DateTime.Now.Year && x.IsDeleted != true).Select(x => new ListItemModel()
                {
                    Id = x.Id,
                    Name = x.AccountNumber
                }).ToListAsync();
            }
            catch (Exception ex)
            {
                Log.Error(ex, "StripeManager=>>GetSaveCardsByUserId");
            }
            return lstCards;
        }

        /// <summary>
        /// delete all saved user cards
        /// </summary>
        /// <param name="stripeCustomerId"></param>
        /// <returns></returns>
        public static async Task<Tuple<bool,string>> DeleteUserSaveCards(long stripeCustomerId)
        {
            bool status = false;
            string message = "Something Went Wrong!";
            try
            {
                var dc = new FMSContext();
                dc.StripeCustomerCardDetails.Where(x => x.StripeCustomerId == stripeCustomerId && !x.IsDeleted).ToList().ForEach(x =>
                {
                    x.IsDeleted = true;
                });
                await dc.SaveChangesAsync();
                status = true;
                message = "Cards Deleted Successfully!";
            }
            catch (Exception ex)
            {
                Log.Error(ex, "StripeManager=>>DeleteUserSaveCards");
            }
            return Tuple.Create(status,message);
        }

        /// <summary>
        /// check whether credit card is expire or not
        /// </summary>
        /// <param name="cardId"></param>
        /// <returns></returns>
        public static async Task<Tuple<bool, string>> IsCardExpire(int cardId)
        {
            bool status = false;
            string message = "Something Went Wrong!";
            try
            {
                var dc = new FMSContext();
                var cardDetail = await dc.StripeCustomerCardDetails.Where(x => x.Id == cardId && x.IsDeleted != true
                                 && x.ExpirationMonth <= DateTime.Now.Month
                                 && x.ExpirationYear <= DateTime.Now.Year).FirstOrDefaultAsync();
                if (cardDetail == null)
                {
                    status = true;
                    message = "Card Is Expire. Please Use Other Card!";
                }
                message = message == "Something Went Wrong!" ? "Card Is Not Expire Yet!" : message;
            }
            catch (Exception ex)
            {
                status = false;
                message = "Something Went Wrong!";
                Log.Error(ex, "StripeManager=>>IsCardExpire");
            }
            return Tuple.Create(status, message);
        }

        /// <summary>
        /// update charge status detail by chargeId
        /// </summary>
        /// <param name="stripeChargeModel"></param>
        /// <returns></returns>
        public static async Task<Tuple<bool, string>> UpdateChargeStatusDetailByChargeId(StripeChargeModel stripeChargeModel)
        {
            bool status = false;
            string message = "Something Went Wrong!";
            try
            {
                var dc = new FMSContext();
                var chargeDetail = await dc.StripeCharges.FirstOrDefaultAsync(x => x.StripeChargeId == stripeChargeModel.StripeChargeId && x.IsDeleted != true);
                if (chargeDetail != null)
                {
                    chargeDetail.IsPaid = stripeChargeModel.IsPaid;
                    chargeDetail.PaymentStatus = (int)stripeChargeModel.PaymentStatus;
                    chargeDetail.ModifiedOn = DateTime.Now;
                    if (stripeChargeModel.PaymentStatus == PaymentStatusType.Failed)
                    {
                        chargeDetail.FailureCode = stripeChargeModel.FailureCode;
                        chargeDetail.FailureMessage = stripeChargeModel.FailureMessage;
                    }

                    await dc.SaveChangesAsync();
                    status = true;
                    message = "Charge Status Updated Successfully!";
                }
            }
            catch (Exception ex)
            {
                status = false;
                message = "Something Went Wrong!";
                Log.Error(ex, "StripeManager=>>UpdateChargeStatusDetailByChargeId");
            }
            return Tuple.Create(status, message);
        }
        #endregion

        #region stripe api functions

        #region stripe connect account
        public async Task<Account> GetConnectAccountDetailById(string connectAccountId)
        {
            try
            {
                StripeConfiguration.ApiKey = _options.StripeApiPrivateKey;
                var accountService = new AccountService();
                var accountUpdateOption = new AccountUpdateOptions();
                if (!string.IsNullOrEmpty(connectAccountId))
                {
                    return await accountService.GetAsync(connectAccountId);
                }
                return null;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "StripeApiManager=>>GetConnectAccountDetailById");
                return null;
            }
        }

        /// <summary>
        /// create connect account in stripe
        /// </summary>
        /// <param name="stripeConnectAccountModel"></param>
        /// <returns></returns>
        public Tuple<bool, string> CreateStripeConnectAccount(StripeConnectAccountModel stripeConnectAccountModel)
        {
            bool status = false;
            string message = "Something Went Wrong!";
            try
            {
                stripeConnectAccountModel.Currency = GetCurrencyByCountry(stripeConnectAccountModel.BankCountryName);
                StripeConfiguration.ApiKey = _options.StripeApiPrivateKey;
                string rountingNumber = stripeConnectAccountModel.BranchName + stripeConnectAccountModel.BankName;

                var accountCreateOptions = new AccountCreateOptions
                {
                    Type = EnumManager.GetEnumDescription<StripeAccountType>(stripeConnectAccountModel.ConnectAccountType),
                    Country = stripeConnectAccountModel.Country,
                    Email = stripeConnectAccountModel.Email,
                    Company = new AccountCompanyOptions()
                    {
                        Name = stripeConnectAccountModel.CompanyName,
                        Address = new AddressOptions()
                        {
                            Line1 = stripeConnectAccountModel.Address,
                            Country = stripeConnectAccountModel.Country,
                            State = stripeConnectAccountModel.State,
                            City = stripeConnectAccountModel.City,
                            PostalCode = stripeConnectAccountModel.Postal
                        },
                        Phone = stripeConnectAccountModel.CompanyPhone,
                        TaxId = stripeConnectAccountModel.BusinessNumber,
                    },
                    ExternalAccount = new AccountBankAccountOptions()
                    {
                        AccountHolderName = stripeConnectAccountModel.OwnerFirstName + " " + stripeConnectAccountModel.OwnerLastName,
                        AccountHolderType = EnumManager.GetEnumDescription<StripeConnectBusinessType>(stripeConnectAccountModel.BusinessType),
                        AccountNumber = stripeConnectAccountModel.AccountNumber,
                        Country = stripeConnectAccountModel.BankCountryName,
                        Currency = stripeConnectAccountModel.Currency,
                        RoutingNumber = rountingNumber
                    },
                    BusinessType = EnumManager.GetEnumDescription<StripeConnectBusinessType>(stripeConnectAccountModel.BusinessType),
                    Capabilities = new AccountCapabilitiesOptions
                    {
                        CardPayments = new AccountCapabilitiesCardPaymentsOptions
                        {
                            Requested = true,
                        },
                        Transfers = new AccountCapabilitiesTransfersOptions
                        {
                            Requested = true,
                        },
                    },
                    TosAcceptance = new AccountTosAcceptanceOptions
                    {
                        Date = DateTime.Now,
                        Ip = stripeConnectAccountModel.IPAddress, // provide request's IP address
                    }
                };
                var accountService = new AccountService();
                var response = accountService.Create(accountCreateOptions);

                if (response != null && !string.IsNullOrEmpty(response.Id))
                {
                    long? nullDob = null;
                    //create person with respect to connect account.
                    StripePersonRequestModel stripePersonRequestModel = new StripePersonRequestModel()
                    {
                        ConnectAccountId = response.Id,
                        FirstName = stripeConnectAccountModel.OwnerFirstName,
                        LastName = stripeConnectAccountModel.OwnerLastName,
                        DobDay = stripeConnectAccountModel.OwnerDOB != null ? stripeConnectAccountModel.OwnerDOB.Value.Day : nullDob,
                        DobMonth = stripeConnectAccountModel.OwnerDOB != null ? stripeConnectAccountModel.OwnerDOB.Value.Month : nullDob,
                        DobYear = stripeConnectAccountModel.OwnerDOB != null ? stripeConnectAccountModel.OwnerDOB.Value.Year : nullDob,
                        Email = stripeConnectAccountModel.Email,
                        Address1 = stripeConnectAccountModel.Address,
                        Country = stripeConnectAccountModel.Country,
                        State = stripeConnectAccountModel.State,
                        City = stripeConnectAccountModel.City,
                        PostalCode = stripeConnectAccountModel.Postal,
                        Phone = stripeConnectAccountModel.Phone,
                        IsRepresentative = true,
                        RelationshipTitle = "Owner"
                    };

                    var personResponse = CreateConnectAccountPerson(stripePersonRequestModel);
                    if (personResponse != null && !string.IsNullOrEmpty(personResponse.Id))
                    {
                        status = true;
                        message = "Connect Account Created Successfully!";

                        //save connect account response in payment integration table
                        stripeConnectAccountModel.StringConnectAccountId = response.Id;
                        stripeConnectAccountModel.PersonId = personResponse.Id;
                        stripeConnectAccountModel.StripeCreatedDate = response.Created;
                        stripeConnectAccountModel.IsChargeEnabled = response.ChargesEnabled;
                        stripeConnectAccountModel.IsPayoutEnabled = response.PayoutsEnabled;
                        stripeConnectAccountModel.VerificationStatus = personResponse.Verification.Status;
                        stripeConnectAccountModel.JsonRequest = JsonConvert.SerializeObject(accountCreateOptions);
                        stripeConnectAccountModel.JsonResponse = JsonConvert.SerializeObject(response);
                        stripeConnectAccountModel.PersonJsonRequest = JsonConvert.SerializeObject(stripePersonRequestModel);
                        stripeConnectAccountModel.PersonJsonResponse = JsonConvert.SerializeObject(personResponse);
                    }
                }
            }
            catch (Exception ex)
            {
                status = false;
                message = ex.Message;
                Log.Error(ex, "StripeApiManager=>>CreateStripeConnectAccount");
            }
            return Tuple.Create(status, message);
        }

        /// <summary>
        /// update stripe connect account info
        /// </summary>
        /// <param name="id"></param>
        /// <param name="accountUpdateOptions"></param>
        /// <returns></returns>
        public Tuple<bool, string> UpdateStripeConnectAccountInfo(StripeConnectAccountModel stripeConnectAccountModel)
        {
            bool status = false;
            string message = "Something Went Wrong!";
            try
            {
                StripeConfiguration.ApiKey = _options.StripeApiPrivateKey;
                var accountService = new AccountService();
                var accountUpdateOption = new AccountUpdateOptions();
                if (!string.IsNullOrEmpty(stripeConnectAccountModel.StringConnectAccountId))
                {
                    var connectAccountDetail = accountService.Get(stripeConnectAccountModel.StringConnectAccountId);
                    if (connectAccountDetail != null)
                    {
                        bool isUpdateRequired = false;
                        #region compare the field and update it accordingly
                        if (connectAccountDetail.Email != stripeConnectAccountModel.Email)
                        {
                            isUpdateRequired = true;
                            accountUpdateOption.Email = stripeConnectAccountModel.Email;
                        }
                        if (connectAccountDetail.ExternalAccounts != null && connectAccountDetail.ExternalAccounts.Data != null && connectAccountDetail.ExternalAccounts.Data.Count > 0)
                        {
                            string accountHolderName = stripeConnectAccountModel.OwnerFirstName + " " + stripeConnectAccountModel.OwnerLastName;
                            string routingNumber = stripeConnectAccountModel.BranchName + stripeConnectAccountModel.BankName;
                            //var serializeBankInfo = JsonConvert.SerializeObject(connectAccountDetail.ExternalAccounts.Data[0]);
                            //var bankInfo = JsonConvert.DeserializeObject<AccountBankAccountOptions>(serializeBankInfo);
                            var bankInfo = JsonConvert.DeserializeObject<ConnectAccountResponseModel>(JsonConvert.SerializeObject(connectAccountDetail));
                            var connectAccountJsonResponse = JsonConvert.DeserializeObject<ConnectAccountResponseModel>(stripeConnectAccountModel.JsonResponse);

                            if (bankInfo != null && bankInfo.ExternalAccounts!=null && bankInfo.ExternalAccounts.BankDetails!=null && bankInfo.ExternalAccounts.BankDetails.Length>0
                                || connectAccountJsonResponse != null && connectAccountJsonResponse.ExternalAccounts != null && connectAccountJsonResponse.ExternalAccounts.BankDetails != null
                                && connectAccountJsonResponse.ExternalAccounts.BankDetails.Length>0)
                            {
                                var bankDetails = bankInfo.ExternalAccounts.BankDetails[0];
                                var connectAccountBankDetail = connectAccountJsonResponse.ExternalAccounts.BankDetails[0];
                                if (bankDetails != null && connectAccountBankDetail != null)
                                {
                                    if (connectAccountBankDetail.Last4Digit != bankDetails.Last4Digit)
                                    {
                                        isUpdateRequired = true;
                                        accountUpdateOption.ExternalAccount = new AccountBankAccountOptions()
                                        {
                                            AccountHolderName = accountHolderName,
                                            AccountNumber =stripeConnectAccountModel.AccountNumber,
                                            Country = (stripeConnectAccountModel.BankCountryName != bankDetails.Country) ? stripeConnectAccountModel.BankCountryName : bankDetails.Country,
                                            Currency = (stripeConnectAccountModel.BankCountryName != bankDetails.Country) ? stripeConnectAccountModel.Currency :bankDetails.Currency,
                                            RoutingNumber =(routingNumber!= bankDetails.RoutingNumber)?routingNumber:bankDetails.RoutingNumber,
                                            AccountHolderType = bankDetails.AccountHolderType,
                                        };
                                    }
                                }
                            }
                        }
                        if (connectAccountDetail.Company != null)
                        {
                            if (connectAccountDetail.Company.Name != stripeConnectAccountModel.CompanyName
                               || !connectAccountDetail.Company.TaxIdProvided
                               || connectAccountDetail.Company.Phone != stripeConnectAccountModel.CompanyPhone)
                            {
                                isUpdateRequired = true;
                                accountUpdateOption.Company = new AccountCompanyOptions()
                                {
                                    Name = connectAccountDetail.Company.Name != stripeConnectAccountModel.CompanyName ? stripeConnectAccountModel.CompanyName : connectAccountDetail.Company.Name,
                                    TaxId = connectAccountDetail.Company.TaxIdRegistrar != stripeConnectAccountModel.BusinessNumber ? stripeConnectAccountModel.BusinessNumber : connectAccountDetail.Company.TaxIdRegistrar,
                                    Phone = connectAccountDetail.Company.Phone != stripeConnectAccountModel.CompanyPhone ? stripeConnectAccountModel.Phone : connectAccountDetail.Company.Phone,
                                };
                            }

                            if (connectAccountDetail.Company.Address != null)
                            {
                                if (connectAccountDetail.Company.Address.Line1 != stripeConnectAccountModel.Address
                                   || connectAccountDetail.Company.Address.Country != stripeConnectAccountModel.Country
                                   || connectAccountDetail.Company.Address.State != stripeConnectAccountModel.State
                                   || connectAccountDetail.Company.Address.City != stripeConnectAccountModel.City
                                   || connectAccountDetail.Company.Address.PostalCode != stripeConnectAccountModel.Postal)
                                {
                                    isUpdateRequired = true;
                                    accountUpdateOption.Company = accountUpdateOption.Company == null ? new AccountCompanyOptions() : accountUpdateOption.Company;
                                    accountUpdateOption.Company.Address = new AddressOptions()
                                    {
                                        Line1 = connectAccountDetail.Company.Address.Line1 != stripeConnectAccountModel.Address ? stripeConnectAccountModel.Address : connectAccountDetail.Company.Address.Line1,
                                        Country = connectAccountDetail.Company.Address.Country != stripeConnectAccountModel.Country ? stripeConnectAccountModel.Country : connectAccountDetail.Company.Address.Country,
                                        State = connectAccountDetail.Company.Address.State != stripeConnectAccountModel.State ? stripeConnectAccountModel.State : connectAccountDetail.Company.Address.State,
                                        City = connectAccountDetail.Company.Address.City != stripeConnectAccountModel.City ? stripeConnectAccountModel.City : connectAccountDetail.Company.Address.City,
                                        PostalCode = connectAccountDetail.Company.Address.PostalCode != stripeConnectAccountModel.Postal ? stripeConnectAccountModel.Postal : connectAccountDetail.Company.Address.PostalCode
                                    };
                                }
                            }
                            else
                            {
                                isUpdateRequired = true;
                                accountUpdateOption.Company = accountUpdateOption.Company == null ? new AccountCompanyOptions() : accountUpdateOption.Company;
                                accountUpdateOption.Company.Address = new AddressOptions()
                                {
                                    Line1 = stripeConnectAccountModel.Address,
                                    Country = stripeConnectAccountModel.Country,
                                    State = stripeConnectAccountModel.State,
                                    City = stripeConnectAccountModel.City,
                                    PostalCode = stripeConnectAccountModel.Postal
                                };
                            }
                        }
                        #endregion

                        if (isUpdateRequired)
                        {
                            var response = accountService.Update(stripeConnectAccountModel.StringConnectAccountId, accountUpdateOption);
                            stripeConnectAccountModel.JsonResponse = JsonConvert.SerializeObject(response);
                        }
                        status = true;
                        message = "Connect Account Info Updated Successfully!";
                    }
                    else
                    {
                        message = "Connect Account Does Not Exist!";
                    }
                }
                else
                {
                    message = "Connect Account Does Not Exist!";
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "StripeApiManager=>>UpdateStripeConnectAccountInfo");
            }
            return Tuple.Create(status, message);
        }

        /// <summary>
        /// create person for connect account
        /// </summary>
        /// <param name="stripePersonRequestModel"></param>
        /// <returns></returns>
        public Person CreateConnectAccountPerson(StripePersonRequestModel stripePersonRequestModel)
        {
            try
            {
                StripeConfiguration.ApiKey = _options.StripeApiPrivateKey;

                var personCreateOptions = new PersonCreateOptions
                {
                    FirstName = stripePersonRequestModel.FirstName,
                    LastName = stripePersonRequestModel.LastName,
                    Dob = new DobOptions()
                    {
                        Day = stripePersonRequestModel.DobDay,
                        Month = stripePersonRequestModel.DobMonth,
                        Year = stripePersonRequestModel.DobYear
                    },
                    Email = stripePersonRequestModel.Email,
                    Address = new AddressOptions()
                    {
                        Line1 = stripePersonRequestModel.Address1,
                        Country = stripePersonRequestModel.Country,
                        State = stripePersonRequestModel.State,
                        City = stripePersonRequestModel.City,
                        PostalCode = stripePersonRequestModel.PostalCode
                    },
                    Phone = stripePersonRequestModel.Phone,
                    Relationship = new PersonRelationshipOptions()
                    {
                        Representative = true,
                        Title = "Owner"
                    }
                };
                var personService = new PersonService();
                var personResponse = personService.Create(stripePersonRequestModel.ConnectAccountId, personCreateOptions);
                return personResponse;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "StripeApiManager=>>CreateConnectAccountPerson");
            }
            return null;
        }

        /// <summary>
        /// update person detail
        /// </summary>
        /// <returns></returns>
        public Tuple<bool, string> UpdatePersonDetail(StripePersonRequestModel stripePersonRequestModel)
        {
            bool status = false;
            string message = "Something Went Wrong!";
            try
            {
                StripeConfiguration.ApiKey = _options.StripeApiPrivateKey;
                var personService = new PersonService();
                var personUpdateOption = new PersonUpdateOptions();
                if (!string.IsNullOrEmpty(stripePersonRequestModel.PersonId))
                {
                    bool isUpdationRequired = false;
                    var personDetail = personService.Get(stripePersonRequestModel.ConnectAccountId, stripePersonRequestModel.PersonId);
                    if (personDetail != null)
                    {
                        if (personDetail.FirstName != stripePersonRequestModel.FirstName)
                        {
                            isUpdationRequired = true;
                            personUpdateOption.FirstName = stripePersonRequestModel.FirstName;
                        }
                        if (personDetail.LastName != stripePersonRequestModel.LastName)
                        {
                            isUpdationRequired = true;
                            personUpdateOption.LastName = stripePersonRequestModel.LastName;
                        }
                        if (personDetail.Email != stripePersonRequestModel.Email)
                        {
                            isUpdationRequired = true;
                            personUpdateOption.Email = stripePersonRequestModel.Email;
                        }
                        if (personDetail.Phone != stripePersonRequestModel.Phone)
                        {
                            isUpdationRequired = true;
                            personUpdateOption.Phone = stripePersonRequestModel.Phone;
                        }

                        if (personDetail.Dob != null)
                        {
                            if (personDetail.Dob.Day != stripePersonRequestModel.DobDay
                                || personDetail.Dob.Month != stripePersonRequestModel.DobMonth
                                || personDetail.Dob.Year != stripePersonRequestModel.DobYear)
                            {
                                isUpdationRequired = true;
                                personUpdateOption.Dob = new DobOptions()
                                {
                                   Day=personDetail.Dob.Day != stripePersonRequestModel.DobDay?stripePersonRequestModel.DobDay:personDetail.Dob.Day,
                                   Month = personDetail.Dob.Month != stripePersonRequestModel.DobMonth?stripePersonRequestModel.DobMonth:personDetail.Dob.Month,
                                   Year = personDetail.Dob.Year != stripePersonRequestModel.DobYear?stripePersonRequestModel.DobYear:personDetail.Dob.Year
                                };
                            }
                        }
                        else
                        {
                            isUpdationRequired = true;
                            personUpdateOption.Dob = new DobOptions()
                            {
                                Day = stripePersonRequestModel.DobDay,
                                Month = stripePersonRequestModel.DobMonth,
                                Year = stripePersonRequestModel.DobYear
                            };
                        }

                        if (personDetail.Address != null)
                        {
                            if (personDetail.Address.Line1 != stripePersonRequestModel.Address1
                                || personDetail.Address.Country != stripePersonRequestModel.Country
                                || personDetail.Address.State != stripePersonRequestModel.State
                                || personDetail.Address.City != stripePersonRequestModel.City
                                || personDetail.Address.PostalCode != stripePersonRequestModel.PostalCode)
                            {
                                isUpdationRequired = true;
                                personUpdateOption.Address=new AddressOptions()
                                {
                                    Line1 = personDetail.Address.Line1 != stripePersonRequestModel.Address1?stripePersonRequestModel.Address1:personDetail.Address.Line1,
                                    Country = personDetail.Address.Country != stripePersonRequestModel.Country?stripePersonRequestModel.Country:personDetail.Address.Country,
                                    State = personDetail.Address.State != stripePersonRequestModel.State?stripePersonRequestModel.State:personDetail.Address.State,
                                    City = personDetail.Address.City != stripePersonRequestModel.City?stripePersonRequestModel.City:personDetail.Address.City,
                                    PostalCode= personDetail.Address.PostalCode != stripePersonRequestModel.PostalCode? stripePersonRequestModel.PostalCode:personDetail.Address.PostalCode
                                };
                            }
                        }
                        else
                        {
                            isUpdationRequired = true;
                            personUpdateOption.Address = new AddressOptions()
                            {
                                Line1 = stripePersonRequestModel.Address1,
                                Country = stripePersonRequestModel.Country,
                                State = stripePersonRequestModel.State,
                                City = stripePersonRequestModel.City,
                                PostalCode = stripePersonRequestModel.PostalCode
                            };
                        }

                        if (isUpdationRequired)
                        {
                            var personResponse = personService.Update(stripePersonRequestModel.ConnectAccountId, stripePersonRequestModel.PersonId, personUpdateOption);
                            if (personResponse != null)
                            {
                                stripePersonRequestModel.VerificationStatus =personResponse.Verification!=null?personResponse.Verification.Status:"";
                                stripePersonRequestModel.JsonResponse = JsonConvert.SerializeObject(personResponse);
                            }
                        }

                        status = true;
                        message = "Person Detail Updated Successfully!";
                    }
                    else
                    {
                        message = "Person Does Not Exist!";
                    }
                }
                else
                {
                    message = "Person Does Not Exist!";
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "StripeApiManager=>>UpdatePersonDetail");
            }
            return Tuple.Create(status, message);
        }

        /// <summary>
        /// delete stripe connect account
        /// </summary>
        /// <param name="connectAccountId"></param>
        /// <returns></returns>
        public Tuple<bool, string> DeleteStripeConnectAccount(string connectAccountId)
        {
            bool status = false;
            string message = "Something Went Wrong!";
            try
            {
                StripeConfiguration.ApiKey = _options.StripeApiPrivateKey;

                var service = new AccountService();
                service.Delete(connectAccountId);
                status = true;
                message = "Connect Account Deleted Successfully!";
            }
            catch (Exception ex)
            {
                status = false;
                message = ex.Message;
                Log.Error(ex, "StripeApiManager=>>DeleteStripeConnectAccount");
            }
            return Tuple.Create(status, message);
        }
        #endregion

        #region stripe file upload
        /// <summary>
        /// upload multiple file by stripe
        /// </summary>
        /// <param name="stripeConnectAccountDocumentViewModel"></param>
        /// <returns></returns>
        public Tuple<bool, string> StripeFileMultipleUpload(StripeConnectAccountDocumentViewModel stripeConnectAccountDocumentViewModel)
        {
            bool status = false;
            string message = "Something Went Wrong!";
            try
            {
                if (stripeConnectAccountDocumentViewModel != null && stripeConnectAccountDocumentViewModel.lstDocument != null && stripeConnectAccountDocumentViewModel.lstDocument.Count > 0)
                {
                    StripeConfiguration.ApiKey = _options.StripeApiPrivateKey;

                    foreach (var attachment in stripeConnectAccountDocumentViewModel.lstDocument)
                    {
                        var fileCreateOptions = new FileCreateOptions
                        {
                            File = new MemoryStream(attachment.DocumentBytes),
                            Purpose = EnumManager.GetEnumDescription<FileUploadingPurpose>(attachment.DocumentPurpose),
                        };
                        var fileService = new FileService();
                        var upload = fileService.Create(fileCreateOptions);
                        if (upload != null && !string.IsNullOrEmpty(upload.Id))
                        {
                            attachment.StripeFileId = upload.Id;
                            attachment.JsonRequest = JsonConvert.SerializeObject(attachment);
                            if (attachment.IsBackSide)
                            {
                                var options = new PersonUpdateOptions
                                {
                                    Verification = new PersonVerificationOptions
                                    {
                                        Document = new PersonVerificationDocumentOptions
                                        {
                                            Back = upload.Id,
                                        },
                                    },
                                };
                                var service = new PersonService();
                                var person = service.Update(stripeConnectAccountDocumentViewModel.StripeConnectAccountId, stripeConnectAccountDocumentViewModel.StripePersonId, options);
                                attachment.JsonResponse = JsonConvert.SerializeObject(person);
                            }
                            else if (attachment.IsFrontSide)
                            {
                                var options = new PersonUpdateOptions
                                {
                                    Verification = new PersonVerificationOptions
                                    {
                                        Document = new PersonVerificationDocumentOptions
                                        {
                                            Front = upload.Id,
                                        },
                                    },
                                };
                                var service = new PersonService();
                                var person = service.Update(stripeConnectAccountDocumentViewModel.StripeConnectAccountId, stripeConnectAccountDocumentViewModel.StripePersonId, options);
                                attachment.JsonResponse = JsonConvert.SerializeObject(person);
                            }
                            else
                            {
                                var options = new PersonUpdateOptions
                                {
                                    Verification = new PersonVerificationOptions
                                    {
                                        AdditionalDocument = new PersonVerificationAdditionalDocumentOptions
                                        {
                                            Front = upload.Id,
                                        },
                                    },
                                };
                                var service = new PersonService();
                                var person = service.Update(stripeConnectAccountDocumentViewModel.StripeConnectAccountId, stripeConnectAccountDocumentViewModel.StripePersonId, options);
                                attachment.JsonResponse = JsonConvert.SerializeObject(person);
                            }
                        }
                    }
                    status = true;
                    message = "Connect Account Documents Uploaded Save Successfully!";
                }
            }
            catch (Exception ex)
            {
                status = false;
                message = "Something Went Wrong!";
                Log.Error(ex, "StripeApiManager=>>StripeFileUpload");
            }
            return Tuple.Create(status, message);
        }

        /// <summary>
        /// upload file in stripe
        /// </summary>
        /// <param name="stripeConnectAccountDocumentModel"></param>
        /// <returns></returns>
        public Tuple<bool, string> StripeFileUpload(StripeConnectAccountDocumentModel stripeConnectAccountDocumentModel)
        {
            bool status = false;
            string message = "Something Went Wrong!";
            try
            {
                StripeConfiguration.ApiKey = _options.StripeApiPrivateKey;
                var fileCreateOptions = new FileCreateOptions
                {
                    File = new MemoryStream(stripeConnectAccountDocumentModel.DocumentBytes),
                    Purpose = EnumManager.GetEnumDescription<FileUploadingPurpose>(stripeConnectAccountDocumentModel.DocumentPurpose),
                };
                var fileService = new FileService();
                var upload = fileService.Create(fileCreateOptions);
                if (upload != null && !string.IsNullOrEmpty(upload.Id))
                {
                    stripeConnectAccountDocumentModel.StripeFileId = upload.Id;
                    stripeConnectAccountDocumentModel.JsonRequest = JsonConvert.SerializeObject(stripeConnectAccountDocumentModel);
                    if (stripeConnectAccountDocumentModel.IsBackSide)
                    {
                        var options = new PersonUpdateOptions
                        {
                            Verification = new PersonVerificationOptions
                            {
                                Document = new PersonVerificationDocumentOptions
                                {
                                    Back = upload.Id,
                                },
                            },
                        };
                        var service = new PersonService();
                        var person = service.Update(stripeConnectAccountDocumentModel.StripeConnectAccountId, stripeConnectAccountDocumentModel.StripePersonId, options);
                        stripeConnectAccountDocumentModel.JsonResponse = JsonConvert.SerializeObject(person);
                    }
                    else if (stripeConnectAccountDocumentModel.IsFrontSide)
                    {
                        var options = new PersonUpdateOptions
                        {
                            Verification = new PersonVerificationOptions
                            {
                                Document = new PersonVerificationDocumentOptions
                                {
                                    Front = upload.Id,
                                },
                            },
                        };
                        var service = new PersonService();
                        var person = service.Update(stripeConnectAccountDocumentModel.StripeConnectAccountId, stripeConnectAccountDocumentModel.StripePersonId, options);
                        stripeConnectAccountDocumentModel.JsonResponse = JsonConvert.SerializeObject(person);
                    }
                    else
                    {
                        var options = new PersonUpdateOptions
                        {
                            Verification = new PersonVerificationOptions
                            {
                                AdditionalDocument = new PersonVerificationAdditionalDocumentOptions
                                {
                                    Front = upload.Id,
                                },
                            },
                        };
                        var service = new PersonService();
                        var person = service.Update(stripeConnectAccountDocumentModel.StripeConnectAccountId, stripeConnectAccountDocumentModel.StripePersonId, options);
                        stripeConnectAccountDocumentModel.JsonResponse = JsonConvert.SerializeObject(person);
                    }
                }
                status = true;
                message = "Connect Account Documents Mapping Save Successfully!";
            }
            catch (Exception ex)
            {
                status = false;
                message = "Something Went Wrong!";
                Log.Error(ex, "StripeApiManager=>>StripeFileUpload");
            }
            return Tuple.Create(status, message);
        }
        #endregion

        #region stripe customer account
        /// <summary>
        /// create and update stripe customer
        /// </summary>
        /// <param name="stripeCustomerModel"></param>
        /// <returns></returns>
        public Tuple<bool, string> CreateStripeCustomer(StripeCustomerModel stripeCustomerModel)
        {
            bool status = false;
            string message = "Something Went Wrong!";
            try
            {
                StripeConfiguration.ApiKey = _options.StripeApiPrivateKey;
                var customerService = new CustomerService();
                var newCustomer = new CustomerCreateOptions()
                {
                    Name = stripeCustomerModel.UserName,
                    Email = stripeCustomerModel.UserEmail,
                    Description = string.IsNullOrEmpty(stripeCustomerModel.Description) ? (stripeCustomerModel.UserName + " (" + stripeCustomerModel.UserEmail + ")") : stripeCustomerModel.Description,
                    Phone = stripeCustomerModel.PhoneNumber,
                    Address = new AddressOptions()
                    {
                        Line1 = stripeCustomerModel.Address1,
                        Line2 = stripeCustomerModel.Address2,
                        City = stripeCustomerModel.City,
                        State = stripeCustomerModel.State,
                        Country = stripeCustomerModel.Country,
                        PostalCode = stripeCustomerModel.PostalCode
                    }
                };

                Customer stripeCustomer = new Customer();
                stripeCustomer = customerService.Create(newCustomer);

                stripeCustomerModel.StripeCustomerId = stripeCustomer.Id;
                stripeCustomerModel.LiveMode = stripeCustomer.Livemode;
                stripeCustomerModel.StripeCreatedDateTime = stripeCustomer.Created;
                stripeCustomerModel.JsonResponse = JsonConvert.SerializeObject(stripeCustomer);
                status = true;
                message = "Stripe Customer Added Successfully!";
            }
            catch (Exception ex)
            {
                status = false;
                message = "Something Went Wrong!";
                Log.Error(ex, "StripeApiManager=>>CreateStripeCustomer");
            }
            return Tuple.Create(status, message);
        }

        /// <summary>
        /// update stripe customer info
        /// </summary>
        /// <param name="stripeCustomerModel"></param>
        /// <returns></returns>
        public Tuple<bool, string> UpdateStripeCustomer(StripeCustomerModel stripeCustomerModel)
        {
            bool status = false;
            string message = "Something Went Wrong!";
            try
            {
                StripeConfiguration.ApiKey = _options.StripeApiPrivateKey;
                var customerService = new CustomerService();

                Customer stripeCustomer = new Customer();

                if (string.IsNullOrEmpty(stripeCustomerModel.StripeCustomerId))
                {
                    status = false;
                    message = "Customer Not Found!";
                }
                else
                {
                    var customerUpdateOptions = new CustomerUpdateOptions
                    {
                        Name = stripeCustomerModel.UserName,
                        Email = stripeCustomerModel.UserEmail,
                        Phone = stripeCustomerModel.PhoneNumber,
                        Description = string.IsNullOrEmpty(stripeCustomerModel.Description) ? (stripeCustomerModel.UserName + " (" + stripeCustomerModel.UserEmail + ")") : stripeCustomerModel.Description,
                        Address = new AddressOptions()
                        {
                            Line1 = stripeCustomerModel.Address1,
                            Line2 = stripeCustomerModel.Address2,
                            City = stripeCustomerModel.City,
                            State = stripeCustomerModel.State,
                            Country = stripeCustomerModel.Country,
                            PostalCode = stripeCustomerModel.PostalCode
                        }
                    };
                    stripeCustomer = customerService.Update(stripeCustomerModel.StripeCustomerId, customerUpdateOptions);

                    stripeCustomerModel.StripeCustomerId = stripeCustomer.Id;
                    stripeCustomerModel.LiveMode = stripeCustomer.Livemode;
                    stripeCustomerModel.StripeCreatedDateTime = stripeCustomer.Created;
                    stripeCustomerModel.JsonResponse = JsonConvert.SerializeObject(stripeCustomer);

                    status = true;
                    message = "Stripe Customer Detail Updated Successfully!";
                }
            }
            catch (Exception ex)
            {
                status = false;
                message = "Something Went Wrong!";
                Log.Error(ex, "StripeApiManager=>>UpdateStripeCustomer");
            }
            return Tuple.Create(status, message);
        }

        /// <summary>
        /// create stripe token for customer card detail
        /// </summary>
        /// <param name="stripeCustomerCardDetailModel"></param>
        /// <returns></returns>
        public Tuple<bool, string> CreateStripeTokenForCustomerCardDetail(StripeCustomerCardDetailModel stripeCustomerCardDetailModel)
        {
            bool status = false;
            string message = "Something Went Wrong!";
            try
            {
                StripeConfiguration.ApiKey = _options.StripeApiPrivateKey;
                var tokenOption = new TokenCreateOptions
                {
                    Card = new TokenCardOptions
                    {
                        Number = stripeCustomerCardDetailModel.CardNumber,
                        ExpYear = stripeCustomerCardDetailModel.ExpirationYear,
                        ExpMonth = stripeCustomerCardDetailModel.ExpirationMonth,
                        Cvc = stripeCustomerCardDetailModel.Cvc
                    }
                };

                var service = new TokenService();
                var stripeToken = service.Create(tokenOption);
                stripeCustomerCardDetailModel.CardToken = stripeToken.Id;
                status = true;
                message = "Stripe Token Created Successfully!";
            }
            catch (System.Net.WebException webEx)
            {
                status = false;
                message = webEx.Message;
                Log.Error(webEx, "StripeApiManager=>>CreateStripeTokenForCustomerCardDetail");
            }
            catch (Exception ex)
            {
                status = false;
                message = ex.Message;
                Log.Error(ex, "StripeApiManager=>>CreateStripeTokenForCustomerCardDetail");
            }
            return Tuple.Create(status, message);
        }

        /// <summary>
        /// add credit card detail for stripe customer
        /// </summary>
        /// <param name="stripeCustomerCardDetailModel"></param>
        /// <returns></returns>
        public Tuple<bool, string> AddCardForStripeCustomer(StripeCustomerCardDetailModel stripeCustomerCardDetailModel)
        {
            bool status = false;
            string message = "Something Went Wrong!";
            try
            {
                StripeConfiguration.ApiKey = _options.StripeApiPrivateKey;
                CardService cardService = new CardService();
                Card stripeCard = new Card();

                if (!string.IsNullOrEmpty(stripeCustomerCardDetailModel.StripeCustomerId))
                {
                    var cardOption = new CardCreateOptions()
                    {
                        Source = stripeCustomerCardDetailModel.CardToken,
                    };
                    stripeCard = cardService.Create(stripeCustomerCardDetailModel.StripeCustomerId, cardOption);

                    #region save card detail in stripe card table
                    //masked the account number
                    stripeCustomerCardDetailModel.CardNumber = DataManager.MaskedNumber(stripeCustomerCardDetailModel.CardNumber);
                    stripeCustomerCardDetailModel.StripeCardId = stripeCard.Id;
                    stripeCustomerCardDetailModel.CardType = stripeCard.Brand;
                    stripeCustomerCardDetailModel.Last4 = stripeCard.Last4;
                    stripeCustomerCardDetailModel.CvcCheck = stripeCard.CvcCheck == "pass" ? true : false;
                    stripeCustomerCardDetailModel.Fingerprint = stripeCard.Fingerprint;
                    stripeCustomerCardDetailModel.JsonResponse = JsonConvert.SerializeObject(stripeCard);
                    stripeCustomerCardDetailModel.StripeCustomerId = stripeCustomerCardDetailModel.StripeCustomerId;

                    status = true;
                    message = "Stripe Customer Card Detail Added Successfully!";
                    #endregion
                }
                else
                {
                    status = false;
                    message = "Please First Create Customer!";
                }
            }
            catch (Exception ex)
            {
                status = false;
                message = "Something Went Wrong!";
                Log.Error(ex, "StripeApiManager=>>AddCardForStripeCustomer");
            }
            return Tuple.Create(status, message);
        }

        /// <summary>
        /// update card detail for existing stripe customer
        /// </summary>
        /// <param name="stripeCustomerCardDetailModel"></param>
        /// <returns></returns>
        public Tuple<bool, string> UpdateStripeCustomerCard(StripeCustomerCardDetailModel stripeCustomerCardDetailModel)
        {
            bool status = false;
            string message = "Something Went Wrong!";
            try
            {
                StripeConfiguration.ApiKey = _options.StripeApiPrivateKey;
                CardService cardService = new CardService();
                Card stripeCard = new Card();

                if (string.IsNullOrEmpty(stripeCustomerCardDetailModel.StripeCardId))
                {
                    message = "Please Add Card First!";
                }
                else
                {
                    var options = new CardUpdateOptions
                    {
                        Name = stripeCustomerCardDetailModel.UserName,
                        AddressLine1 = stripeCustomerCardDetailModel.Address1,
                        AddressLine2 = stripeCustomerCardDetailModel.Address2,
                        AddressCity = stripeCustomerCardDetailModel.City,
                        AddressState = stripeCustomerCardDetailModel.State,
                        AddressCountry = stripeCustomerCardDetailModel.Country,
                        AddressZip = stripeCustomerCardDetailModel.PostalCode,
                        ExpYear = stripeCustomerCardDetailModel.ExpirationYear,
                        ExpMonth = stripeCustomerCardDetailModel.ExpirationMonth,
                        Validate = true
                    };
                    stripeCard = cardService.Update(stripeCustomerCardDetailModel.StripeCustomerId, stripeCustomerCardDetailModel.StripeCardId, options);

                    #region save card detail in stripe card table
                    //masked the account number
                    stripeCustomerCardDetailModel.CardNumber = DataManager.MaskedNumber(stripeCustomerCardDetailModel.CardNumber);
                    stripeCustomerCardDetailModel.StripeCardId = stripeCard.Id;
                    stripeCustomerCardDetailModel.CardType = stripeCard.Brand;
                    stripeCustomerCardDetailModel.Last4 = stripeCard.Last4;
                    stripeCustomerCardDetailModel.CvcCheck = stripeCard.CvcCheck == "pass" ? true : false;
                    stripeCustomerCardDetailModel.Fingerprint = stripeCard.Fingerprint;
                    stripeCustomerCardDetailModel.JsonResponse = JsonConvert.SerializeObject(stripeCard);
                    stripeCustomerCardDetailModel.StripeCustomerId = stripeCustomerCardDetailModel.StripeCustomerId;

                    status = true;
                    message = "Stripe Customer Card Detail Updated Successfully!";
                    #endregion
                }
            }
            catch (Exception ex)
            {
                status = false;
                message = "Something Went Wrong!";
                Log.Error(ex, "StripeApiManager=>>UpdateStripeCustomerCard");
            }
            return Tuple.Create(status, message);
        }

        /// <summary>
        /// charge existing stripe customer
        /// </summary>
        /// <param name="stripeChargeModel"></param>
        /// <returns></returns>
        public Tuple<bool, string> ChargeExistingStripeCustomer(StripeChargeModel stripeChargeModel)
        {
            bool status = false;
            string message = "Something Went Wrong!";
            try
            {
                StripeConfiguration.ApiKey = _options.StripeApiPrivateKey;

                if (!string.IsNullOrEmpty(stripeChargeModel.StripeCardId))
                {
                    var chargeCreateOptions = new ChargeCreateOptions
                    {
                        Amount = stripeChargeModel.PaidAmountInCents,
                        Currency = stripeChargeModel.Currency.ToString(),
                        Customer = stripeChargeModel.StripeCustomerId,
                        Source = stripeChargeModel.StripeCardId,
                        Description = stripeChargeModel.Description,
                    };
                    var chargeService = new ChargeService();
                    var chargeResponse = chargeService.Create(chargeCreateOptions);
                    if (chargeResponse != null && chargeResponse.Paid)
                    {
                        stripeChargeModel.CustomerId = stripeChargeModel.CustomerId;
                        stripeChargeModel.StripeCardId = stripeChargeModel.StripeCardId;
                        stripeChargeModel.StripeChargeId = chargeResponse.Id;
                        stripeChargeModel.IsPaid = chargeResponse.Paid;
                        stripeChargeModel.FailureCode = chargeResponse.FailureCode;
                        stripeChargeModel.FailureMessage = chargeResponse.FailureMessage;
                        stripeChargeModel.TransactionId = chargeResponse.BalanceTransactionId;
                        stripeChargeModel.LiveMode = chargeResponse.Livemode;
                        stripeChargeModel.RequestDateTime = chargeResponse.Created;
                        #region payment status depend upon status
                        if (chargeResponse.Status == "succeeded")
                        {
                            stripeChargeModel.PaymentStatus = PaymentStatusType.Completed;
                        }
                        else if (chargeResponse.Status == "pending")
                        {
                            stripeChargeModel.PaymentStatus = PaymentStatusType.Pending;
                        }
                        else if (chargeResponse.Status == "failed")
                        {
                            stripeChargeModel.PaymentStatus = PaymentStatusType.Failed;
                        }
                        #endregion
                        stripeChargeModel.JsonResponse = JsonConvert.SerializeObject(chargeResponse);

                        status = true;
                        message = "Stripe Charges Done Successfully!";
                    }
                    else
                    {
                        message = chargeResponse.FailureMessage;
                    }
                }
                else
                {
                    status = false;
                    message = "Card Could Not Be Found!";
                }
            }
            catch (Exception ex)
            {
                status = false;
                message = ex.Message;
                Log.Error(ex, "StripeApiManager=>>ChargeExistingStripeCustomer");
            }
            return Tuple.Create(status, message);
        }

        /// <summary>
        /// destination charge existing stripe customer
        /// </summary>
        /// <param name="stripeChargeModel"></param>
        /// <returns></returns>
        public Tuple<bool, string> DestinationChargeExistingStripeCustomer(StripeChargeModel stripeChargeModel)
        {
            bool status = false;
            string message = "Something Went Wrong!";
            try
            {
                StripeConfiguration.ApiKey = _options.StripeApiPrivateKey;

                if (!string.IsNullOrEmpty(stripeChargeModel.StripeCardId))
                {
                    var paymentIntentService = new PaymentIntentService();
                    var paymentIntentCreateOptions = new PaymentIntentCreateOptions
                    {
                        Amount = stripeChargeModel.PaidAmountInCents,
                        Currency = stripeChargeModel.Currency.ToString(),
                        PaymentMethodTypes = new List<string> { "card" },
                        PaymentMethod = stripeChargeModel.StripeCardId,
                        Customer = stripeChargeModel.StripeCustomerId,
                        ApplicationFeeAmount = stripeChargeModel.ApplicationFeeAmount,
                        Confirm = true,
                        OnBehalfOf = stripeChargeModel.StripeConnectAccountId,
                        TransferData = new PaymentIntentTransferDataOptions
                        {
                            Destination = stripeChargeModel.StripeConnectAccountId,
                        },
                    };
                    var chargeResponse = paymentIntentService.Create(paymentIntentCreateOptions);
                    if (chargeResponse != null && string.IsNullOrEmpty(chargeResponse.CancellationReason))
                    {
                        stripeChargeModel.CustomerId = stripeChargeModel.CustomerId;
                        stripeChargeModel.StripeCardId = stripeChargeModel.StripeCardId;
                        stripeChargeModel.StripeChargeId = chargeResponse.Id;
                        stripeChargeModel.IsPaid = true;
                        stripeChargeModel.FailureMessage = chargeResponse.CancellationReason;
                        stripeChargeModel.TransactionId = chargeResponse.ClientSecret;
                        stripeChargeModel.LiveMode = chargeResponse.Livemode;
                        stripeChargeModel.RequestDateTime = chargeResponse.Created;
                        stripeChargeModel.JsonResponse = JsonConvert.SerializeObject(chargeResponse);
                        #region payment status depend upon status
                        if (chargeResponse.Status == "succeeded")
                        {
                            stripeChargeModel.PaymentStatus = PaymentStatusType.Completed;
                        }
                        else if (chargeResponse.Status == "pending")
                        {
                            stripeChargeModel.PaymentStatus = PaymentStatusType.Pending;
                        }
                        else if (chargeResponse.Status == "failed")
                        {
                            stripeChargeModel.PaymentStatus = PaymentStatusType.Failed;
                        }
                        #endregion

                        status = true;
                        message = "Destination Charge Done Successfully!";
                    }
                    else
                    {
                        message = chargeResponse.CancellationReason;
                    }
                }
                else
                {
                    status = false;
                    message = "Card Could Not Be Found!";
                }
            }
            catch (Exception ex)
            {
                status = false;
                message = ex.Message;
                Log.Error(ex, "StripeApiManager=>>DestinationChargeExistingStripeCustomer");
            }
            return Tuple.Create(status, message);
        }

        /// <summary>
        /// refund the provided amount
        /// </summary>
        /// <param name="stripeRefundModel"></param>
        /// <returns></returns>
        public Tuple<bool, string> RefundExistingStripeCustomer(StripeRefundModel stripeRefundModel)
        {
            bool status = false;
            string message = "Something Went Wrong!";
            try
            {
                StripeConfiguration.ApiKey = _options.StripeApiPrivateKey;
                if (stripeRefundModel.ChargeId != 0 && !string.IsNullOrEmpty(stripeRefundModel.StripeChargeId))
                {
                    var refundCreateOptions = new RefundCreateOptions
                    {
                        Amount = stripeRefundModel.AmountInCents,
                        Charge = stripeRefundModel.StripeChargeId,
                    };

                    var refundService = new RefundService();
                    var refundResponse = refundService.Create(refundCreateOptions);
                    if (refundResponse != null && !string.IsNullOrEmpty(refundResponse.Status) && refundResponse.Status.ToLower() == "succeeded")
                    {
                        stripeRefundModel.StripeRefundId = refundResponse.Id;
                        stripeRefundModel.RequestDateTime = refundResponse.Created;
                        stripeRefundModel.JsonResponse = JsonConvert.SerializeObject(refundResponse);
                        status = true;
                        message = "Amount Refunded Successfully!";
                    }
                    else
                    {
                        status = true;
                        message = "Something Went Wrong!";
                    }
                }
                else
                {
                    status = false;
                    message = "Please First Perform Some Charge!";
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
                Log.Error(ex, "StripeApiManager=>>ChargeExistingStripeCustomer");
            }
            return Tuple.Create(status, message);
        }

        /// <summary>
        /// check whether charge is successful or not
        /// </summary>
        /// <param name="chargeId"></param>
        /// <returns></returns>
        public bool IsStripeChargeSucceeded(string chargeId)
        {
            bool isChargeDone = false;
            try
            {
                StripeConfiguration.ApiKey = _options.StripeApiPrivateKey;
                var chargeService = new ChargeService();
                var stripeCharge = chargeService.Get(chargeId);
                if (stripeCharge != null && stripeCharge.Status != null && stripeCharge.Status.ToLower() == "succeeded")
                {
                    isChargeDone = true;
                }
            }
            catch (Exception ex)
            {
                isChargeDone = false;
                Log.Error(ex, "StripeApiManager=>>IsStripeChargeSucceeded");
            }
            return isChargeDone;
        }
        #endregion

        #region stripe webhook
        /// <summary>
        /// create webhook endpoint
        /// </summary>
        /// <param name="endpointUrl"></param>
        /// <param name="connectAccountId"></param>
        public WebhookEndpoint CreateWebhookEndpoint(string endpointUrl, string connectAccountId)
        {
            try
            {
                StripeConfiguration.ApiKey = _options.StripeApiPrivateKey;
                var options = new WebhookEndpointCreateOptions
                {
                    Url = endpointUrl,
                    EnabledEvents = new List<String>
                    {
                        "[’*’]"
                    },
                    Connect = true
                };
                var request = new RequestOptions();
                request.StripeAccount = connectAccountId;
                var service = new WebhookEndpointService();
                var endpoint = service.Create(options);
                return endpoint;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "StripeApiManager=>>CreateWebhookEndpoint");
            }
            return null;
        }

        #endregion
        #endregion

        #region common method
        public string GetCurrencyByCountry(string countryCode)
        {
            string currency = "";
            try
            {
                if (countryCode == CountryCode.IN.ToString())
                {
                    currency = CurrencyType.INR.ToString();
                }
                else if (countryCode == CountryCode.CA.ToString())
                {
                    currency = CurrencyType.CAD.ToString();
                }
                else if (countryCode == CountryCode.US.ToString())
                {
                    currency = CurrencyType.USD.ToString();
                }
                else
                {
                    currency = CurrencyType.GBP.ToString();
                }
            }
            catch(Exception ex)
            {
                Log.Error(ex, "StripeManager==>GetCurrencyByCountry");
            }
            return currency;
        }
        #endregion
    }
}
