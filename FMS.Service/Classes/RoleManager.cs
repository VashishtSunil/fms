﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using FMS.Core.DataModels;
using FMS.Core.Enums;
using FMS.Core.Models;
using FMS.Data.Context;
using Serilog;


namespace FMS.Services.Classes
{
    public class RoleManager
    {
        #region Role

        /// <summary>
        /// get user role detail by userId
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static RoleModel GetUserRoleByUserId(int userId)
        {
            var dc = new FMSContext();
            return (from user in dc.Users
                    join userRole in dc.UserInRole
                    on user.Id equals userRole.UserId
                    join role in dc.Roles
                    on userRole.RoleId equals role.Id //dc  on p.Id equals userRole.RoleId
                    where userRole.IsDeleted == false
                    && user.Id == userId
                    select new RoleModel
                    {
                        RoleName = role.RoleName,
                        Id = role.Id,
                        ParentId = user.RoleId,
                        OwnerId = role.OwnerId,
                    }).FirstOrDefault();

        }

        /// <summary>
        /// Get all Data of Role to bind with data-table 
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public static async Task<Tuple<List<RoleModel>, string, int>> GetDataTableList(jQueryDataTableParamModel param)
        {
            List<RoleModel> roleModels = new List<RoleModel>();
            var dc = new FMSContext();
            var sortColumnIndex = param.iSortCol_0;

            var qry = from role in dc.Roles
                      join User in dc.Users on role.Id equals User.RoleId
                      into roleList
                      where role.IsDeleted == false && role.IsSeeded != true
                      select new RoleModel
                      {
                          Id = role.Id,
                          RoleName = role.RoleName,
                          Users = roleList.Count() > 0 ? roleList.Where(x => x.IsDeleted != true).Count() : 0,
                          OwnerId = role.OwnerId,
                          IsActive = role.IsActive
                      };

            //Get total count
            var totalRecords = await qry.CountAsync();

            #region Searching 
            if (!string.IsNullOrEmpty(param.sSearch))
            {
                var toSearch = param.sSearch.ToLower();
                qry = qry.Where(c => c.RoleName.ToLower().Contains(toSearch)
                || c.Users.ToString().Contains(toSearch));
            }
            #endregion

            #region  Sorting 
            switch (sortColumnIndex)
            {
                //Sort by RoleName
                case 0:
                    switch (param.sSortDir_0)
                    {
                        case "desc":
                            qry = qry.OrderByDescending(a => a.RoleName);
                            break;
                        case "asc":
                            qry = qry.OrderBy(a => a.RoleName);
                            break;
                        default:
                            qry = qry.OrderBy(a => a.RoleName);
                            break;
                    }

                    break;

                case 1:
                    switch (param.sSortDir_0)
                    {
                        case "desc":
                            qry = qry.OrderByDescending(a => a.Users);
                            break;
                        case "asc":
                            qry = qry.OrderBy(a => a.Users);
                            break;
                        default:
                            qry = qry.OrderBy(a => a.Users);
                            break;
                    }

                    break;

                default:
                    qry = qry.OrderBy(a => a.RoleName);
                    break;
            }
            #endregion

            #region  Paging 
            if (param.iDisplayLength != -1)
                qry = qry.Skip(param.iDisplayStart).Take(param.iDisplayLength);
            #endregion

            return new Tuple<List<RoleModel>, string, int>(
               await
                    (from p in qry
                     select new RoleModel
                     {
                         Id = p.Id,
                         RoleName = p.RoleName,
                         Users = p.Users,
                         OwnerId = p.OwnerId,
                         IsActive = p.IsActive
                     }).ToListAsync(),
                param.sEcho,
                totalRecords);

        }

        /// <summary>
        /// get by selected id 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static RoleModel GetById(long id)
        {
            var dc = new FMSContext();
            return (from p in dc.Roles
                    where p.IsDeleted == false && p.Id == id
                    //Binding Data with Model
                    select new RoleModel
                    {
                        Id = p.Id,
                        RoleName = p.RoleName,
                        OwnerId = p.OwnerId,
                        IsActive = p.IsActive,
                        IsDeleted = p.IsDeleted,
                        CreatedOn = p.CreatedOn
                    }).FirstOrDefault();
        }

        /// <summary>
        /// Used to add Update/Add Role
        /// </summary>
        /// <param name="roleModel"></param>
        /// <returns></returns>
        public static bool Update(RoleModel roleModel)
        {
            var dc = new FMSContext();
            var roles = dc.Roles.FirstOrDefault(x => x.Id == roleModel.Id);
            try
            {
                // for add new record
                if (roles == null)
                {
                    roles = new Role
                    {
                        Id = roleModel.Id,
                        RoleName = roleModel.RoleName,
                        ParentId = roleModel.ParentId,
                        AccessLevel = roleModel.AccessLevel,
                        OwnerId = roleModel.OwnerId,
                        IsDeleted = roleModel.IsDeleted,
                        IsActive = roleModel.IsActive,
                        CreatedOn = DateTime.Now,
                        CreatedBy = roleModel.CreatedBy
                    };
                    dc.Roles.Add(roles);
                }
                // for update 
                else
                {
                    roles.Id = roleModel.Id;
                    roles.RoleName = roleModel.RoleName;
                    roles.ParentId = roleModel.ParentId;
                    roles.AccessLevel = roleModel.AccessLevel;
                    roles.OwnerId = roleModel.OwnerId;
                    //Base Entities            
                    roles.IsActive = roleModel.IsActive;
                    roles.IsDeleted = roleModel.IsDeleted;
                    roles.ModifiedOn = DateTime.Now;
                    roles.ModifiedBy = roleModel.ModifiedBy;
                }
                dc.SaveChanges();
                roleModel.Id = roles.Id;
                return true;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "RoleManager=>>Update");
                return false;
            }
        }

        /// <summary>
        /// Soft delete selected Role According to Id 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns>Boolean Result</returns>
        public static bool DeleteRole(long id)
        {
            var dc = new FMSContext();
            var roles = dc.Roles.FirstOrDefault(x => x.Id == id && x.IsDeleted == false);
            if (roles == null)
                return true;
            roles.IsDeleted = true;
            return dc.SaveChanges() > 0;
        }

        /// <summary>
        /// Update Role Status
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static bool UpdateRoleStatus(long id)
        {
            var dc = new FMSContext();
            var roles = dc.Roles.FirstOrDefault(x => x.Id == id && x.IsDeleted == false);
            if (roles == null)
                return true;

            roles.IsActive = !roles.IsActive;
            return dc.SaveChanges() > 0;
        }

        /// <summary>
        /// check is role is duplicate or not
        /// </summary>
        /// <param name="roleModel"></param>
        /// <returns></returns>
        public static bool IsRoleDuplicate(RoleModel roleModel)
        {
            var dc = new FMSContext();
            var roleDeatil = dc.Roles.FirstOrDefault(x => x.RoleName == roleModel.RoleName && x.IsDeleted != true);
            if (roleDeatil == null)
                return false;
            if (roleDeatil.Id != roleModel.Id)
                return true;
            return false;
        }

        /// <summary>
        /// Get User Roles
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static List<RoleModel> GetUserRoles(long userId)
        {
            var dc = new FMSContext();
            return (from user in dc.Users
                    join userRole in dc.UserInRole
                    on user.Id equals userRole.UserId
                    join role in dc.Roles on userRole.RoleId equals role.Id //dc  on p.Id equals userRole.RoleId
                    where userRole.IsDeleted == false &&
                    userRole.UserId == userId
                    && role.IsDeleted != true
                    && role.IsSeeded != true
                    select new RoleModel
                    {
                        RoleName = role.RoleName,
                        Id = role.Id,
                        ParentId = user.RoleId
                    }).ToList();
        }

        /// <summary>
        /// Role list for drop down
        /// </summary>
        /// <returns></returns>
        public static SelectList GetSelectList()
        {
            SelectList selectListItFMS = null;
            try
            {
                var dc = new FMSContext();
                var role = dc.Roles.Where(x => x.IsDeleted == false && x.RoleName != UserRole.SuperAdmin.ToString()).ToList();

                if (role != null && role.Count > 0)
                {
                    selectListItFMS = new SelectList(role, "Id", "RoleName");
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "RoleManager=>>GetSelectList");
            }
            return selectListItFMS;
        }

        /// <summary>
        /// Add User Role
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static bool AddUserRole(UserInRoleModel model)
        {
            bool status = false;
            try
            {
                var dc = new FMSContext();
                var userInRole = dc.UserInRole.FirstOrDefault(x => x.UserId == model.UserId && !x.IsDeleted);
                if (userInRole == null)
                {
                    userInRole = new UserInRole
                    {
                        UserId = model.UserId,
                        RoleId = model.RoleId,
                        IsDeleted = false,
                        CreatedBy = model.CreatedBy,
                        CreatedOn = DateTime.Now
                    };
                    dc.UserInRole.Add(userInRole);
                    status = dc.SaveChanges() > 0;
                }
                else
                {
                    userInRole.UserId = model.UserId;
                    userInRole.RoleId = model.RoleId;
                    userInRole.ModifiedOn = DateTime.Now;
                    userInRole.ModifiedBy = model.ModifiedBy;
                    status = dc.SaveChanges() > 0;
                }
            }
            catch (Exception ex)
            {
                status = false;
                Serilog.Log.Error(ex, "RoleManager=>>AddUserRole");
            }
            return status;
        }

        /// <summary>
        /// get Role by selected RoleName 
        /// </summary>
        /// <param name="RoleName"></param>
        /// <returns></returns>
        public static RoleModel GetRoleByName(string RoleName)
        {
            var dc = new FMSContext();
            return (from p in dc.Roles
                    where p.IsDeleted == false && p.RoleName == RoleName
                    select new RoleModel
                    {
                        Id = p.Id,
                        RoleName = p.RoleName,
                    }).FirstOrDefault();
        }

        /// <summary>
        /// get current user roles
        /// </summary>
        /// <param name="roles"></param>
        /// <returns></returns>
        public static List<RoleModel> GetUserRoles(int[] roles)
        {
            List<RoleModel> lstRole = new List<RoleModel>();
            try
            {
                var dc = new FMSContext();
                lstRole = (from role in dc.Roles
                           where roles.Contains(role.Id)
                           select new RoleModel
                           {
                               RoleName = role.RoleName,
                               Id = role.Id,
                               ParentId = role.ParentId,
                               AccessLevel = role.AccessLevel,
                               OwnerId = role.OwnerId,
                               CategoryId = role.CategoryId
                           }).ToList();
            }
            catch (Exception ex)
            {
                lstRole = null;
                Serilog.Log.Error(ex, "RoleManager=>>GetUserRoles");
            }
            return lstRole;
        }

        #endregion

        #region RoleFunction

        /// <summary>
        /// delete all Role Function
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public static Tuple<bool, string> DeleteRoleFunction(int roleId)
        {
            bool status = false;
            string message = "Something Went Wrong!";
            try
            {
                var dc = new FMSContext();
                var roleFunctionMapping = dc.RoleFunction.Where(x => x.RoleId == roleId).ToList();
                if (roleFunctionMapping != null && roleFunctionMapping.Count > 0)
                {
                    roleFunctionMapping.ForEach(p => dc.Entry(p).State = EntityState.Deleted);
                    dc.SaveChanges();
                }
                status = true;
                message = "Role Function Mapping Deleted Successfully!";
            }
            catch (Exception ex)
            {
                status = false;
                message = "Something Went Wrong!";
                Serilog.Log.Error(ex, "RoleManager=>>DeleteRoleFunction");
            }
            return Tuple.Create(status, message);
        }

        /// <summary>
        /// Update Role Function
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static bool UpdateRoleFunction(RoleFunctionModel model)
        {
            bool status = false;
            try
            {
                var dc = new FMSContext();
                var roleFunctions = dc.RoleFunction.FirstOrDefault(x => x.Id == model.Id && !x.IsDeleted);
                if (roleFunctions == null)
                {
                    RoleFunction roleFunction = new RoleFunction
                    {
                        RoleId = model.RoleId,
                        FunctionId = model.FunctionId,
                        ClaimId = model.ClaimId,
                        IsCreate = model.IsCreate,
                        IsEdit = model.IsEdit,
                        IsDelete = model.IsDelete,
                        IsView = model.IsView,
                        IsDeleted = false,
                        CreatedBy = model.CreatedBy,
                        CreatedOn = DateTime.Now
                    };
                    dc.RoleFunction.Add(roleFunction);
                    status = dc.SaveChanges() > 0;
                }
                else
                {
                    roleFunctions.RoleId = model.RoleId;
                    roleFunctions.FunctionId = model.FunctionId;
                    roleFunctions.ClaimId = model.ClaimId;
                    roleFunctions.IsCreate = model.IsCreate;
                    roleFunctions.IsEdit = model.IsEdit;
                    roleFunctions.IsDelete = model.IsDelete;
                    roleFunctions.IsView = model.IsView;
                    roleFunctions.ModifiedOn = DateTime.Now;
                    roleFunctions.ModifiedBy = model.ModifiedBy;
                    status = dc.SaveChanges() > 0;
                }
            }
            catch (Exception ex)
            {
                status = false;
                Serilog.Log.Error(ex, "RoleManager=>>UpdateRoleFunction");
            }
            return status;
        }

        #endregion
    }
}
