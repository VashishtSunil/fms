﻿
using FMS.Core.Models;
using FMS.Data.Context;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FMS.Services.Classes.RequestResponse
{
    public class RequestResponseManager
    {
        /// <summary>
        /// Used to add Update/Add Request Response
        /// </summary>
        /// <param name="requestResponseModel"></param>
        /// <returns>Bool Result</returns>
        public static bool UpdateRequestResponse(RequestResponseModel requestResponseModel)
        {
            var dc = new FMSContext();
            var requestResponse = dc.RequestResponses.FirstOrDefault(x => x.Id == requestResponseModel.Id);
            try
            {
                // for add new record
                if (requestResponse == null)
                {
                    requestResponse = new Core.DataModels.RequestResponse
                    {
                        Token = requestResponseModel.Token,
                        UniqId = requestResponseModel.UniqId,
                        Method = requestResponseModel.Method,
                        Request = requestResponseModel.Request,
                        RequestDate = DateTime.Now,
                        Response = requestResponseModel.Response,
                        ResponseCode = requestResponseModel.ResponseCode,
                        ResponseDate = DateTime.Now,
                        CreatedBy = ""
                    };
                    dc.RequestResponses.Add(requestResponse);
                    dc.SaveChanges();
                }
                // for update 
                else
                {

                    requestResponse.Response = requestResponseModel.Response;
                    requestResponse.ResponseDate = DateTime.Now;
                    requestResponse.ResponseCode = requestResponseModel.ResponseCode;
                    requestResponse.ModifiedBy ="";
                    requestResponse.ModifiedOn = DateTime.Now;
                    dc.SaveChanges();

                }
                requestResponseModel.Id = requestResponse.Id;
                return true;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "RequestResponseManager=>>UpdateRequestResponse");
                return false;
            }
        }

        /// <summary>
        /// get request response  by selected id 
        /// </summary>
        /// <param name="requestResponseId"></param>
        /// <returns>RequestResponseModel</returns>
        public static RequestResponseModel GetRequestResponseById(long requestResponseId)
        {
            var dc = new FMSContext();
            return (from p in dc.RequestResponses
                    where p.IsDeleted == false && p.Id == requestResponseId
                    //Binding Data with Model
                    select new RequestResponseModel
                    {
                        Token = p.Token,
                        UniqId = p.UniqId,
                        Method = p.Method,
                        Request = p.Request,
                        RequestDate = p.RequestDate,
                        Response = p.Response,
                        ResponseDate = p.ResponseDate,
                        ResponseCode = p.ResponseCode,

                        //Base Entities TODO : 
                        //Id = p.Id,
                        //CreatedOn = p.CreatedOn,
                        //CreatedBy = p.CreatedBy,
                        //ModifiedOn = p.ModifiedOn,
                        //ModifiedBy = p.ModifiedBy

                    }).FirstOrDefault();
        }

        /// <summary>
        /// Get request responseId
        /// </summary>
        /// <returns>List of RequestResponse</returns>
        public static List<RequestResponseModel> GetRequestResponses()
        {
            var dc = new FMSContext();
            return (from p in dc.RequestResponses
                    where p.IsDeleted == false
                    select new RequestResponseModel
                    {
                        Token = p.Token,
                        UniqId = p.UniqId,
                        Method = p.Method,
                        Request = p.Request,
                        RequestDate = p.RequestDate,
                        Response = p.Response,
                        ResponseDate = p.ResponseDate,
                        ResponseCode = p.ResponseCode,

                        //Base Entities TODO: 
                        //Id = p.Id,
                        //CreatedOn = p.CreatedOn,
                        //CreatedBy = p.CreatedBy,
                        //ModifiedOn = p.ModifiedOn,
                        //ModifiedBy = p.ModifiedBy
                    }).ToList();
        }

        /// <summary>
        /// Soft delete selected request response According to Id 
        /// </summary>
        /// <param name=" requestResponseId"></param>
        /// <returns>Bool Result</returns>
        public static bool DeleteRequestResponse(long requestResponseId)
        {
            var dc = new FMSContext();
            var requestResponse = dc.RequestResponses.FirstOrDefault(x => x.Id == requestResponseId && x.IsDeleted == false);
            if (requestResponse == null)
                return true;
            requestResponse.IsDeleted = true;
            return dc.SaveChanges() > 0;
        }


    }
}
