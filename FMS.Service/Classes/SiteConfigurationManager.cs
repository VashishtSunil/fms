﻿using FMS.Core.Enums;
using FMS.Core.Models;
using FMS.Core.Models.Stripe;
using FMS.Data.Context;
using Serilog;
using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace FMS.Services.Classes
{
    public class SiteConfigurationManager
    {
        /// <summary>
        /// get configuration by key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public async static Task<SiteConfigurationModel> GetConfigurationByKey(string key)
        {
            SiteConfigurationModel siteConfigurationModel = new SiteConfigurationModel();
            try
            {
                var dc = new FMSContext();
                siteConfigurationModel = await dc.SiteConfigurations.Where(u => u.Key == key && !u.IsDeleted).Select(x => new SiteConfigurationModel()
                {
                    Id = x.Id,
                    Key = x.Key,
                    Value = x.Value,
                    Description = x.Description
                }).FirstOrDefaultAsync();
            }
            catch (Exception ex)
            {
                siteConfigurationModel = null;
                Log.Error(ex, "SiteConfigurationManager=>>GetConfigurationByKey");
            }
            return siteConfigurationModel;
        }

        /// <summary>
        /// get stripe keys based on environment(dev or prod)
        /// </summary>
        /// <param name="isLiveMode"></param>
        /// <returns></returns>
        public async static Task<StripeKeyModel> GetStripeKeys()
        {
            StripeKeyModel stripeKeyModel = new StripeKeyModel();
            try
            {
                var dc = new FMSContext();
                var privatekey = await dc.SiteConfigurations.Where(u => u.Key == SiteConfigType.StripeApiPrivateKey.ToString() && !u.IsDeleted).FirstOrDefaultAsync();
                var publicKey = await dc.SiteConfigurations.Where(u => u.Key == SiteConfigType.StripeApiPublicKey.ToString() && !u.IsDeleted).FirstOrDefaultAsync();

                stripeKeyModel.StripeApiPrivateKey = privatekey != null ? privatekey.Value : "";
                stripeKeyModel.StripeApiPublicKey = publicKey != null ? publicKey.Value : "";
            }
            catch (Exception ex)
            {
                Log.Error(ex, "SiteConfigurationManager =>> GetStripeKeys");
            }
            return stripeKeyModel;
        }
    }
}
