﻿var NewsViewModel = new function () {
    var thisViewModel = this;

    //#region Role

    var tblNews = "tblNews";

    this.NewsListView = function () {
        var responsiveDt = undefined;
        var breakpointDefinition = {
            tablet: 1024,
            phone: 480
        };
        $('#' + tblNews).dataTable({
            "bStateSave": false,
            "iDisplayLength": 10,
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            //for CSV buttons
            "dom": "Blfrtip",
            buttons: [
                {
                    extend: 'csv',
                    text: 'Export',
                    filename: function () {
                        var d = new Date();
                        var n = d.getTime();
                        return 'News' + n;
                    }
                }],
            cache: true,
            "responsive": true,
            "bServerSide": true,
            "bProcessing": true,
            "bJqueryUI": true,
            "sPaginationType": "full_numbers",
            "aaSorting": [[0, "asc"]],
            "sAjaxSource": baseUrl + "/News/list",
            "preDrawCallback": function () {
                // Initialize the responsive datatables helper once.
                if (!responsiveDt) {
                    responsiveDt = new ResponsiveDatatablesHelper($('#' + tblNews), breakpointDefinition);
                }
            },
            "rowCallback": function (nRow, data) {

                responsiveDt.createExpandIcon(nRow);
            },
            "drawCallback": function () {
                responsiveDt.respond();
                $("a.edit").tooltip({
                    placement: 'top',
                    trigger: 'hover',
                    title: 'Edit News'
                });
                $("a.edit").removeAttr("title");
                $("a.delete").tooltip({
                    placement: 'top',
                    trigger: 'hover',
                    title: 'Delete News'
                });
                $("a.delete").removeAttr("title");
                //
                InitBootstrapSwitch();
            },
            "aoColumns": [
                { "mDataProp": "Title", "sClass": "center" },
                { "mDataProp": "Description", "sClass": "center" },
                {
                    "sClass": "center",
                    "mRender": function (data, type, full) {
                        if (full.PublishDateString !== null) {
                            return getDatePartFromDbUTCDate(full.PublishDateString);
                        }
                        else {
                            return null;
                        }
                    }
                },
                {
                    "orderable": false,
                    "mRender": function (data, type, full) {
                        var actionHtml = "";
                        if (full.IsActive) {
                            actionHtml = "<a  class='bootstrapSwitch-checkbox text-primary'  data-url='/news/changenewsstatus'  data-id='" + full.Id + "' ><i class='fa fa-toggle-on  fa-lg'></i></a>";
                        }
                        else {
                            actionHtml = "<a  class='bootstrapSwitch-checkbox text-primary'  data-url='/news/changenewsstatus'  data-id='" + full.Id + "' ><i class='fas fa-toggle-off  fa-lg'></i></a>";
                        }
                        return actionHtml;
                    }
                },
                {
                    "orderable": false,
                    "sWidth": "15%",
                    "mRender": function (data, type, full) {
                        var actionString = "";
                        actionString = actionString + "<a class=''  data-TagId=" + full.Id + "   data-name=" + full.Title + " onClick=NewsViewModel.updateNews('" + full.Id + "',true)><i class='far fa-eye text-primary fa-sm mr-2'></i></a>"
                        actionString = actionString + "<a class=''  data-TagId=" + full.Id + "   data-name=" + full.Title + " onClick=NewsViewModel.updateNews('" + full.Id + "',false)><i class='far fa-edit text-primary fa-sm mr-2'></i></a>"
                        actionString = actionString + "<a class='delete'  data-Id=" + full.Id + "   data-name=" + full.Title + " onClick=NewsViewModel.deleteNews(this)><i class='fas fa-trash-alt text-danger fa-sm'></i></a>"
                        return actionString;
                    }
                }
            ]
            ,
            columnDefs: [
                {
                    "defaultContent": " ",
                    "targets": "_all"
                }],
            "autoWidth": true
        });
    };

    // open update Role modal with data
    this.updateNews = function (newsId, isReadonly) {
        this.openNewsModal(newsId, isReadonly);
    };

    //open Role model
    this.openNewsModal = function (id, isReadonly) {
        GetData("/news/openmodal", openModalSuccess, {
            formId: "frmAddNewNews",
            id: id,
            isReadonly: isReadonly
        });
    };

    // open News success method
    this.updateNewsSuccess = function (success, message) {
        if (success === false) {
            failAlert(message);
        }
        else {
            successAlert(message);
            closeCommonModel();
            refreshDataTable(tblNews);
        }
    };

    // delete News
    this.deleteNews = function (e) {
        var data = $(e).data();
        swal({
            title: "Are you sure?",
            text: "Please confirm you wish to delete News " + data.name,
            icon: "warning",
            buttons: true,
            dangerMode: true
        })
            .then((willDelete) => {
                if (willDelete) {
                    PostData("/News/deletenews", this.deleteNewsSuccess, { id: data.id });
                }
            });
    };

    //delete Role success
    this.deleteNewsSuccess = function (result) {
        if (result) {
            SwalSuccess("Record deleted successfully!");
        }
        else {
            SwalError("Something went wrong!");
            refreshDataTable(tblNews);
        }
    };


    function getDatePartFromDbUTCDate(dateTimeAsString) {
        var date = new Date(dateTimeAsString);
        // if you want to get locale time, use
        return (date.toString().split(' ')[0]) + ", " + (date.toLocaleString('default', { month: 'short' })) + " " + date.getDate() + ", " + date.getFullYear();
    };

    this.validateBasicRuleDetailForm = function (element) {
        if (tinymce.get("txtNewsDescription").getContent() == "") {
            failAlert("Please enter contest rule.");
            return false;
        }
        if (element != null && element != undefined && element != "") {
            var id = $(element).attr("id");
            if (id != undefined && id != null && id != "") {
                var formName = $(element).attr("data-formId");
                if ($("#" + formName).valid()) {
                    // Save and on success show this 
                    //save Rule 
                    $("#btnfrmAddNewNewsSubmit").click();
                }
            }
        }
    }

    //status success
    this.statusSuccess = function (result) {
        if (result) {
            SwalSuccess("Status changed successfully!");
            refreshDataTable(tblNews);
        }
        else {
            SwalError("Something went wrong!");
            refreshDataTable(tblNews);
        }
    };

    this.beforeSubmit = function () {
        var tinymceEditorContent = tinymce.activeEditor.getContent();
        $("#txtNewsDescription").val(tinymceEditorContent);
        $("#btnFrmNewsSubmit").click();
    }
    //#endregion News
};