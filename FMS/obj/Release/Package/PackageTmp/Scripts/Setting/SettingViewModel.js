﻿var SettingViewModel = new function () {
    var thisViewModel = this;

    //#region Setting

    var tblSlider = "tblSlider";

    this.SettingListView = function () {
        var responsiveDt = undefined;
        var breakpointDefinition = {
            tablet: 1024,
            phone: 480
        };
        $('#' + tblSlider).dataTable({
            "bStateSave": false,
            "iDisplayLength": 10,
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            "dom": "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            cache: true,
            "responsive": true,
            "bServerSide": true,
            "bProcessing": true,
            "bJqueryUI": true,
            "sPaginationType": "full_numbers",
            "aaSorting": [[0, "asc"]],
            "sAjaxSource": baseUrl + "/setting/list",
            "preDrawCallback": function () {
                // Initialize the responsive datatables helper once.
                if (!responsiveDt) {
                    responsiveDt = new ResponsiveDatatablesHelper($('#' + tblSlider), breakpointDefinition);
                }
            },
            "rowCallback": function (nRow, data) {
                responsiveDt.createExpandIcon(nRow);
            },
            "drawCallback": function () {
                responsiveDt.respond();
                responsiveDt.respond();
                $("a.edit").tooltip({
                    placement: 'top',
                    trigger: 'hover',
                    title: 'Edit Slider'
                });
                $("a.edit").removeAttr("title");
                $("a.delete").tooltip({
                    placement: 'top',
                    trigger: 'hover',
                    title: 'Delete Slider'
                });
                $("a.delete").removeAttr("title");
                //
                InitBootstrapSwitch();
            },
            "aoColumns": [
                { "mDataProp": "Heading", "sClass": "center" },
                { "mDataProp": "SubHeading", "sClass": "center" },

                {
                    "orderable": false,
                    "mRender": function (data, type, full) {
                        var actionHtml = "";
                        if (full.IsActive) {
                            actionHtml = "<a  class='bootstrapSwitch-checkbox text-primary'  data-url='/setting/changeclientsliderstatus'  data-id='" + full.Id + "' ><i class='fa fa-toggle-on  fa-lg'></i></a>";
                        }
                        else {
                            actionHtml = "<a  class='bootstrapSwitch-checkbox text-primary'  data-url='/setting/changeclientsliderstatus'  data-id='" + full.Id + "' ><i class='fas fa-toggle-off  fa-lg'></i></a>";
                        }
                        return actionHtml;
                    }
                },
                {
                    "orderable": false,
                    "sWidth": "30%",
                    "mRender": function (data, type, full) {
                        var actionString = "";
                        actionString = actionString + "<a class=''  data-TagId=" + full.Id + "   data-name=" + full.Heading + " onClick=SettingViewModel.updateClientSlider('" + full.Id + "',true)><i class='far fa-eye text-primary fa-sm mr-2'></i></a>"
                        actionString = actionString + "<a class=''  data-TagId=" + full.Id + "   data-name=" + full.Heading + " onClick=SettingViewModel.updateClientSlider('" + full.Id + "',false)><i class='far fa-edit text-primary fa-sm mr-2'></i></a>"
                        actionString = actionString + "<a class='delete'  data-userId=" + full.Id + "   data-name=" + full.Heading + " onClick=SettingViewModel.deleteClientSlider(this)><i class='fas fa-trash-alt text-danger fa-sm'></i></a>"
                        return actionString;
                    }
                }
            ]
            ,
            columnDefs: [
                {
                    "defaultContent": " ",
                    "targets": "_all"
                }],
            "autoWidth": true
        });
    };

    // update Client Slider 
    this.updateClientSlider = function (clientSliderId, isReadonly) {
        this.openClientSliderModal(clientSliderId, isReadonly);
    };

    //open Client Slider Modal
    this.openClientSliderModal = function (id, isReadonly) {
        if ($("#frmAddNewBasicInfo").valid()) {
            var basicInfoLogo = $("#BasicInfoLogo").val();
            if (basicInfoLogo == undefined || basicInfoLogo == 0 || basicInfoLogo == "") {
                SwalError("Please first upload logo!");
            }
            else {
                GetData("/setting/openmodal", openModalSuccess, {
                    formId: "frmAddNewClientSlider",
                    id: id,
                    isReadonly: isReadonly
                });
            }
        }
    };

    // update Client Slider Success
    this.updateClientSliderSuccess = function (success, message) {
        if (success === false) {
            failAlert(message);
        }
        else {
            successAlert(message);
            closeCommonModel();
            refreshDataTable(tblSlider);
        }
    };

    // update Basic Info Success
    this.updateBasicInfoSuccess = function (success, message) {
        if (success === false) {
            failAlert(message);
        }
        else {
            successAlert(message);
            setInterval('location.reload()', 3000);
        }
    };

    // delete Client Slider
    this.deleteClientSlider = function (e) {
        var data = $(e).data();
        swal({
            title: "Are you sure?",
            text: "Please confirm you wish to delete Client Slider " + data.name,
            icon: "warning",
            buttons: true,
            dangerMode: true
        })
            .then((willDelete) => {
                if (willDelete) {
                    PostData("/setting/deleteclientslider", this.deleteClientSliderSuccess, { id: data.userid });
                }
            });
    };

    //delete Client Slider Success
    this.deleteClientSliderSuccess = function (result) {
        if (result) {
            SwalSuccess("Record deleted successfully!");
            refreshDataTable(tblSlider);
        }
        else {
            SwalError("Something went wrong!");
            refreshDataTable(tblSlider);
        }
    };

    //status success
    this.statusSuccess = function (result) {
        if (result) {
            SwalSuccess("Status changed successfully!");
            refreshDataTable(tblSlider);
        }
        else {
            SwalError("Something went wrong!");
            refreshDataTable(tblSlider);
        }
    };

    // Save Slider
    this.SaveSlider = function (button) {
        if ($("#frmAddNewClientSlider").valid()) {
            //check logo validation
            var sliderLogoId = $("#SliderLogo").val();
            if (sliderLogoId == undefined || sliderLogoId == 0 || sliderLogoId == "") {
                SwalError("Please first upload logo!");
            }
            else {
                //check checkbox checked or not
                if ($("input[type=checkbox]").is(":checked")) {
                    var txtButtonText = $("#buttonText").val();
                    var txtButtonLink = $("#buttonLink").val();
                    //check textbox validation
                    if (txtButtonText == undefined || txtButtonText == 0 || txtButtonText == ""
                        || txtButtonLink == undefined || txtButtonLink == 0 || txtButtonLink == "") {
                        SwalError("Please enter button text & button link!");
                    }
                    else {
                        $("#btn-frmAddNewClientSlider-submit").click();
                    }
                }
                else {
                    $("#btn-frmAddNewClientSlider-submit").click();
                }
            }
        }
    };

    //check logo validation
    this.beforeSubmit = function () {
        if ($("#frmAddNewBasicInfo").valid()) {
            var basicInfoLogo = $("#BasicInfoLogo").val();
            if (basicInfoLogo == undefined || basicInfoLogo == 0 || basicInfoLogo == "") {
                SwalError("Please first upload logo!");
            }
            else {
                $("#btnAddBasicInfo").click();
            }
        }
    }

    // Copy Site Url
    this.CopySiteUrl = function (element) {
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val(element).select();
        document.execCommand("copy");
        $("#btnCopyUrl").addClass("d-none");
        $(".copyurl").removeClass("d-none");
        setInterval('$(".copyurl").addClass("d-none"), $("#btnCopyUrl").removeClass("d-none")', 1000);
    }

    //#endregion Setting

};