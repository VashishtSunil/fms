﻿var RoleViewModel = new function () {
    var thisViewModel = this;

    //#region Role

    var tblRole = "tblRole";

    this.RoleListView = function (data) {
        var permission;
        if (data.success) {
            permission = data.message;
        }

        if (!permission.IsCreate) {
            //get the button of create
            $("#btnAddRole").remove();
        }
        var responsiveDt = undefined;
        var breakpointDefinition = {
            tablet: 1024,
            phone: 480
        };
        $('#' + tblRole).dataTable({
            "bStateSave": false,
            "iDisplayLength": 10,
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            //for CSV buttons
            "dom": "Blfrtip",
            buttons: [
                {
                    extend: 'csv',
                    text: 'Export',
                    filename: function () {
                        var d = new Date();
                        var n = d.getTime();
                        return 'Role' + n;
                    }
                }],
            cache: true,
            "responsive": true,
            "bServerSide": true,
            "bProcessing": true,
            "bJqueryUI": true,
            "sPaginationType": "full_numbers",
            "aaSorting": [[0, "asc"]],
            "sAjaxSource": baseUrl + "/Role/list",
            "preDrawCallback": function () {
                // Initialize the responsive datatables helper once.
                if (!responsiveDt) {
                    responsiveDt = new ResponsiveDatatablesHelper($('#' + tblRole), breakpointDefinition);
                }
            },
            "rowCallback": function (nRow, data) {

                responsiveDt.createExpandIcon(nRow);
            },
            "drawCallback": function () {
                responsiveDt.respond();
                $("a.edit").tooltip({
                    placement: 'top',
                    trigger: 'hover',
                    title: 'Edit Role'
                });
                $("a.edit").removeAttr("title");
                $("a.delete").tooltip({
                    placement: 'top',
                    trigger: 'hover',
                    title: 'Delete Role'
                });
                $("a.delete").removeAttr("title");
                //
                InitBootstrapSwitch();
            },
            "aoColumns": [
                { "mDataProp": "RoleName", "sClass": "center" },
                { "mDataProp": "Users", "sClass": "center" },
                {
                    "orderable": false,
                    "mRender": function (data, type, full) {
                        var actionHtml = "";
                        if (full.IsActive) {
                            actionHtml = "<a  class='bootstrapSwitch-checkbox text-primary'  data-url='/Role/ChangeRoleStatus'  data-id='" + full.Id + "' ><i class='fa fa-toggle-on  fa-lg'></i></a>";
                        }
                        else {
                            actionHtml = "<a  class='bootstrapSwitch-checkbox text-primary'  data-url='/Role/ChangeRoleStatus'  data-id='" + full.Id + "' ><i class='fas fa-toggle-off  fa-lg'></i></a>";
                        }
                        return actionHtml;
                    }
                },
                {
                    "orderable": false,
                    "sWidth": "15%",
                    "mRender": function (data, type, full) {
                        var actionString = "";

                        if (permission.IsView) {
                            actionString = actionString + "<a class=''  data-TagId=" + full.Id + "   data-name=" + full.RoleName + " onClick=RoleViewModel.updateRole('" + full.Id + "',true)><i class='far fa-eye text-primary fa-sm mr-2'></i></a>"
                        }
                        if (permission.IsEdit) {
                            actionString = actionString + "<a class=''  data-TagId=" + full.Id + "   data-name=" + full.RoleName + " onClick=RoleViewModel.updateRole('" + full.Id + "',false)><i class='far fa-edit text-primary fa-sm mr-2'></i></a>"
                        }
                        if (permission.IsDelete) {
                            actionString = actionString + "<a class='delete'  data-roleId=" + full.Id + "   data-name=" + full.RoleName + " onClick=RoleViewModel.deleteRole(this)><i class='fas fa-trash-alt text-danger fa-sm'></i></a>"
                        }
                        return actionString;
                    }
                }
            ]
            ,
            columnDefs: [
                {
                    "defaultContent": " ",
                    "targets": "_all"
                }],
            "autoWidth": true
        });
    };

    // open update Role modal with data
    this.updateRole = function (roleId, isReadonly) {
        this.openRoleModal(roleId, isReadonly);

    };

    //open Role model
    this.openRoleModal = function (id, isReadonly) {
        GetData("/role/openmodal", openModalSuccess, {
            formId: "frmAddNewRole",
            id: id,
            isReadonly: isReadonly
        });
    };

    // open Role success method
    this.updateRoleSuccess = function (success, message) {
        if (success === false) {
            failAlert(message);
        }
        else {
            successAlert(message);
            closeCommonModel();
            refreshDataTable(tblRole);
        }
    };

    // delete Role
    this.deleteRole = function (e) {
        var data = $(e).data();
        swal({
            title: "Are you sure?",
            text: "Please confirm you wish to delete Role " + data.name,
            icon: "warning",
            buttons: true,
            dangerMode: true
        })
            .then((willDelete) => {
                if (willDelete) {
                    PostData("/Role/deleterole", this.deleteRoleSuccess, { id: data.roleid });

                }
            });
    };

    //delete Role success
    this.deleteRoleSuccess = function (result) {
        if (result) {
            SwalSuccess("Record deleted successfully!");
            refreshDataTable(tblRole);
        }
        else {
            SwalError("Something went wrong!");
            refreshDataTable(tblRole);
        }
    };

    //#endregion Role
};