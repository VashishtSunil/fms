﻿var ProfileViewModel = new function () {
    var thisViewModel = this;

    //#region Profile

    // update Profile success method
    this.updateProfileSuccess = function (success, message) {
        if (success === false) {
            failAlert(message);
        }
        else {
            successAlert(message);
            closeCommonModel();
            location.reload();
        }
    };

    // Profile List View
    this.ProfileListView = function (data) {
        var permission;
        if (data.success) {
            permission = data.message;
        }

        if (!permission.IsCreate) {
            //get the button of create 
            $("#btnAddProfile").remove();
        }

    };

    //#endregion Profile

};