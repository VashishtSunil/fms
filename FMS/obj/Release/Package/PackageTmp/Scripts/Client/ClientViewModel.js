﻿var ClientViewModel = new function () {
    var thisViewModel = this;

    //#region Client

    var tblClient = "tblClient";

    this.ClientListView = function () {
        var responsiveDt = undefined;
        var breakpointDefinition = {
            tablet: 1024,
            phone: 480
        };
        $('#' + tblClient).dataTable({
            "bStateSave": false,
            "iDisplayLength": 10,
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            //for CSV buttons
            "dom": "Blfrtip",
            buttons: [
                {
                    extend: 'csv',
                    text: 'Export',
                    filename: function () {
                        var d = new Date();
                        var n = d.getTime();
                        return 'Client' + n;
                    }
                }],
            cache: true,
            "responsive": true,
            "bServerSide": true,
            "bProcessing": true,
            "bJqueryUI": true,
            "sPaginationType": "full_numbers",
            "aaSorting": [[0, "asc"]],
            "sAjaxSource": baseUrl + "/Client/list",
            "preDrawCallback": function () {
                // Initialize the responsive datatables helper once.
                if (!responsiveDt) {
                    responsiveDt = new ResponsiveDatatablesHelper($('#' + tblClient), breakpointDefinition);
                }
            },
            "rowCallback": function (nRow, data) {

                responsiveDt.createExpandIcon(nRow);
            },
            "drawCallback": function () {
                responsiveDt.respond();
                $("a.edit").tooltip({
                    placement: 'top',
                    trigger: 'hover',
                    title: 'Edit Client'
                });
                $("a.edit").removeAttr("title");
                $("a.delete").tooltip({
                    placement: 'top',
                    trigger: 'hover',
                    title: 'Delete Client'
                });
                $("a.delete").removeAttr("title");
                //
                InitBootstrapSwitch();
            },
            "aoColumns": [
                { "mDataProp": "FullName", "sClass": "center" },
                { "mDataProp": "Email", "sClass": "center" },
                { "mDataProp": "Phone", "sClass": "center" },
                { "mDataProp": "Address", "sClass": "center" },
                {
                    "orderable": false,
                    "mRender": function (data, type, full) {
                        var actionHtml = "";
                        if (full.IsActive) {
                            actionHtml = "<a  class='bootstrapSwitch-checkbox text-primary'  data-url='/client/changeclientstatus'  data-id='" + full.Id + "' ><i class='fa fa-toggle-on  fa-lg'></i></a>";
                        }
                        else {
                            actionHtml = "<a  class='bootstrapSwitch-checkbox text-primary'  data-url='/client/changeclientstatus'  data-id='" + full.Id + "' ><i class='fas fa-toggle-off  fa-lg'></i></a>";
                        }
                        return actionHtml;
                    }
                },
                {
                    "orderable": false,
                    "mRender": function (data, type, full) {
                        return "<span><a href='" + baseUrl + "/landing/index?key=" + full.SiteKey + "' class='view dt-control pl-3 pr-3' data-toggle= 'tooltip' data-placement='top' title= 'Click to view landing'><button class= 'btn btn-primary btn-sm'>Click here</button></a></span>";
                    }
                },
                {
                    "orderable": false,
                    "sWidth": "15%",
                    "mRender": function (data, type, full) {
                        var actionString = "";
                        actionString = actionString + "<a class=''  data-TagId=" + full.Id + "   data-name=" + full.FirstName + " onClick=ClientViewModel.updateClient('" + full.Id + "',true)><i class='far fa-eye text-primary fa-sm mr-2'></i></a>"
                        actionString = actionString + "<a class=''  data-TagId=" + full.Id + "   data-name=" + full.FirstName + " onClick=ClientViewModel.updateClient('" + full.Id + "',false)><i class='far fa-edit text-primary fa-sm mr-2'></i></a>"
                        actionString = actionString + "<a class='delete'  data-userId=" + full.Id + "   data-name=" + full.FirstName + " onClick=ClientViewModel.deleteClient(this)><i class='fas fa-trash-alt text-danger fa-sm'></i></a>"
                        return actionString;
                    }
                }
            ]
            ,
            columnDefs: [
                {
                    "defaultContent": " ",
                    "targets": "_all"
                }],
            "autoWidth": true
        });
    };

    // open update User modal with data
    this.updateClient = function (userId, isReadonly) {
        this.openClientModal(userId, isReadonly);

    };

    //open User model
    this.openClientModal = function (id, isReadonly) {
        GetData("/Client/openmodal", openModalSuccess, {
            formId: "frmAddNewClient",
            id: id,
            isReadonly: isReadonly
        });
    };

    // open User success method
    this.updateClientSuccess = function (success, message) {
        if (success === false) {
            failAlert(message);
        }
        else {
            successAlert(message);
            closeCommonModel();
            refreshDataTable(tblClient);
        }
    };

    // delete User
    this.deleteClient = function (e) {
        var data = $(e).data();
        swal({
            title: "Are you sure?",
            text: "Please confirm you wish to delete Client " + data.name,
            icon: "warning",
            buttons: true,
            dangerMode: true
        })
            .then((willDelete) => {
                if (willDelete) {
                    PostData("/Client/deleteClient", this.deleteClientSuccess, { id: data.userid });
                }
                refreshDataTable(tblClient);
            });
    };

    //delete User success
    this.deleteClientSuccess = function (result) {
        if (result) {
            SwalSuccess("Record deleted successfully!");
            refreshDataTable(tblClient);
        }
        else {
            SwalError("Something went wrong!");
            refreshDataTable(tblClient);
        }
    };

    //send email success
    this.sendEmailSuccess = function (success, message) {
        if (success === false) {
            failAlert(message);
        }
        else {
            successAlert(message);
            setInterval('location.reload()', 3000);
        }
    };
    //#endregion Client
};