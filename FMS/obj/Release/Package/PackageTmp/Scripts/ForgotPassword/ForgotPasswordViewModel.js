﻿var ForgotPasswordViewModel = new function () {
    var thisViewModel = this;

    //#region Profile

    // update Profile success method
    this.updateForgotPasswordSuccess = function (success, message) {
        if (success === false) {
            failAlert(message);
        }
        else {
            successAlert(message);
            closeCommonModel();
        }
    };

    // Forgot Password List View
    this.ForgotPasswordListView = function (data) {
        var permission;
        if (data.success) {
            permission = data.message;
        }

        if (!permission.IsCreate || !permission.IsEdit) {
            //get the button of create
            $("#btnResetForgotPassWord").remove();
        }

    };

    //#endregion Profile

};