﻿var TrainerViewModel = new function () {
    var thisViewModel = this;

    //#region Trainer

    var tblTrainer = "tblTrainer";

    this.TrainerListView = function () {
        var responsiveDt = undefined;
        var breakpointDefinition = {
            tablet: 1024,
            phone: 480
        };
        $('#' + tblTrainer).dataTable({

            "bStateSave": false,
            "iDisplayLength": 10,
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            //for CSV buttons
            "dom": "Blfrtip",
            buttons: [
                {
                    extend: 'csv',
                    text: 'Export',
                    filename: function () {
                        var d = new Date();
                        var n = d.getTime();
                        return 'Trainer' + n;
                    }
                }],
            cache: true,
            "responsive": true,
            "bServerSide": true,
            "bProcessing": true,
            "bJqueryUI": true,
            "sPaginationType": "full_numbers",
            "aaSorting": [[0, "asc"]],
            "sAjaxSource": baseUrl + "/Trainer/list",
            "preDrawCallback": function () {
                // Initialize the responsive datatables helper once.
                if (!responsiveDt) {
                    responsiveDt = new ResponsiveDatatablesHelper($('#' + tblTrainer), breakpointDefinition);
                }
            },
            "rowCallback": function (nRow, data) {

                responsiveDt.createExpandIcon(nRow);
            },
            "drawCallback": function () {
                responsiveDt.respond();
                $("a.edit").tooltip({
                    placement: 'top',
                    trigger: 'hover',
                    title: 'Edit Trainer'
                });
                $("a.edit").removeAttr("title");
                $("a.delete").tooltip({
                    placement: 'top',
                    trigger: 'hover',
                    title: 'Delete Trainer'
                });
                $("a.delete").removeAttr("title");
                //
                InitBootstrapSwitch();
            },
            "aoColumns": [
                { "mDataProp": "FullName", "sClass": "center" },
                { "mDataProp": "Email", "sClass": "center" },
                { "mDataProp": "Phone", "sClass": "center" },
                { "mDataProp": "Address", "sClass": "center" },
                {
                    "orderable": false,
                    "mRender": function (data, type, full) {
                        var actionHtml = "";
                        if (full.IsActive) {
                            actionHtml = "<a  class='bootstrapSwitch-checkbox text-primary'  data-url='/trainer/changetrainerstatus'  data-id='" + full.Id + "' ><i class='fa fa-toggle-on  fa-lg'></i></a>";
                        }
                        else {
                            actionHtml = "<a  class='bootstrapSwitch-checkbox text-primary'  data-url='/trainer/changetrainerstatus'  data-id='" + full.Id + "' ><i class='fas fa-toggle-off  fa-lg'></i></a>";
                        }
                        return actionHtml;
                    }
                },
                {
                    "orderable": false,
                    "sWidth": "15%",
                    "mRender": function (data, type, full) {
                        var actionString = "";
                        actionString = actionString + "<a class=''  data-TagId=" + full.Id + "   data-name=" + full.FirstName + " onClick=TrainerViewModel.updateTrainer('" + full.Id + "',true)><i class='far fa-eye text-primary fa-sm mr-2'></i></a>"
                        actionString = actionString + "<a class=''  data-TagId=" + full.Id + "   data-name=" + full.FirstName + " onClick=TrainerViewModel.updateTrainer('" + full.Id + "',false)><i class='far fa-edit text-primary fa-sm mr-2'></i></a>"
                        actionString = actionString + "<a class='delete'  data-userId=" + full.Id + "   data-name=" + full.FirstName + " onClick=TrainerViewModel.deleteTrainer(this)><i class='fas fa-trash-alt text-danger fa-sm'></i></a>"
                        return actionString;
                    }
                }
            ]
            ,
            columnDefs: [
                {
                    "defaultContent": " ",
                    "targets": "_all"
                }],
            "autoWidth": true
        });
    };

    // open update User modal with data
    this.updateTrainer = function (userId, isReadonly) {
        this.openTrainerModal(userId, isReadonly);

    };

    //open User model
    this.openTrainerModal = function (id, isReadonly) {
        GetData("/Trainer/openmodal", openModalSuccess, {
            formId: "frmAddNewTrainer",
            id: id,
            isReadonly: isReadonly
        });
    };

    // open User success method
    this.updateTrainerSuccess = function (success, message) {
        if (success === false) {
            failAlert(message);
        }
        else {
            successAlert(message);
            closeCommonModel();
            refreshDataTable(tblTrainer);
        }
    };

    // delete User
    this.deleteTrainer = function (e) {
        var data = $(e).data();
        swal({
            title: "Are you sure?",
            text: "Please confirm you wish to delete Trainer " + data.name,
            icon: "warning",
            buttons: true,
            dangerMode: true
        })
            .then((willDelete) => {
                if (willDelete) {
                    PostData("/Trainer/deleteTrainer", this.deleteTrainerSuccess, { id: data.userid });

                }
                refreshDataTable(tblTrainer);
            });
    };



    //status success
    this.statusSuccess = function (result) {
        if (result) {
            SwalSuccess("Status changed successfully!");
            refreshDataTable(tblTrainer);
        }
        else {
            successAlert("Something went wrong!");
            refreshDataTable(tblTrainer);
        }
    };

    //delete User success
    this.deleteTrainerSuccess = function (result) {
        if (result) {
            SwalSuccess("Record deleted successfully!");
            refreshDataTable(tblTrainer);
        }
        else {
            SwalError("Something went wrong!");
            refreshDataTable(tblTrainer);
        }
    };

    //send email success
    this.sendEmailSuccess = function (success, message) {
        if (success === false) {
            failAlert(message);
        }
        else {
            successAlert(message);
            setInterval('location.reload()', 3000);
        }
    };
    //#endregion Trainer
};