﻿//#region PlanViewModel Method
var PlanViewModel = new function () {
    var thisViewModel = this;
    var wizardName = ["plan", "planFeature"];
    var activeWizardName = "plan";
    var planFooters = ["basicInfoModule", "planFeatureModule"];
    var tblPlanFeature = "tblPlanFeature";

    //#region Plan 

    var tblPlan = "tblPlan";

    this.planListView = function () {
        var responsiveDt = undefined;
        var breakpointDefinition = {
            tablet: 1024,
            phone: 480
        };
        $('#' + tblPlan).dataTable({
            "bStateSave": false,
            "iDisplayLength": 10,
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            "dom": "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            cache: true,
            "responsive": true,
            "bServerSide": true,
            "bProcessing": true,
            "bJqueryUI": true,
            "sPaginationType": "full_numbers",
            "aaSorting": [[0, "asc"]],
            "sAjaxSource": baseUrl + "/Plan/getPlanList",
            "preDrawCallback": function () {
                // Initialize the responsive datatables helper once.
                if (!responsiveDt) {
                    responsiveDt = new ResponsiveDatatablesHelper($('#' + tblPlan), breakpointDefinition);
                }
            },
            "rowCallback": function (nRow, data) {
                responsiveDt.createExpandIcon(nRow);
            },
            "drawCallback": function () {
                responsiveDt.respond();
                InitBootstrapSwitch();
            },

            "aoColumns": [

                { "mDataProp": "Name", "sClass": "center" },
                {
                    "mDataProp": "Price", "sClass": "center",
                    "mRender": function (data, type, full) {
                        return ("$" + full.Price);
                    }
                },
                {
                    "mDataProp": "FeatureCount", "sClass": "center",
                },
                {
                    "mDataProp": "PlanLengthInDays", "sClass": "center",
                },
                {
                    "orderable": false,
                    "mRender": function (data, type, full) {
                        var actionHtml = "";
                        if (full.IsActive) {
                            actionHtml = "<a  class='bootstrapSwitch-checkbox text-primary'  data-url='/plan/changeplanstatus'  data-id='" + full.Id + "' ><i class='fa fa-toggle-on  fa-lg'></i></a>";
                        }
                        else {
                            actionHtml = "<a  class='bootstrapSwitch-checkbox text-primary'  data-url='/plan/changeplanstatus'  data-id='" + full.Id + "' ><i class='fas fa-toggle-off  fa-lg'></i></a>";
                        }
                        return actionHtml;
                    }
                },
                {
                    "orderable": false,
                    "sWidth": "20%",
                    "mRender": function (data, type, full) {
                        let toolTipView = (full.IsSeeded != true) ? "View Plan" : "Cannot This View Plan";
                        let toolTipEdit = (full.IsSeeded != true) ? "Edit Plan" : "Cannot This Edit Plan";
                        let toolTipDelete = (full.IsSeeded != true) ? "Delete Plan" : "Cannot This Delete Plan";
                        return "<a class='edit dt-control' onClick=PlanViewModel.openPlanModel('" + full.Id + "',true)  data-toggle= 'tooltip' data-placement='top' title= '" + toolTipView + "'><i class='fas fa-eye text-primary fa-sm mr-2'></i></a>  " +
                            "<a onClick=PlanViewModel.openPlanModel('" + full.Id + "',false) class='edit dt-control'  data-toggle= 'tooltip' data-placement='top' title= '" + toolTipEdit + "'><i class='fas fa-edit text-primary fa-sm mr-2'></i></a>  " +
                            "<a class='delete dt-control' data-toggle= 'tooltip' data-placement='top' title= '" + toolTipDelete + "' data-id=" + full.Id + "   data-name=" + full.Name + " onClick=PlanViewModel.deleteplan(this)><i class='fas fa-trash-alt fa-sm text-danger'></i></a>"
                    }
                }
            ]
            ,
            columnDefs: [
                {
                    "defaultContent": " ",
                    "targets": "_all"
                }],
            "autoWidth": true
        });
    };

    //bind plan features datatable list
    this.planFeatureListView = function () {
        var responsiveDt = undefined;
        var breakpointDefinition = {
            tablet: 1024,
            phone: 480
        };
        let url = baseUrl + "/plan/planfeaturelist";
        let searchParams = new URLSearchParams(window.location.search);
        if (searchParams.has('planid')) {
            let planId = searchParams.get('planid');
            url += "?planid=" + planId;
        }
        $('#' + tblPlanFeature).dataTable({
            "bStateSave": false,
            "iDisplayLength": 10,
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            "dom": "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            cache: true,
            "responsive": true,
            "bServerSide": true,
            "bProcessing": true,
            "bJqueryUI": true,
            "sPaginationType": "full_numbers",
            "aaSorting": [[0, "asc"]],
            "sAjaxSource": url,
            "preDrawCallback": function () {
                // Initialize the responsive datatables helper once.
                if (!responsiveDt) {
                    responsiveDt = new ResponsiveDatatablesHelper($('#' + tblPlanFeature), breakpointDefinition);
                }
            },
            "rowCallback": function (nRow, data) {
                responsiveDt.createExpandIcon(nRow);
            },
            "drawCallback": function () {
                responsiveDt.respond();
            },
            "aoColumns": [

                { "mDataProp": "Name", "sClass": "center" },
                {
                    "mDataProp": "Price", "sClass": "center",
                    "mRender": function (data, type, full) {
                        return ("$" + full.Price);
                    }
                }
            ]
            ,
            "autoWidth": true
        });
    };

    //open plan modal popup
    this.openPlanModel = function (planId, isReadMode) {
        GetData("/plan/openplanmodal", openModalSuccess, {
            formId: "frmAddNewPlanFeature",
            id: planId, isReadMode: isReadMode
        });
    };

    // delete Feature
    this.deleteplan = function (e) {
        var data = $(e).data();
        swal({
            title: "Are you sure?",
            text: "Please confirm you wish to delete plan " + data.name,
            icon: "warning",
            buttons: true,
            dangerMode: true
        })
            .then((willDelete) => {
                if (willDelete) {
                    PostData("/plan/deleteplan", this.deleteplanSuccess, { id: data.id });

                }
            });
    };

    this.deleteplanSuccess = function (result) {
        if (result) {
            SwalSuccess("Record deleted successfully!");
            refreshDataTable(tblPlan);
        }
        else {
            SwalError("Something went wrong!");
        }
    };

    //delete plan success method
    this.deletePlanSuccess = function (success, message) {
        if (success.success) {
            successAlert(success.message);
            refreshDataTable(tblPlan);
        }
        else {
            failAlert(success.message);
            refreshDataTable(tblPlan);
        }
    };

    //function for get info whether plan is in used
    this.isPlanInUsed = function (planId) {
        PostData("/plan/isplaninused", thisViewModel.isPlanInUsedSuccess, { planId: planId });
    };

    this.isPlanInUsedSuccess = function (success, message) {
        if (success.success) {
            if (success.message == "") {
                success.message = "Are you sure you want to delete ";
                var deleteSpanElement = $("#deleteRow" + message.planId)[0];
                thisViewModel.deletePlan(deleteSpanElement, success.message, true)
            }
            else {
                thisViewModel.deletePlan(null, success.message, false);
            }
        }
        else {
            failAlert(success.message);
        }
    };

    // delete plan
    this.deletePlan = function (e, warningMessage, isDeleteAble) {
        if (isDeleteAble) {
            var data = $(e).data();
            Swal.fire({
                text: warningMessage + data.name + "?",
                icon: "warning",
                dangerMode: true,
                reverseButtons: true,
                showCancelButton: true,
                cancelButtonText: "Cancel",
                confirmButtonText: "Confirm",
            }).then((willDelete) => {
                if (willDelete.isConfirmed) {
                    PostData("/plan/deleteplan", thisViewModel.deletePlanSuccess, { planId: data.id });
                }
            });
        }
        else {
            Swal.fire({
                text: warningMessage,
                icon: "warning",
                dangerMode: true,
                showCloseButton: true,
                showCancelButton: true,
                showConfirmButton: false,
                cancelButtonText: "Close"
            });
        }
    };

    //success method for modal popup
    this.updatePlanFeatureModalSuccess = function (success, message) {
        stopLaddaSpinWheel("btnSavePlan");
        if (!success) {
            failAlert(message);
        }
        else {
            //closeCommonModel();
            var msgArray = message.split(',');
            successAlert(msgArray[0]);
            $("#plan-id").val(msgArray[1]);
            $("#planFeature-feature").removeClass("d-none");
            $("#plan-feature").val('');
            var planId = $("#plan-id").val();
            refreshDataTable(tblPlan);
            refreshDataTable(tblPlanFeature);
            thisViewModel.PlanFeatureListView(planId);

        }
    };

    //change plan feature selection when their is any change on first screen
    this.changePlanFeatureSelection = function (checkBoxElement) {
        if (checkBoxElement != undefined && checkBoxElement != null && checkBoxElement != "") {
            var className = $(checkBoxElement).attr("class");

            var checkBox = $(checkBoxElement).attr("data-featureClass");
            if (checkBoxElement.checked) {
                if (className != undefined && className != null && className != "") {
                    className = className.split(' ');
                    $("." + className[0]).each(function () {
                        $(this).attr("value", "true");
                        $(this).attr("checked", "checked");
                    });
                }
                $("." + checkBox).attr("val", "true");
                $("." + checkBox).attr("checked", "checked");
            }
            else {
                if (className != undefined && className != null && className != "") {
                    className = className.split(' ');
                    $("." + className[0]).each(function () {
                        $(this).attr("value", "false");
                        $(this).removeAttr("checked");
                    });
                }
                $("." + checkBox).attr("val", "false");
                $("." + checkBox).removeAttr("checked");
            }
        }
    };

    //update plan
    this.updatePlan = function (btnElement) {
        var formId = $(btnElement).attr("data-formId");
        var id = $(btnElement).attr("id");
        if (formId != null && formId != undefined && formId != "" && $("#" + formId).valid()) {
            startLaddaSpinWheel(id);
            $("#btn-" + formId + "-submit").click();
        }
    };

    //success method for plan
    this.updatePlanSuccess = function (success, message) {
        //start and stop ladda button spin based on current wizard name
        if (PlanViewModel.activeWizardName == wizardName[0]) {
            stopLaddaSpinWheel("btnSavePlan");
        }
        if (!success) {
            if (PlanViewModel.activeWizardName == wizardName[1]) {
                stopLaddaSpinWheel("btnSavePlanFeature");
            }
            failAlert(message);
        }
        else {
            var msgArray = message.split(',');
            //if plan saved from feature screen then save the feature
            //else redirect page to index
            if (PlanViewModel.activeWizardName == wizardName[0]) {
                successAlert(msgArray[0]);
                setTimeout(function () {
                    location.href = baseUrl + "/plan/Index";
                }, 500);
            }
            else {
                $("#PlanId").val(msgArray[1]);
                $("#btn-frmAddNewPlanFeature-submit").click();
            }
        }
    };

    //show the current pannel and hide all other 
    this.showCurrentPanel = function (btnElement) {
        if (btnElement != null && btnElement != undefined || btnElement != "") {
            //form is valid
            if ($("#frmAddNewPlanFeature").valid()) {
                var currentView = $(btnElement).attr("data-viewName");
                //var title = $(btnElement).attr("data-title");
                $(".modal-title").text();
                for (var i = 0; i < planFooters.length; i++) {
                    if (planFooters[i] == currentView) {
                        //attach all selected features to first screen
                        var selctedFeaturesHtml = "";
                        $("#planFeatureModule input[type='checkbox']:checked").each(function () {
                            $(this).attr("checked", "checked");
                            selctedFeaturesHtml = selctedFeaturesHtml + this.outerHTML + $(this).siblings("label").html() + "<br/>";
                        });
                        if (selctedFeaturesHtml != null && selctedFeaturesHtml != undefined && selctedFeaturesHtml != null) {
                            $("#planModuleFeatureCheckBoxList").html(selctedFeaturesHtml);
                            $("#planModuleFeatureCheckBoxList").removeClass("d-none");
                        }
                        $("#" + planFooters[i] + "-footer").removeClass("d-none");
                        $("#" + planFooters[i]).removeClass("d-none");
                    } else {
                        $("#" + planFooters[i] + "-footer").addClass("d-none");
                        $("#" + planFooters[i]).addClass("d-none");
                    }
                }
            }
        }
    };

    //show view(wizard)on button click
    this.showView = function (btnElement) {
        if (btnElement != null && btnElement != undefined && btnElement != "") {
            PlanViewModel.activeWizardName = $(btnElement).attr("data-viewName");
            var divId = $(btnElement).attr("data-divId");
            var planId = $("#Id").val();
            var isReadMode = $("#IsReadMode").val();
            var planName = $("#PlanName").val();

            //update title text based on view and plane name
            if (PlanViewModel.activeWizardName == wizardName[0]) {
                //show wizard 
                $("#PlanFeatureModule").addClass("d-none");
                $("#" + divId).removeClass("d-none");

                if (planId == "0" || planId == "" || planId == undefined) {
                    $(".box-title").text("Add New Pricing Plan");
                }
                else if (isReadMode == "true" || isReadMode == "True") {
                    $(".box-title").text("View " + planName);
                }
                else {
                    $(".box-title").text("Edit Pricing Plan");
                }
            }
            else {
                //show wizard 
                $("#PlanModule").addClass("d-none");
                $("#" + divId).removeClass("d-none");
                if (planId == "0" || planId == "" || planId == undefined) {
                    $(".box-title").text("Add New Pricing Plan → Add Features");
                }
                else if (isReadMode == "true" || isReadMode == "True") {
                    $(".box-title").text("View " + planName + " →  Features");
                }
                else {
                    $(".box-title").text("Edit Pricing Plan → Edit Features");
                }
            }
        }
    };

    //function executed on click on submit button of feature screen
    this.updatePlanFeature = function (btnElement) {
        if ($("#planFeatureModule").find("input[type ='checkbox']:checked").length == 0) {
            failAlert("Please select at least one feature!");
        }
        else {
            var data = $(btnElement).data();
            if (data != null && data != undefined && data != "") {
                $("#" + data.backpagebtnid).click();
            }
        }
    };

    //success function of planFeature screen 
    this.updatePlanFeatureSuccess = function (success, message) {
        stopLaddaSpinWheel("btnSavePlanFeature");
        if (!success) {
            failAlert(message);
        }
        else {
            successAlert(message);
            setTimeout(function () {
                location.href = baseUrl + "/plan/addplan?id=" + $("#PlanId").val();
            }, 500);
        }
    };

    //validate plan module
    this.savePlan = function (element) {
        var data = $(element).data();
        var id = $(element).attr("id");
        if (data != undefined && data != null && data != "") {
            if ($("#" + data.formid).valid()) {
                startLaddaSpinWheel(id);
                $("#btn-" + data.formid + "-submit").click();
            }
        }
    };



    //#endregion Plan

    //#region Plan Feature

    //success method after fetching partial page successfully
    this.addFeature = function (element) {
        if (element != null && element != undefined && element != "") {
            var id = $(element).attr("id");
            if (id != undefined && id != null && id != "") {
                var planId = $("#plan-id").val();
                var feature = $("#plan-feature").val();
                if (feature == undefined || feature == 0 || feature == "") {
                    SwalError("please enter feature first!");
                }
                else {
                    $("#btnfrmAddNewFeatureSubmit").click();
                    closeCommonModel();
                }

            }
        }
    };

    // Plan Feature List 

    this.PlanFeatureListView = function (id) {
        var responsiveDt = undefined;
        var breakpointDefinition = {
            tablet: 1024,
            phone: 480
        };
        $('#' + tblPlanFeature).dataTable({
            "bDestroy": true,
            "scrollY": "140px",
            "scrollCollapse": true,
            "bStateSave": false,
            "iDisplayLength": 10,
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            //for CSV buttons
            "dom": "Brt",
            buttons: [
                {
                    extend: 'csv',
                    text: 'Export',
                    filename: function () {
                        var d = new Date();
                        var n = d.getTime();
                        return 'PlanFeature' + n;
                    }
                }],
            cache: true,
            "responsive": true,
            "bServerSide": true,
            "bProcessing": true,
            "bJqueryUI": true,
            "sPaginationType": "full_numbers",
            "aaSorting": [[0, "asc"]],
            "sAjaxSource": baseUrl + "/plan/getplanfeaturelist?id=" + id,
            "preDrawCallback": function () {
                // Initialize the responsive datatables helper once.
                if (!responsiveDt) {
                    responsiveDt = new ResponsiveDatatablesHelper($('#' + tblPlanFeature), breakpointDefinition);
                }
            },
            "rowCallback": function (nRow, data) {
                responsiveDt.createExpandIcon(nRow);
            },
            "drawCallback": function () {
                responsiveDt.respond();

            },
            "aoColumns": [
                { "mDataProp": "Feature", "sClass": "center" },
                {
                    "orderable": false,
                    "sWidth": "50%",
                    "mRender": function (data, type, full) {
                        var actionString = "";
                        actionString = actionString + "<a class='delete'  data-FeatureId=" + full.Id + "   data-name=" + full.Feature + " onClick=PlanViewModel.deletePlanFeature(this)><i class='fas fa-trash-alt fa-sm text-danger'></i></a>"
                        return actionString;
                    }
                }
            ]
            ,
            columnDefs: [
                {
                    "defaultContent": " ",
                    "targets": "_all"
                }],
            "autoWidth": true
        });
    };

    // delete Plan Feature
    this.deletePlanFeature = function (e) {
        var data = $(e).data();
        swal({
            title: "Are you sure?",
            text: "Please confirm you wish to delete Feature " + data.name,
            icon: "warning",
            buttons: true,
            dangerMode: true
        })
            .then((willDelete) => {
                if (willDelete) {
                    PostData("/plan/deleteplanfeature", this.deletePlanFeatureSuccess, { id: data.featureid });
                }
            });
    };

    // delete Plan Feature Success
    this.deletePlanFeatureSuccess = function (result) {
        if (result) {
            SwalSuccess("Record deleted successfully!");
            refreshDataTable(tblPlanFeature);
        }
        else {
            SwalError("Something went wrong!");
        }
    };

    //#endregion Plan Feature

};
//#endregion PlanViewModel Method