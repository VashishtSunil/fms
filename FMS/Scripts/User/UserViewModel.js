﻿var UserViewModel = new function () {
    var thisViewModel = this;

    //#region User

    var tblUser = "tblUser";

    this.UserListView = function (data) {
        var permission;
        if (data.success) {
            permission = data.message;
        }

        if (!permission.IsCreate) {
            //get the button of create 
            $("#btnAddUser").remove();
        }
        var responsiveDt = undefined;
        var breakpointDefinition = {
            tablet: 1024,
            phone: 480
        };
        $('#' + tblUser).dataTable({
            "bStateSave": false,
            "iDisplayLength": 10,
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            //for CSV buttons
            "dom": "Blfrtip",
            buttons: [
                {
                    extend: 'csv',
                    text: 'Export',
                    filename: function () {
                        var d = new Date();
                        var n = d.getTime();
                        return 'User' + n;
                    }
                }],
            cache: true,
            "responsive": true,
            "bServerSide": true,
            "bProcessing": true,
            "bJqueryUI": true,
            "sPaginationType": "full_numbers",
            "aaSorting": [[0, "asc"]],
            "sAjaxSource": baseUrl + "/User/list",
            "preDrawCallback": function () {
                // Initialize the responsive datatables helper once.
                if (!responsiveDt) {
                    responsiveDt = new ResponsiveDatatablesHelper($('#' + tblUser), breakpointDefinition);
                }
            },
            "rowCallback": function (nRow, data) {

                responsiveDt.createExpandIcon(nRow);
            },
            "drawCallback": function () {
                responsiveDt.respond();
                $("a.edit").tooltip({
                    placement: 'top',
                    trigger: 'hover',
                    title: 'Edit User'
                });
                $("a.edit").removeAttr("title");
                $("a.delete").tooltip({
                    placement: 'top',
                    trigger: 'hover',
                    title: 'Delete User'
                });
                $("a.delete").removeAttr("title");
                //
                InitBootstrapSwitch();
            },
            "aoColumns": [
                { "mDataProp": "Email", "sClass": "center" },
                { "mDataProp": "RoleName", "sClass": "center" },
                {
                    "orderable": false,
                    "mRender": function (data, type, full) {
                        var actionHtml = "";
                        if (full.IsActive) {
                            actionHtml = "<a  class='bootstrapSwitch-checkbox text-primary'  data-url='/User/ChangeUserStatus'  data-id='" + full.Id + "' ><i class='fa fa-toggle-on  fa-lg'></i></a>";
                        }
                        else {
                            actionHtml = "<a  class='bootstrapSwitch-checkbox text-primary'  data-url='/User/ChangeUserStatus'  data-id='" + full.Id + "' ><i class='fas fa-toggle-off  fa-lg'></i></a>";
                        }
                        return actionHtml;
                    }
                },
                {
                    "orderable": false,
                    "sWidth": "15%",
                    "mRender": function (data, type, full) {
                        var actionString = "";
                        if (permission.IsView) {
                            actionString = actionString + "<a class=''  data-TagId=" + full.Id + "   data-name=" + full.Email + " onClick=UserViewModel.updateUser('" + full.Id + "',true)><i class='far fa-eye text-primary fa-sm mr-2'></i></a>"
                        }
                        if (permission.IsEdit) {
                            actionString = actionString + "<a class=''  data-TagId=" + full.Id + "   data-name=" + full.Email + " onClick=UserViewModel.updateUser('" + full.Id + "',false)><i class='far fa-edit text-primary fa-sm mr-2'></i></a>"
                        }
                        if (permission.IsDelete) {
                            actionString = actionString + "<a class='delete'  data-userId=" + full.Id + "   data-name=" + full.Email + " onClick=UserViewModel.deleteUser(this)><i class='fas fa-trash-alt text-danger fa-sm'></i></a>"
                        }
                        return actionString;
                    }
                }
            ]
            ,
            columnDefs: [
                {
                    "defaultContent": " ",
                    "targets": "_all"
                }],
            "autoWidth": true
        });
    };

    // open update User modal with data
    this.updateUser = function (userId, isReadonly) {
        this.openUserModal(userId, isReadonly);

    };

    //open User model
    this.openUserModal = function (id, isReadonly) {
        GetData("/User/openmodal", openModalSuccess, {
            formId: "frmAddNewUser",
            id: id,
            isReadonly: isReadonly
        });
    };

    // open User success method
    this.updateUserSuccess = function (success, message) {
        if (success === false) {
            failAlert(message);
        }
        else {
            successAlert(message);
            closeCommonModel();
            refreshDataTable(tblUser);

        }
    };

    // delete User
    this.deleteUser = function (e) {
        var data = $(e).data();
        swal({
            title: "Are you sure?",
            text: "Please confirm you wish to delete User " + data.name,
            icon: "warning",
            buttons: true,
            dangerMode: true
        })
            .then((willDelete) => {
                if (willDelete) {
                    PostData("/User/deleteuser", this.deleteUserSuccess, { id: data.userid });

                }
            });
    };

    //delete User success
    this.deleteUserSuccess = function (result) {
        if (result) {
            SwalSuccess("Record deleted successfully!");
            refreshDataTable(tblUser);
        }
        else {
            SwalError("Something went wrong!");
            refreshDataTable(tblUser);
        }
    };

    //status success
    this.statusSuccess = function (result) {
        if (result) {
            SwalSuccess("Status changed successfully!");
            refreshDataTable(tblUser);
        }
        else {
            SwalError("Something went wrong!");
            refreshDataTable(tblUser);
        }
    };

    //#endregion User
};