﻿var IngredientViewModel = new function () {
    var thisViewModel = this;

    //#region Ingredient
    var tblIngredient = "tblIngredient";
    this.IngredientListView = function (data) {        
        var responsiveDt = undefined;
        var breakpointDefinition = {
            tablet: 1024,
            phone: 480
        };
        $('#' + tblIngredient).dataTable({
            "bStateSave": false,
            "iDisplayLength": 10,
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            //for CSV buttons
            "dom": "Blfrtip",
            buttons: [
                {
                    extend: 'csv',
                    text: 'Export',
                    filename: function () {
                        var d = new Date();
                        var n = d.getTime();
                        return 'Ingredient' + n;
                    }
                }],
            cache: true,
            "responsive": true,
            "bServerSide": true,
            "bProcessing": true,
            "bJqueryUI": true,
            "sPaginationType": "full_numbers",
            "aaSorting": [[0, "asc"]],
            "sAjaxSource": baseUrl + "/Ingredient/list",
            "preDrawCallback": function () {
                // Initialize the responsive datatables helper once.
                if (!responsiveDt) {
                    responsiveDt = new ResponsiveDatatablesHelper($('#' + tblIngredient), breakpointDefinition);
                }
            },
            "rowCallback": function (nRow, data) {

                responsiveDt.createExpandIcon(nRow);
            },
            "drawCallback": function () {
                responsiveDt.respond();
                $("a.edit").tooltip({
                    placement: 'top',
                    trigger: 'hover',
                    title: 'Edit Ingredient'
                });
                $("a.edit").removeAttr("title");
                $("a.delete").tooltip({
                    placement: 'top',
                    trigger: 'hover',
                    title: 'Delete Ingredient'
                });
                $("a.delete").removeAttr("title");
                InitBootstrapSwitch();
            },
            "aoColumns": [
                { "mDataProp": "Name", "sClass": "center" },
                { "mDataProp": "Calorie", "sClass": "center" },
                { "mDataProp": "Fats", "sClass": "center" },
                { "mDataProp": "Fiber", "sClass": "center" },
                {
                    "orderable": false,
                    "sWidth": "15%",
                    "mRender": function (data, type, full) {
                        var actionString = "";                        
                            actionString = actionString + "<a class=''  data-TagId=" + full.Id + "   data-name=" + full.Name + " onClick=IngredientViewModel.updateIngredient('" + full.Id + "',true)><i class='far fa-eye text-primary fa-sm mr-2'></i></a>"                                          
                            actionString = actionString + "<a class=''  data-TagId=" + full.Id + "   data-name=" + full.Name + " onClick=IngredientViewModel.updateIngredient('" + full.Id + "',false)><i class='far fa-edit text-primary fa-sm mr-2'></i></a>"                                              
                            actionString = actionString + "<a class='delete'  data-ingredientId=" + full.Id + "   data-name=" + full.Name + " onClick=IngredientViewModel.deleteIngredient(this)><i class='fas fa-trash-alt text-danger fa-sm'></i></a>"                       
                        return actionString;
                    }
                }
            ]
            ,
            columnDefs: [
                {
                    "defaultContent": " ",
                    "targets": "_all"
                }],
            "autoWidth": true
        });
    };

    // open update Ingredient modal with data
    this.updateIngredient = function (ingredientId, isReadonly) {
        this.openIngredientModal(ingredientId, isReadonly);

    };

    //open Ingredient model
    this.openIngredientModal = function (id, isReadonly) {
        GetData("/Ingredient/openmodal", openModalSuccess, {
            formId: "frmAddNewIngredient",
            id: id,
            isReadonly: isReadonly
        });
    };

    // open Ingredient success method
    this.updateIngredientSuccess = function (success, message) {
        if (success === false) {
            failAlert(message);
        }
        else {
            successAlert(message);
            closeCommonModel();
            refreshDataTable(tblIngredient);
        }
    };

    // delete Ingredient
    this.deleteIngredient = function (e) {
        var data = $(e).data();
        swal({
            title: "Are you sure?",
            text: "Please confirm you wish to delete Ingredient " + data.name,
            icon: "warning",
            buttons: true,
            dangerMode: true
        })
            .then((willDelete) => {
                if (willDelete) {
                    PostData("/Ingredient/deleteIngredient", this.deleteIngredientSuccess, { id: data.ingredientid });

                }
            });
    };

    //delete Ingredient success
    this.deleteIngredientSuccess = function (result) {
        if (result) {
            SwalSuccess("Record deleted successfully!");
            refreshDataTable(tblIngredient);
        }
        else {
            SwalError("Something went wrong!");
            refreshDataTable(tblIngredient);
        }
    };

    //#endregion Ingredien
};