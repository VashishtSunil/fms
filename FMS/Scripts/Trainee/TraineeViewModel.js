﻿var TraineeViewModel = new function () {
    var thisViewModel = this;

    //#region Trainee

    var tblTrainee = "tblTrainee";

    this.TraineeListView = function () {
        var responsiveDt = undefined;
        var breakpointDefinition = {
            tablet: 1024,
            phone: 480
        };
        $('#' + tblTrainee).dataTable({
            "bStateSave": false,
            "iDisplayLength": 10,
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            //for CSV buttons
            "dom": "Blfrtip",
            buttons: [
                {
                    extend: 'csv',
                    text: 'Export',
                    filename: function () {
                        var d = new Date();
                        var n = d.getTime();
                        return 'Trainee' + n;
                    }
                }],
            cache: true,
            "responsive": true,
            "bServerSide": true,
            "bProcessing": true,
            "bJqueryUI": true,
            "sPaginationType": "full_numbers",
            "aaSorting": [[0, "asc"]],
            "sAjaxSource": baseUrl + "/trainee/list",
            "preDrawCallback": function () {
                // Initialize the responsive datatables helper once.
                if (!responsiveDt) {
                    responsiveDt = new ResponsiveDatatablesHelper($('#' + tblTrainee), breakpointDefinition);
                }
            },
            "rowCallback": function (nRow, data) {

                responsiveDt.createExpandIcon(nRow);
            },
            "drawCallback": function () {
                responsiveDt.respond();
                $("a.edit").tooltip({
                    placement: 'top',
                    trigger: 'hover',
                    title: 'Edit Trainee'
                });
                $("a.edit").removeAttr("title");
                $("a.delete").tooltip({
                    placement: 'top',
                    trigger: 'hover',
                    title: 'Delete Trainee'
                });
                $("a.delete").removeAttr("title");
                //
                InitBootstrapSwitch();
            },
            "aoColumns": [
                { "mDataProp": "FullName", "sClass": "center" },
                { "mDataProp": "Email", "sClass": "center" },
                { "mDataProp": "Phone", "sClass": "center" },
                { "mDataProp": "UserTypeString", "sClass": "center" },

                {
                    "orderable": false,
                    "mRender": function (data, type, full) {
                        var actionHtml = "";
                        if (full.IsActive) {

                            actionHtml = "<a  class='bootstrapSwitch-checkbox text-primary'  data-url='/trainee/changetraineestatus'  data-id='" + full.Id + "' ><i class='fa fa-toggle-on  fa-lg'></i></a>";
                        }
                        else {
                            actionHtml = "<a  class='bootstrapSwitch-checkbox text-primary'  data-url='/trainee/changetraineestatus'  data-id='" + full.Id + "' ><i class='fas fa-toggle-off  fa-lg'></i></a>";
                        }
                        return actionHtml;
                    }
                },
                {
                    "orderable": false,
                    "sWidth": "15%",
                    "mRender": function (data, type, full) {
                        var actionString = "";
                        actionString = actionString + "<a class=''  data-TagId=" + full.Id + "   data-name=" + full.FirstName + " onClick=TraineeViewModel.updateTrainee('" + full.Id + "',true)><i class='far fa-eye text-primary fa-sm mr-2'></i></a>"
                        actionString = actionString + "<a class=''  data-TagId=" + full.Id + "   data-name=" + full.FirstName + " onClick=TraineeViewModel.updateTrainee('" + full.Id + "',false)><i class='far fa-edit text-primary fa-sm mr-2'></i></a>"
                        actionString = actionString + "<a class='delete'  data-userId=" + full.Id + "   data-name=" + full.FirstName + " onClick=TraineeViewModel.deleteTrainee(this)><i class='fas fa-trash-alt text-danger fa-sm'></i></a>"
                        return actionString;
                    }
                },
                {
                    "orderable": false,
                    "mRender": function (data, type, full) {
                        
                        var planActionString = "";
                        planActionString = planActionString + "<span><button onClick=TraineeViewModel.openPlanPurchaseModal('" + full.Id + "',false) class= 'btn btn-primary btn-sm'>₹ Pay</button></a></span>"
                        planActionString = planActionString + "<i class='fas fa-money-check text-primary fa-sm ml-3 data-toggle= 'tooltip'  title= ' View payment history'onClick=TraineeViewModel.openPaymentHistoryModal('" + full.Id + "')></i>"
                        return planActionString;
                    }
                }
            ]
            ,
            columnDefs: [
                {
                    "defaultContent": " ",
                    "targets": "_all"
                }],
            "autoWidth": true
        });
    };

    // open update User modal with data
    this.updateTrainee = function (userId, isReadonly) {
        this.openTraineeModal(userId, isReadonly);

    };

    //open User model
    this.openTraineeModal = function (id, isReadonly) {
        GetData("/Trainee/openmodal", openModalSuccess, {
            formId: "frmAddNewTrainee",
            id: id,
            isReadonly: isReadonly
        });
    };
    this.openPlanPurchaseModal = function (id, isReadonly) {

        GetData("/Trainee/purchaseplan", openModalSuccess, {
            formId: "frmAddNewTrainee",
            id: id,
            isReadonly: isReadonly
        });
    };


    this.PaymentHistoryListView = function (id) {
        debugger
        var responsiveDt = undefined;
        var breakpointDefinition = {
            tablet: 1024,
            phone: 480
        };
        $(tblPaymentHistory).dataTable({
            "bStateSave": false,
            "iDisplayLength": 10,
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            //for CSV buttons
            "dom": "Blfrtip",
            buttons: [
                {
                    extend: 'csv',
                    text: 'Export',
                    filename: function () {
                        var d = new Date();
                        var n = d.getTime();
                        return 'Trainee' + n;
                    }
                }],
            cache: true,
            "responsive": true,
            "bServerSide": true,
            "bProcessing": true,
            "bJqueryUI": true,
            "sPaginationType": "full_numbers",
            "aaSorting": [[0, "asc"]],
            "sAjaxSource": baseUrl + "/trainee/traineepaymenthistorylist?id=" + id,
            "preDrawCallback": function () {
                // Initialize the responsive datatables helper once.
                if (!responsiveDt) {
                    responsiveDt = new ResponsiveDatatablesHelper($(tblPaymentHistory), breakpointDefinition);
                }
            },
            "rowCallback": function (nRow, data) {

                responsiveDt.createExpandIcon(nRow);
            },
           
            "aoColumns": [
                { "mDataProp": "Name", "sClass": "center" },
                { "mDataProp": "Email", "sClass": "center" },
                { "mDataProp": "Phone", "sClass": "center" },
                { "mDataProp": "PaidPrice", "sClass": "center" },
                { "mDataProp": "PaymentModeTypeString", "sClass": "center" },
                { "mDataProp": "CreatedOnString", "sClass": "center" },
            ]
            ,
            columnDefs: [
                {
                    "defaultContent": " ",
                    "targets": "_all"
                }],
            "autoWidth": true
        });
    };

    this.openPaymentHistoryModal = function (id,) {
        debugger

        GetData("/Trainee/opentraineepaymenthistorymodal", openModalSuccess, {
            formId: "frmAddNewTrainee",
            id: id,
        });
    };

    // open User success method
    this.updateTraineeSuccess = function (success, message) {
        if (success === false) {
            failAlert(message);
        }
        else {
            successAlert(message);
            closeCommonModel();
            refreshDataTable(tblTrainee);
        }
    };

    // delete User
    this.deleteTrainee = function (e) {
        var data = $(e).data();
        swal({
            title: "Are you sure?",
            text: "Please confirm you wish to delete Trainee " + data.name,
            icon: "warning",
            buttons: true,
            dangerMode: true
        })
            .then((willDelete) => {
                if (willDelete) {
                    PostData("/Trainee/deleteTrainee", this.deleteTraineeSuccess, { id: data.userid });

                }
                refreshDataTable(tblTrainee);
            });
    };

    //status success

    this.statusSuccess = function (result) {
        if (result) {
            SwalSuccess("Status changed successfully!");
            refreshDataTable(tblTrainee)

        }
        else {
            SwalError("Something went wrong!");
            refreshDataTable(tblTrainee);
        }
    };



    //delete User success
    this.deleteTraineeSuccess = function (result) {
        if (result) {
            SwalSuccess("Record deleted successfully!");
            refreshDataTable(tblTrainee);
        }
        else {
            SwalError("Something went wrong!");
            refreshDataTable(tblTrainee);
        }
    };

    //send email success
    this.sendEmailSuccess = function (success, message) {
        if (success === false) {
            failAlert(message);
        }
        else {
            successAlert(message);
            setInterval('location.reload()', 3000);
        }
    };
    //#endregion Trainee
};