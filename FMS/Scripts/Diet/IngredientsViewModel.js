﻿var IngredientsViewModel = new function () {
    var thisViewModel = this;

    //#region Client

    var tblClient = "tblMealIngredients";

    this.ClientMealsIngredientsListView = function (mealId) {
        var responsiveDt = undefined;
        var breakpointDefinition = {
            tablet: 1024,
            phone: 480
        };
        $('#' + tblClient).dataTable({
            "bStateSave": false,
            "iDisplayLength": 10,
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            //for CSV buttons
            "dom": "Blfrtip",
            buttons: [
                {
                    extend: 'csv',
                    text: 'Export',
                    filename: function () {
                        var d = new Date();
                        var n = d.getTime();
                        return 'Client' + n;
                    }
                }],
            cache: true,
            "responsive": true,
            "bServerSide": true,
            "bProcessing": true,
            "bJqueryUI": true,
            "sPaginationType": "full_numbers",
            "aaSorting": [[0, "asc"]],
            "sAjaxSource": baseUrl + `/DietPlan/GetMeals?trainee=${mealId}`,
            "preDrawCallback": function () {
                // Initialize the responsive datatables helper once.
                if (!responsiveDt) {
                    responsiveDt = new ResponsiveDatatablesHelper($('#' + tblClient), breakpointDefinition);
                }
            },
            "rowCallback": function (nRow, data) {

                responsiveDt.createExpandIcon(nRow);
            },
            "drawCallback": function () {
                responsiveDt.respond();
                $("a.meal").tooltip({
                    placement: 'top',
                    trigger: 'hover',
                    title: 'Meals'
                });
                //
                InitBootstrapSwitch();
            },
            "aoColumns": [
                { "mDataProp": "Ingredient", "sClass": "center" },
                { "mDataProp": "Unit", "sClass": "center" },
                {
                    "orderable": false,
                    "sWidth": "15%",
                    "mRender": function (data, type, full) {
                        var actionString = "";
                        actionString = actionString + `<a class='meal' href='/DietPlan/Meals/${full.id}'><i class='far fa-eye text-primary fa-sm mr-2'></i></a>`
                        return actionString;
                    }
                }
            ]
            ,
            columnDefs: [
                {
                    "defaultContent": " ",
                    "targets": "_all"
                }],
            "autoWidth": true
        });
    };

    this.openClientMealIngredientModal = function (id, isReadonly) {
        GetData("/DietPlan/openMealIngredientModal", openModalSuccess, {
            formId: "frmAddNewClientMealIngredient",
            id: id,
        });
    };

    this.updateClientMealIngredientSuccess = function (success, message) {
        if (success === false) {
            failAlert(message);
        }
        else {
            successAlert(message);
            closeCommonModel();
            refreshDataTable(tblClient);
        }
    };
   
    //#endregion Client
};