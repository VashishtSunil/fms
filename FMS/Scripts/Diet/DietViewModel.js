﻿var DietViewModel = new function () {
    var thisViewModel = this;

    //#region Client

    var tblClient = "tblClient";

    this.ClientListView = function () {
        var responsiveDt = undefined;
        var breakpointDefinition = {
            tablet: 1024,
            phone: 480
        };
        $('#' + tblClient).dataTable({
            "bStateSave": false,
            "iDisplayLength": 10,
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            //for CSV buttons
            "dom": "Blfrtip",
            buttons: [
                {
                    extend: 'csv',
                    text: 'Export',
                    filename: function () {
                        var d = new Date();
                        var n = d.getTime();
                        return 'Client' + n;
                    }
                }],
            cache: true,
            "responsive": true,
            "bServerSide": true,
            "bProcessing": true,
            "bJqueryUI": true,
            "sPaginationType": "full_numbers",
            "aaSorting": [[0, "asc"]],
            "sAjaxSource": baseUrl + "/Client/list",
            "preDrawCallback": function () {
                // Initialize the responsive datatables helper once.
                if (!responsiveDt) {
                    responsiveDt = new ResponsiveDatatablesHelper($('#' + tblClient), breakpointDefinition);
                }
            },
            "rowCallback": function (nRow, data) {

                responsiveDt.createExpandIcon(nRow);
            },
            "drawCallback": function () {
                responsiveDt.respond();
                $("a.meal").tooltip({
                    placement: 'top',
                    trigger: 'hover',
                    title: 'Meal'
                });
                //
                InitBootstrapSwitch();
            },
            "aoColumns": [
                { "mDataProp": "FullName", "sClass": "center" },
                {
                    "orderable": false,
                    "sWidth": "15%",
                    "mRender": function (data, type, full) {
                        var actionString = "";
                        actionString = actionString + `<a class='' href='/DietPlan/Diets?trainee=${full.Id}'><i class='far fa-eye text-primary fa-sm mr-2'></i></a>`
                        return actionString;
                    }
                }
            ]
            ,
            columnDefs: [
                {
                    "defaultContent": " ",
                    "targets": "_all"
                }],
            "autoWidth": true
        });
    };


    var tblDiet = "tblDiet";
    this.DietListView = function (userId) {
        var responsiveDt = undefined;
        var breakpointDefinition = {
            tablet: 1024,
            phone: 480
        };
        $('#' + tblDiet).dataTable({
            "bStateSave": false,
            "iDisplayLength": 10,
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            //for CSV buttons
            "dom": "Blfrtip",
            buttons: [
                {
                    extend: 'csv',
                    text: 'Export',
                    filename: function () {
                        var d = new Date();
                        var n = d.getTime();
                        return 'Client' + n;
                    }
                }],
            cache: true,
            "responsive": true,
            "bServerSide": true,
            "bProcessing": true,
            "bJqueryUI": true,
            "sPaginationType": "full_numbers",
            "aaSorting": [[0, "asc"]],
            "sAjaxSource": baseUrl + `/DietPlan/DietsList?id=${userId}`,
            "preDrawCallback": function () {
                // Initialize the responsive datatables helper once.
                if (!responsiveDt) {
                    responsiveDt = new ResponsiveDatatablesHelper($('#' + tblDiet), breakpointDefinition);
                }
            },
            "rowCallback": function (nRow, data) {

                responsiveDt.createExpandIcon(nRow);
            },
            "drawCallback": function () {
                responsiveDt.respond();
                $("a.meal").tooltip({
                    placement: 'top',
                    trigger: 'hover',
                    title: 'Meal'
                });
                //
                InitBootstrapSwitch();
            },
            "aoColumns": [
                { "mDataProp": "DietName", "sClass": "center" },
                {
                    "orderable": false,
                    "sWidth": "15%",
                    "mRender": function (data, type, full) {
                        var actionString = "";
                        actionString = actionString + `<a class='' href='/DietPlan/Meals?diet=${full.Id}'><i class='far fa-eye text-primary fa-sm mr-2'></i></a>`
                        actionString = actionString + `<a class=''  onclick="DietViewModel.openDietModal('${full.Id}', ${ full.UserId }, false)"><i class='far fa-edit text-primary fa-sm mr-2'></i></a>`
                        actionString = actionString + `<a class='delete' data-id="${ full.Id } "   data-name="${ full.DietName } " onClick=DietViewModel.deleteDiet(this)><i class='fas fa-trash-alt text-danger fa-sm'></i></a>`
                        return actionString;
                    }
                }
            ],
           
            columnDefs: [
                {
                    "defaultContent": " ",
                    "targets": "_all"
                }],
            "autoWidth": true
        });
       
    };

    // open update User modal with data
    this.updateDiet = function (userId, isReadonly) {
        this.openDietModal(userId, isReadonly);

    };

    //open User model
    this.openDietModal = function (id, user, isReadonly) {
        GetData("/DietPlan/opendietmodal", openModalSuccess, {
            formId: "frmAddNewDiet",
            id: id,
            userId: user,
            isReadonly: isReadonly
        });
    };

    // open User success method
    this.updateDietSuccess = function (success, message) {
        if (success === false) {
            failAlert(message);
        }
        else {
            successAlert(message);
            closeCommonModel();
            refreshDataTable(tblDiet);
        }
    };

    

    this.openMealModel = () => {
        GetData("/DietPlan/openmodal", openModalSuccess, {
            formId: "frmAddNewClient",
            id: id,
            isReadonly: isReadonly
        });
    };

    // delete User
    this.deleteDiet = function (e) {
        var data = $(e).data();
        swal({
            title: "Are you sure?",
            text: "Please confirm you wish to delete Diet " + data.name,
            icon: "warning",
            buttons: true,
            dangerMode: true
        })
            .then((willDelete) => {
                if (willDelete) {
                    PostData("/DietPlan/deleteDiet", this.deleteDietSuccess, { id: data.id });
                }
                refreshDataTable(tblClient);
            });
    };

    //delete User success
    this.deleteDietSuccess = function (result) {
        if (result) {
            SwalSuccess("Record deleted successfully!");
        }
        else {
            SwalError("Something went wrong!");
        }
        refreshDataTable(tblDiet);
    };

    //#endregion Diet
};