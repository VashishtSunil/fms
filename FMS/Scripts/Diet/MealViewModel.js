﻿var MealViewModel = new function () {
    var thisViewModel = this;

    //#region Client

    var tblClient = "tblClientMeals";

    this.ClientMealsListView = function (dietId) {
        var responsiveDt = undefined;
        var breakpointDefinition = {
            tablet: 1024,
            phone: 480
        };
        $('#' + tblClient).dataTable({
            "bStateSave": false,
            "iDisplayLength": 10,
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            //for CSV buttons
            "dom": "Blfrtip",
            buttons: [
                {
                    extend: 'csv',
                    text: 'Export',
                    filename: function () {
                        var d = new Date();
                        var n = d.getTime();
                        return 'Client' + n;
                    }
                }],
            cache: true,
            "responsive": true,
            "bServerSide": true,
            "bProcessing": true,
            "bJqueryUI": true,
            "sPaginationType": "full_numbers",
            "aaSorting": [[0, "asc"]],
            "sAjaxSource": baseUrl + `/DietPlan/GetMeals?diet=${dietId}`,
            "preDrawCallback": function () {
                // Initialize the responsive datatables helper once.
                if (!responsiveDt) {
                    responsiveDt = new ResponsiveDatatablesHelper($('#' + tblClient), breakpointDefinition);
                }
            },
            "rowCallback": function (nRow, data) {

                responsiveDt.createExpandIcon(nRow);
            },
            "drawCallback": function () {
                responsiveDt.respond();
                $("a.meal").tooltip({
                    placement: 'top',
                    trigger: 'hover',
                    title: 'Meals'
                });
                //
                InitBootstrapSwitch();
            },
            "aoColumns": [
                { "mDataProp": "MealName", "sClass": "center" },
                {
                    "orderable": false,
                    "sWidth": "15%",
                    "mRender": function (data, type, full) {
                        var actionString = "";
                        actionString = actionString + `<a class='meal' href='/DietPlan/Ingredients/${full.Id}'><i class='far fa-eye text-primary fa-sm mr-2'></i></a>`
                        return actionString;
                    }
                }
            ]
            ,
            columnDefs: [
                {
                    "defaultContent": " ",
                    "targets": "_all"
                }],
            "autoWidth": true
        });
    };

    this.addClientMeal = function (dietId) {
        $.ajax({
            type: "Get",
            url: `/DietPlan/addMeal?diet=${dietId}`,
            success: function (html) {
                $("#mealsSection").append(html);
            },
            error: () => failAlert("Something went wrong"),
        });
        
    }

    this.removeMeal = this.removeMealIngredient = function (e, mealId) {

        let form = $(e).closest('div.meal-container');
       
        fetch(`/DietPlan/DeleteMeal?mealId=${mealId}`,{
            method: "DELETE",
            headers: { 'Content-Type': 'application/json', 'Accept': 'application/json' },
        }).then((response) => {
            if (response) {
                form.remove();
            }
        }).catch(console.error)

    }

    this.addMealIngredient = function (e, mealId) {
        $.ajax({
            type: "Get",
            url: `/DietPlan/addMealIngredient?mealId=${mealId}`,
            success: function (html) {
                $($(e).closest('.card-body')).find('.ingredient-section').append(html);
            },
            error: () => failAlert("Something went wrong"),
        });

    }

    this.saveMealIngredients = function (e) {

        let forms = $("#mealsSection").find('form.ingredient-form')
            //$($(e).closest('.card-body')).find('form');
        let ingredients = [];
        $.each(forms, (i, item) => {
            ingredients.push($(item).serializeArray().reduce((o, kv) => ({ ...o, [kv.name]: kv.value * 1 }), {}));
        })
        fetch(`/DietPlan/SaveMealIngredient`, {
            method: "POST",
            headers: { 'Content-Type': 'application/json', 'Accept': 'application/json' },
            body: JSON.stringify( ingredients )
        }).then((response) => {
            if (response.status == 200) {
                successAlert("Action performed successfully");
            }else{
                failAlert("Spmething went wrong.");
            }
        }).catch(console.error)

    }

    this.removeMealIngredient = function (e, ingredientId) {
        let form = $(e).closest('form.ingredient-form');
        fetch(`/DietPlan/DeleteMealIngredient?ingredientId=${ingredientId}`,{
            method: "DELETE",
            headers: { 'Content-Type': 'application/json', 'Accept': 'application/json' },
        }).then((response) => {
            if (response) {
                form.remove();
            }
        }).catch(console.error)

    }
   
    //#endregion Client
};