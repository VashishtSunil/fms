﻿let SubscriptionViewModel = new function () {
    let thisViewModel = this;


        //#region Payment History
        let tblIngredient = "tblIngredient";
        this.IngredientListView = function (data) {
            let responsiveDt = undefined;
            let breakpointDefinition = {
                tablet: 1024,
                phone: 480
            };
            $('#' + tblIngredient).dataTable({
                "bStateSave": false,
                "iDisplayLength": 10,
                "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
                //for CSV buttons
                "dom": "Blfrtip",
                buttons: [
                    {
                        extend: 'csv',
                        text: 'Export',
                        filename: function () {
                            let d = new Date();
                            let n = d.getTime();
                            return 'Subscription' + n;
                        }
                    }],
                cache: true,
                "responsive": true,
                "bServerSide": true,
                "bProcessing": true,
                "bJqueryUI": true,
                "sPaginationType": "full_numbers",
                "aaSorting": [[0, "asc"]],
                "sAjaxSource": baseUrl + "/Subscription/list",
                "preDrawCallback": function () {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveDt) {
                        responsiveDt = new ResponsiveDatatablesHelper($('#' + tblIngredient), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow, data) {

                    responsiveDt.createExpandIcon(nRow);
                },
                "drawCallback": function () {
                    responsiveDt.respond();
                    $("a.edit").tooltip({
                        placement: 'top',
                        trigger: 'hover',
                        title: 'Edit Ingredient'
                    });
                    $("a.edit").removeAttr("title");
                    $("a.delete").tooltip({
                        placement: 'top',
                        trigger: 'hover',
                        title: 'Delete Ingredient'
                    });
                    $("a.delete").removeAttr("title");
                    InitBootstrapSwitch();
                },
                "aoColumns": [
                    { "mDataProp": "PaidPrice", "sClass": "center" },
                    { "mDataProp": "PaymentMode", "sClass": "center" },
                    { "mDataProp": "PaymentStatus", "sClass": "center" },
                    { "mDataProp": "CreatedOn", "sClass": "center" },
                   
                    {
                        "orderable": false,
                        "sWidth": "15%",
                        "mRender": function (data, type, full) {
                            let actionString = "";
                            actionString = actionString + "<a class=''  data-TagId=" + full.Id + "   data-name=" + full.PaidPrice + " onClick=IngredientViewModel.updateIngredient('" + full.Id + "',true)><i class='far fa-eye text-primary fa-sm mr-2'></i></a>"
                            actionString = actionString + "<a class='delete'  data-ingredientId=" + full.Id + "   data-name=" + full.Name + " onClick=IngredientViewModel.deleteIngredient(this)><i class='fas fa-trash-alt text-danger fa-sm'></i></a>"
                            return actionString;
                        }
                    }
                ]
                ,
                columnDefs: [
                    {
                        "defaultContent": " ",
                        "targets": "_all"
                    }],
                "autoWidth": true
            });
        };


        //#endregion Payment History



    //Open Subscription Charge Modal
    this.openSubscriptionChargeModal = function (planId) {
   
        PostData("/Subscription/opensubscriptionmodal?planId=" + planId,
            openModalSuccess, { formId: "frmAddpaymentdetails" }, null, "subscription-modal-button");
    };
    //check for enter details are valid or not for card
    this.saveClientCard = function () {
    

        $("#card-token").val("");
        let txtCardNumber = $('#CardNumber');
        let txtExpiryMonth = $('#ExpirationMonth');
        let txtExpiryYear = $('#ExpirationYear');
        let txtCVC = $('#Cvc');
        let returnStatus = true;

        if (txtCardNumber.val() === "") {
            returnStatus = false;
            txtCardNumber.siblings("span:eq(1)").show();
        }
        else {
            txtCardNumber.siblings("span:eq(1)").hide();
            //validation for cvv allow only numeric value
            let isValid = /^\d*$/.test(txtCardNumber.val());
            if (isValid) {
                txtCardNumber.siblings("span:eq(0)").hide();
                returnStatus = true;
            }
            else {
                txtCardNumber.siblings("span:eq(0)").show();
                returnStatus = false;
            }
        }
        if (txtExpiryMonth.val() === "") {
            returnStatus = false;
            txtExpiryMonth.siblings("span:eq(1)").show();
        }
        else {
            txtExpiryMonth.siblings("span:eq(1)").hide();
        }
        if (txtExpiryYear.val() === "") {
            returnStatus = false;
            txtExpiryYear.siblings("span:eq(1)").show();
        }
        else {
            txtExpiryYear.siblings("span:eq(1)").hide();
        }
        if (txtCVC.val() === "") {
            returnStatus = false;
            txtCVC.siblings("span:eq(1)").show();
        }
        else {
            txtCVC.siblings("span:eq(1)").hide();
            //validation for cvv allow only numeric value
            isValid = /^\d*$/.test(txtCVC.val());
            if (isValid) {
                txtCVC.siblings("span:eq(0)").hide();
                returnStatus = true;
            }
            else {
                txtCVC.siblings("span:eq(0)").show();
                returnStatus = false;
            }
        }
        
        if (!returnStatus) {
            return false;
        } else {

            thisViewModel.paymentMethod = "";// Make it dynamic incase we use other payment provider TODO
            if (thisViewModel.paymentMethod.toLowerCase() === "stripe") {
                //set it on upper drop down change even + on login -- remains to implement 
                Stripe.setPublishableKey($("#stripe-publishableKey").val());
                // create stripe token for card
                Stripe.card.createToken({
                    number: $('#CardNumber').val(),
                    cvc: $('#Cvc').val(),
                    exp_month: $('#ExpirationMonth').val(),
                    exp_year: $('#ExpirationYear').val()
                }, thisViewModel.stripeResponseHandler);
            }
        }
    };
    // stripe Response Handler 
    this.stripeResponseHandler = function (status, response) {
       
        let $form = $('#frmAddpaymentdetails');
        if (thisViewModel.isCreditCardInfoAdded) {
            $form.find('button').prop('disabled', true);
        }

        if (response.error) {
            if (thisViewModel.isCreditCardInfoAdded) {
                stopLaddaSpinWheel("btn-AddPaymentDetail-button");
                failBigBox("Card  Not  Added", response.error.message);
                $form.find('button').prop('disabled', false); // Re-enable submission
            }
            else if (thisViewModel.isCreditCardAddedUsingPaymentModule) {
                stopLaddaSpinWheel("btn-MakePaymentForPlaceOrder-button");
                OrderPaymentViewModel.updatedCreditCardInfoSuccess(false, response.error.message, thisViewModel.paymentMethod);
            }
            else {
                stopLaddaSpinWheel("btn-AddNewClient-button");
                $('.payment-errors').text(response.error.message);
            }
        }
        else { // Token was created! 
            $('.payment-errors').text("");
            // Token was created!
            this.paymentDetailAddedOnly = true;
            // Get the token ID:
            $('.payment-errors').text("");
            let token = response.id;
            let stripeCustomerDetailModel = {
                UserId: $('#Subsciption-UserId').val(),
                CardNumber: $('#CardNumber').val(),
                ExMonth: $('#ExMonth').val(),
                ExYear: $('#ExYear').val(),
                Cvc: $('#Cvc').val(),
                StripeToken: token,
                PaymentMethod: thisViewModel.paymentMethod,
                PlanId: $('#Subsciption-PlanId').val()
            };
            //post data to create customer and card 
            PostData("/subscription/chargesubscription", thisViewModel.chargesubscriptionSuccess, { stripeCustomerDetailModel: stripeCustomerDetailModel }, null, "btn-ChargeSubscription");

        }
    };

    //charge subscription Success
    this.chargesubscriptionSuccess = function (result) {
        //let msgArray = result.msg.split(",");
        if (result.success) {
            // if (msgArray[1] !== "") {
            successSmallBox("Subscription", result.message);
            // PersonViewModel.latestPaymentDetailsView(msgArray[1]);
            //check  whether stripe card is last one hide selected customer charge  option
            //if ($("#paymentDetail .panel-default").length === 1) {
            //    // to hide the 
            //    $('#' + 'charge-' + $("#Id").val()).removeClass("hidden");
            //        //}
            //    }
            //    else {
            //        successSmallBox("Card Added", msgArray[0]);
            setTimeout(function () { window.location.reload(); }, 2000);
            //    }
        }
        else {
            failSmallBox("Subscription no Done", result.message);
        }
    }


    this.chargesubscriptionByCashSuccess = function (result) {
   
        if (result.success) {

            successAlert("Subscription", result.message);
         
          
            setTimeout(function () { window.location.reload(); }, 2000);
        
        }
        else {
            failAlert("Subscription no Done", result.message);
        }
    }

    //Cancel User Subscription Modal
    this.openCancelSubscriptionModal = function (planId, longOwnerId) {
        PostData("/subscription/cancelusersubscriptionmodal?planId=" + planId + "&longOwnerId=" + longOwnerId,
            openModalSuccess, { formId: "frmCancelSubscription" }, null, "subscription-modal-button");
    };
    // cancel
    this.cancelSubscriptionSubmit = function () {
        let txtReasonForUnsubscription = $("#ReasonForUnsubscription");
        if (txtReasonForUnsubscription.val().trim() === "") {
            returnStatus = false;
            //txtReasonForUnsubscription.siblings("span:eq(1)").show();
        } else {
            // txtReasonForUnsubscription.siblings("span:eq(1)").hide();
            $('#btn-frmCancelSubscription-submit').click();
        }
    };

    //Delete user subscription success
    this.deleteSubscriptionSuccess = function (result) {
        if (result) {
            successSmallBox("Subscription", "User Unsubscribed Successfully");
            setTimeout(function () { window.location.reload(); }, 2000);
        }
        else {
            failSmallBox("Subscription", "Something went wrong");
        }
    };

    //charge the subscription amount
    this.chargeSubscription = function (element) {
        let formId = $(element).attr("data-formId");
        if (formId != undefined && formId != null && formId == "") {
            if ($("#" + formId).valid()) {
                startLaddaSpinWheel("btn-ChargeSubscription");
                $("#btn-" + formId + "-submit").click();
            }
        }
    };

    //success function for charge subscription
    this.chargeSubscriptionSuccess = function (success, message) {
        stopLaddaSpinWheel("btn-ChargeSubscription");
        if (!success) {
            failAlert(message);
        }
        else {
            successAlert(message);
            closeCommonModel();
            location.reload();
        }
    };



    this.savePayementByCash = function () {
      
        thisViewModel.paymentMethod = "cash";
        let paymentModel = {                   
            PaymentMethod: thisViewModel.paymentMethod,
            PlanId: $('#Subsciption-PlanId').val(),
            Id: $('#Subsciption-UserId').val(),
            Charge: $('#Price').val()
        };
        PostData("/subscription/chargesubscription", thisViewModel.chargesubscriptionByCashSuccess, { paymentModel: paymentModel }, null, "btn-ChargeSubscriptionByCash");
    };


    // #Region

    // #End region
};



