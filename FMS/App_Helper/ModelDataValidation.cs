﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

using System.Collections.Generic;
using FMS.Services.Classes.RequestResponse;
using System.Web.Http;
using FMS.Core.Enums;
using FMS.Core.Models;
using FMS.Services.Classes.ApiUser;
using FMS.Services.Classes.Token;

/// <summary>
///  Null Check Attribute Class
/// </summary>
public class CheckModelDataAttribute : ActionFilterAttribute
{
    public override void OnActionExecuting(HttpActionContext actionContext)
    {
        var model = actionContext.ActionArguments["model"];
        if (model != null)
        {
            var modelAction = ((FMS.Core.Payloads)model).Action;

            if (!string.IsNullOrEmpty(modelAction))
            {
         
                if (!(Enum.GetNames(typeof(Actions)).Any(x => x.ToLower() == modelAction.ToLower())))
                {
                    
                    actionContext.Response = actionContext.Request.CreateErrorResponse(
                                  HttpStatusCode.BadRequest, "Invalid Action");
                }
                else
                {
                    //check for all in 
                    if (modelAction.ToLower() == Actions.Update.GetDescription().ToLower())
                    {
                        if (!actionContext.ModelState.IsValid)
                            actionContext.Response = actionContext.Request.CreateErrorResponse(
                                     HttpStatusCode.BadRequest, actionContext.ModelState);
                    }
                }
            }
            else
            {
                actionContext.Response = actionContext.Request.CreateErrorResponse(
                            HttpStatusCode.BadRequest, "Action is Empty");
            }
        }
        else
        {
            actionContext.Response = actionContext.Request.CreateErrorResponse(
                            HttpStatusCode.BadRequest, "Model is not valid");
        }
    }
}


/// <summary>
///  Validate Api User
/// </summary>
public class ValidateAPIUserAttribute : ActionFilterAttribute
{
    public override void OnActionExecuting(HttpActionContext actionContext)
    {
        var model = actionContext.ActionArguments["model"];
        if (model != null)
        {
            // Initializing variables with model data
            string modelClientId = ((FMS.Core.Payloads)model).UserId;
            string modelClientSecret = ((FMS.Core.Payloads)model).Password;
            string modelApiKey = ((FMS.Core.Payloads)model).ApiKey;
            string modelClientKey = ((FMS.Core.Payloads)model).Key;

            //Initializing values in ApiUserModel
            ApiUserModel apiUserModel = new ApiUserModel() {
                ClientId = modelClientId,
                ClientSecret = modelClientSecret,
                ApiKey = modelApiKey,
                ClientKey = modelClientKey
            };

            //Null Check model data 
            if ( 
                !String.IsNullOrWhiteSpace(modelClientId) &&
                !String.IsNullOrWhiteSpace(modelClientSecret) &&
                !String.IsNullOrWhiteSpace(modelApiKey) &&
                !String.IsNullOrWhiteSpace(modelClientKey) 
                )
            {
                //Checking keys 
                if (!ApiUserManager.IsAPICredentilaValid(apiUserModel))
                    actionContext.Response = actionContext.Request.CreateErrorResponse(
                            HttpStatusCode.BadRequest, "Invalid API Keys please contact admin.");
            }
            else
            {
                actionContext.Response = actionContext.Request.CreateErrorResponse(
                            HttpStatusCode.BadRequest, "Invalid API Keys please contact admin");
            }
        }
        else
        {
            actionContext.Response = actionContext.Request.CreateErrorResponse(
                            HttpStatusCode.BadRequest, "Model is not valid");
        }
    }
}

/// <summary>
///  Log Api User
/// </summary>
public class LogAPIUserAttribute : ActionFilterAttribute
{
    public override void OnActionExecuting(HttpActionContext actionContext)
    {
        var model = actionContext.ActionArguments["model"];
        string token = actionContext.Request.Headers.Authorization.Scheme;
        //Model Null check
        if (model != null)
        {
            if(TokenManager.IsTokenValid(token))
            { 
                if(TokenManager.IsTokenExpired(token))
                {
                    //Initializing values in RequestResponseModel
                    RequestResponseModel requestResponseModel = new RequestResponseModel()
                    {
                        Token = token,
                        UniqId = token,
                        Method = actionContext.Request.Method.ToString(),
                        Request = actionContext.Request.RequestUri.ToString(),
                        CreatedBy = token,
                    };
                    bool response = RequestResponseManager.UpdateRequestResponse(requestResponseModel);

                    //Error Checking
                    if (!response)
                    {
                        actionContext.Response = actionContext.Request.CreateErrorResponse(
                                    HttpStatusCode.BadRequest, "Error occurred ");
                    }
                    else
                    {
                        //Setting RequestResponseModel in Request.Properties as RequestResponseId
                        actionContext.Request.Properties.Add(new KeyValuePair<string, object>("RequestResponseId", requestResponseModel));
                    }
                }
                else
                {
                    actionContext.Response = actionContext.Request.CreateErrorResponse(
                               HttpStatusCode.BadRequest, "Token Is Expired");
                }

            }
            else
            {
                actionContext.Response = actionContext.Request.CreateErrorResponse(
                               HttpStatusCode.BadRequest, "Un-Authorized");
            }
           


            ////Converting model data in json
            //var jsonData = Newtonsoft.Json.JsonConvert.SerializeObject(model);

            ////Initializing values in ActivityModel
            //ActivityModel activityModel = new ActivityModel()
            //{
            //    Activity = ((CouponFactory.Core.Payloads)model).Action,
            //    DeviceId = DeviceManager.GetIpAndMacAddress().FirstOrDefault().MacAddress,
            //    IpAddress = DeviceManager.GetIpAndMacAddress().FirstOrDefault().IpAddress,
            //    Lat = null,
            //    Lng = null,
            //    Data = jsonData,
            //    CreatedBy = TokenManager.ParseToken(token),
            //};

            ////Updating Activity and Request Response table
            //bool activity = ActivityManager.UpdateActivity(activityModel);
            
        }
        else
        {
            actionContext.Response = actionContext.Request.CreateErrorResponse(
                            HttpStatusCode.BadRequest, "Model is not valid");
        }
    }
    

    public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
    {
        //Getting RequestResponseModel from HttpActionExecutedContext.Request.Properties
        RequestResponseModel requestResponse = (RequestResponseModel)actionExecutedContext.Request.Properties["RequestResponseId"];

        //Initializing values in RequestResponseModel
        RequestResponseModel requestResponseModel = new RequestResponseModel()
        {
            Id = requestResponse.Id,
            Response = actionExecutedContext.Response.Content.ReadAsStringAsync().Result,
            ResponseCode = actionExecutedContext.Response.StatusCode.ToString(),
        };

        //Updating Database
        if (!RequestResponseManager.UpdateRequestResponse(requestResponseModel))
        {
            actionExecutedContext.Response = actionExecutedContext.Request.CreateErrorResponse(
                        HttpStatusCode.BadRequest, "Error occurred ");
        }

    }
}

public class IsTokenValidAttribute : ActionFilterAttribute
{
    public override void OnActionExecuting(HttpActionContext actionContext)
    {
        string token = actionContext.Request.Headers.Authorization.Scheme;
        if (TokenManager.IsTokenValid(token))
        {
            if (!TokenManager.IsTokenExpired(token))
            {
                actionContext.Response = actionContext.Request.CreateErrorResponse(
                          HttpStatusCode.Unauthorized, "Token Is Expired");
            }
        }
        else
        {
            actionContext.Response = actionContext.Request.CreateErrorResponse(
                           HttpStatusCode.Unauthorized, "Un-Authorized");
        }
    }
}



