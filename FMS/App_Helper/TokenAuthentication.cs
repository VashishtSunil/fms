﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http.Filters;
using FMS.Core.Models;
using FMS.Services.Classes.RequestResponse;
using FMS.Services.Classes.Token;
using System.Web.Http;
using System.Threading.Tasks;
using System.Threading;
using System.Net.Http.Headers;
using System.Collections.Generic;

   /// <summary>
    /// Save Token in Db
    /// </summary>
    public class SaveTokenAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            var model = actionExecutedContext.ActionContext.ActionArguments["model"];
            object tokenObject; string token = "";
            if(actionExecutedContext.Request.Properties.TryGetValue("Token",out tokenObject))
            {
                token = (string)actionExecutedContext.Request.Properties["Token"];
            }
            //Initializing values in RequestResponseModel
            RequestResponseModel requestResponseModel = new RequestResponseModel()
            {
                Token = token,
                UniqId = ((FMS.Core.Payloads)model).UserId,
                Method = actionExecutedContext.Request.Method.ToString(),
                Request = actionExecutedContext.Request.RequestUri.ToString(),                
                Response = actionExecutedContext.Response.Content.ReadAsStringAsync().Result,
                ResponseCode = actionExecutedContext.Response.StatusCode.ToString(),
            };

            //Updating Database
            if (!RequestResponseManager.UpdateRequestResponse(requestResponseModel))
            {
                actionExecutedContext.Response = actionExecutedContext.Request.CreateErrorResponse(
                            HttpStatusCode.BadRequest, "Error occurred ");
            }
        }
    }



    /// <summary>
    /// Token Authentication Failure Response Phrase and Code
    /// </summary>
    public class AuthenticationFailureResult : IHttpActionResult
    {

        public string ReasonPhrase { get; private set; }

        public HttpRequestMessage Request { get; private set; }

        public HttpStatusCode Status { get; private set; }

        public string Message { get; private set; }

        public AuthenticationFailureResult(HttpRequestMessage request, string message, 
            string reasonPhrase, HttpStatusCode status = HttpStatusCode.Unauthorized)
        {
            ReasonPhrase = reasonPhrase;
            Request = request;           
            Message = message;
            Status = status;
        }

        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            return Task.FromResult(Execute());
        }

        private HttpResponseMessage Execute()
        {
            var response = Request.CreateResponse(Status, new Dictionary<string, object>() { { "Message", Message } });
            response.RequestMessage = Request;
            response.ReasonPhrase = ReasonPhrase;
            return response;
        }
    }

    /// <summary>
    /// Add Challenge On Unauthorized Result 
    /// </summary>
    public class AddChallengeOnUnauthorizedResult : IHttpActionResult
    {
        public AddChallengeOnUnauthorizedResult(AuthenticationHeaderValue challenge, IHttpActionResult innerResult)
        {
            Challenge = challenge;
            InnerResult = innerResult;
        }

        public AuthenticationHeaderValue Challenge { get; private set; }

        public IHttpActionResult InnerResult { get; private set; }

        public async Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            HttpResponseMessage response = await InnerResult.ExecuteAsync(cancellationToken);

            if (response.StatusCode == HttpStatusCode.Unauthorized)
            {
                // Only add one challenge per authentication scheme.
                if (!response.Headers.WwwAuthenticate.Any((h) => h.Scheme == Challenge.Scheme))
                {
                    response.Headers.WwwAuthenticate.Add(Challenge);
                }
            }

            return response;
        }
    }

