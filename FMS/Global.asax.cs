﻿using FMS.App_Start;
using FMS.Data.Context;
using Rollbar;
using Serilog;
using Serilog.Core;
using Serilog.Events;
using System;
using System.Configuration;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
namespace FMS
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            StartLogging();
            MigrateManagementContext();
            //vashishtsunil007@gmail.com
            RollbarLocator.RollbarInstance.Configure(new RollbarConfig("e543f6e7d0a247c6b0aae28755cdd1c5"));
            RollbarLocator.RollbarInstance.Info("Rollbar is configured properly For FMS");
        }

        /// <summary>
        /// Start Serilog instance.
        /// </summary>
        public void StartLogging()
        {
            //Initialize Serilog instance.
            var logEventLevel = (LogEventLevel)Enum.Parse(typeof(LogEventLevel), ConfigurationManager.AppSettings["LogLevel"]);
            var logSwitch = new LoggingLevelSwitch(logEventLevel);
            Log.Logger = new LoggerConfiguration()
                        //.WriteTo.LiterateConsole()
                        .MinimumLevel.ControlledBy(logSwitch)
                        .WriteTo.File(AppDomain.CurrentDomain.BaseDirectory + "AppData\\Log\\" + ConfigurationManager.AppSettings["EnvironmentCode"] + "-" + "{Date}.txt",
                        rollingInterval: RollingInterval.Hour, rollOnFileSizeLimit: true, fileSizeLimitBytes: 10048576,
                        shared: true, retainedFileCountLimit: null,
                        outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{Level}] [{SourceContext}] {Message}{NewLine}{Exception}")
                       .CreateLogger();
        }

        /// <summary>
        /// Migrate Management Context
        /// </summary>
        private static void MigrateManagementContext()
        {
            try
            {
                System.Data.Entity.Database.SetInitializer(new System.Data.Entity.MigrateDatabaseToLatestVersion<FMSContext, Data.Configuration.Configuration>());
                var configuration = new FMS.Data.Configuration.Configuration();
                configuration.AutomaticMigrationsEnabled = true;
                configuration.AutomaticMigrationDataLossAllowed = true;
                var migrator = new System.Data.Entity.Migrations.DbMigrator(configuration);
                migrator.Update();            

            }
            catch (Exception ex)
            {
                RollbarLocator.RollbarInstance.Error(ex);
                Log.Error(ex, "MigrateManagementContext");
            }
        }
    }
}
