﻿using FMS.App_Start;
using FMS.Core.Models;
using FMS.Services.Classes;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace FMS.Controllers
{
    [AuthorizationFilter]
    public class RoleController : BaseController
    {
        #region Role
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Get Role List
        /// </summary>
        /// <param name="parms"></param>
        /// <returns></returns>
        public async Task<ActionResult> List(jQueryDataTableParamModel parms)
        {
            try
            {
                var Roles = await RoleManager.GetDataTableList(parms);
                return Json(new
                {
                    aaData = Roles.Item1,
                    iTotalRecords = Roles.Item3,
                    iTotalDisplayRecords = Roles.Item3
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "RoleController =>> List");
                return ReturnAjaxErrorMessage(ex.Message);
            }
        }

        /// <summary>
        /// Open Role Modal
        /// </summary>
        /// <param name="id"></param>
        /// <param name="isReadOnly"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult OpenModal(long id, bool isReadOnly)
        {
            if (!Request.IsAjaxRequest())
                return null;
            RoleModel roleModel = new RoleModel();
            ViewBag.Header = (id > 0) ? "Edit Role" : "Add New Role";
            if (id > 0)
            {
                roleModel = RoleManager.GetById(id);
            }
            else
            {
                roleModel.IsDeleted = false;
            }

            // Get All Function List
            roleModel.LstFunction = FunctionManager.GetAllFunctionList(id);
            roleModel.IsReadMode = isReadOnly;
            //check for read only 
            if (isReadOnly)
            {
                ViewBag.Header = "View Role";
            }
            return PartialView("~/Views/Role/_AddEdit.cshtml", roleModel);
        }

        /// <summary>
        /// Add new role
        /// </summary>
        ///  <param name="roleModel"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Add(RoleModel roleModel)
        {
            ModelState.Remove("Id");
            string successMessage = (roleModel.Id != 0) ? "Role Updated Successfully!" : " Role Added Successfully!";
            try
            {
                if (ModelState.IsValid)
                {
                    roleModel.OwnerId = Convert.ToInt32(SessionHelper.UserId);
                    var result = RoleManager.Update(roleModel);
                    if (result)
                    {
                        // Role Function All Delete
                        RoleManager.DeleteRoleFunction(roleModel.Id);

                        //update -- only add data for marked checkbox
                        foreach (var roleFunction in roleModel.LstFunction.Where(x => x.IsDelete || x.IsCreate || x.IsView || x.IsEdit).ToList())
                        {
                            RoleFunctionModel roleFunctionModel = new RoleFunctionModel()
                            {
                                RoleId = roleModel.Id,
                                FunctionId = roleFunction.Id,
                                IsCreate = roleFunction.IsCreate,
                                IsDelete = roleFunction.IsDelete,
                                IsEdit = roleFunction.IsEdit,
                                IsView = roleFunction.IsView

                            };
                            RoleManager.UpdateRoleFunction(roleFunctionModel);

                        }

                        return Json(new
                        {
                            success = true,
                            message = successMessage,
                            JsonRequestBehavior.AllowGet
                        });
                    }
                }
                return Json(new
                {
                    success = false,
                    message = "Something Went Wrong!",
                    JsonRequestBehavior.AllowGet
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    success = false,
                    message = "Something Went Wrong!",
                    JsonRequestBehavior.AllowGet
                });
            }
        }

        /// <summary>
        /// Delete Role
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool DeleteRole(long id)
        {
            try
            {
                var result = RoleManager.DeleteRole(id);
                if (result)
                {
                    UserManager.DeleteAllUserByRoleId(id);

                }
                return (result) ? true : false;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "RoleController Delete");
                return false;
            }
        }

        /// <summary>
        /// Change Role Status
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool ChangeRoleStatus(long id)
        {
            try
            {
                return (RoleManager.UpdateRoleStatus(id)) ? true : false;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "RoleController=>ChangeStatus ");
                return false;
            }
        }

        /// <summary>
        /// check whether role name is duplicate or not
        /// </summary>
        /// <param name="roleModel"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        public JsonResult IsRoleDuplicate(RoleModel roleModel)
        {
            return Json(!RoleManager.IsRoleDuplicate(roleModel));
        }

        #endregion
    }
}