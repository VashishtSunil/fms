﻿using FMS.App_Start;
using FMS.Core.Helper;
using FMS.Core.Models;
using FMS.Services.Classes;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;


namespace FMS.Controllers
{
    public class PlanController : BaseController
    {
        #region Method

        #region Plan

        /// <summary>
        /// view for showing plan
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// view for plan screen
        /// </summary>
        /// <param name="planModel"></param>
        /// <returns></returns>
        [AllowAnonymous]
        public async Task<ActionResult> AddPlan(PlanModel planModel)
        {
            ModelState.Clear();
            planModel = planModel == null ? new PlanModel() : planModel;
            bool isReadMode = planModel.IsReadMode;
            if (planModel.Id > 0)
            {
                planModel = await PlanManager.GetPlanById(planModel.Id);
                planModel = planModel == null ? new PlanModel() : planModel;
                //planModel.LstFeature = await PlanManager.GetPlanFeatures(planModel.Id);
            }
            planModel.IsReadMode = isReadMode;
            string currentUserId = !string.IsNullOrEmpty(SessionHelper.UserId) ? SessionHelper.UserId : "";
            planModel.CreatedBy = string.IsNullOrEmpty(planModel.CreatedBy) ? currentUserId : planModel.CreatedBy;
            planModel.ModifiedBy = string.IsNullOrEmpty(planModel.ModifiedBy) ? currentUserId : planModel.ModifiedBy;

            return View("~/Views/Plan/AddPlan.cshtml", planModel);
        }

        /// <summary>
        /// plan feature view
        /// </summary>
        /// <param name="planId"></param>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult PlanFeature(int planId, bool isReadMode = false)
        {
            PlanFeatureModel planFeatureModel = new PlanFeatureModel();
            planFeatureModel.Id = planId;
            planFeatureModel.IsReadMode = isReadMode;
            planFeatureModel.CreatedBy = !string.IsNullOrEmpty(SessionHelper.UserId) ? SessionHelper.UserId : "";

            //Call an asynchronous method
            return PartialView("~/Views/Plan/_PlanFeature.cshtml", planFeatureModel);
        }

        /// <summary>
        /// add/update plan
        /// </summary>
        /// <param name="planModel"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        public async Task<ActionResult> UpdatePlan(PlanModel planModel)
        {
            if (planModel.Id == 0)
            {
                ModelState.Remove("Feature");
            }
            bool responseStatus = false;
            string responseMessage = "Something Went Wrong!";
            if (ModelState.IsValid)
            {
                if (!PlanManager.IsPlanDuplicate(planModel))
                {
                    //assign ownerId
                    planModel.OwnerId = DataManager.ToNullableInt(SessionHelper.UserId);
                    planModel.SitePortalOwnerId = SessionHelper.SitePortalOwnerId;
                    var response = await PlanManager.UpdatePlan(planModel);
                    responseStatus = response.Item1;
                    responseMessage = response.Item2;
                    if (planModel.LstFeature != null && planModel.LstFeature.Count > 0)
                    {
                        //PlanFeatureModel planFeatureModel = new PlanFeatureModel()
                        //{
                        //    PlanId = planModel.Id,
                        //    CreatedBy = planModel.CreatedBy,
                        //    LstFeature = planModel.LstFeature
                        //};
                        //await PlanManager.UpdatePlanFeatureMapping(planFeatureModel);
                    }
                }
                else
                {
                    responseStatus = false;
                    responseMessage = "Plan Already Exist!";
                }
            }
            return Json(new
            {
                success = responseStatus,
                message = responseMessage,
                JsonRequestBehavior.AllowGet
            });
        }

        /// <summary>
        /// Check If plan name is exist  
        /// </summary>
        /// <param name="planModel"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        public JsonResult IsPlanDuplicate(PlanModel planModel)
        {
            return Json(!PlanManager.IsPlanDuplicate(planModel));
        }

        /// <summary>
        /// get all plan data table list
        /// </summary>
        /// <param name="parms"></param>
        /// <returns></returns>
        [AllowAnonymous]
        public async Task<ActionResult> GetPlanList(jQueryDataTableParamModel parms)
        {
            var response = await PlanManager.GetPlansDataTable(parms);
            return Json(new
            {
                aaData = response.Item1,
                iTotalRecords = response.Item3,
                iTotalDisplayRecords = response.Item3
            }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Delete Plan 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool DeletePlan(long id)
        {
            try
            {
                var result = PlanManager.DeletePlan(id);
                if (result)
                {
                    PlanManager.DeleteAllPlanFeatureByPlanId(id);

                }
                return (result) ? true : false;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "PlanController Delete");
                return false;
            }
        }

        /// <summary>
        /// Change Plan Status
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool ChangePlanStatus(long id)
        {
            try
            {
                return (PlanManager.UpdatePlanStatus(id)) ? true : false;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "PlanController=>ChangePlanStatus ");
                return false;
            }
        }

        #region modal popup methods

        /// <summary>
        /// open plan modal popup
        /// </summary>
        /// <param name="id"></param>
        /// <param name="isReadMode"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        public async Task<ActionResult> OpenPlanModal(int id, bool isReadMode = false)
        {

            if (!Request.IsAjaxRequest())
                return null;
            PlanModel planModel = new PlanModel();
            if (id == 0)
            {
                planModel.planFeature = "d-none";
            }
            if (id > 0)
            {
                planModel = await PlanManager.GetPlanById(id);
                planModel = planModel == null ? new PlanModel() : planModel;

            }
            planModel.IsReadMode = isReadMode;

            string currentUserId = !string.IsNullOrEmpty(SessionHelper.UserId) ? SessionHelper.UserId : "";
            planModel.CreatedBy = string.IsNullOrEmpty(planModel.CreatedBy) ? currentUserId : planModel.CreatedBy;
            planModel.ModifiedBy = string.IsNullOrEmpty(planModel.ModifiedBy) ? currentUserId : planModel.ModifiedBy;

            return PartialView("~/Views/Plan/_AddEditPlanFeature.cshtml", planModel);
        }

        /// <summary>
        /// save the info of plan modal popup
        /// </summary>
        /// <param name="planModel"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        public async Task<ActionResult> UpdatePlanModal(PlanModel planModel)
        {

            bool responseStatus = false;
            string responseMessage = "Something Went Wrong!";
            if (ModelState.IsValid)
            {
                if (!PlanManager.IsPlanDuplicate(planModel))
                {       
                    //assign ownerId
                    planModel.OwnerId = DataManager.ToNullableInt(SessionHelper.UserId);
                    planModel.SitePortalOwnerId = SessionHelper.SitePortalOwnerId;
                    var response = await PlanManager.UpdatePlan(planModel);
                    responseStatus = response.Item1;
                    responseMessage = response.Item2;
                    if (responseStatus && planModel.Feature != null)
                    {
                        PlanFeatureModel planFeature = new PlanFeatureModel()
                        {
                            PlanId = planModel.Id,
                            Feature = planModel.Feature,
                            OwnerId = planModel.OwnerId,

                        SitePortalOwnerId =planModel.SitePortalOwnerId,
                        
                    };

                       
                        

                        PlanManager.UpdatePlanFeature(planFeature);
                    }

                }
                else
                {
                    responseStatus = false;
                    responseMessage = "Plan Already Exist!";
                }
            }
            return Json(new
            {
                success = responseStatus,
                message = responseMessage,
                JsonRequestBehavior.AllowGet
            });
        }

        #endregion

        #endregion 

        #region View features associated with plan

        /// <summary>
        ///view for selected plan feature
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult ViewPlanFeature()
        {
            return View();
        }

        /// <summary>
        /// get all plan feature data table list
        /// </summary>
        /// <param name="parms"></param>
        /// <returns></returns>
        [AllowAnonymous]
        public async Task<ActionResult> GetPlanFeatureList(jQueryDataTableParamModel parms)
        {
            var response = await PlanManager.GetPlanFeatureDataTable(parms);
            return Json(new
            {
                aaData = response.Item1,
                iTotalRecords = response.Item3,
                iTotalDisplayRecords = response.Item3
            }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Delete Plan Feature
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool DeletePlanFeature(long id)
        {
            try
            {
                return (PlanManager.DeletePlanFeature(id)) ? true : false;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "UserController Delete");
                return false;
            }
        }

        #endregion 

        #endregion 
    }
}