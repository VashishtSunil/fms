﻿using FMS.App_Start;
using FMS.Core.Enums;
using FMS.Core.Helper;
using FMS.Core.Models;
using FMS.Services.Classes;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;


namespace FMS.Controllers
{
    public class DietPlanController : BaseController
    {
        #region Method



        /// <summary>
        /// view for showing diets
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Diets(int trainee)
        {
            ViewBag.Trainee = trainee;
            return View();
        }
        public ActionResult DietsList(jQueryDataTableParamModel parms)
        {
            try
            {
                
                var response = DietPlanManager.GetDietByUserDataTable(parms);
                return Json(new
                {
                    aaData = response.Item1,
                    iTotalRecords = response.Item3,
                    iTotalDisplayRecords = response.Item3
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "RoleController =>> List");
                return ReturnAjaxErrorMessage(ex.Message);
            }
        }

        /// <summary>
        /// Open Client Modal
        /// </summary>
        /// <param name="id"></param>
        /// <param name="isReadOnly"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult OpenDietModal(int id, int userId, bool isReadOnly)
        {
            if (!Request.IsAjaxRequest())
                return null;
            bool isEdit = id > 0;
            var dietModel = isEdit ? DietPlanManager.GetDiet(id) : new DietModel
            {
                UserId = userId
            };
            ViewBag.Header = isEdit ? "Edit Diet" : "Add New Diet";
            //check for read only 
            if (isReadOnly)
            {
                ViewBag.Header = "View Diet";
            }
            return PartialView("~/Views/DietPlan/_AddEditDiet.cshtml", dietModel);
        }

        /// <summary>
        /// Open Client Modal
        /// </summary>
        /// <param name="id"></param>
        /// <param name="isReadOnly"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddDiet(DietModel model)
        {
            var msg = "Something went wrong";

            if (ModelState.IsValid)
            {
                bool?  resp = DietPlanManager.AddDiet(model);

                 msg = resp.HasValue && resp.Value ? "Diet added succesfully" : resp.HasValue && !resp.Value ? msg : "Internal serevr error";

                return Json(new
                {
                    success = resp.HasValue && resp.Value,
                    message = msg,
                    JsonRequestBehavior.AllowGet
                });

            }
            return Json(new
            {
                success = false,
                message = msg,
                JsonRequestBehavior.AllowGet
            });

        }

        public bool DeleteDiet(int id)
        {
                return DietPlanManager.DeleteDiet(id);
        }
        public ActionResult Meals(int diet)
        {
            ViewBag.DietId = diet;
            var meals = DietPlanManager.GetMealsWithIngredientByUser(diet);
            return View(meals);
        }
        public ActionResult AddMeal(int diet)
        {
            ViewBag.Diet = diet;
            DietPlanMealModel model = new DietPlanMealModel
            {
                MealName = "Meal",
                DietId = diet
            };
            DietPlanManager.AddMeal(model);
            DietPlanMealIngredientModel mealIngredient = new DietPlanMealIngredientModel
            {
                MealId = model.Id,
                MealName = model.MealName,
                Ingredients = DietPlanManager.GetIngredientsSelectList()
            };
            return PartialView("~/Views/DietPlan/_AddMeal.cshtml", mealIngredient);
        }


        public ActionResult DeleteMeal(int mealId)
        {
            bool resp = DietPlanManager.DeleteMeal(mealId);
            return Json(new { success = resp }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddMealIngredient(int mealId)
        {
            DietPlanMealIngredientModel mealIngredient = new DietPlanMealIngredientModel
            {
                MealId = mealId,
                Ingredients = DietPlanManager.GetIngredientsSelectList()
            };
            return PartialView("~/Views/DietPlan/_AddMealInputCtrl.cshtml", mealIngredient);
        }

        [HttpPost]
        public ActionResult SaveMealIngredient([System.Web.Http.FromBody] List<DietPlanMealIngredientModel> ingredients)
        {
            bool resp = DietPlanManager.AddMealIngredient(ingredients);
            return Json(new { success = resp }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult DeleteMealIngredient(int ingredientId)
        {
            bool resp = DietPlanManager.DeleteMealIngredient(ingredientId);
            return Json(new { success = resp }, JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        /// Open Client Modal
        /// </summary>
        /// <param name="id"></param>
        /// <param name="isReadOnly"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult OpenMealModal(long id, bool isReadOnly)
        {
            if (!Request.IsAjaxRequest())
                return null;
            //
            var userModel = new UserModel();
            ViewBag.Header = (id > 0) ? "Edit Meal" : "Add New Meal";
            //Get UserBy Id 
            if (id > 0)
            {
                userModel = UserManager.GetUserById(id);

                userModel.MainRoleId = userModel.RoleId;
                var lstUserRoles = RoleManager.GetUserRoles(id);

                var userRole = lstUserRoles.Where(x => x.RoleName != UserRole.Trainer.ToString() || x.RoleName != UserRole.Trainee.ToString()).FirstOrDefault();
                if (userRole != null)
                {
                    userModel.RoleId = userRole.Id;
                }
            }
            else
            {
                userModel.IsActive = false;
                userModel.IsDeleted = false;
            }
            //check for user 
            if (userModel == null)
            {
                userModel = new UserModel();
            }
            //Get Role List 
            userModel.RoleList = AppendList(RoleManager.GetSelectList(), "role");

            //Set Readonly
            userModel.IsReadMode = isReadOnly;
            //check for read only 
            if (isReadOnly)
            {
                ViewBag.Header = "View Client";
            }
            return PartialView("~/Views/DietPlan/_AddEditMeal.cshtml", userModel);
        }

        #region Ingredientd
        public ActionResult Ingredients(int trainee)
        {
            ViewBag.TraineeId = trainee;
            return View();
        }



        /// <summary>
        /// Open Client Modal
        /// </summary>
        /// <param name="id"></param>
        /// <param name="isReadOnly"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult openMealIngredientModal(long id, bool isReadOnly)
        {
            if (!Request.IsAjaxRequest())
                return null;
            //
            var userModel = new UserModel();
            ViewBag.Header = (id > 0) ? "Edit Meal Ingredient" : "Add New Meal Ingredient";
            //Get UserBy Id 
            if (id > 0)
            {
                userModel = UserManager.GetUserById(id);

                userModel.MainRoleId = userModel.RoleId;
                var lstUserRoles = RoleManager.GetUserRoles(id);

                var userRole = lstUserRoles.Where(x => x.RoleName != UserRole.Trainer.ToString() || x.RoleName != UserRole.Trainee.ToString()).FirstOrDefault();
                if (userRole != null)
                {
                    userModel.RoleId = userRole.Id;
                }
            }
            else
            {
                userModel.IsActive = false;
                userModel.IsDeleted = false;
            }
            //check for user 
            if (userModel == null)
            {
                userModel = new UserModel();
            }
            //Get Role List 
            userModel.RoleList = AppendList(RoleManager.GetSelectList(), "role");

            //Set Readonly
            userModel.IsReadMode = isReadOnly;
            //check for read only 
            if (isReadOnly)
            {
                ViewBag.Header = "View Client";
            }
            return PartialView("~/Views/DietPlan/_AddEditMeal.cshtml", userModel);
        }

        #endregion

        #endregion
    }
}