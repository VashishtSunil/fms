﻿using Rollbar;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace FMS.Controllers
{
    public class BaseController : Controller
    {
        /// <summary>
        /// On Exception
        /// </summary>
        /// <param name="filterContext"></param>
        protected override void OnException(ExceptionContext filterContext)
        {
            RollbarLocator.RollbarInstance.Error(filterContext.Exception);
            Log.Error(filterContext.Exception, "OnException");
            filterContext.ExceptionHandled = true;
            RollbarLocator.RollbarInstance.Error(filterContext.Exception);

            //when session is null, that means user is not login, then we will redirect the user
            //to login page 
            if (filterContext.Exception.GetType().Name == "ArgumentNullException" && filterContext.Exception.Message.ToLower().Contains("userid is null"))
            {
                if (!Response.IsRequestBeingRedirected)
                    Response.Redirect("~/account/LoginUser", true);
            }
            else
            {
                //redirect to home page
                Response.Redirect("~/", true);
            }
        }

        /// <summary>
        /// appendList
        /// </summary>
        /// <param name="list"></param>
        /// <param name="option"></param>
        /// <returns></returns>
        public static SelectList AppendList(SelectList list, string option)
        {
            List<SelectListItem> _list = new List<SelectListItem>();
            if (list != null)
            {
                _list = list.ToList();
            }
            _list.Insert(0, new SelectListItem() { Value = "", Text = "Please Select " + option });
            return new SelectList((IEnumerable<SelectListItem>)_list, "Value", "Text");
        }

        /// <summary>
        /// set multi select list
        /// </summary>
        /// <param name="list"></param>
        /// <param name="selectedOptions"></param>
        /// <returns></returns>
        public static MultiSelectList AppendMultiSelectList(dynamic list,int[] selectedOptions)
        {
            return new MultiSelectList(list, "Value", "Text", selectedOptions);
        }

        #region DBStrings
        //public static string GetLeads = "select * from lead where IsDeleted = 0 order by ClientId desc";
        //public static string GetClients = "select * from client where IsDeleted = 0 order by ClientId desc";
        //public static string GetProgramNameForClient = "select STUFF((SELECT ', ' + cast(t1.ProgramName as nvarchar(200)) from Client c join  Program as t1 on t1.ClientId = c.ClientId where c.ClientId = @id FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'),1,2,'') programs from Client t where t.ClientId = @id;";
        //public static string GetForms = "select f.FormId,f.FormName,o.Orientation from forms as f inner join Orientation as o on f.ISPortrait = o.OrientationId where f.ClientId=@id and IsDeleted = 0 order by FormId desc";
        public static string GetForms = "select * from forms  where ClientId=@id and IsDeleted = 0 order by FormId desc";

        //public static string GetTeams = "select * from CRMTeams where IsDeleted = 0 and clientid=@id";
        //public static string GetAccess = "select * from CRMAccess where IsDeleted = 0 and ClientId=@id";
        //public static string GetMarkets = "select * from Markets where IsDeleted = 0";

        //public static string GetClient = "Select * from Program where ClientId= @id";

        //public static string GetConsumerList = "Select * from Consumer where IsActive = 0";

        //public static string GetCampaignByClientId = "select* from Program where ClientId = @id";

        #endregion

        /// <summary>
        /// Json
        /// </summary>
        /// <param name="data"></param>
        /// <param name="contentType"></param>
        /// <param name="contentEncoding"></param>
        /// <param name="behavior"></param>
        /// <returns></returns>
        protected override JsonResult Json(object data, string contentType, Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new JsonResult()
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                JsonRequestBehavior = behavior,
                MaxJsonLength = Int32.MaxValue
            };
        }

        /// <summary>
        /// Json Success
        /// </summary>
        /// <param name="message"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        protected JsonResult JsonSuccess(string message = null, object data = null)
        {
            if (string.IsNullOrEmpty(message))
                return Json(new { success = true, data = data }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = true, message = message, data = data }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Json Error
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        protected JsonResult JsonError(string message)
        {
            return Json(new { success = false, message = message }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Return Ajax Model Error
        /// </summary>
        /// <returns></returns>
        protected JsonResult ReturnAjaxModelError()
        {
            return Json(new
            {
                success = false,
                Message = string.Join("\n", ModelState.Keys.SelectMany(k => ModelState[k].Errors)
                                .Select(m => m.ErrorMessage).ToArray())
            });
        }

        /// <summary>
        /// Return Ajax Success Message
        /// </summary>
        /// <param name="successMessage"></param>
        /// <returns></returns>
        protected JsonResult ReturnAjaxSuccessMessage(string successMessage)
        {
            return Json(new
            {
                success = true,
                message = successMessage
            });
        }

        /// <summary>
        /// Return Ajax Error Message
        /// </summary>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        protected JsonResult ReturnAjaxErrorMessage(string errorMessage)
        {
            return Json(new
            {
                success = false,
                message = errorMessage
            });
        }

        /// <summary>
        /// Return Ajax Success Data 
        /// </summary>
        /// <returns></returns>
        protected JsonResult ReturnAjaxSuccessData(object value)
        {
            return Json(new
            {
                success = true,
                message = value
            });
        }

        /// <summary>
        /// Return Json Result
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        protected JsonResult ReturnJsonResult(object value)
        {
            return Json(value);
        }

        /// <summary>
        /// appendList
        /// </summary>
        /// <param name="list"></param>
        /// <param name="option"></param>
        /// <returns></returns>
        public static SelectList AppendList(SelectList list, string option, int? value = null)
        {
            List<SelectListItem> _list = new List<SelectListItem>();
            if (list != null)
            {
                _list = list.ToList();
            }
            _list.Insert(0, new SelectListItem() { Value = value != null ? value.Value.ToString() : "", Text = "Please Select " + option });
            return new SelectList((IEnumerable<SelectListItem>)_list, "Value", "Text");
        }

    }
}

