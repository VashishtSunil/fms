﻿using FMS.App_Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FMS.Controllers
{
    [AuthorizationFilter]
    public class ReportingController : BaseController
    {
        // GET: Reporting
        public ActionResult Index()
        {
            return View();
        }
    }
}