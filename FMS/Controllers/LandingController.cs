﻿using System;
using FMS.Services.Classes;
using System.Web.Mvc;
using FMS.Core.Models;
using FMS.Core.Helper;
using System.Threading.Tasks;

using System.Linq;
using FMS.App_Start;


namespace FMS.Controllers
{
    public class LandingController : BaseController
    {
        // GET: Landing
        public async Task<ActionResult> Index(string key)
        
        {
            ViewBag.IsKeyValid = false;
            LandingViewModel landingViewModel = new LandingViewModel();
            if (!string.IsNullOrEmpty(key))
            {
                var client = UserManager.GetClientAdminByKey(key);
                ViewBag.ClientInformation = client;
                if (client != null)//no client n reference to key
                {
                    var clients = await UserManager.GetCountByUserId(client.Id);
                    landingViewModel.LstSlider = ClientBasicInfoManager.GetSlidersList(client.Id);
                    landingViewModel.LstPlan = PlanManager.GetPlansList(client.Id);
                    landingViewModel.Client = clients.Item1;
                    landingViewModel.Trainer = clients.Item2;
                    landingViewModel.Years = clients.Item3;
                    landingViewModel.Key = key;
                    ViewBag.key = client.Id;
                    ViewBag.IsKeyValid = true;
                }
                else
                {
                    ViewBag.Message = "Invalid key entered";
                }
            }
            else
            {
                ViewBag.Message = "Please enter vaild key";
            }
            return View(landingViewModel);
        }


        /// <summary>
        /// Contact us
        /// </summary>
        /// <returns></returns>

        public ActionResult ContactUs(string siteKey)

        
        {
            var client = UserManager.GetClientAdminByKey(siteKey);
            ViewBag.Key = client.Id;
            ViewBag.ClientInformation = client;
            ViewBag.IsKeyValid = true;
            return View();
        }

        /// <summary>
        /// Sent email 
        /// </summary>
        /// <param name="contactUsModel"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<JsonResult> SendEmail(ContactUsModel contactUsModel)
        {
            ModelState.Remove("OwnerId");
            if (ModelState.IsValid)
            {
                string successMessage = "Email Sent Successfully!";
                //send Email here         
                var paths = (AppDomain.CurrentDomain.BaseDirectory + @"Views\Shared\EmailTemplate\SetPassword.cshtml");
                var EmailBody = System.IO.File.ReadAllText(paths);

                var callbackUrl = Url.Action("ResetUserAccessPassword", "Account", new { userId = contactUsModel.Id, email = contactUsModel.Email }, protocol: Request.Url.Scheme);
                string body = EmailBody.Replace("#name#", contactUsModel.Name).Replace("#url#", callbackUrl);

                string subject = contactUsModel.Subject;
                EmailModel emailModel = new EmailModel
                {
                    FromEmail = "FMS",
                    From = "FMS",
                    ToEmail = contactUsModel.Email,
                    Subject = subject,
                    Body = body
                };
                await EmailSender.SendEmailAsync(contactUsModel.Email, subject, body);
                return Json(new
                {
                    success = true,
                    message = successMessage,
                    JsonRequestBehavior.AllowGet
                });
            }
            else
            {
                return Json(new
                {
                    success = false,
                    message = "Something Went Wrong!",
                    JsonRequestBehavior.AllowGet
                });
            }
        }
    }

}