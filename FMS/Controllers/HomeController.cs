using System;
using System.Linq;
using System.Web.Mvc;
using FMS.App_Start;
using FMS.Core.Enums;
using FMS.Core.Models;
using FMS.Services.Classes;

namespace FMS.Controllers
{

    [AuthorizationFilter]
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            //FMSModel FMSModel = new FMSModel();
            //var roleIds = SessionHelper.UserRoles;
            //var userId = SessionHelper.UserId;
            //if (!string.IsNullOrEmpty(roleIds))
            //{
            //    var roleIdArray = roleIds.Split(',');
            //    int[] integerRoleIds = roleIdArray.Select(int.Parse).ToArray();
            //    if (integerRoleIds != null && integerRoleIds.Length > 0)
            //    {
            //        var roleDetail = RoleManager.GetUserRoles(integerRoleIds);
            //        //if (roleDetail != null && roleDetail.Count > 0 && roleDetail.Any(x => x.RoleName == "Student"))
            //        //{
            //        //    return View("~/Views/Home/_StudentFMS.cshtml", FMSManager.GetAllByUserId(Convert.ToInt32(userId)));
            //        //}
            //        //else if (roleDetail != null && roleDetail.Count > 0 && roleDetail.Any(x => x.RoleName == "SuperAdmin"))
            //        //{
            //        return RedirectToAction("Index", "FMS");
            //        //}
            //    }
            //}
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parentId"></param>
        /// <returns></returns>
        //public ActionResult FMSs()
        //{
        //    return View(FMSManager.GetAll());
        //}

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        /// <summary>
        /// Only dump 
        /// </summary>
        /// <returns></returns>
        public ActionResult DumpData()
        {
            //LocationManager.DumpData();
            return null;
        }
    }
}