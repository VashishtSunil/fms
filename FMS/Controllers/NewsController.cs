﻿using FMS.App_Start;
using FMS.Core.Models;
using FMS.Services.Classes;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Filters;
using System.Web.Mvc;

namespace FMS.Controllers
{
    
    public class NewsController : BaseController
    {
        #region News
        public ActionResult Index()
        
        {
            return View();
        }


        /// <summary>
        /// Get News List
        /// </summary>
        /// <param name="parms"></param>
        /// <returns></returns>
        public async Task<ActionResult> List(jQueryDataTableParamModel parms)
        {
            try
            {
                var news = await NewsManager.GetDataTableList(parms);
                return Json(new
                {
                    aaData = news.Item1,
                    iTotalRecords = news.Item3,
                    iTotalDisplayRecords = news.Item3
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "RoleController =>> List");
                return ReturnAjaxErrorMessage(ex.Message);
            }
        }

        /// <summary>
        /// Open News Modal
        /// </summary>
        /// <param name="id"></param>
        /// <param name="isReadOnly"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult OpenModal(int id, bool isReadOnly)
        {
            if (!Request.IsAjaxRequest())
                return null;
            NewsModel newsModel = new NewsModel();
            ViewBag.Header = (id > 0) ? "Edit News" : "Add New News";
            if (id > 0)
            {
                newsModel = NewsManager.GetById(id);
            }
            else
            {
                newsModel.IsDeleted = false;
            }

            // Get All Function List
            newsModel.IsReadMode = isReadOnly;
            //check for read only 
            if (isReadOnly)
            {
                ViewBag.Header = "View News";
            }
            return PartialView("~/Views/News/_AddEdit.cshtml", newsModel);
        }

        /// <summary>
        /// Add new news
        /// </summary>
        ///  <param name="newsModel"></param>
        /// <returns></returns>
        /// 
       
        [HttpPost]
        public JsonResult Add(NewsModel newsModel)
        {
            ModelState.Remove("Id");
            string successMessage = (newsModel.Id != 0) ? "News Updated Successfully!" : " News Added Successfully!";
            try
            {
                if (ModelState.IsValid)
                {
                    newsModel.OwnerId = Convert.ToInt32(SessionHelper.UserId);
                    newsModel.SitePortalOwnerId = SessionHelper.SitePortalOwnerId;
                    var result = NewsManager.Update(newsModel);
                    if (result)
                    {                          

                        return Json(new
                        {
                            success = true,
                            message = successMessage,
                            JsonRequestBehavior.AllowGet
                        });
                    }
                }
                return Json(new
                {
                    success = false,
                    message = "Something Went Wrong!",
                    JsonRequestBehavior.AllowGet
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    success = false,
                    message = "Something Went Wrong!",
                    JsonRequestBehavior.AllowGet
                });
            }
        }

        /// <summary>
        /// Delete News
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool DeleteNews(long id)
        {
            try
            {
                var result = NewsManager.DeleteNews(id);
                if (result)
                {
                    UserManager.DeleteAllUserByRoleId(id);

                }
                return (result) ? true : false;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "DeleteController Delete");
                return false;
            }
        }

        /// <summary>
        /// Change News Status
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool ChangeNewsStatus(long id)
        {
            try
            {
                return (NewsManager.UpdateNewsStatus(id)) ? true : false;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "NewsController=>ChangeNewsStatus");
                return false;
            }
        }


    }



    #endregion
}