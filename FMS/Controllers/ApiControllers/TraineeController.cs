﻿using FMS.App_Start;
using FMS.Core.Enums;
using FMS.Core.Helper;
using FMS.Core.Models;
using FMS.Services.Classes;
using Serilog;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FMS.Controllers.ApiControllers
{
    [Route("api/[controller]")]
    public class TraineeController : ApiBase
    {
        #region Trainee
        /// <summary>
        /// Add new trainee
        /// </summary>
        /// <param name="userModel"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<HttpResponseMessage> Add([System.Web.Http.FromBody] UserModel userModel)
        {
            if (userModel != null)
            {
                if (userModel.Id == 0 && UserManager.IsUserEmailDuplicate(userModel))
                {
                    return Request.CreateErrorResponse(HttpStatusCode.Found, ApiResponseMessages.EmailFound);
                }
                string createdOrModified = "api";
                string successMessage = (userModel.Id != 0) ? ApiResponseMessages.TraineeUpdated : ApiResponseMessages.TraineeAdded;
                try
                {
                    if (ModelState.IsValid)
                    {
                        userModel.CreatedBy = createdOrModified;
                        userModel.ModifiedBy = createdOrModified;
                        userModel.Username = userModel.Email;
                        //for email 
                        int eventType = 0;
                        int? roleId = userModel.RoleId;


                        var role = RoleManager.GetRoleByName(UserRole.Trainee.ToString());
                        userModel.RoleId = role.Id;
                        userModel.RoleName = role.RoleName;
                        //Remove all the role of user Assighned
                        if (userModel.Id > 0)
                        {
                            //check for Chamnges in Email and then shoot the  email 
                            var userDetail = UserManager.GetUserById(userModel.Id);

                            if (userDetail.Email != userModel.Email)
                            {
                                //change pass word if email is changed
                                userModel.Password = Guid.NewGuid().ToString();
                                eventType = (int)EmailEvents.UpdateEmailOfUser;
                            }
                            else
                            {
                                userModel.Password = userDetail.Password;
                            }

                            //Role All delete-> delete mapping on edit 
                            UserManager.DeleteUserRoles(userModel.Id);
                        }
                        else
                        {
                            userModel.Password = Guid.NewGuid().ToString();
                            eventType = (int)EmailEvents.AddUser;
                        }
                        //update User 

                        var result = UserManager.Update(userModel);
                        if (result)
                        {
                            UserInRoleModel userRoleModel = new UserInRoleModel()
                            {
                                UserId = userModel.Id,
                                RoleId = role.Id,
                                CreatedBy = createdOrModified,
                                ModifiedBy = createdOrModified
                            };
                            UserManager.UpdateUserInRole(userRoleModel);
                            //get userdeatils
                            var userdetails = UserManager.GetUserDetail(userModel.Id);
                            if (userdetails == null)
                            {
                                userdetails = new UserDetailModel();
                            }
                            //userdetails.UserId = userModel.Id;
                            userdetails.FirstName = userModel.FirstName;
                            userdetails.LastName = userModel.LastName;
                            userdetails.Phone = userModel.Phone;
                            userdetails.Address = userModel.Address;
                            UserManager.UpdateUserDetail(userdetails);

                            //Get role 
                            if (eventType > 0)
                            {
                                //send Email here 
                                string code = CipherHelper.GenerateCode();

                                var paths = (AppDomain.CurrentDomain.BaseDirectory + @"Views\Shared\EmailTemplate\SetPassword.cshtml");
                                var EmailBody = System.IO.File.ReadAllText(paths);

                                var callbackUrl = this.Url.Link("Default", new { Controller = "Account", Action = "ResetUserAccessPassword", userId = userModel.Id, token = code, email = userModel.Email });
                                string body = EmailBody.Replace("#name#", userModel.FirstName).Replace("#url#", callbackUrl);
                                string subject = string.Empty;
                                if ((int)EmailEvents.AddUser == eventType)
                                {
                                    subject = "New Signup";
                                }
                                if ((int)EmailEvents.UpdateEmailOfUser == eventType)
                                {
                                    subject = "Email Update";
                                }

                                EmailModel emailModel = new EmailModel
                                {
                                    FromEmail = "FMS",
                                    From = "FMS",
                                    ToEmail = userModel.Email,
                                    Subject = subject,
                                    Body = body
                                };
                                //await EmailSender.SendEmailAsync(userModel.Email, subject, body, "");
                                await UserManager.UpdateUserCode(userModel.Email, code, false);

                            }
                            return Request.CreateErrorResponse(HttpStatusCode.OK, successMessage);
                        }
                    }
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ApiResponseMessages.SWW);
                }
                catch (Exception ex)
                {
                    Log.Error(ex, "ApiControllers.TraineeController => Add");
                    return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ApiResponseMessages.SWW);
                }
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ApiResponseMessages.SWW);
            }
        }

        /// <summary>
        /// Check If Email is exist  
        /// </summary>
        /// <param name="userModel"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        public HttpResponseMessage CheckUserEmail([System.Web.Http.FromBody] UserModel userModel)
        {

            if (!UserManager.IsEmailDuplicate(userModel))
            {
                return Request.CreateErrorResponse(HttpStatusCode.OK, ApiResponseMessages.EmailNotFound);
            }
            return Request.CreateErrorResponse(HttpStatusCode.Found, ApiResponseMessages.EmailFound);

        }

        /// <summary>
        /// Delete Role
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public HttpResponseMessage DeleteTrainee([System.Web.Http.FromBody] long id)
        {
            try
            {
                if (UserManager.DeleteUser(id))
                {
                    return Request.CreateErrorResponse(HttpStatusCode.OK, ApiResponseMessages.TraineeDeleted);
                }
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ApiResponseMessages.SWW);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "ApiControllers.TraineeController => DeleteTrainee");
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ApiResponseMessages.SWW);
            }
        }

        /// <summary>
        /// Change User Status
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public HttpResponseMessage ChangeTraineeStatus(long id)
        {
            try
            {
                if (UserManager.UpdateUserStatus(id))
                {
                    return Request.CreateErrorResponse(HttpStatusCode.OK, ApiResponseMessages.TraineeStatusUpdated);
                }
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ApiResponseMessages.SWW);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "ApiControllers.TraineeController => ChangeTraineeStatus");
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ApiResponseMessages.SWW);
            }
        }

        #endregion
    }
}