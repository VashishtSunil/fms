﻿using FMS.Core.Enums;
using FMS.Services.Classes.Token;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FMS.Controllers.ApiControllers
{
    public class ApiBase : ApiController
    {

        /// <summary>
        /// GetSuccessMessage
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, string> GetSuccessResponse()
        {
            return new Dictionary<string, string>()
                   {
                        {"status", "success"}
                   };
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="errorMessageCode"></param>
        /// <param name="language"></param>
        /// <returns></returns>
        public Dictionary<string, string> GetErrorMessage(
            int errorMessageCode,
            int language)
        {

            var errorMessage = "No errro message found please add!";
            var error = "";
            if (error != null)
            {
                errorMessage = "";
            }

            return new Dictionary<string, string>()
                   {
                        { "errornum", errorMessageCode.ToString()},
                        { "errormsg", errorMessage }
                   };
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="apiHeader"></param>
        /// <returns></returns>
        public string GetValueFromHeader(ApiHeader apiHeader)
        {
            var re = Request;
            var headers = re.Headers;

            switch (apiHeader)
            {
                //Member 
                case ApiHeader.Member:
                    return headers.GetValues("Member").FirstOrDefault().ToString();
                //Brand
                case ApiHeader.Brand:
                    return headers.GetValues("Brand").FirstOrDefault().ToString();
                //Brand
                case ApiHeader.Channel:
                    return headers.GetValues("Channel").FirstOrDefault().ToString();
                //Language
                case ApiHeader.Language:
                    if (headers.Contains("Language"))
                    {
                        return headers.GetValues("Language").FirstOrDefault().ToString();
                    }
                    else
                    {
                        return Language.English.ToString();
                    }
                //Default  
                default:
                    return null;
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="language"></param>
        /// <returns></returns>
        public int GetLanguageCode(string language)
        {
            if (language.Contains(Language.English.GetDescription()))
            {
                return Language.English.GetHashCode();
            }
            return 1;
        }

        /// <summary>
        /// Header  Is Valid
        /// </summary>
        /// <returns></returns>
        public Tuple<bool, HttpResponseMessage> IsHeaderValid(bool isForSetting = false)
        {
            var re = Request;
            var headers = re.Headers;

            //check for Authorization
            if (headers.Authorization == null)
            {
                if (TokenManager.IsTokenValid(headers.Authorization.ToString()))
                {
                    if (TokenManager.IsTokenExpired(headers.Authorization.ToString()))
                    {
                        return Tuple.Create(false, Request.CreateResponse(HttpStatusCode.BadRequest, "Token Is Expired"));
                    }
                }
                else
                {
                    return Tuple.Create(false, Request.CreateResponse(HttpStatusCode.BadRequest, "Invalid Token"));
                }

            }

            return Tuple.Create(true, Request.CreateResponse(HttpStatusCode.OK));

        }


        /// <summary>
        /// Check token is valid
        /// </summary>
        /// <returns></returns>
        public Tuple<bool, HttpResponseMessage> IsTokenValid()
        {
            var re = Request;
            var headers = re.Headers;

            if (!string.IsNullOrEmpty(headers.Authorization.ToString()) && TokenManager.IsTokenValid(headers.Authorization.ToString()))
            {
                //check for further validation in Database 

                return Tuple.Create(true, Request.CreateResponse(HttpStatusCode.OK));
            }
            return Tuple.Create(false, Request.CreateResponse(HttpStatusCode.BadRequest, "Invalid Token"));
        }

    }
}