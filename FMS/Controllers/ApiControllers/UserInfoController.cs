﻿using FMS.Core;
using FMS.Core.Enums;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using FMS.Services.Classes.Token;
using FMS.Core.Models;
using FMS.Core.Models.APIModel;
using FMS.Services.Classes;

namespace FMS.Controllers.ApiControllers
{
    [RoutePrefix("api")]
    public class UserInfoController : ApiBase
    {
        [Route("userinfo")]
     
        [AcceptVerbs("GET", "POST")]
        [LogAPIUser]
        [CheckModelData]
        public HttpResponseMessage UserInfoOperations(UserInfoModel model)
        {
            try
            {
                //Conversion of string to enum 
                Actions action = model.Action.ToEnum<Actions>();

                switch (action)
                {
                    //Create Token 
                    case Actions.Get:
                        return Get(model.Id);

                    //Create Token 
                    case Actions.Getall:
                        return Get(model.Id);

                    //Default  
                    default:
                        return Request.CreateResponse(HttpStatusCode.BadRequest, RequestResponse.Error.GetDescription());
                }
            }
            catch (Exception ex)
            {
                //Log.Error(ex, "API ==>> TokenController =>> TokenOprations ");
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex);
            }

        }

        #region Methods 
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public HttpResponseMessage Get(long id)
        {
            UserInfoModel userInfoModel = new UserInfoModel();
            //get user Data 
            var user = UserManager.GetUserById(id);
            if (user!=null)
            {
                userInfoModel.FirstName = user.FirstName??"";
                userInfoModel.LastName = user.LastName??"";
                userInfoModel.Email = user.Email??"";
                userInfoModel.Phone = user.Phone??"";

            }

            var userDetail = UserManager.GetUserDetail(id);
            if (userDetail != null)
            {
                userInfoModel.FirstName = userInfoModel.FirstName ?? userDetail.FirstName??"";
                userInfoModel.LastName = userInfoModel.LastName ?? userDetail.LastName ?? "";
                //userInfoModel.Email = userInfoModel.Email ?? userDetail.Email ?? "";
                userInfoModel.Phone = userInfoModel.Phone ?? userDetail.Phone ?? "";
                userInfoModel.ProfilePic = userDetail.ProfilePic;
               

            }
            // Get user Detail data 
            return (user == null) ?
              Request.CreateResponse(HttpStatusCode.NotFound, RequestResponse.NotFound.GetDescription()) :
              Request.CreateResponse(HttpStatusCode.OK, userInfoModel);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public HttpResponseMessage GetAll(long id)
        {
            var user = UserManager.GetUserDetail(id);
            return (user == null) ?
              Request.CreateResponse(HttpStatusCode.NotFound, RequestResponse.NotFound.GetDescription()) :
              Request.CreateResponse(HttpStatusCode.OK, user);
        }

        #endregion
    }
}