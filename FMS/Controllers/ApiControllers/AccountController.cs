﻿using FMS.App_Start;
using FMS.Core.Enums;
using FMS.Core.Helper;
using FMS.Core.Models;
using FMS.Services.Classes;
using FMS.Services.Classes.Token;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
//using System.Web.Http;
using System.Web.Mvc;

namespace FMS.Controllers.ApiControllers
{
    [Route("api/[controller]")]
    public class AccountController : TokensController
    {
        #region Methods
        #region Login

        // GET: Account

        /// <summary>
        /// login page method
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        /// 

        [HttpPost]
        [AllowAnonymous]
        public async Task<HttpResponseMessage> LoginUser([System.Web.Http.FromBody] LoginModel login)
        {

            Serilog.Log.Information("User Login Process Start!");

            if (login != null)
            {
                var user = await UserManager.Login(login);
                Serilog.Log.Information("User Info:" + user);

                if (user != null && user.Id > 0)
                {
                    //SessionHelper.UserId = user.Id.ToString();
                    //string[] roles = user.Role.Split(',');
                    var resp = CreateToken(new Core.Payloads
                    {
                        Action = EnumManager.GetEnumDescription(Actions.Login),
                        UserId = user.Id.ToString(),
                        Password = user.Password,

                    });
                    if (resp.IsSuccessStatusCode)
                    {
                        string content = await resp.Content.ReadAsStringAsync();
                        var tokenDic = JsonConvert.DeserializeObject<Dictionary<string, string>>(content);
                        string token = tokenDic["token"];
                        var userData = new
                        {
                            userId = user.Id.ToString(),
                            username = user.Username ?? user.Email,
                            name = $"{user.FirstName} {user.LastName}",
                            email = user.Email,
                            token = token
                        };
                        return Request.CreateResponse(HttpStatusCode.OK, userData);
                    }
                    else
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ApiResponseMessages.LoginFail);
                    }
                }
            }
            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ApiResponseMessages.LoginFail);
        }


       
        /// <summary>
        /// Get user data with token
        /// </summary>
        /// <returns></returns>

        [IsTokenValid]
        [AllowAnonymous]
        public HttpResponseMessage GetUserDataWithToken()
        {
            string token = Request.Headers.Authorization.ToString();
            var user = UserManager.GetUserDetailWithToken(token);
            var userData = new
            {
                userId = user.Id.ToString(),
                username = user.Username,
                name = $"{user.FirstName} {user.LastName}",
                email = user.Email,
                token = token
            };
            return Request.CreateResponse(HttpStatusCode.OK, userData);
        }

        /// <summary>
        /// token validation check
        /// </summary>
        /// <returns></returns>
        [IsTokenValid]
        [AllowAnonymous]
        public HttpResponseMessage ValidationCheck()
        {
            //if token valid then retrun ok
            return Request.CreateErrorResponse(HttpStatusCode.OK, "valid");
        }


        /// <summary>
        /// Refresh user token
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        public async Task<HttpResponseMessage> RefreshToken()
        {
            string token = Request.Headers.Authorization.ToString();
            var user = UserManager.GetUserDetailWithToken(token);
            if (user == null)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ApiResponseMessages.SWW);
            }

            if (!await TokenManager.DeleteToken(token))
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ApiResponseMessages.SWW);
            }

            var resp = CreateToken(new Core.Payloads
            {
                Action = EnumManager.GetEnumDescription(Actions.Reset),
                UserId = user.Id.ToString(),
                Password = user.Password,

            });
            if (resp.IsSuccessStatusCode)
            {
                string content = await resp.Content.ReadAsStringAsync();
                var tokenDic = JsonConvert.DeserializeObject<Dictionary<string, string>>(content);
                string newToken = tokenDic["token"];
                var userData = new
                {
                    userId = user.Id.ToString(),
                    username = user.Username,
                    name = $"{user.FirstName} {user.LastName}",
                    email = user.Email,
                    token = newToken
                };
                return Request.CreateResponse(HttpStatusCode.OK, userData);
            }
            return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ApiResponseMessages.SWW);
        }



        /// <summary>
        /// Reset Password
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public HttpResponseMessage ResetUserPassword(ResetPasswordModel model)
        {

            model.UserType = "Admin";
            var message = string.Empty;
            if (model.UserType == "Admin")
            {
                var user = AccountManager.GetUserByEmailAndCode(model.Email, model.Code);
                if (user == null)
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiResponseMessages.SWW);
                }
                var passUpdate = AccountManager.ResetUserPassword(model.Password, model.Email);
            }
            return Request.CreateResponse(HttpStatusCode.OK, ApiResponseMessages.Success);
        }


        /// <summary>
        /// Check Email
        /// </summary>
        /// <param name="emailId"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public async Task<HttpResponseMessage> CheckEmail(string Email)
        {
            var user = await UserManager.IsEmailExist(Email);
            return (user.Email != null) ? Request.CreateResponse(HttpStatusCode.Found, ApiResponseMessages.EmailFound) : Request.CreateResponse(HttpStatusCode.OK, ApiResponseMessages.EmailNotFound);
        }

        [System.Web.Http.HttpGet]
        [AllowAnonymous]
        public HttpResponseMessage IsValidGymKey()
        {
            IEnumerable<string> headerValues = Request.Headers.GetValues(ApiHeaderProperties.GymKey);
            if (headerValues.Count() > 0)
            {
                var token = headerValues.FirstOrDefault();
                var user = AccountManager.VerifyGymKey(token);
                if (user != null)
                    return Request.CreateResponse(HttpStatusCode.OK, user);
                else
                    return Request.CreateErrorResponse(HttpStatusCode.NoContent, ApiResponseMessages.NoContent);
            }
            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ApiResponseMessages.NotValid);
        }

        #endregion
        #endregion

    }
}