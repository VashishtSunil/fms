﻿
using FMS.Core.Helper;
using FMS.Services.Classes;
//using Rollbar.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace FMS.Controllers.ApiControllers
{
    public class NewsController : ApiBase
    {
        [IsTokenValid]
        [System.Web.Http.HttpGet]
        public async Task<HttpResponseMessage> GetAll()
        {
            try
            {
                IEnumerable<string> headerValues = Request.Headers.GetValues(ApiHeaderProperties.PortalOwnerId);
                if (headerValues.Count() > 0)
                {
                    var id = headerValues.FirstOrDefault();
                    var news = await NewsManager.GetAllAsync(Convert.ToInt32(id));
                    if (news.Count > 0)
                        return Request.CreateResponse(HttpStatusCode.OK, news);
                    else
                        return Request.CreateResponse(HttpStatusCode.NoContent, ApiResponseMessages.NoContent);
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest, ApiResponseMessages.BadRequest);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NoContent, ApiResponseMessages.NoContent);
            }
            return Request.CreateErrorResponse(HttpStatusCode.NoContent, ApiResponseMessages.NoContent);
        }
          
        [IsTokenValid]
        [HttpGet]
        public HttpResponseMessage Get()
        {
            IEnumerable<string> headerValues = Request.Headers.GetValues(ApiHeaderProperties.NewsId);
            if (headerValues.Count() > 0)
            {
                var id = headerValues.FirstOrDefault();
                return Request.CreateResponse(HttpStatusCode.OK, NewsManager.GetById(Convert.ToInt32(id)));
            }
            return Request.CreateResponse(HttpStatusCode.BadRequest, ApiResponseMessages.BadRequest);
        }
    }
}