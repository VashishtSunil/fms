﻿using FMS.Core;
using FMS.Core.Enums;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using FMS.Services.Classes.Token;
using FMS.Core.Models;

namespace FMS.Controllers.ApiControllers
{
    [RoutePrefix("api")]
    public class TokensController : ApiBase
    {
        [Route("token")]
        [CheckModelData]
        [SaveToken]
        public HttpResponseMessage TokenOperations(Payloads model)
        {
            try
            {
                //Conversion of string to enum 
                Actions action = model.Action.ToEnum<Actions>();

                switch (action)
                {
                    //Create Token 
                    case Actions.Authenticate:
                        return CreateToken(model);

                    //Default  
                    default:
                        return Request.CreateResponse(HttpStatusCode.BadRequest, RequestResponse.Error.GetDescription());
                }
            }
            catch (Exception ex)
            {
                //Log.Error(ex, "API ==>> TokenController =>> TokenOprations ");
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex);
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public HttpResponseMessage CreateToken(Payloads model)
        {
            var tokenResponse = TokenManager.GenerateToken(model);
            if (tokenResponse.Item1)
            {
                //save token 
                ApiTokenModel apiTokenModel = new ApiTokenModel()
                {
                    ClientId = model.UserId,
                    TokenId = tokenResponse.Item3,
                    ValidUntil = DateTime.Now.AddDays(1),//for now valid upto one day 
                    CreatedBy = "api",
                    CreatedOn = DateTime.Now

                };
                //update the token 
                if (TokenManager.UpdateApiToken(apiTokenModel))
                {
                    Request.Properties.Add(new KeyValuePair<string, object>("Token", tokenResponse.Item3));
                    return Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>()
            {
                { "token", tokenResponse.Item3 }
            });
                }
                else
                {
                    int errorCode = ErrorMessageCode.UnexpectedError.GetHashCode();
                    var errorMessage = ErrorMessageCode.UnexpectedError.GetDescription();
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new Dictionary<string, string>()
                   {
                        {"errornum",errorCode.ToString()},
                        { "errormsg", errorMessage!=null?errorMessage:"" }
                   });

                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, tokenResponse.Item2);
            }
        }
    }
}