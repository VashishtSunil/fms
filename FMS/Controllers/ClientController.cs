﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using FMS.App_Start;
using FMS.Core.Enums;
using FMS.Core.Helper;
using FMS.Core.Models;
using FMS.Services.Classes;
using Serilog;

namespace FMS.Controllers
{
    public class ClientController : BaseController
    {
        // GET: Client
        #region User

        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// Get Client List
        /// </summary>
        /// <param name="parms"></param>
        /// <returns></returns>
        public async Task<ActionResult> List(jQueryDataTableParamModel parms)
        {
            try
            {
                var userId = SessionHelper.UserId;
                // get ownerId 
                parms.id = userId;
                // get roleId
                var role = RoleManager.GetRoleByName(UserRole.Admin.ToString());
                var response = await UserManager.GetUsersDataTable(parms, role.Id);

                return Json(new
                {
                    aaData = response.Item1,
                    iTotalRecords = response.Item3,
                    iTotalDisplayRecords = response.Item3
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "RoleController =>> List");
                return ReturnAjaxErrorMessage(ex.Message);
            }
        }

        /// <summary>
        /// Open Client Modal
        /// </summary>
        /// <param name="id"></param>
        /// <param name="isReadOnly"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult OpenModal(long id, bool isReadOnly)
        {
            if (!Request.IsAjaxRequest())
                return null;
            //
            var userModel = new UserModel();
            ViewBag.Header = (id > 0) ? "Edit Client" : "Add New Client";
            //Get UserBy Id 
            if (id > 0)
            {
                userModel = UserManager.GetUserById(id);

                userModel.MainRoleId = userModel.RoleId;
                var lstUserRoles = RoleManager.GetUserRoles(id);

                var userRole = lstUserRoles.Where(x => x.RoleName != UserRole.Trainer.ToString() || x.RoleName != UserRole.Trainee.ToString()).FirstOrDefault();
                if (userRole != null)
                {
                    userModel.RoleId = userRole.Id;
                }
            }
            else
            {
                userModel.IsActive = false;
                userModel.IsDeleted = false;
            }
            //check for user 
            if (userModel == null)
            {
                userModel = new UserModel();
            }
            //Get Role List 
            userModel.RoleList = AppendList(RoleManager.GetSelectList(), "role");

            //Set Readonly
            userModel.IsReadMode = isReadOnly;
            //check for read only 
            if (isReadOnly)
            {
                ViewBag.Header = "View Client";
            }
            return PartialView("~/Views/Client/_AddEdit.cshtml", userModel);
        }


        /// <summary>
        /// Add New Client
        /// </summary>
        /// <param name="userModel"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<JsonResult> Add(UserModel userModel)
        {
            if (userModel.Id == 0)
            {
                //check for dupliacte record
                if (UserManager.IsUserEmailDuplicate(userModel))
                {
                    return Json(new
                    {
                        success = false,
                        message = "Client with Email already exists in database.",
                        JsonRequestBehavior.AllowGet
                    });
                }                
            }
            ModelState.Remove("Id");
            ModelState.Remove("RoleId");
            ModelState.Remove("TypeId");
            string successMessage = (userModel.Id != 0) ? "Client Updated Successfully!" : "Client Added Successfully!";
            try
            {
                if (ModelState.IsValid)
                {
                    string userId = !string.IsNullOrEmpty(SessionHelper.UserId) ? SessionHelper.UserId : "";
                    userModel.OwnerId = Convert.ToInt32(userId);
                    userModel.CreatedBy = userId;
                    userModel.ModifiedBy = userId;
                    //for email 
                    int eventType = 0;
                    int? roleId = userModel.RoleId;
                    var role = RoleManager.GetRoleByName(UserRole.Admin.ToString());
                    userModel.RoleId = role.Id;
                    userModel.RoleName = role.RoleName;
                    //Remove all the role of user Assighned
                    if (userModel.Id > 0)
                    {
                        //check for Changes in Email and then shoot the  email 
                        var userDetail = UserManager.GetUserById(userModel.Id);
                        if (userDetail.Email != userModel.Email)
                        {
                            //change pass word if email is changed
                            userModel.Password = Guid.NewGuid().ToString();
                            eventType = (int)EmailEvents.UpdateEmailOfUser;
                        }
                        else
                        {
                            userModel.Password = userDetail.Password;
                        }
                        //Role All delete-> delete mapping on edit 
                        UserManager.DeleteUserRoles(userModel.Id);
                    }
                    else
                    {
                        userModel.Password = Guid.NewGuid().ToString();
                        eventType = (int)EmailEvents.AddUser;
                    }
                    //set owner and siteowner 
                    userModel.OwnerId = Convert.ToInt32(SessionHelper.UserId);                   
                    userModel.PlanExpirationDateTime = DateTime.Now.AddDays(30);                   
                    //update User 
                    var result = UserManager.Update(userModel);
                    if (result)
                    {
                        //here we are updaing siteportal id to current user
                        userModel.SitePortalOwnerId = userModel.Id;
                        UserManager.Update(userModel);
                        SiteSettingModel subscriptionSetting = new SiteSettingModel()
                        {
                            Key = "DefaultPlanLength",
                            Value = "30",
                            Value2 = "",
                            Description = "30 days default plan length for tasting orders.",
                            ControlType = 0,
                            OwnerId = userModel.OwnerId ?? 0,
                            SitePortalOwnerId = userModel.SitePortalOwnerId,
                        };
                        //Update Sitesetting
                        var setting = UserManager.UpdateSubscriptionSetting(subscriptionSetting);
                        UserInRoleModel userRoleModel = new UserInRoleModel()
                        {
                            UserId = userModel.Id,
                            RoleId = role.Id,
                            CreatedBy = userId,
                            ModifiedBy = userId
                        };
                        UserManager.UpdateUserInRole(userRoleModel);
                        var userdetails = UserManager.GetUserDetail(userModel.Id);
                        if (userdetails == null)
                        {
                            userdetails = new UserDetailModel();
                        }
                        userdetails.UserId = userModel.Id;
                        userdetails.FirstName = userModel.FirstName;
                        userdetails.LastName = userModel.LastName;
                        userdetails.Phone = userModel.Phone;
                        userdetails.Address = userModel.Address;
                        UserManager.UpdateUserDetail(userdetails);

                        UserMapModel userMapModel = new UserMapModel()
                        {
                            OwnerId = userdetails.Id,
                            UserDetailId = userdetails.Id,
                            UserType = role.Id,
                            UserId = userModel.Id,
                            IsActive = userModel.IsActive,
                        };
                        // update User mapping
                        UserManager.UpdateUserMap(userMapModel);                   
                        //Get role 
                        if (eventType > 0)
                        {
                            //send Email here 
                            string code = CipherHelper.GenerateCode();
                            var paths = (AppDomain.CurrentDomain.BaseDirectory + @"Views\Shared\EmailTemplate\SetPassword.cshtml");
                            var EmailBody = System.IO.File.ReadAllText(paths);
                            var callbackUrl = Url.Action("ResetUserAccessPassword", "Account", new { userId = userModel.Id, token = code, email = userModel.Email }, protocol: Request.Url.Scheme);
                            string body = EmailBody.Replace("#name#", userModel.FirstName).Replace("#url#", callbackUrl);
                            string subject = string.Empty;
                            if ((int)EmailEvents.AddUser == eventType)
                            {
                                subject = "New Signup";
                            }
                            if ((int)EmailEvents.UpdateEmailOfUser == eventType)
                            {
                                subject = "Email Update";
                            }
                            EmailModel emailModel = new EmailModel
                            {
                                FromEmail = "FMS",
                                From = "FMS",
                                ToEmail = userModel.Email,
                                Subject = subject,
                                Body = body
                            };
                            await EmailSender.SendEmailAsync(userModel.Email, subject, body);
                            await UserManager.UpdateUserCode(userModel.Email, code, false);
                        }
                        return Json(new
                        {
                            success = true,
                            message = successMessage,
                            JsonRequestBehavior.AllowGet
                        });
                    }
                }
                return Json(new
                {
                    success = false,
                    message = "Something Went Wrong!",
                    JsonRequestBehavior.AllowGet
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    success = false,
                    message = "Something Went Wrong!",
                    JsonRequestBehavior.AllowGet
                });
            }
        }

        /// <summary>
        /// Check If Email is exist  
        /// </summary>
        /// <param name="userModel"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        public JsonResult CheckUserEmail(UserModel userModel)
        {
            return Json(!UserManager.IsEmailDuplicate(userModel));
        }

        /// <summary>
        /// Delete Client
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool DeleteClient(long id)
        {
            try
            {
                return UserManager.DeleteUser(id) ? true : false;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "UserController Delete");
                return false;
            }
        }

        /// <summary>
        /// Change Client Status
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool ChangeClientStatus(long id)
        {
            try
            {
                return UserManager.UpdateUserStatus(id) ? true : false;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "UserController=>ChangeStatus ");
                return false;
            }
        }


        [HttpPost]
        [AllowAnonymous]
        public async Task<JsonResult> CheckSiteKey(string SiteKey)
        {
            var user = await UserManager.IsSiteKeyExist(SiteKey);
            return (user.SiteKey == null) ? Json(true, JsonRequestBehavior.AllowGet) : Json(false, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}