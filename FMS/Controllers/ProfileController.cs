﻿using FMS.App_Start;
using FMS.Core.Models;
using FMS.Services.Classes;
using System;
using System.Web.Mvc;

namespace FMS.Controllers
{
    [AuthorizationFilter]
    public class ProfileController : BaseController
    {
        #region Profile
        public ActionResult Index()
        {
            var userId = SessionHelper.UserId;
            var userDetailModel = new UserDetailModel();
            if (!string.IsNullOrEmpty(userId))
            {
                userDetailModel = UserManager.GetUserDetail(Convert.ToInt32(userId));
            }
            userDetailModel = userDetailModel == null ? new UserDetailModel() : userDetailModel;
            userDetailModel.UserId = Convert.ToInt32(userId);
            return View(userDetailModel);
        }

        /// <summary>
        /// Update User Detail
        /// </summary>
        ///  <param name="userDetailModel"></param>
        /// <returns></returns> 
        [HttpPost]
        public JsonResult Add(UserDetailModel userDetailModel)
        {
            ModelState.Remove("Id");
            string successMessage = (userDetailModel.Id != 0) ? "Profile Updated Successfully!" : " Profile Added Successfully!";
            try
            {
                if (ModelState.IsValid)
                {
                    var result = UserManager.UpdateUserDetail(userDetailModel);
                    if (result)
                    {
                        return Json(new
                        {
                            success = true,
                            message = successMessage,
                            JsonRequestBehavior.AllowGet
                        });
                    }
                }
                return Json(new
                {
                    success = false,
                    message = "Something Went Wrong!",
                    JsonRequestBehavior.AllowGet
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    success = false,
                    message = "Something Went Wrong!",
                    JsonRequestBehavior.AllowGet
                });
            }
        }

        #endregion
    }
}