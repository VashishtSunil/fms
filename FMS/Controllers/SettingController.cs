﻿using FMS.App_Start;
using FMS.Core.Helper;
using FMS.Core.Models;
using FMS.Services.Classes;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace FMS.Controllers
{
    [AuthorizationFilter]
    public class SettingController : BaseController
    {

        #region Basic Information

        // GET: Setting
        public ActionResult Index()
        {
            var roleIds = SessionHelper.UserRoles;
            var userId = SessionHelper.UserId;
            var clientBasicInfoViewModel = new ClientBasicInfoViewModel();
            if (!string.IsNullOrEmpty(userId))
            {
                clientBasicInfoViewModel = ClientBasicInfoManager.GetClientBasicInfoByUserId(Convert.ToInt32(userId));
                clientBasicInfoViewModel = clientBasicInfoViewModel == null ? new ClientBasicInfoViewModel() : clientBasicInfoViewModel;

            }
            clientBasicInfoViewModel = clientBasicInfoViewModel == null ? new ClientBasicInfoViewModel() : clientBasicInfoViewModel;
            clientBasicInfoViewModel.ClientId = Convert.ToInt32(userId);
            if (!string.IsNullOrEmpty(roleIds))
            {
                var roleIdArray = roleIds.Split(',');
                int[] integerRoleIds = roleIdArray.Select(int.Parse).ToArray();
                if (integerRoleIds != null && integerRoleIds.Length > 0)
                {
                    var roleDetail = RoleManager.GetUserRoles(integerRoleIds);
                    if (roleDetail != null && roleDetail.Count > 0 && roleDetail.Any(x => x.RoleName == "Admin"))
                    {
                        return View("~/Views/Setting/_BasicInfo.cshtml", clientBasicInfoViewModel);
                    }
                }
            }
            return View();
        }

        /// <summary>
        /// Update client Basic Info
        /// </summary>
        ///  <param name="clientBasicInfoViewModel"></param>
        /// <returns></returns> 
        [HttpPost]
        [AllowAnonymous]
        public JsonResult AddBasicInfo(ClientBasicInfoViewModel clientBasicInfoViewModel)
        {
            ModelState.Remove("Id");
            string userId = !string.IsNullOrEmpty(SessionHelper.UserId) ? SessionHelper.UserId : "";
            string successMessage = (clientBasicInfoViewModel.Id != 0) ? "Basic Info Updated Successfully!" : "Basic Info Added Successfully!";
            try
            {
                clientBasicInfoViewModel.ClientId = Convert.ToInt32(userId);
                clientBasicInfoViewModel.OwnerId = DataManager.ToNullableInt(SessionHelper.UserId);
                clientBasicInfoViewModel.SitePortalOwnerId = SessionHelper.SitePortalOwnerId;
                if (ModelState.IsValid)
                {
                    var result = ClientBasicInfoManager.UpdateClientBasicInfo(clientBasicInfoViewModel);
                    if (result)
                    {
                        return Json(new
                        {
                            success = true,
                            message = successMessage,
                            JsonRequestBehavior.AllowGet
                        });
                    }
                }
                return Json(new
                {
                    success = false,
                    message = "Something Went Wrong!",
                    JsonRequestBehavior.AllowGet
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    success = false,
                    message = "Something Went Wrong!",
                    JsonRequestBehavior.AllowGet
                });
            }
        }

        #endregion

        #region Slider

        /// <summary>
        /// Get Slider List
        /// </summary>
        /// <param name="parms"></param>
        /// <returns></returns>
        public async Task<ActionResult> List(jQueryDataTableParamModel parms)
        {
            try
            {
                var userId = SessionHelper.UserId;

                // get clientId 
                parms.id = userId;
                var Sliders = await ClientBasicInfoManager.GetDataTableList(parms);
                return Json(new
                {
                    aaData = Sliders.Item1,
                    iTotalRecords = Sliders.Item3,
                    iTotalDisplayRecords = Sliders.Item3
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "SettingController =>> List");
                return ReturnAjaxErrorMessage(ex.Message);
            }
        }

        /// <summary>
        /// Open Setting Modal
        /// </summary>
        /// <param name="id"></param>
        /// <param name="isReadOnly"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult OpenModal(long id, bool isReadOnly)
        {
            if (!Request.IsAjaxRequest())
                return null;
            ClientSliderViewModel clientSliderViewModel = new ClientSliderViewModel();
            ViewBag.Header = (id > 0) ? "Edit Slider" : "Add New Slider";
            if (id > 0)
            {
                clientSliderViewModel = ClientBasicInfoManager.GetById(id);
            }
            else
            {
                clientSliderViewModel.IsDeleted = false;
            }
            clientSliderViewModel.IsReadMode = isReadOnly;
            //check for read only 
            if (isReadOnly)
            {
                ViewBag.Header = "View Slider";
            }
            return PartialView("~/Views/Setting/_AddEditSlider.cshtml", clientSliderViewModel);
        }

        /// <summary>
        /// Update client Slider
        /// </summary>
        ///  <param name="clientSliderViewModel"></param>
        /// <returns></returns> 
        [HttpPost]
        [AllowAnonymous]
        public JsonResult AddSlider(ClientSliderViewModel clientSliderViewModel)
        {
            ModelState.Remove("Id");
            string userId = !string.IsNullOrEmpty(SessionHelper.UserId) ? SessionHelper.UserId : "";
            string successMessage = (clientSliderViewModel.Id != 0) ? "Slider Updated Successfully!" : "Slider Added Successfully!";
            try
            {
                clientSliderViewModel.ClientId = Convert.ToInt32(userId);
                clientSliderViewModel.OwnerId = DataManager.ToNullableInt(SessionHelper.UserId);
                clientSliderViewModel.SitePortalOwnerId = SessionHelper.SitePortalOwnerId;
                if (ModelState.IsValid)
                {
                    var result = ClientBasicInfoManager.UpdateClientSlider(clientSliderViewModel);
                    if (result)
                    {
                        return Json(new
                        {
                            success = true,
                            message = successMessage,
                            JsonRequestBehavior.AllowGet
                        });
                    }

                }
                return Json(new
                {
                    success = false,
                    message = "Something Went Wrong!",
                    JsonRequestBehavior.AllowGet
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    success = false,
                    message = "Something Went Wrong!",
                    JsonRequestBehavior.AllowGet
                });
            }
        }

        /// <summary>
        /// Change Client Slider Status
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool ChangeClientSliderStatus(long id)
        {
            try
            {
                return (ClientBasicInfoManager.UpdateClientSliderStatus(id)) ? true : false;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "SettingController=>ChangeClientSliderStatus ");
                return false;
            }
        }

        /// <summary>
        /// Delete ClientSlider
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool DeleteClientSlider(long id)
        {
            try
            {
                var result = ClientBasicInfoManager.DeleteClientSlider(id);

                return (result) ? true : false;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "SettingController Delete");
                return false;
            }
        }

        #endregion

    }
}