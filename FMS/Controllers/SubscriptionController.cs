﻿using FMS.App_Start;
using FMS.Core.Enums;
using FMS.Core.Models;
using FMS.Core.Models.Payment;
using FMS.Core.Models.Stripe;
using FMS.Core.Models.Subscription;
using FMS.Services.Classes;
using System;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FMS.Controllers
{
    public class SubscriptionController : Controller
    {
        // GET: Subscription
        public ActionResult Index()
        {
            string ownerId = SessionHelper.UserId;
            int Id = Convert.ToInt32(ownerId);

            var user = UserManager.GetUserById(Id);
            SubscriptionViewModel subscriptionViewModel = new SubscriptionViewModel
            {
                paymentPlans = PlanManager.GetPlansList(user.SitePortalOwnerId)
            };
            // change the value 

            return View(subscriptionViewModel);
        }

        /// <summary>
        /// Open Subscription Charge Modal
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult OpenSubScriptionModal(long planId)
        {
            if (!Request.IsAjaxRequest())
                return null;

            var planData = SubscriptionManager.GetPaymentPlanByPlanId(planId);
            StripeCustomerCardDetailModel addSubscriptionModel = new StripeCustomerCardDetailModel()
            {
                PlanId = Convert.ToInt32(planId),
                PlanName = planData.Name,
                Charge = planData.Price
            };
            //Stor Id will be used and User Id while Charging for stor subscription 
            return PartialView("~/Views/Subscription/_ChargeSubscription.cshtml", addSubscriptionModel);
        }

        /// <summary>
        /// charge the subscription amount
        /// </summary>
        /// <param name="paymentModel"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        public async Task<ActionResult> ChargeSubscription(PaymentModel paymentModel)
        {
            bool responseStatus = false;
            string responseMessage = "Something Went Wrong!";
            if (ModelState.IsValid)
            {
                string userId = SessionHelper.UserId;
                bool isLiveMode = false;
                if (FMSConfigurationManager.EnvironmentCode == "PROD")
                {
                    isLiveMode = true;
                }
                if (paymentModel.UserId <= 0)
                {
                    paymentModel.UserId = Convert.ToInt32(userId);
                }
                

                
                if (paymentModel.PaymentMethod == PaymentGatewayType.Cash)
                {
                    var plan = await PlanManager.GetPlanById(paymentModel.PlanId);
                    var storeSubscription = SubscriptionManager.GetUserSubscriptionDetail(paymentModel.Id);
                    if (storeSubscription.PlanExpirationDateTime > DateTime.Now)
                    {
                        DateTime planExpiryDate = storeSubscription.PlanExpirationDateTime;
                        DateTime TodayDate = DateTime.Now;
                        var numberOfDays = (planExpiryDate - TodayDate).Days;
                        plan.PlanLengthInDays = plan.PlanLengthInDays + numberOfDays;
                        
                    }
                    
                    int days = plan.PlanLengthInDays ;
                    UserSubscriptionModel userSubscriptionModel = new UserSubscriptionModel()

                    {
                        PlanId = paymentModel.PlanId,
                        PlanPrice = plan.Price,
                        GatewayCustomerId = userId,
                        CreatedBy = userId,
                        IsActive = true,
                        PricePaid = plan.Price,
                        PlanExpirationDateTime = DateTime.Now.AddDays(days),
                        UserId = paymentModel.Id,
                    };

                    var subscriptionDetails = await SubscriptionManager.UpdateUserSubscription(userSubscriptionModel);
                    if (subscriptionDetails.Item1 == true)
                    {
                        PaymentHistoryModel paymentHistoryModel = new PaymentHistoryModel()
                        {
                            PaidPrice = userSubscriptionModel.PricePaid,
                            PaymentStatus = PaymentStatusType.Completed,
                            UserId = userSubscriptionModel.UserId,
                            CreatedBy = userId,
                            PaymentMode = PaymentGatewayType.Cash,
                            SubscriptionId = userSubscriptionModel.Id


                        };
                        var paymenthistory = await PaymentManager.UpdatePaymentHistory(paymentHistoryModel);
                        return Json(new
                        {
                            success = subscriptionDetails.Item2,
                            message = subscriptionDetails.Item2,
                            JsonRequestBehavior.AllowGet
                        });
                    }
                }
                #region fetch stripe keys
                var stripeKeys =await SiteConfigurationManager.GetStripeKeys();
                #endregion
               
                if(stripeKeys!=null && 
                    !string.IsNullOrEmpty(stripeKeys.StripeApiPrivateKey) && 
                    !string.IsNullOrEmpty(stripeKeys.StripeApiPublicKey))
                {
                    StripeManager stripeManager = new StripeManager(stripeKeys);

                    //fetch user details
                    var userDetail = UserManager.GetUserById(paymentModel.UserId);
                    #region create token for card details 
                    StripeCustomerCardDetailModel cardDetaiModel = new StripeCustomerCardDetailModel()
                    {
                        CardNumber = paymentModel.CardNumber,
                        ExpirationMonth = paymentModel.ExMonth,
                        ExpirationYear = paymentModel.ExYear,
                        Cvc = paymentModel.Cvc,
                        CreatedBy=userId,
                        ModifiedBy=userId
                    };
                    var tokenResponse = stripeManager.CreateStripeTokenForCustomerCardDetail(cardDetaiModel);
                    #endregion

                    if (tokenResponse.Item1)
                    {
                        #region create or update stripe customer
                        StripeCustomerModel stripeCustomerModel = new StripeCustomerModel()
                        {
                            Id = paymentModel.CustomerId,
                            StripeCustomerId = paymentModel.GatewayCustomerId,
                            UserName = userDetail.FullName,
                            Description = "Stripe Account For User:" + userDetail.Email,
                            UserEmail = userDetail.Email,
                            PhoneNumber = userDetail.Phone,
                            Address1 = userDetail.Address,
                            //City = userDetail.,
                            //State = clientBankingInfoModel.State,
                            Country = CountryCode.IN.ToString(),
                            //PostalCode = userDetail.,
                            UserId = paymentModel.UserId,
                            CreatedBy = userId,
                            ModifiedBy = userId
                        };
                        var stripeCustomerResponse = paymentModel.CustomerId== 0 ? stripeManager.CreateStripeCustomer(stripeCustomerModel) : stripeManager.UpdateStripeCustomer(stripeCustomerModel);
                        if (stripeCustomerResponse.Item1)
                        {
                            var customerResponse = await StripeManager.UpdateStripeCustomerDetail(stripeCustomerModel);
                            #region save card details
                            //first check is user already have the card then soft delete the existing cards
                            await StripeManager.DeleteUserSaveCards(stripeCustomerModel.Id);

                            cardDetaiModel.StripeCustomerId = stripeCustomerModel.StripeCustomerId;
                            var cardDetailResponse =stripeManager.AddCardForStripeCustomer(cardDetaiModel);
                            if (cardDetailResponse.Item1)
                            {
                                var cardResponse = await StripeManager.UpdateStripeCustomerCardDetail(cardDetaiModel);
                                #region charge the payment
                                int amountInCents = Convert.ToInt32(Math.Round(decimal.Parse(paymentModel.Charge.ToString()), 2) * 100);
                                StripeChargeModel chargeModel = new StripeChargeModel()
                                {
                                    Amount = paymentModel.Charge,
                                    CardId = cardDetaiModel.Id,
                                    PaidAmountInCents = amountInCents,

                                    StripeCustomerId=stripeCustomerModel.StripeCustomerId,
                                    StripeCardId = cardDetaiModel.StripeCardId,
                                    StripeConnectAccountId ="",
                                    Currency = (int)CurrencyType.INR,
                                    Description ="Subscription charge for plan:"+paymentModel.PlanName,
                                    ApplicationFeeAmount =0,
                                    ApplicationFeeAmountInCents =0,
                                    LiveMode = isLiveMode
                                };

                                var chargeResponse = stripeManager.ChargeExistingStripeCustomer(chargeModel);
                                if (chargeResponse.Item1)
                                {
                                    //save the charge detail
                                    await StripeManager.SaveStripeCustomerCharge(chargeModel);
                                    #region payment history
                                    PaymentHistoryModel paymentHistoryModel = new PaymentHistoryModel()
                                    {
                                        GatewayCustomerId = chargeModel.StripeCustomerId,
                                        GatewayCardId = chargeModel.StripeCardId,
                                        GatewayChargeId = chargeModel.StripeChargeId,

                                        UserId = paymentModel.UserId,
                                        PaidPrice = chargeModel.Amount,
                                        PaidPriceInCent = chargeModel.PaidAmountInCents,
                                        Gateway = PaymentGatewayType.Stripe,
                                        Description = chargeModel.Description,
                                        PaymentStatus = PaymentStatusType.Completed
                                    };
                                    var paymentHistory = PaymentManager.UpdatePaymentHistory(paymentHistoryModel);
                                    #endregion

                                    responseStatus = true;
                                    responseMessage = "Plan Subscription Successful!";
                                }
                                else
                                {
                                    responseMessage = chargeResponse.Item2;
                                }
                                #endregion

                            }
                            else
                            {
                                responseMessage = cardDetailResponse.Item2;
                            }
                            #endregion
                        }
                        #endregion
                        else
                        {
                            responseMessage = stripeCustomerResponse.Item2;
                        }
                    }
                    else
                    {
                        //error message regarding card details not valid
                        responseMessage = tokenResponse.Item2;
                    }
                }
                else
                {
                    //show error message
                    responseMessage = "Stripe is not integrated yet!";
                }
            }
            return Json(new
            {
                success = responseStatus,
                message = responseMessage,
                JsonRequestBehavior.AllowGet
            });
        }



        [AllowAnonymous]
        [HttpPost]
        public async Task<ActionResult> ChargeSubscriptionByCash(PaymentModel paymentModel)
        {
            string userId = SessionHelper.UserId;         
            bool responseStatus = false;
            string responseMessage = "Something Went Wrong!";
        
            if (ModelState.IsValid)
            {
                
                if (paymentModel.UserId <= 0)
                {
                    paymentModel.UserId = Convert.ToInt32(userId);
                }
                var plan = await PlanManager.GetPlanById(paymentModel.PlanId);
                int days = plan.PlanLengthInDays;
                UserSubscriptionModel userSubscriptionModel = new UserSubscriptionModel()

                {
                    PlanId = paymentModel.PlanId,
                    PlanPrice = paymentModel.Charge,
                    GatewayCustomerId = userId,
                    CreatedBy = userId,
                    IsActive = true,
                    PricePaid = paymentModel.Charge,
                    PlanExpirationDateTime = DateTime.Now.AddDays(days),
                    UserId = paymentModel.UserId,
                };
               
                var subscriptionDetails = await SubscriptionManager.UpdateUserSubscription(userSubscriptionModel);
                if (subscriptionDetails.Item1 == true)
                {
                    PaymentHistoryModel paymentHistoryModel = new PaymentHistoryModel()
                    {
                        PaidPrice = userSubscriptionModel.PricePaid,
                        PaymentStatus = PaymentStatusType.Completed,
                        UserId = userSubscriptionModel.UserId,
                        CreatedBy = userId,
                        PaymentMode = PaymentGatewayType.Cash,
                        SubscriptionId = userSubscriptionModel.Id
                        
                        
                    };
                    var paymenthistory = await PaymentManager.UpdatePaymentHistory(paymentHistoryModel);
                    return Json(new
                    {
                        success = subscriptionDetails.Item2,
                        message = subscriptionDetails.Item2,
                        JsonRequestBehavior.AllowGet
                    });
                }                                        
            }
            return Json(new
            {
                success = responseStatus,
                message = responseMessage,
                JsonRequestBehavior.AllowGet
            });
        }


        /// <summary>
        /// Payment history list
        /// </summary>
        /// <param name="parms"></param>
        /// <returns></returns>
        public async Task<ActionResult> List(jQueryDataTableParamModel parms)
        {
            try
            {
                var paymentHistorylist = await SubscriptionManager.GetDataTableList(parms, SessionHelper.UserId);
                return Json(new
                {
                    aaData = paymentHistorylist.Item1,
                    iTotalRecords = paymentHistorylist.Item3,
                    iTotalDisplayRecords = paymentHistorylist.Item3
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
                    
        }

    }
}