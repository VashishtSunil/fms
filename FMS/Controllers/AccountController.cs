﻿using FMS.App_Start;
using FMS.Core.Enums;
using FMS.Core.Helper;
using FMS.Core.Models;
using FMS.Services.Classes;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FMS.Controllers
{
    public class AccountController : Controller
    {
        #region Method

        #region Login

        // GET: Account
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// login page
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public ActionResult LoginUser()
        {
            return View(new LoginModel());
        }

        /// <summary>
        /// login page method
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> LoginUser(LoginModel login)
        {
           
            if (!ModelState.IsValid)
                return View("~/views/account/loginuser.cshtml");

            Serilog.Log.Information("User Login Process Start!");

            if (ModelState.IsValid)
            {
                var user = await UserManager.Login(login);
                Serilog.Log.Information("User Info:" + user);

                if (user != null && user.Id > 0)
                {
                    SessionHelper.UserId = user.Id.ToString();
                    //

                    string[] roles = user.Role.Split(',');
                    if (roles.Count() > 0)
                    {
                        HttpContext.Session["UserRole"] = user.Role;
                    }
                    ///Member / Index
                    return RedirectToAction("Index", "Home");

                }
            }                   
            TempData["UserError"] = "The Email or Password is incorrect.";
            return View("~/views/account/loginuser.cshtml");
        }

        /// <summary>
        /// Forgot Password
        /// </summary>
        /// <param name="Detail"></param>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult ForgotPassword(string Detail)
        {
            ForgotPasswordModel model = new ForgotPasswordModel
            {
                Parameter = Detail
            };
            return View(model);
        }

        /// <summary>
        /// Forgot User Password
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotUserPassword(ForgotPasswordModel model)
        {
            if (ModelState.IsValid)
            {
                var paths = (AppDomain.CurrentDomain.BaseDirectory + @"Views\Shared\EmailTemplate\ForgetPassword.cshtml");
                var EmailBody = System.IO.File.ReadAllText(paths);

                var user = await UserManager.IsEmailExist(model.Email);

                if (user != null)
                {
                    string code = CipherHelper.GenerateCode();

                    var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, email = model.Email, token = code }, protocol: Request.Url.Scheme);
                    string body = EmailBody.Replace("#name#", user.Username).Replace("#url#", callbackUrl);
                    string subject = "Forget Password";
                    EmailModel emailModel = new EmailModel
                    {
                        FromEmail = "crm@FMSmatters.com",
                        From = "FMS_Matters",
                        ToEmail = user.Email,
                        Subject = "Forget Password",
                        Body = body,

                    };
                    await EmailSender.SendEmailAsync(model.Email, subject, body);

                    await UserManager.UpdateUserCode(model.Email, code, false);
                    return RedirectToAction("ForgotPasswordConfirmation", "Account");


                }
                ModelState.AddModelError("", "User not found");
                return View("~/Views/Account/ForgotPassword.cshtml", model);
            }
            return View("~/Views/Account/ForgotPassword.cshtml", model);
        }

        /// <summary>
        /// Forgot Password Confirmation
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        /// <summary>
        /// Reset Password
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="token"></param>
        /// <param name="userType"></param>
        /// <param name="email"></param>
        /// <returns></returns>

        [AllowAnonymous]
        public ActionResult ResetPassword(string userId, string token, string userType, string email)
        {
            var obj = new ResetPasswordModel
            {
                Code = token,
                UserType = userType,
                Email = email
            };
            return token == null ? View("Error") : View(obj);
        }

        /// <summary>
        /// Reset Password
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ResetUserPassword(ResetPasswordModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            model.UserType = "Admin";
            var message = string.Empty;
            if (model.UserType == "Admin")
            {
                var user = AccountManager.GetUserByEmailAndCode(model.Email, model.Code);
                if (user == null)
                {
                    TempData["Error"] = "Token is expired please resend mail.";
                    return RedirectToAction("Logout", "Account");
                }
                var passUpdate = AccountManager.ResetUserPassword(model.Password, model.Email);
                message = passUpdate.Item2;
            }

            return RedirectToAction("ResetPasswordConfirmation", "Account", new { userType = model.UserType, message = message });

        }


        /// <summary>
        /// Logout 
        /// </summary>
        /// <returns></returns>
        public ActionResult Logout()
        {
            SessionHelper.KillCurrentSession();
            return RedirectToAction("LoginUser", "Account");

        }

        /// <summary>
        /// Check Email
        /// </summary>
        /// <param name="emailId"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public async Task<JsonResult> CheckEmail(string Email)
        {
            var user = await UserManager.IsEmailExist(Email);
            return (user.Email == null) ? Json(true, JsonRequestBehavior.AllowGet) : Json(false, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Register

        /// <summary>
        /// Register
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult Register(string clientId)
        {
            RegisterViewModel registerViewModel = new RegisterViewModel();
            registerViewModel.OwnerId = Convert.ToInt32(clientId);

            return View(registerViewModel);
        }

        /// <summary>
        /// Register User
        /// </summary>
        /// <param name="registerViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> RegisterUser(RegisterViewModel registerViewModel)
        {
            string msg = String.Empty;
            try
            {
                if (!ModelState.IsValid)
                    return View("~/Views/Account/Register.cshtml");
                Serilog.Log.Information("User Register Process Start!");
                if (ModelState.IsValid)
                {
                    string code = CipherHelper.GenerateCode();
                    registerViewModel.Code = code;
                    registerViewModel.OwnerId = registerViewModel.OwnerId;              
                    var user = await UserManager.Register(registerViewModel);
                    if (user.Item1)
                    {
                        UserInRoleModel userRoleModel = new UserInRoleModel()
                        {
                            UserId = registerViewModel.Id,
                            RoleId = registerViewModel.RoleId,
                        };
                        UserManager.UpdateUserInRole(userRoleModel);

                        UserDetailModel userDetailModel = new UserDetailModel()
                        {
                            UserId = registerViewModel.Id,
                            FirstName = registerViewModel.FirstName,
                            LastName = registerViewModel.LastName,
                            Phone = registerViewModel.Phone,
                            Address = registerViewModel.Address
                        };
                        UserManager.UpdateUserDetail(userDetailModel);
                        var paths = (AppDomain.CurrentDomain.BaseDirectory + @"Views\Shared\EmailTemplate\VerifyEmail.cshtml");
                        var EmailBody = System.IO.File.ReadAllText(paths);
                        var callbackUrl = Url.Action("ResetUserAccessPassword", "Account", new { userId = user.Item3.Id, email = registerViewModel.Email, token = code }, protocol: Request.Url.Scheme);
                        string body = EmailBody.Replace("#name#", user.Item3.Username).Replace("#url#", callbackUrl);
                        string subject = "Verify your account";
                        await EmailSender.SendEmailAsync(registerViewModel.Email, subject, body);
                    }
                    Serilog.Log.Information("User Register:" + user.Item2);
                    msg = user.Item2;
                }
            }
            catch (Exception ex)
            {
                msg = "Something went wrong";
            }
            return RedirectToAction("RegisterConfirmation", new { message = msg });
        }

        /// <summary>
        /// Register Confirmation View
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public ActionResult RegisterConfirmation(string message)
        {
            ViewBag.Message = message;
            return View();
        }

        /// <summary>
        /// Verify Email
        /// </summary>
        /// <param name="email"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public ActionResult VerifyEmail(string email, string token)
        {
            var verify = UserManager.VerifyEmail(email, token);
            ViewBag.Message = verify.Item2;
            return View();
        }

        #endregion

        #region  ResetUserAccessPassword

        /// <summary>
        /// Reset User Access Password
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="token"></param>
        /// <param name="userType"></param>
        /// <param name="email"></param>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult ResetUserAccessPassword(string userId, string token, string userType, string email)
        {
            var obj = new ResetPasswordViewModel
            {
                Code = token,
                UserType = userType,
                Email = email
            };
            return token == null ? View("Error") : View(obj);
        }

        /// <summary>
        /// Reset User Access Password
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetUserAccessPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
                return View("~/Views/Account/ResetUserAccessPassword.cshtml", model);

            var user = await UserManager.IsEmailExist(model.Email);
            if (user != null && user.Id > 0)
            {
                if (user.Code == model.Code && !await UserManager.IsCodeUsed(model.Email, model.Code))
                {
                    var result = await UserManager.ResetPassword(model.Email, model.Password);
                    string message = (result) ? "Successfully" : "Something went worng! Please try again";

                    if (result)
                    {
                        return RedirectToAction("ResetPasswordConfirmation", "Account", new { userType = model.UserType, message = message});
                    }
                }
                else
                {
                    TempData["Error"] = "Token is expired please resend mail.";
                }
            }
            else
            {
                TempData["UserError"] = "User Not Found.";
            }
            return View("~/Views/Account/ResetUserAccessPassword.cshtml", model);
        }

        /// <summary>
        /// Reset Password Confirmation
        /// </summary>
        /// <param name="userType"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation(string userType, string message)
        {
            var obj = new ResetPasswordModel();
            obj.UserType = userType;
            ViewBag.Message = message;
            return View(obj);
        }

        #endregion

        #region Forgot Password

        /// <summary>
        /// Forgot Password Profile
        /// </summary>
        /// <returns></returns>
        public ActionResult ForgotPasswordProfile()
        {
            var userId = SessionHelper.UserId;
            var forgotPasswordViewModel = new ForgotPasswordViewModel();
            if (!string.IsNullOrEmpty(userId))
            {
                var userDetail = UserManager.GetUserById(Convert.ToInt32(userId));
                if (userDetail != null)
                {
                    forgotPasswordViewModel.Id = userDetail.Id;
                    forgotPasswordViewModel.Email = userDetail.Email;
                    forgotPasswordViewModel.IsActive = userDetail.IsActive;

                }
            }
            return View(forgotPasswordViewModel);
        }

        /// <summary>
        /// Update Forgot Password
        /// </summary>
        ///  <param name="forgotPasswordViewModel"></param>
        /// <returns></returns> 
        [HttpPost]
        public JsonResult AddForgotPassword(ForgotPasswordViewModel forgotPasswordViewModel)
        {
            ModelState.Remove("Id");
            string successMessage = (forgotPasswordViewModel.Id != 0) ? "Password Updated Successfully!" : "Password Add Successfully!";
            try
            {
                if (ModelState.IsValid)
                {
                    var result = UserManager.UpdateForgotPassword(forgotPasswordViewModel);
                    if (result)
                    {
                        return Json(new
                        {
                            success = true,
                            message = successMessage,
                            JsonRequestBehavior.AllowGet
                        });
                    }
                }
                return Json(new
                {
                    success = false,
                    message = "Something Went Wrong!",
                    JsonRequestBehavior.AllowGet
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    success = false,
                    message = "Something Went Wrong!",
                    JsonRequestBehavior.AllowGet
                });
            }
        }

        /// <summary>
        /// get User Permission
        /// </summary>
        /// <param name="path"></param>
        /// <param name="userRoles"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetUserPermission(string path, string userRoles)
        {
            return Json(new
            {
                success = true,
                message = FunctionManager.GetPermissionByUrl(path, userRoles),
                JsonRequestBehavior.AllowGet
            });
        }

        #endregion

        #endregion

    }
}