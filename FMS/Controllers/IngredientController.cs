﻿using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using FMS.App_Start;
using FMS.Core.Models;
using FMS.Services.Classes;
using Serilog;

namespace FMS.Controllers
{
    [AuthorizationFilter]
    public class IngredientController : BaseController
    {
        #region Ingredient
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Get Ingredient List
        /// </summary>
        /// <param name="parms"></param>
        /// <returns></returns>
        public async Task<ActionResult> List(jQueryDataTableParamModel parms)
        {
            try
            {
                var Ingredients = await IngredientManager.GetDataTableList(parms);
                return Json(new
                {
                    aaData = Ingredients.Item1,
                    iTotalRecords = Ingredients.Item3,
                    iTotalDisplayRecords = Ingredients.Item3
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "IngredientController =>> List");
                return ReturnAjaxErrorMessage(ex.Message);
            }
        }

        /// <summary>
        /// Open Ingredient Modal
        /// </summary>
        /// <param name="id"></param>
        /// <param name="isReadOnly"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult OpenModal(long id, bool isReadOnly)
        {
            if (!Request.IsAjaxRequest())
                return null;
            IngredientsModel IngredientModel = new IngredientsModel();
            ViewBag.Header = (id > 0) ? "Edit Ingredient" : "Add New Ingredient";
            if (id > 0)
            {
                IngredientModel = IngredientManager.GetById(id);
            }
            else
            {
                IngredientModel.IsDeleted = false;
            }
            // Get All Function List           
            IngredientModel.IsReadMode = isReadOnly;
            //check for read only 
            if (isReadOnly)
            {
                ViewBag.Header = "View Ingredient";
            }
            return PartialView("~/Views/Ingredient/_AddEdit.cshtml", IngredientModel);
        }

        /// <summary>
        /// Add new Ingredient
        /// </summary>
        ///  <param name="IngredientModel"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult AddIngredient(IngredientsModel IngredientsModel)
        {
            ModelState.Remove("Id");
            string successMessage = (IngredientsModel.Id != 0) ? "Ingredient Updated Successfully!" : " Ingredient Added Successfully!";
            try
            {
                if (ModelState.IsValid)
                {
                    var result = IngredientManager.Update(IngredientsModel);
                    if (result)
                    {                                           
                        return Json(new
                        {
                            success = true,
                            message = successMessage,
                            JsonRequestBehavior.AllowGet
                        });
                    }
                }
                return Json(new
                {
                    success = false,
                    message = "Something Went Wrong!",
                    JsonRequestBehavior.AllowGet
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    success = false,
                    message = "Something Went Wrong!",
                    JsonRequestBehavior.AllowGet
                });
            }
        }

        /// <summary>
        /// Delete Ingredient
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool DeleteIngredient(int id)
        {
            try
            {
                var result = IngredientManager.DeleteIngredient(id);
                return result ? true : false;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "IngredientController Delete");
                return false;
            }
        }
       

        /// <summary>
        /// check whether Ingredient name is duplicate or not
        /// </summary>
        /// <param name="IngredientModel"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        public JsonResult IsIngredientDuplicate(IngredientsModel IngredientModel)
        {
            return Json(!IngredientManager.IsIngredientDuplicate(IngredientModel));
        }
        #endregion
    }
}