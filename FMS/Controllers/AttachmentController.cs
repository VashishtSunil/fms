﻿
using FMS.App_Start;
using FMS.Core.Enums;
using FMS.Core.Models;
using FMS.Services.Classes;
using Serilog;
using System;
using System.IO;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace EmailMarketing.Controllers
{
    public class AttachmentController : Controller
    {
        #region Attachment 

        /// <summary>
        /// Upload Attachment ToLocal
        /// </summary>
        /// <returns></returns>
        public ActionResult UploadAttachmentToLocal()
        {
            bool status = false;
            dynamic response = "";
            try
            {
                //throw new InvalidOperationException("Logfile cannot be read-only");
                //AttachmentModel attachmentModel = null;
                if (Request.Files.Count > 0)
                {
                    foreach (string file in Request.Files)
                    {
                        var uploadedfile = Request.Files[file];
                        if (uploadedfile != null)
                        {
                            if (uploadedfile.FileName != null)
                            {
                                //code for saving uploading file in db as well as in folder
                                string fileName = Path.GetFileName(uploadedfile.FileName);
                                fileName = fileName.Replace(' ', '_');
                                string dummyFilename = DateTime.Now.ToString("MM-dd-yyyy") + "-" + file + "_" + fileName;
                                //code for logo image upload
                                var filePhysicalLocation = AppDomain.CurrentDomain.BaseDirectory + "Media\\";
                                Directory.CreateDirectory(filePhysicalLocation);
                                var fileVirtualPath = "Media/";
                                string filePath = Path.Combine(Server.MapPath(fileVirtualPath), dummyFilename);
                                string fileExtension = Path.GetExtension(filePath);
                                MimeType fileExtensionEnums = MimeType.None;
                                #region converted fileExtension into MimeType enums
                                if (Enum.IsDefined(typeof(MimeType), fileExtension.Replace(".", "").ToLower()))
                                {
                                    //If it exists
                                    fileExtensionEnums = ((MimeType)Enum.Parse(typeof(MimeType), fileExtension.Replace(".", "").ToLower()));
                                }
                                #endregion
                                uploadedfile.SaveAs(filePath);
                                status = true;
                                response = dummyFilename;
                            }
                        }
                    }
                }
                else
                {
                    response = "Please Select File First!";
                }
            }
            catch (Exception ex)
            {
                status = false;
                response = ex.Message;// "Something Went Wrong! Please Use Another File.";
            }
            return Json(new
            {
                success = status,
                message = response,
                JsonRequestBehavior.AllowGet
            });
        }

        /// <summary>
        /// delete attachment
        /// </summary>
        /// <param name="name"></param>
        /// <param name="assetId"></param>
        /// <returns></returns>
        public async Task<bool> DeleteAttachment(string name, int assetId)
        {
            try
            {
                string userId = SessionHelper.UserId;
                string fullPath = Request.MapPath("~/Attachment/Media/" + name);
                if (System.IO.File.Exists(fullPath))
                {
                    System.IO.File.Delete(fullPath);
                }
                #region update location=null from asset table as well
            
                #endregion
                return true;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Attachment Deleted");
                return false;
            }
        }

        /// <summary>
        /// Upload Attachment
        /// </summary>
        /// <returns></returns>
        public ActionResult UploadAttachment()
        {
            bool status = false;
            dynamic response = "";
            try
            {
                AttachmentModel attachmentModel = null;
                if (Request.Files.Count > 0)
                {
                    foreach (string file in Request.Files)
                    {
                        var uploadedfile = Request.Files[file];
                        if (uploadedfile != null)
                        {

                            if (uploadedfile.FileName != null)
                            {
                                string fileName = Path.GetFileName(uploadedfile.FileName);
                                fileName = fileName.Replace(' ', '_');
                                int size = uploadedfile.ContentLength;
                                byte[] bytes;
                                using (BinaryReader br = new BinaryReader(uploadedfile.InputStream))
                                {
                                    bytes = br.ReadBytes(uploadedfile.ContentLength);
                                }
                                string fileExtension = Path.GetExtension(fileName);

                                MimeType fileExtensionEnums = MimeType.None;
                                #region converted fileExtension into MimeType enums
                                if (Enum.IsDefined(typeof(MimeType), fileExtension.Replace(".", "").ToLower()))
                                {
                                    //If it exists
                                    fileExtensionEnums = ((MimeType)Enum.Parse(typeof(MimeType), fileExtension.Replace(".", "").ToLower()));
                                }
                                #endregion

                                attachmentModel = new AttachmentModel()
                                {
                                    CreatedBy = !string.IsNullOrEmpty(SessionHelper.UserId) ? SessionHelper.UserId : "",
                                    MIME = fileExtensionEnums,
                                    Size = size,
                                    Location = "",
                                    DummyFileName = "",
                                    FileName = fileName,
                                    ImageBytes = bytes,
                                    ContentType = uploadedfile.ContentType
                                };
                            }

                        }
                    }

                    if (attachmentModel != null)
                    {
                        var result = AttachmentManager.UpdateAttachments(attachmentModel);

                        if (result.Item1)
                        {

                            status = true;
                            response = attachmentModel.Id + "," + attachmentModel.FileName;
                        }
                        else
                        {
                            status = false;
                            response = result.Item2;
                        }
                    }
                }
                else
                {
                    response = "Please Select File First!";
                }
            }
            catch (Exception ex)
            {
                status = false;
                response = "Something Went Wrong! Please Use Another File.";
            }
            return Json(new
            {
                success = status,
                message = response,
                JsonRequestBehavior.AllowGet
            });
        }

        /// <summary>
        /// delete attachment and its mapping based on its id
        /// </summary>
        /// <param name="attachmentId"></param>
        /// <returns></returns>
        public async Task<bool> DeleteAttachmentById(int attachmentId)
        {
            try
            {
                string userId = SessionHelper.UserId;
                var response = AttachmentManager.DeleteAttachmentById(attachmentId, userId);
                if (response.Item1)
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "DeleteSelectedProgram");
                return false;
            }
        }

        /// <summary>
        /// show the image on the based of its id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public FileContentResult ImagePreview(int id)
        {
            byte[] byteArray = new byte[0];
            string applicationType = "image/jpeg";
            var attachment = AttachmentManager.GetById(id);
            if (attachment != null)
            {
                byteArray = attachment.ImageBytes == null ? new byte[0] : attachment.ImageBytes;
                applicationType = string.IsNullOrEmpty(attachment.ContentType) ? "image/jpeg" : attachment.ContentType;
            }
            return new FileContentResult(byteArray, applicationType);
        }

        #endregion
    }
}