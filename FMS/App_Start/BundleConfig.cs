﻿using System.Web;
using System.Web.Optimization;

namespace FMS
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/landingPageScript").Include(

                       "~/Scripts/landing/js/bootstrap.bundle.min.js",
                       "~/Scripts/landing/js/tiny-slider.js",
                       "~/Scripts/landing/js/glightbox.min.js",
                       "~/Scripts/landing/js/aos.js",
                       "~/Scripts/landing/js/google-map.js",
                       "~/Scripts/landing/js/rellax.min.js",
                        "~/Scripts/landing/js/main.js"

                       ));



            bundles.Add(new ScriptBundle("~/bundles/landingPageScriptFile").Include(

                     "~/Content/lib/jquery-ui/jquery-ui.js",
                     "~/Content/lib/datatables/jquery.dataTables.min.js",
                     "~/Content/lib/datatables-bs4/js/dataTables.bootstrap4.min.js",
                     "~/Content/js/datatables.responsive.js",
                     "~/Content/lib/datatables-buttons/js/dataTables.buttons.min.js",
                     "~/Content/lib/datatables-buttons/js/dataTables.buttons.min.js",
                     "~/Content/lib/datatables/datatables.responsive.min.js",
                     "~/Content/lib/datatables/responsive.bootstrap4.min.js",
                      "~/Content/lib/datatables-buttons/js/vfs_fonts.js",
                      "~/Content/lib/datatables-buttons/js/buttons.html5.min.js",
                     "~/Content/lib/summernote/summernote-bs4.min.js",
                     "~/Scripts/bootstrap-switch.js",
                     "~/Scripts/jquery.validate.min.js",
                     "~/Content/lib/jquery-validation-unobtrusive/jquery.validate.unobtrusive.min.js",
                     "~/Content/lib/toastr/toastr.min.js",
                     "~/Content/lib/ladda/spin.min.js",
                     "~/Content/lib/ladda/ladda.min.js",
                     "~/Content/lib/ladda/ladda.jquery.min.js",
                     "~/Content/lib/sweetalert/sweetalert.min.js",
                     "~/Content/lib/select2/js/select2.full.min.js",
                     "~/Content/lib/moment/moment.min.js",
                     "~/Content/lib/bootstrap-confirm-button/bootstrap-confirm-button.min.js",
                     "~/Content/lib/dropzone/dropzone.js",
                     "~/Content/js/Constants.js",
                     "~/Content/js/Utils.js",
                     "~/Content/js/MainViewModel.js",
                     "~/Content/js/site.js",
                     "~/Content/js/custom.js"
                  ));


            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.bundle.min.js",
                      "~/Scripts/sidenav/sidenav.js",
                      "~/Content/fontawesome/css/fontawesome_5.js"
                      ));
            //Site Js 
            bundles.Add(new ScriptBundle("~/bundles/siteJs").Include(
                     //"~/Content/lib/bootstrap/dist/js/bootstrap.bundle.js",
                     "~/Content/lib/jquery-ui/jquery-ui.js",
                     // "~/Content/js/adminlte.min.js",
                     "~/Content/lib/datatables/jquery.dataTables.min.js",
                     "~/Content/lib/datatables-bs4/js/dataTables.bootstrap4.min.js",
                     "~/Content/js/datatables.responsive.js",
                     "~/Content/lib/datatables-buttons/js/dataTables.buttons.min.js",
                     "~/Content/lib/datatables-buttons/js/dataTables.buttons.min.js",
                     "~/Content/lib/datatables/datatables.responsive.min.js",
                     "~/Content/lib/datatables/responsive.bootstrap4.min.js",
                     "~/Content/lib/datatables-buttons/js/jszip.min.js",
                     "~/Content/lib/datatables-buttons/js/pdfmake.min.js",
                      "~/Content/lib/datatables-buttons/js/vfs_fonts.js",
                      "~/Content/lib/datatables-buttons/js/buttons.html5.min.js",
                     "~/Content/lib/summernote/summernote-bs4.min.js",
                     "~/Scripts/bootstrap-switch.js",
                      "~/Scripts/bootstrap-toggle.min.js",
                     "~/Content/lib/ekko-lightbox/ekko-lightbox.min.js",
                     "~/Scripts/jquery.validate.min.js",
                     "~/Content/lib/jquery-validation-unobtrusive/jquery.validate.unobtrusive.min.js",
                     "~/Content/lib/toastr/toastr.min.js",
                     "~/Content/lib/ladda/spin.min.js",
                     "~/Content/lib/ladda/ladda.min.js",
                     "~/Content/lib/ladda/ladda.jquery.min.js",
                     "~/Content/lib/sweetalert/sweetalert.min.js",
                     "~/Content/lib/select2/js/select2.full.min.js",
                     "~/Content/lib/datepicker/js/bootstrap-datepicker.min.js",
                     "~/Content/lib/moment/moment.min.js",
                     "~/Content/lib/daterangepicker/daterangepicker.js",
                     "~/Content/lib/bootstrap-confirm-button/bootstrap-confirm-button.min.js",
                     "~/Content/lib/dropzone/dropzone.js",
                     "~/Content/lib/tinymce/tinymce.min.js",
                     "~/Content/lib/jqueryui/jquery-ui.min.js",
                     "~/Content/dist/js/adminlte.min.js",
                     "~/Content/js/Constants.js",
                     "~/Content/js/Utils.js",
                     "~/Content/js/MainViewModel.js",
                     "~/Content/js/site.js",
                     "~/Content/js/custom.js"
                    
                  ));


            bundles.Add(new StyleBundle("~/Content/css/fmsUI").Include(
                      "~/Content/css/main.css",
                      "~/Content/css/sidenav.css",
                      "~/Content/fontawesome/css/all.min.css",
                      // for datatable
                      "~/Content/lib/datatables-bs4/css/dataTables.bootstrap4.min.css",
                      "~/Content/lib/datatables/responsive.bootstrap4.min.css",
                      "~/Content/lib/datatables-bs4/css/buttons.dataTables.min.css",
                       // for
                       "~/Content/bootstrap-switch.css",
                       "~/Content/lib/select2/css/select2.min.css",
                       "~/Content/lib/dropzone/dropzone.css",
                        "~/Content/lib/toastr/toastr.css",
                        "~/Content/lib/ladda/ladda-themeless.min.css",
                        "~/Content/lib/datepicker/css/bootstrap-datepicker.css",
                         "~/Content/css/bootstrap-toggle.min.css",
                        "~/Content/lib/jqueryui/jquery-ui.css"
                      ));

            bundles.Add(new StyleBundle("~/Content/auth").Include(
                "~/Content/css/bootstrap.min.css",
                 "~/Content/css/floating-labels.css"
                 ));


            bundles.Add(new StyleBundle("~/bundles/landingPageStyle").Include(
              "~/Content/css/landing/animate.css",
               "~/Content/css/landing/aos.css",
                "~/Content/css/landing/flaticon.css",
                 "~/Content/css/landing/glightbox.min.css",
                  "~/Content/css/landing/landing.css",
                   "~/Content/css/landing/tiny-slider.css",
                   "~/Content/fontawesome/css/all.min.css",
                     "~/Content/css/landing/main.css",
                      "~/Content/lib/toastr/toastr.css",
                        "~/Content/lib/ladda/ladda-themeless.min.css"

               ));
        }
    }
}
