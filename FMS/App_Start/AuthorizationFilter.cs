﻿using System.Web.Mvc;
using IAuthorizationFilter = System.Web.Mvc.IAuthorizationFilter;

namespace FMS.App_Start
{
    public class AuthorizationFilter : AuthorizeAttribute, IAuthorizationFilter
    {
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (filterContext.ActionDescriptor.IsDefined(typeof(AllowAnonymousAttribute), true)
                || filterContext.ActionDescriptor.ControllerDescriptor.IsDefined(typeof(AllowAnonymousAttribute), true))
            {
                // Don't check for authorization as AllowAnonymous filter is applied to the action or controller

                //check if current user has the role of client admin
                //var userRoles = App_Start.SessionHelper.UserRoles;
                //if (!string.IsNullOrEmpty(userRoles))
                //{
                //    var roleIdArray = userRoles.Split(',');
                //    int[] integerRoleIds = roleIdArray.Select(int.Parse).ToArray();
                //    if (integerRoleIds != null && integerRoleIds.Length > 0)
                //    {
                //        var rolesDetail = RoleManager.GetUserRoles(integerRoleIds);
                //        if(rolesDetail != null && rolesDetail.Count > 0 && rolesDetail.Any(x => x.RoleName == "Basic Client" || x.RoleName == "Pro Client"))
                //        {
                //            //that means current user is client admin
                //            //so fetch whether this user is in free time period or not
                //            //fetch user subscription data
                //            var userSubscriptionDetail=PaymentManager.GetUserSubscriptionDetail(Convert.ToInt32(App_Start.SessionHelper.UserId));
                //            if(userSubscriptionDetail!=null && userSubscriptionDetail.PlanExpirationDateTime.Date <= DateTime.Now.Date)
                //            {
                //               filterContext.Result = new RedirectResult("/Payment/PaymentSetting");
                //            }
                //        }
                //    }
                //}
                return;
            }

            // Check for authorization
            if (!string.IsNullOrEmpty(SessionHelper.UserId))
            {
                // string actionName = HttpContext.Current.Request.RequestContext.RouteData.GetRequiredString("action");

                //string controllerName = HttpContext.Current.Request.RequestContext.RouteData.GetRequiredString("controller");

                //var acccessReult = AuthorizationManager.IsUrlAccessibleByRole(App_Start.SessionHelper.UserRoles, controllerName, actionName);
                ////check for access 
                //if (!acccessReult.Item1)
                //{
                //    filterContext.Result = new RedirectResult("/Account/LoginUser");
                //}
                return;

            }
            else
            {
                filterContext.Result = new RedirectResult("/Account/LoginUser");
            }
        }
    }
}