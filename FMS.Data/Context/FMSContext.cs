using FMS.Core;
using FMS.Core.DataModels;
using FMS.Core.DataModels.Attachment;
using EntityFramework.DynamicFilters;
using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;

using FMS.Core.DataModels.ApiUser;
using FMS.Core.DataModels.Plan;
using FMS.Core.DataModels.SiteConfiguration;
using FMS.Core.DataModels.Subscription;
using FMS.Core.DataModels.Stripe;
using FMS.Core.DataModels.Payment;
using FMS.Core.DataModels.Setting;
using FMS.Core.DataModels.Diet;

namespace FMS.Data.Context
{
    public class FMSContext : DbContext
    {
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer<FMSContext>(null);
            base.OnModelCreating(modelBuilder);

            // for soft delete filter 
            modelBuilder.Filter("IsDeleted",
             (ISoftDelete d) => d.IsDeleted, false);
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

        }
        public FMSContext()
            : base("DefaultConnection")
        {
        }

        #region ADO DbSets
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<User> Users { get; set; }
        //API user 
        public DbSet<ApiUser> ApiUsers { get; set; }
        public DbSet<ApiToken> ApiTokens { get; set; }
        public DbSet<RequestResponse> RequestResponses { get; set; }


        //Look up 
        public DbSet<LookUpDomain> LookUpDomains { get; set; }
        public DbSet<LookUpDomainValue> LookUpDomainValues { get; set; }

        public virtual DbSet<UserInRole> UserInRole { get; set; }
        public virtual DbSet<UserMap> UserMap { get; set; }
        public virtual DbSet<RoleFunction> RoleFunction { get; set; }

        //for Couse basic objects 

        //plan and its feature
        public virtual DbSet<Plan> Plans { get; set; }
        public virtual DbSet<PlanFeature> PlanFeatures { get; set; }

        public virtual DbSet<Function> Functions { get; set; }
        public virtual DbSet<Country> Countries { get; set; }
        public virtual DbSet<State> States { get; set; }
   
        public virtual DbSet<UserDetail> UserDetails { get; set; }
        public virtual DbSet<ClientBasicInfo> ClientBasicInfos { get; set; }
        public virtual DbSet<ClientSlider> ClientSliders { get; set; }
        //Attachemnt
        public DbSet<Attachment> Attachments { get; set; }
        public DbSet<AttachmentMapping> AttachmentMappings { get; set; }
        //public DbSet<PaymentPlan> PaymentPlans { get; set; }
        //public DbSet<PaymentPlanFeature> PaymentPlanFeatures { get; set; }

        //News
        public DbSet<News> News { get; set; }

        //site configuration table
        public DbSet<SiteConfiguration> SiteConfigurations { get; set; }

        //subscription table
        public DbSet<UserSubscription> UserSubscriptions { get; set; }
        
        //stripe tables
        public DbSet<StripeConnectAccount> StripeConnectAccounts { get; set; }
        public DbSet<StripeConnectAccountDocument> StripeConnectAccountDocuments { get; set; }
        public DbSet<StripeCustomer> StripeCustomers { get; set; }
        public DbSet<StripeCustomerCardDetail> StripeCustomerCardDetails { get; set; }
        public DbSet<StripeCharge> StripeCharges { get; set; }
        public DbSet<StripeRefund> StripeRefunds { get; set; }

        //payment tables
        public DbSet<PaymentHistory> PaymentHistories { get; set; }
        public DbSet<RefundHistory> RefundHistories { get; set; }

        //subscription setting table
        public DbSet<SiteSetting> SiteSettings { get; set; }
        public DbSet<Ingredient> Ingredients { get; set; }
        public DbSet<Diet> Diets { get; set; }
        public DbSet<DietMeal> DietMeals { get; set; }
        public DbSet<DietMealIngredient> DietMealIngredients { get; set; }
        #endregion

        // Automatically add the times the entity got created/modified
        public override int SaveChanges()
        {
            string tempInfo = String.Empty;

            var entries = ChangeTracker.Entries().ToList();
            for (int i = 0; i < entries.Count; i++)
            {
                if (entries[i].State == EntityState.Unchanged || entries[i].State == EntityState.Detached || entries[i].State == EntityState.Deleted) continue;

                var hasInterfaceInheritDb = entries[i].Entity as BaseEntity;
                if (hasInterfaceInheritDb == null) continue;

                if (entries[i].State == EntityState.Added)
                {
                    var created = entries[i].Property("CreatedOn");
                    if (created != null)
                    {
                        created.CurrentValue = DateTime.Now;
                    }
                }
                if (entries[i].State == EntityState.Modified)
                {
                    var modified = entries[i].Property("ModifiedOn");
                    if (modified != null)
                    {
                        modified.CurrentValue = DateTime.Now;
                    }
                }
            }
            return base.SaveChanges();
        }
    }
}
