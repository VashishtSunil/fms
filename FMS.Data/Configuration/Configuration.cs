﻿
using FMS.Core.DataModels;
using System.Data.Entity.Migrations;
using System.Linq;
using FMS.Core.Enums;
using FMS.Data.Context;
using System.Collections.Generic;
using System;
using FMS.Core.DataModels.Plan;
using FMS.Core.DataModels.SiteConfiguration;
using FMS.Core.DataModels.Diet;
using System.Xml.Linq;

namespace FMS.Data.Configuration
{
    public class Configuration : DbMigrationsConfiguration<FMSContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(FMSContext context)
        {
            #region LookUpDomain
            var lookUpDomain = context.LookUpDomains.Where(x => x.Code == LookUpDomainCode.SiteSetting.ToString()).FirstOrDefault();
            if (lookUpDomain == null)
            {
                LookUpDomain lookUp = new LookUpDomain()
                {
                    Code = LookUpDomainCode.SiteSetting.ToString(),
                    IsDeleted = false,
                };
                context.LookUpDomains.Add(lookUp);
                lookUpDomain = lookUp;
            }




            #endregion

            #region Portals



            var fmsPortalFunHome = context.Functions.Where(x => x.Key == "Home").FirstOrDefault();
            if (fmsPortalFunHome == null)
            {
                fmsPortalFunHome = new Function()
                {

                    Key = "Home",
                    Title = "Home",
                    Url = "/Home/Index",
                    DisplayOrder = 1,
                    IsDeleted = false,
                    IsActive = true

                };
                context.Functions.Add(fmsPortalFunHome);
            }
            var fmsPortalFunClients = context.Functions.Where(x => x.Key == "Clients").FirstOrDefault();
            if (fmsPortalFunClients == null)
            {
                fmsPortalFunClients = new Function()
                {

                    Key = "Clients",
                    Title = "Client",
                    Url = "/Client/Index",
                    DisplayOrder = 2,
                    IsDeleted = false,
                    IsActive = true

                };
                context.Functions.Add(fmsPortalFunClients);
            }
            var fmsPortalFunRoles = context.Functions.Where(x => x.Key == "Roles").FirstOrDefault();
            if (fmsPortalFunRoles == null)
            {
                fmsPortalFunRoles = new Function()
                {

                    Key = "Roles",
                    Title = "Roles",
                    Url = "/Role/Index",
                    DisplayOrder = 2,
                    IsDeleted = false,
                    IsActive = true

                };
                context.Functions.Add(fmsPortalFunRoles);
            }
     
            var fmsPortalFunPlans = context.Functions.Where(x => x.Key == "Plans").FirstOrDefault();
            if (fmsPortalFunPlans == null)
            {
                fmsPortalFunPlans = new Function()
                {

                    Key = "Plans",
                    Title = "Plans",
                    Url = "/Plan/Index",
                    DisplayOrder = 4,
                    IsDeleted = false,
                    IsActive = true

                };
                context.Functions.Add(fmsPortalFunPlans);
            }

            var fmsPortalFunTrainees = context.Functions.Where(x => x.Key == "Trainees").FirstOrDefault();
            if (fmsPortalFunTrainees == null)
            {
                fmsPortalFunTrainees = new Function()
                {

                    Key = "Trainees",
                    Title = "Trainees",
                    Url = "/Trainee/Index",
                    DisplayOrder = 5,
                    IsDeleted = false,
                    IsActive = true

                };
                context.Functions.Add(fmsPortalFunTrainees);
            }

            var fmsPortalFunTrainers = context.Functions.Where(x => x.Key == "Trainers").FirstOrDefault();
            if (fmsPortalFunTrainers == null)
            {
                fmsPortalFunTrainers = new Function()
                {

                    Key = "Trainers",
                    Title = "Trainers",
                    Url = "/Trainer/Index",
                    DisplayOrder = 6,
                    IsDeleted = false,
                    IsActive = true

                };
                context.Functions.Add(fmsPortalFunTrainers);
            }

            var fmsPortalFunSettings = context.Functions.Where(x => x.Key == "Settings").FirstOrDefault();
            if (fmsPortalFunSettings == null)
            {
                fmsPortalFunSettings = new Function()
                {

                    Key = "Settings",
                    Title = "Settings",
                    Url = "/Setting/Index",
                    DisplayOrder = 7,
                    IsDeleted = false,
                    IsActive = true

                };
                context.Functions.Add(fmsPortalFunSettings);
            }

            var fmsPortalFunNews = context.Functions.Where(x => x.Key == "News").FirstOrDefault();
            if (fmsPortalFunNews == null)
            {
                fmsPortalFunNews = new Function()
                {

                    Key = "News",
                    Title = "News",
                    Url = "/News/Index",
                    DisplayOrder = 8,
                    IsDeleted = false,
                    IsActive = true

                };
                context.Functions.Add(fmsPortalFunNews);
            }
            var fmsPortalFunSubscription = context.Functions.Where(x => x.Key == "Subscription").FirstOrDefault();
            if (fmsPortalFunSubscription == null)
            {
                fmsPortalFunSubscription = new Function()
                {

                    Key = "Subscription",
                    Title = "Subscription",
                    Url = "/Subscription/Index",
                    DisplayOrder = 9,
                    IsDeleted = false,
                    IsActive = true

                };
                context.Functions.Add(fmsPortalFunSubscription);
            }
            var fmsPortalFunIngredient = context.Functions.Where(x => x.Key == "Ingredient").FirstOrDefault();
            if (fmsPortalFunIngredient == null)
            {
                fmsPortalFunIngredient = new Function()
                {
                    Key = "Ingredient",
                    Title = "Ingredient",
                    Url = "/Ingredient/Index",
                    DisplayOrder = 10,
                    IsDeleted = false,
                    IsActive = true
                };
                context.Functions.Add(fmsPortalFunIngredient);
            }
            context.SaveChanges();

            var fmsPortalFunDietPlan = context.Functions.Where(x => x.Key == "DietPlan").FirstOrDefault();
            if (fmsPortalFunDietPlan == null)
            {
                fmsPortalFunDietPlan = new Function()
                {

                    Key = "DietPlan",
                    Title = "Diet Plan",
                    Url = "/DietPlan/Index",
                    DisplayOrder = 10,
                    IsDeleted = false,
                    IsActive = true

                };
                context.Functions.Add(fmsPortalFunDietPlan);
            }
            context.SaveChanges();
            #endregion

            #region Roles

            // Super Admin Role
            var superAdminRole = context.Roles.Where(x => x.RoleName == UserRole.SuperAdmin.ToString() && x.IsSeeded == true).FirstOrDefault();
            if (superAdminRole == null)
            {
                Role role = new Role()
                {
                    RoleName = UserRole.SuperAdmin.ToString(),
                    IsDeleted = false,
                    IsSeeded = true
                };
                context.Roles.Add(role);
                context.Roles.Add(role);
                superAdminRole = role;
            }

            #region superadmin role feature mapping  

            var superAdminFunClient = context.RoleFunction.Where(x => x.RoleId == superAdminRole.Id && x.FunctionId == fmsPortalFunClients.Id && x.IsDeleted == false).FirstOrDefault();
            if (superAdminFunClient == null)
            {
                RoleFunction roleFunction = new RoleFunction
                {
                    RoleId = superAdminRole.Id,
                    FunctionId = fmsPortalFunClients.Id,
                    IsActive = true,
                    IsDeleted = false,

                    IsCreate = true,
                    IsEdit = true,
                    IsDelete = true,
                    IsView = true

                };
                context.RoleFunction.Add(roleFunction);
                context.SaveChanges();

            }

            var superAdminFunHome = context.RoleFunction.Where(x => x.RoleId == superAdminRole.Id && x.FunctionId == fmsPortalFunHome.Id && x.IsDeleted == false).FirstOrDefault();
            if (superAdminFunHome == null)
            {
                RoleFunction roleFunction = new RoleFunction
                {
                    RoleId = superAdminRole.Id,
                    FunctionId = fmsPortalFunHome.Id,
                    IsActive = true,
                    IsDeleted = false,

                    IsCreate = true,
                    IsEdit = true,
                    IsDelete = true,
                    IsView = true

                };
                context.RoleFunction.Add(roleFunction);
                context.SaveChanges();

            }

            var superAdminFunRole = context.RoleFunction.Where(x => x.RoleId == superAdminRole.Id && x.FunctionId == fmsPortalFunRoles.Id && x.IsDeleted == false).FirstOrDefault();
            if (superAdminFunRole == null)
            {
                RoleFunction roleFunction = new RoleFunction
                {
                    RoleId = superAdminRole.Id,
                    FunctionId = fmsPortalFunRoles.Id,
                    IsActive = true,
                    IsDeleted = false,

                    IsCreate = true,
                    IsEdit = true,
                    IsDelete = true,
                    IsView = true

                };
                context.RoleFunction.Add(roleFunction);
                context.SaveChanges();

            }


           

            #endregion

            //  Admin Role
            var adminRole = context.Roles.Where(x => x.RoleName == UserRole.Admin.ToString() && x.IsSeeded == true).FirstOrDefault();
            if (adminRole == null)
            {
                Role role = new Role()
                {
                    RoleName = UserRole.Admin.ToString(),
                    IsDeleted = false,
                    IsSeeded = true
                };
                context.Roles.Add(role);
                adminRole = role;
            }

            #region admin role feature mapping  

            var adminFunHome = context.RoleFunction.Where(x => x.RoleId == adminRole.Id && x.FunctionId == fmsPortalFunHome.Id && x.IsDeleted == false).FirstOrDefault();
            if (adminFunHome == null)
            {
                adminFunHome = new RoleFunction
                {
                    RoleId = adminRole.Id,
                    FunctionId = fmsPortalFunHome.Id,
                    IsActive = true,
                    IsDeleted = false,

                    IsCreate = true,
                    IsEdit = true,
                    IsDelete = true,
                    IsView = true
                };
                context.RoleFunction.Add(adminFunHome);
                context.SaveChanges();

            }
            var adminFunNews = context.RoleFunction.Where(x => x.RoleId == adminRole.Id && x.FunctionId == fmsPortalFunNews.Id && x.IsDeleted == false).FirstOrDefault();
            if (adminFunNews == null)
            {
                RoleFunction roleFunction = new RoleFunction
                {
                    RoleId = adminRole.Id,
                    FunctionId = fmsPortalFunNews.Id,
                    IsActive = true,
                    IsDeleted = false,

                    IsCreate = true,
                    IsEdit = true,
                    IsDelete = true,
                    IsView = true

                };
                context.RoleFunction.Add(roleFunction);
                context.SaveChanges();

            }


            var adminFunPlan = context.RoleFunction.Where(x => x.RoleId == adminRole.Id && x.FunctionId == fmsPortalFunPlans.Id && x.IsDeleted == false).FirstOrDefault();
            if (adminFunPlan == null)
            {
                adminFunPlan = new RoleFunction
                {
                    RoleId = adminRole.Id,
                    FunctionId = fmsPortalFunPlans.Id,
                    IsActive = true,
                    IsDeleted = false,
                    IsCreate = true,
                    IsEdit = true,
                    IsDelete = true,
                    IsView = true

                };
                context.RoleFunction.Add(adminFunPlan);
                context.SaveChanges();

            }

            var adminFunTrainee = context.RoleFunction.Where(x => x.RoleId == adminRole.Id && x.FunctionId == fmsPortalFunTrainees.Id && x.IsDeleted == false).FirstOrDefault();
            if (adminFunTrainee == null)
            {
                adminFunTrainee = new RoleFunction
                {
                    RoleId = adminRole.Id,
                    FunctionId = fmsPortalFunTrainees.Id,
                    IsActive = true,
                    IsDeleted = false,
                    IsCreate = true,
                    IsEdit = true,
                    IsDelete = true,
                    IsView = true

                };
                context.RoleFunction.Add(adminFunTrainee);
                context.SaveChanges();

            }


            var adminFunTrainer = context.RoleFunction.Where(x => x.RoleId == adminRole.Id && x.FunctionId == fmsPortalFunTrainers.Id && x.IsDeleted == false).FirstOrDefault();
            if (adminFunTrainer == null)
            {
                adminFunTrainer = new RoleFunction
                {
                    RoleId = adminRole.Id,
                    FunctionId = fmsPortalFunTrainers.Id,
                    IsActive = true,
                    IsDeleted = false,
                    IsCreate = true,
                    IsEdit = true,
                    IsDelete = true,
                    IsView = true

                };
                context.RoleFunction.Add(adminFunTrainer);
                context.SaveChanges();

            }

            var adminFunSetting = context.RoleFunction.Where(x => x.RoleId == adminRole.Id && x.FunctionId == fmsPortalFunSettings.Id && x.IsDeleted == false).FirstOrDefault();
            if (adminFunSetting == null)
            {
                RoleFunction roleFunction = new RoleFunction
                {
                    RoleId = adminRole.Id,
                    FunctionId = fmsPortalFunSettings.Id,
                    IsActive = true,
                    IsDeleted = false,
                    IsCreate = true,
                    IsEdit = true,
                    IsDelete = true,
                    IsView = true

                };
                context.RoleFunction.Add(roleFunction);
                context.SaveChanges();

            }
            


            #endregion

            // Trainer Role
            var trainerRole = context.Roles.Where(x => x.RoleName == UserRole.Trainer.ToString() && x.IsSeeded == true).FirstOrDefault();
            if (trainerRole == null)
            {
                Role role = new Role()
                {
                    RoleName = UserRole.Trainer.ToString(),
                    IsDeleted = false,
                    IsSeeded = true
                };
                context.Roles.Add(role);
                trainerRole = role;
            }

            #region trainer role feature mapping  

            var trainerFunHome = context.RoleFunction.Where(x => x.RoleId == trainerRole.Id && x.FunctionId == fmsPortalFunHome.Id && x.IsDeleted == false).FirstOrDefault();
            if (trainerFunHome == null)
            {
                RoleFunction roleFunction = new RoleFunction
                {
                    RoleId = trainerRole.Id,
                    FunctionId = fmsPortalFunHome.Id,
                    IsActive = true,
                    IsDeleted = false,

                    IsCreate = true,
                    IsEdit = true,
                    IsDelete = true,
                    IsView = true

                };
                context.RoleFunction.Add(roleFunction);
                context.SaveChanges();

            }

            var trainerFunTrainee = context.RoleFunction.Where(x => x.RoleId == trainerRole.Id && x.FunctionId == fmsPortalFunTrainees.Id && x.IsDeleted == false).FirstOrDefault();
            if (trainerFunTrainee == null)
            {
                RoleFunction roleFunction = new RoleFunction
                {
                    RoleId = trainerRole.Id,
                    FunctionId = fmsPortalFunTrainees.Id,
                    IsActive = true,
                    IsDeleted = false,

                    IsCreate = true,
                    IsEdit = true,
                    IsDelete = true,
                    IsView = true

                };
                context.RoleFunction.Add(roleFunction);
                context.SaveChanges();

            }

            #endregion

            // Trainee Role
            var traineeRole = context.Roles.Where(x => x.RoleName == UserRole.Trainee.ToString() && x.IsSeeded == true).FirstOrDefault();
            if (traineeRole == null)
            {
                Role role = new Role()
                {
                    RoleName = UserRole.Trainee.ToString(),
                    IsDeleted = false,
                    IsSeeded = true
                };
                context.Roles.Add(role);
                traineeRole = role;
            }

            #region trainee role feature mapping  

            var traineeFunHome = context.RoleFunction.Where(x => x.RoleId == traineeRole.Id && x.FunctionId == fmsPortalFunHome.Id && x.IsDeleted == false).FirstOrDefault();
            if (traineeFunHome == null)
            {
                RoleFunction roleFunction = new RoleFunction
                {
                    RoleId = traineeRole.Id,
                    FunctionId = fmsPortalFunHome.Id,
                    IsActive = true,
                    IsDeleted = false,

                    IsCreate = true,
                    IsEdit = true,
                    IsDelete = true,
                    IsView = true
                };
                context.RoleFunction.Add(roleFunction);
                context.SaveChanges();
            }
            var traineeFunSubscription = context.RoleFunction.Where(x => x.RoleId == traineeRole.Id && x.FunctionId == fmsPortalFunSubscription.Id && x.IsDeleted == false).FirstOrDefault();
            if (traineeFunSubscription == null)
            {
                RoleFunction roleFunction = new RoleFunction
                {
                    RoleId = traineeRole.Id,
                    FunctionId = fmsPortalFunSubscription.Id,
                    IsActive = true,
                    IsDeleted = false,
                    IsCreate = true,
                    IsEdit = true,
                    IsDelete = true,
                    IsView = true
                };
                context.RoleFunction.Add(roleFunction);
                context.SaveChanges();
            }
            var traineeFundiet = context.RoleFunction.Where(x => x.RoleId == traineeRole.Id && x.FunctionId == fmsPortalFunDietPlan.Id && x.IsDeleted == false).FirstOrDefault();
            if (traineeFundiet == null)
            {
                RoleFunction roleFunction = new RoleFunction
                {
                    RoleId = traineeRole.Id,
                    FunctionId = fmsPortalFunDietPlan.Id,
                    IsActive = true,
                    IsDeleted = false,
                    IsCreate = true,
                    IsEdit = true,
                    IsDelete = true,
                    IsView = true
                };
                context.RoleFunction.Add(roleFunction);
                context.SaveChanges();
            }

            #endregion

            context.SaveChanges();
            #endregion

            #region SuperAdmin 
            var admin = context.Users.Any(x => x.Username == "admin@admin.com");
            if (!admin)
            {
                User user = new User()
                {
                    Email = "admin@admin.com",
                    //Admin123!
                    Password = "tb5NJ8jwFAXp1KEPz6s6Tz7JLhPEqfWrEuYaxEZ+tPk6Dk0nxyKUgZK3Wi59c1R9zpAMCq+w8P++hMizZLpH+w==",
                    RoleId = superAdminRole.Id,
                    Role = superAdminRole.RoleName,
                    Username = "admin@admin.com",
                    IsEmailVerified = true,
                    IsActive = true,
                    PlanExpirationDateTime=DateTime.Now.AddYears(100)
                };
                context.Users.Add(user);
                context.SaveChanges();
            }


            #endregion

            #region API user


            #endregion

            #region Add basic and pro plan
            var basicPlan = context.Plans.Where(x => x.Name == "Basic Plan" && !x.IsDeleted && x.IsSeeded == true).FirstOrDefault();
            if (basicPlan == null)
            {
                basicPlan = new Plan()
                {
                    Name = "Basic Plan",
                    IsSeeded = true,
                    Price = 19,
                    PlanLengthInDays = 30,
                    CreatedOn = DateTime.Now,
                    IsActive = true,
                };
                context.Plans.Add(basicPlan);
                context.SaveChanges();

                #region insert feature w.r.t basic plan
                List<PlanFeature> basicPlanFeatures = new List<PlanFeature>()
                {
                    //new PlanFeature(){ PlanId=basicPlan.Id,FeatureId=leadCaptureFeature.Id},
                    //new PlanFeature(){ PlanId=basicPlan.Id,FeatureId=leadDistributionFeature.Id},
                    //new PlanFeature(){ PlanId=basicPlan.Id,FeatureId=leadEscalationFeature.Id},
                    //new PlanFeature(){ PlanId=basicPlan.Id,FeatureId=emailAutomationFeature.Id},
                    //new PlanFeature(){ PlanId=basicPlan.Id,FeatureId=leadQualificationFeature.Id},
                    //new PlanFeature(){ PlanId=basicPlan.Id,FeatureId=leadPipelineFeature.Id},
                    //new PlanFeature(){ PlanId=basicPlan.Id,FeatureId=emailCommunicationFeature.Id},
                    //new PlanFeature(){ PlanId=basicPlan.Id,FeatureId=reportingFeature.Id}
                };
                context.PlanFeatures.AddRange(basicPlanFeatures);
                context.SaveChanges();
                #endregion
            }

            var proPlan = context.Plans.Where(x => x.Name == "Pro Plan" && !x.IsDeleted && x.IsSeeded == true).FirstOrDefault();
            if (proPlan == null)
            {
                proPlan = new Plan()
                {
                    Name = "Pro Plan",
                    IsSeeded = true,
                    Price = 49,
                    PlanLengthInDays = 30,
                    CreatedOn = DateTime.Now,
                    IsActive = true,
                };
                context.Plans.Add(proPlan);
                context.SaveChanges();

                #region insert feature w.r.t basic plan
                List<PlanFeature> proPlanFeatures = new List<PlanFeature>()
                {
                    //new PlanFeature(){ PlanId=proPlan.Id,FeatureId=leadCaptureFeature.Id},
                    //new PlanFeature(){ PlanId=proPlan.Id,FeatureId=leadDistributionFeature.Id},
                    //new PlanFeature(){ PlanId=proPlan.Id,FeatureId=leadEscalationFeature.Id},
                    //new PlanFeature(){ PlanId=proPlan.Id,FeatureId=leadQualificationFeature.Id},
                    //new PlanFeature(){ PlanId=proPlan.Id,FeatureId=leadPipelineFeature.Id},
                    //new PlanFeature(){ PlanId=proPlan.Id,FeatureId=multiLanguageEmailFeature.Id},
                    //new PlanFeature(){ PlanId=proPlan.Id,FeatureId=reportingFeature.Id},
                    //new PlanFeature(){ PlanId=proPlan.Id,FeatureId=mobileAppsFeature.Id}
                };
                context.PlanFeatures.AddRange(proPlanFeatures);
                context.SaveChanges();
                #endregion
            }
            #endregion

            #region Add Ingredients
            var egg = context.Ingredients.Where(x => x.Name == "Egg" && !x.IsDeleted).FirstOrDefault();
            if (egg == null)
            {
                egg = new Ingredient()
                {
                    Name = "Egg",
                    Calorie = 88,
                    Fats = 4,
                    Carbs = 8,
                    Protein= 9,
                    Sugar= 8,
                    Units = 1,
                    CreatedOn = DateTime.Now
                };
                context.Ingredients.Add(egg);
                context.SaveChanges();
            }
            var apple = context.Ingredients.Where(x => x.Name == "Apple" && !x.IsDeleted).FirstOrDefault();
            if (apple == null)
            {
                apple = new Ingredient()
                {
                    Name = "Apple",
                    Calorie = 52,
                    Fats = 4,
                    Carbs = 13,
                    Units = 2,
                    Sugar  = 10,
                    CreatedOn = DateTime.Now
                };
                context.Ingredients.Add(apple);
                context.SaveChanges();
            }
            var chicken = context.Ingredients.Where(x => x.Name == "Chicken" && !x.IsDeleted).FirstOrDefault();
            if (chicken == null)
            {
                chicken = new Ingredient()
                {
                    Name = "Chicken",
                    Calorie = 900,
                    Fats = 99,
                    Carbs = 0,
                    Sugar = 0,
                    Fiber = 0,
                    Units = 1,
                    CreatedOn = DateTime.Now
                };
                context.Ingredients.Add(chicken);
                context.SaveChanges();
            }           
            #endregion

            #region site configuration
            var siteConfigurationDbs = new List<SiteConfiguration>();
            if (!context.SiteConfigurations.Any(x => x.Key == SiteConfigType.StripeApiPrivateKey.ToString() && x.IsDeleted != true))
            {
                //StripeApiPrivateKey
                var stripeApiPrivateKey = new SiteConfiguration()
                {
                    Key = SiteConfigType.StripeApiPrivateKey.ToString(),
                    Value = "sk_test_51IhxGYSJPt30xBBEUDs1EJcVoMStXogAeoxsu1lAGA4HfSoP4yRmEV8P4o3pgETMHVzOnnsQvABKfZPj7fFqRfuO00ZTFU3fn7",
                    Description = "Stripe Api Private Key",
                    IsDeleted = false,
                    CreatedOn = DateTime.UtcNow
                };
                siteConfigurationDbs.Add(stripeApiPrivateKey);
            }

            if (!context.SiteConfigurations.Any(x => x.Key == SiteConfigType.StripeApiPublicKey.ToString() && x.IsDeleted != true))
            {
                //StripeApiPublicKey 
                var devStripeApiPublicKey = new SiteConfiguration()
                {
                    Key = SiteConfigType.StripeApiPublicKey.ToString(),
                    Value = "pk_test_51IhxGYSJPt30xBBEhIURDMHcio8yK6Bpo79PkOHpWHvuWAjlRYlYnJRyeaplwq475Gw4KeVoShyZJDM2YUVlMM2L0043fVT37J",
                    Description = "Stripe Api Public Key",
                    IsDeleted = false,
                    CreatedOn = DateTime.UtcNow
                };
                siteConfigurationDbs.Add(devStripeApiPublicKey);
            }

            if (!context.SiteConfigurations.Any(x => x.Key == SiteConfigType.StripeClientId.ToString() && x.IsDeleted != true))
            {
                //stripe client-id 
                var stripeClientId = new SiteConfiguration()
                {
                    Key = SiteConfigType.StripeClientId.ToString(),
                    Value = "ca_IcnsKWnaKqKEcYoSLLw72Cl0YdehQLaD",
                    Description = "Stripe Connect Account Client Id",
                    IsDeleted = false,
                    CreatedOn = DateTime.UtcNow
                };
                siteConfigurationDbs.Add(stripeClientId);
            }

            if (!context.SiteConfigurations.Any(x => x.Key == SiteConfigType.StripeAccountType.ToString() && x.IsDeleted != true))
            {
                //stripe account type
                var stripeAccountType = new SiteConfiguration()
                {
                    Key = SiteConfigType.StripeAccountType.ToString(),
                    Value = StripeAccountType.Standard.GetHashCode().ToString(),
                    Description = "Stripe Connect Account Type",
                    IsDeleted = false,
                    CreatedOn = DateTime.UtcNow
                };
                siteConfigurationDbs.Add(stripeAccountType);
            }

            if (!context.SiteConfigurations.Any(x => x.Key == SiteConfigType.SubscriptionFreeTimePeriod.ToString() && x.IsDeleted != true))
            {
                //free subscription time period
                var stripeAccountType = new SiteConfiguration()
                {
                    Key = SiteConfigType.SubscriptionFreeTimePeriod.ToString(),
                    Value = "14",
                    Description = "Time period for free subscription",
                    IsDeleted = false,
                    CreatedOn = DateTime.UtcNow
                };
                siteConfigurationDbs.Add(stripeAccountType);
            }

            if (siteConfigurationDbs != null && siteConfigurationDbs.Count > 0)
            {
                context.SiteConfigurations.AddRange(siteConfigurationDbs);
                base.Seed(context);
                context.SaveChanges();
            }
            #endregion
          
        }
    }
}
